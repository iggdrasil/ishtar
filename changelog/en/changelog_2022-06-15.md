v4.0.80 - 2025-02-19
--------------------

### Bug fixes ###
- search forms: fix filter for datation fields (#4281, #6193)


v4.0.79 - 2025-02-10
--------------------

### Bug fixes ###
- context record forms: do not crash on fields reordering when some of them are filtered (#6166)

v4.0.78 - 2024-09-15
--------------------

### Bug fixes ###
- fix document galery (#6022)

v4.0.77 - 2024-06-05
--------------------

### Bug fixes ###
- hot fix geo forms - no `history_creator`

v4.0.76 - 2024-06-05
--------------------

### Bug fixes ###
- document and geo forms: fix overload of history creator on modification (refs #5964)

v4.0.75 - 2024-04-16
--------------------

### Technical ###
- Find: add index to free text search

v4.0.74 - 2024-04-16
--------------------

### Technical ###
- JINJA formula: padding and deduplicate filters

v4.0.73 - 2024-03-19
--------------------

### Bug fixes ###
- fix gallery for documents

v4.0.72 - 2024-02-05
--------------------

### Technical ###
- update IGN/INSEE import script

v4.0.71 - 2024-01-17
--------------------

### Bug fixes ###
- area: fix automatic slug generation (#5715)
- document table: fix line duplication for authors (#5709). **Warning**: count in table is not relevant anymore, real correction will be available on version v4.1

v4.0.70 - 2023-12-19
--------------------

### Technical ###
- context record relation: force one to one relation on context record relation -> fix archaeological find listing on context record sheet


v4.0.69 - 2023-12-18
--------------------

### Technical ###
- values associated with archaeological find for templates: use "base_find_" prefix and simple prefix (#5695)
- account form: on modification set profile_type to readonly in order to prevent some errors on deletion (#5696)


v4.0.68 - 2023-11-30
--------------------

### Bug fixes ###
- admin container: fix search

### Technical ###
- import: intercept integrity error on post-treatment

v4.0.67 - 2023-11-29
--------------------

### Bug fixes ###
- GIS form - remove "timestamp" fields (#5673)
- Instance Profile:
    - fix migration: provide default for complete id and cached label (#5674)
    - fix "default center" field (#5675)
- Document table: fix performance issue on sort (#5667)
- Document bulk update: fix to add new author (#5681)
- Custom form - add missing forms types (#5668)
- New Geo item form: center map widget on the center of the main geo item or use default center of the instance (#5676)


v4.0.66 - 2023-11-22
--------------------

### Bug fixes ###
- templates - container values: fix find list order (#5661)

v4.0.65 - 2023-11-15
--------------------

### Bug fixes ###
- search: fix -type="*" search for types with no hierarchy

v4.0.64 - 2023-11-14
--------------------

Many improvement in post-treatments. Better performance for imports are expected.

### Technical ###
- remove bulk update: should not be useful anymore and can severely degrade performance
- improve parcel post-treatments - add timestamp to prevent multiple geo and cached_label edition



v4.0.63 - 2023-11-08
--------------------

### Bug fixes ###
- Statistics: fix image display (#5650)
- Password expiration: delete cache after password reset

v4.0.62 - 2023-10-27
--------------------

### Bug fixes ###
- Operation sheet: add missing preventive archaeology fields, improve date format
- Operation sheet: fix filter on the table of relations between context records (#5647)

v4.0.61 - 2023-10-18
--------------------

### Bug fixes ###
- JS: fix calendar localisation (#5642)
- Custom forms: fix crash when removing fields already filtered (#5643)

v4.0.60 - 2023-10-10
--------------------

### Bug fixes ###
- manage photos with altered bits (#5626)

v4.0.59 - 2023-09-29
--------------------

### Bug fixes ###
- fix missing translations
- remove "BASE_URL" on image sheet (unnecessary locally and buggy on remote access)
- areas automatically created by operation with many towns are not "available" by default

v4.0.58 - 2023-09-25
--------------------

### Bug fixes ###
- Federation:
  - instead of deleting excluded fields, set them to null
  - distant sheets: add missing default permission check
  - absolute URL for ressources

v4.0.57 - 2023-09-13
--------------------

### Technical ###
- wider main div on mobile device

### Bug fixes ###
- fix photo selector on mobile device

v4.0.56 - 2023-09-06
--------------------

### Features/improvements ###
- improve online documentation

### Technical ###
- imports - linking: remove irrelevant keys
- imports - geo: fix import association when import_key is matched
- fix update of cached coordinates on precision change of the SRS


v4.0.55 - 2023-08-07
--------------------

### Technical ###
- `ishtar_maintenance` script: add "filter" option in order to limit script to a specific query
- fix crash with no search counted
- fix cascade update for base finds
- optimise post-treatments: prevent unnecessary cascade update (refs #5617)

v4.0.54 - 2023-07-19
--------------------

### Features/improvements ###
- wizards: automatic scroll on field when navigating with TAB key
- improve layout: update password form, open registration form
- update password form: redirect to start page
- open registration form: disabled by default
- reset email form
- button to show password on admin change password form
- default username naming style is firstname.name

### Bug fixes ###
- fix find redirection after geo item creation/modification
- account form: fix slugify of default username
- fix document quick packaging (#5611)

v4.0.53 - 2023-07-06
--------------------

### Features/improvements ###
- wizards: validate forms with CTRL+ENTER

### Bug fixes ###
- wizards: fix autofocus of first field
- do not overload when no complete_id is available

v4.0.52 - 2023-07-06
--------------------

### Bug fixes ###
- message after creation: use the generate_cached_label if no label is available

### Technical ###
- fix infinite loop on regenerate_all_id


v4.0.51 - 2023-07-05
--------------------

### Features/improvements ###
- sheet: add a refresh button to update informations

### Bug fixes ###
- fix parcel table - wizard summary (inappropriate l10n)
- check of external identifiers when updating cached labels (#5603)


v4.0.50 - 2023-07-03
--------------------

### Features/improvements ###
- update documentation
- improve changelog display
- update translations

### Bug fixes ###
- Operation form: the operation code field (Patriarche) is no longer optional
- fix "*" search for text fields (#5325)
- forms - unit input: fix round float display
- sheet: fix float display for hectare
- fix crash on qrcode generation for base finds

v4.0.49 - 2023-06-21
--------------------

### Features/improvements ###
- improve changelog display
- Sheet: display spatial reference system with coordinates of points

### Technical ###
- JS: fix TrackPositionControl with https


v4.0.48 - 2023-06-08
--------------------

### Features/improvements ###
- Search criteria - geo - "Z lower than", "Z higher than"
- Sheet: display coordinates of points

### Technical ###
- Default round and round z for each spatial reference system 
- Do not put debug log in root path on Debian
- Fix mimetype of settings.js


v4.0.47 - 2023-06-08
--------------------

### Technical ###
- Improve post treatments after imports
- Many fix (or potential fix) in templates (inappropriate l10n) (#5594)


v4.0.46 - 2023-05-26
--------------------

### Features/improvements ###
- Towns/geographical areas :
    - attachment of documents
    - setting up of sheet
    - links from other sheet to the sheets of municipalities/geographical areas

### Bug fixes ###
- recalculation of the weight of a container when find is removed (#5470)


### Technical ###
- JS: fix UnitWidget (inappropriate l10n)
- fix HTML import page (inappropriate l10n)


v4.0.45 - 2023-05-15
--------------------

### Features/improvements ###
- Administration: improved display of subcategories (in color)
- Document - bulk update: add modification of tags (#5415)

### Bug fixes ###
- Fix town search for warehouses and organizations
- Fix container search for documents

### Technical ###
- Migration font-awesome -> fork-awesome
- Inline JS migrated to a dynamic, cacheable settings.js
- Container import: do not crash on empty container type
- media_find_missing_files command: added set-alt-media-dir option to link missing media from an alternate directory and set-empty-image option to link to dummy images for missing images

v4.0.44 - 2023-04-17
--------------------

### Features/improvements ###
- Display of a changelog with alert display when updates are made
- Containers: manage history
- Security:
    - Manage expiration of passwords
    - Manage strong password policy (ISHTAR_STRONG_PASSWORD_POLICY) with "Each character type" validator
    - Add default auth validator
    - Default timeout for session is set to 5 days
    - Optional security for login attempt: loging, deactivate account after many failed login.

### Bug fixes ###
- Json fields: fix bad save of multi values
- Cascade update from warehouse to containers (#5432)
- Sheets: fix history view with associated geo
- Containers: remove division search
- Image detail: do not display Modify link when not relevant (#5438)
- Fix french label for geo types (#5577)
- Fix permissions for treatments requests (#5441)

### Technical ###
- Load task refactoring - manage external_id regen with tasks
- Update and fix translations (#5578, #5579, #5581)
- Importer export: fix pre_importer call
- Force using 128 bits salt for password hasher
- Mark false security issues detected by bandit
- Fix low severity security issues detected by bandit

v4.0.43 - 2023-03-17
--------------------

### Features/improvements ###
- General:
    - add custom cached_label configuration for each main item
    - add "created" field on main items in order to facilitate queries
- settings: add "ISHTAR_SECURE_OPTIONS" to activate Django secured options
- Sheet document: better UI for files
- Model: add history for document and containers
- Menu: remove "Administration" entry - put Account management in "Directory" entry
- Admin:
    - Global variable: edit in table, add import/export in CSV/JSON
    - overload index to add subsection headers
- Geo: create/edit form - new openlayers version - add default IGN tiles
- Import: improve bad encoding detection
- Search:
    - add created before/after field
    - Operation: add "old code" field
    - Find: add "remain" field (operations and sites)
    - Account: add "profile type" field
- Free text search:
    - use accent and unaccented string
    - add french_archeo thesaurus config
    - File - add "year-index"
- Maintenance scripts: delete deprecated and migrate to ishtar_maintenance
- Put a serif font for <pre>
- Sheet:
    - Treatment file: add associated image and document list

### Bug fixes ###
- Search:
    - fix many excluded facet
    - improve many facet query
    - Document - source type is now a hierarchic search
    - Context record: add identification and activity criteria
- File: filter plan action when preventive_operator is activated in profile 
- Profile:
    - do not display geo item list when mapping is deactivated
    - custom footer message
- Explicit message on associated container deletion when a warehouse is deleted
- Administrative act: add a warning when associated item is deleted
- Imports: register automatically type models for export
- Site: quick action for creation of virtual operation from site with many operations
- UI:
    - sheet header - add caret to show collapse
    - show/hide password on login
- Forms:
    - custom form - can add header message
    - Find: remove TAQ/TPQ check

- Sheet:
    - fix treatment and file treatment sheet display (bad QR code link)
    - Operation - statistics number of parcels fix
    - basket: display shared with by alpha order

v4.0.42 - 2023-01-25
--------------------

### Features/improvements ###
- Treatment sheet: improve layout - add detail links

### Technical ###
- Installation: fix installation script to allow custom DB PORT and DB HOST
- Imports: complex formatting of formulas

v4.0.41 - 2023-01-23
--------------------

### Technical ###
- Fix javascript filter on field

v4.0.40 - 2023-01-22
--------------------

### Technical ###
- Improve some translations

v4.0.39 - 2023-01-20
--------------------

### Features/improvements ###
- Person search: salutation/title fields

v4.0.38 - 2023-01-20
--------------------

### Bug fixes ###
- PDF export: fix new line on <pre> blocks
- Warehouse sheet: correction of duplicate labels


v4.0.37 - 2023-01-20
--------------------

### Bug fixes ###
- Find search: fix discovery date search
- Find/container sheet: correction of date and decimal number display


v4.0.36 - 2023-01-20
--------------------

### Features/improvements ###
- Find sheet refactoring
- Free search:
    - "raw" index for reference (add in index whole reference and split ref)
    - improve parent only search index
- Warehouse: new slug field to prevent ID change when name is changed
- Warehouse: cached town column
- Change labels
    - Identifiant musée -> Numéro d'inventaire musée
    - Localisation (fiche) -> Localisation géographique
    - Date de découverte (exacte ou TPQ) -> Date de découverte (exacte ou début)
    - Date de découverte (TAQ) -> Date de découverte (fin)


v4.0.35 - 2023-01-18
--------------------

### Bug fixes ###
- WIP: fix types searches with *
- Imports: fix cultural attribution import
- Table: fix default sort when pk is not "id" 
- Sheet:
    - warehouse: fix localisation - fix data
    - container: fix localisation - fix data
- Admin: container and warehouse performance

v4.0.34 - 2023-01-13
--------------------

### Bug fixes ###
- Syndication:
    - sheet - fix set default permissions
    - admin ApiSearchModel - fix exporter filter for Find
    - sheet - fix display detail of a dict (display associated label)
    - table - select/deselect all
    - table - reinit headers on load

v4.0.33 - 2023-01-12
--------------------

### Features/improvements ###
- Search result - export: export only selected
- Commands: manage update search vector with maintenance script
- utils: adapt to v4 OSM relations importer
- Syndication ui: better color for external sources
- Syndication - export external sources
- Search: sort by ID by default
- UI bootstrap for stats

### Bug fixes ###
- Fix geo finds for external sources: do not display
- Fix pinned search for external source
- Fix strange bug on statistics with criteria

v4.0.32 - 2022-12-12
--------------------

### Features/improvements ###
- Sheet: set default permissions to False
- Sheet find: do not display container information if no permission
- Image field: can use camera for mobile device
- Import value formater: can use {item} notation for format

### Bug fixes ###
- Full text search: add "simple" config in search

v4.0.31 - 2022-12-05
--------------------

### Features/improvements ###
- Admin: importer model export
- Imports: manage defaults with "__"

### Bug fixes ###
- Migration v4: disable M2M post treatment

v4.0.30 - 2022-12-02
--------------------

### Improvements ###
- Use supervisor instead of systemd script for celery daemon
- Forms: minor template fix


v4.0.29 - 2022-11-29
--------------------

### Improvements ###
- Context record: add Unit to indexed search
- Import: add ActType to imported types
- Admin: small improvements on syndication

### Bug fixes ###
- Fix redirect URL after logout
- Fix choice display - non-relevant translation is deactivated
- Minor translation fixes


v4.0.28 - 2022-11-18
--------------------

### Improvements ###
- Admin: site profile - better form layout
- Imports: display post-process advance
- File refactoring
- Remove dead code

### Bug fixes ###
- Fix import when no object is created and no error is caught
- Operation from file creation - fix automatic town association and parcel recopy

v4.0.27 - 2022-11-10
--------------------

## Features ##
- Improve tests
- French translation

### Bug fix ###
- Fix m2m display for historized items


v4.0.26 - 2022-11-08
--------------------

### Features ###
- remove deprecated x, y, z fields on main item forms

### Bug fix ###
- Find sheet: fix map display
- Find form: fix order of decoration field


v4.0.25 - 2022-11-07
--------------------

### Features ###
- Geodata: display first item associated as a name when no name is provided

### Bug fix ###
- Geodata import: fix total number evaluation on imports
- Basket modification: fix wizard update


v4.0.24 - 2022-10-27
--------------------

### Bug fix ###
- Geodata save: transactions to limit deadlocks


v4.0.23 - 2022-10-26
--------------------

### Features ###
- Geo data: zoom to extent

### Bug fix ###
- Geodata post save: transactions and targeted post save to limit deadlocks

v4.0.22 - 2022-10-14
--------------------

### Features ###
- Context records: Excavation technic become many-to-many

v4.0.21 - 2022-10-14
--------------------

### Features ###
- Geo: quick add geo action for context records, sites and finds
- Geo data: manage m2m association and cascade association

### Bug fix ###

- Geo data: fix main_geodata association test
- fix some admin pages

v4.0.20 - 2022-10-03
--------------------

### Features ###

Geo form - redirect to source item after edit


v4.0.19 - 2022-09-18
--------------------

Backport of v3 changes.

v4.0.18 - 2022-09-12
--------------------

### Features ###

- Import GIS: default to 0 skip lines (for geopackage and SHP it must be 0)

### Bug fix ###

- Geo - migration script: more tolerant with no geo item data - main geo is poly when defined 

v4.0.17 - 2022-09-10
--------------------

### Bug fix ###

- Geo: fix zoom to the extent - fix item counts

v4.0.16 - 2022-09-09
--------------------

### Features ###

- Geo: display associated context records and associated finds on sheet map
- Geo: display lines and polygons on search

### Bug fix ###

- Fix safe_or template tags

v4.0.15 - 2022-08-30
--------------------

### Features ###

- "is locked" search

### Bug fix ###
- Fix INSEE number generation on error with years
- Fix many count search such has "has-finds"

v4.0.14 - 2022-08-05
--------------------

### Features ###

- Import: manage CSV geo data

### Bug fix ###

- Geo sheet: fix display coordinates

v4.0.13 - 2022-08-05
--------------------

### Bug fix ###

- Imports: CSV check fix encoding check
- Imports: add post importer actions in order to manage account importer

v4.0.12 - 2022-08-05
--------------------

### Bug fix ###

- fix bug for vanilla installation (db query before db is initialized)

v4.0.11 - 2022-08-02
--------------------

### Bug fix ###

- Admin - custom form: fix form choice initialization
- File:
    - Parcel form: fix town initialization
    - Remove Parcel formset from wizard
    - Fix modification when general contractor is missing

v4.0.10 - 2022-07-22
--------------------

### Features ###

- Admin - Custom fields: exports and imports CSV, json

### Bug fix ###

- Sheet document - fix permalink content
- migrate_to_geo_v4:
    - fix migration of item multi_polygon_source_item and point_source_item
    - fix "Contour" label

v4.0.9 - 2022-07-21
-------------------

### Bug fix ###

- Import creation: fix importer type filter

v4.0.8 - 2022-07-18
-------------------

### Features ###

- Update French translations

v4.0.7 - 2022-07-18
-------------------

### Features ###

- Operation wizard refactoring

v4.0.6 - 2022-07-17
--------------------

### Bug fix ###

- Site creation: fix post message

v4.0.5 - 2022-07-17
--------------------

### Bug fix ###

- Geo - sheet: fix coordinates display
- Account deletion: fix summary

v4.0.4 - 2022-07-17
--------------------

### Features ###

- update translations

v4.0.3 - 2022-07-15
--------------------

### Bug fix ###

- migrate_to_geo_v4: fix operation and site import when SRID != 4326

v4.0.2 - 2022-06-15
--------------------

### Bug fix ###

- migrate_to_geo_v4: fix main item import when SRID=4326

v4.0.1 - 2022-06-15
--------------------

### Main features ###

- Migration to Django 2.2
- New geo item management
- Syndication of ishtar databases

### Features ###

- specific form for parcel management
- quick search action: grouping treatment
- better UI for imports

### Bug fix ###

- increase length of phone numbers
