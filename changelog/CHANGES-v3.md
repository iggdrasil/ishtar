---
title: Ishtar changelog
date: 2022-11-04
---

Ishtar changelog
================

v3.2.11 - 2022-12-06
--------------------

### Bug fix ###
- Imports: add ActType to available types

v3.2.10 - 2022-11-04
--------------------

### Bug fix ###
- Imports: fix post process when returned item is not a list


v3.2.9 - 2022-10-23
-------------------

### Bug fix ###
- Imports: fix dict merge on data update

v3.2.8 - 2022-10-17
-------------------

### Bug fix ###
- Fix quick treatment act modify URL match

v3.2.7 - 2022-10-03
-------------------

### Bug fix ###
- Fix delete wizard with no "class_verbose_name"


v3.2.6 - 2022-09-28
-------------------

### Features ###

- organization: add columns to table - add town search criteria
- merge pages: display all fields on table

v3.2.5 - 2022-09-18
--------------------

### Features ###

- utils - `create_osm_town` create town from OSM relation

v3.2.4 - 2022-09-18
--------------------

### Bug fix ###

- Town: catch error on surface calculate

v3.2.3 - 2022-07-12
--------------------

### Features ###

- Update generated documentation

### Bug fix ###

- Preventive file: fix copy id
- Account form: fix bad initialization

v3.2.2 - 2022-07-11
--------------------

### Features ###

- Preventive file:
 - add missing fields
 - improve admin
 - fix translations

v3.2.1 - 2022-07-08
--------------------

### Features ###

- Preventive forms for budget plan
- Performance: revoke cascade task
- JSON types: multi valued choices
- Forms:
  - context record: change field order
- Admin:
  - available/unavailable action
  - context record relations: reverse relations and logical relations columns
- Modify bulk action:
  - site: remains, cultural attributions, collaborators,
  - operation: collaborators
  - find: conservation commentary
  - context record: town, relation - type, archaeological site, parcel
  - document: format, scale, support
  - basket: properties
- Sheet quick action:
  - context record: add a find
  - basket: modify properties
  - person: create account
  - document: add related document
- Search:
  - document: operation year, operation type
  - context record - by operation name
- Commands:
  - ishtar_maintenance_task: fix operation missing parcels
- Tables:
  - operation - scientific column
- Sheet:
  - organization: sort member by alphabetic, add operations where organization is operator
- Generate preview image for PDF
- Import - New/Edit: verify associated images is a valid zip file

### Bug fix ###

- Search: fix url for person and organization
- Geo: use own precise multipolygone centroid before parent precise point
- Many to one treatment: fix attribute conservation
- Sheet: silently fails when missing graph relations image is missing
- Import delete: do not crash when no SLUG is available for an attached item
- JSON fields: fix multi-value choices fields


v3.1.75 - 2022-06-09
--------------------

### Features ###

- custom index: "whole_db" key is available for index on the whole db


v3.1.74 - 2022-06-08
--------------------

### Features ###

- ID: allow use "data" field to generate ID

v3.1.73 - 2022-06-07
--------------------

### Features ###

- Import: more "human" error messages

v3.1.72 - 2022-06-02
--------------------

### Bug fix ###

- Import: fix default value for base item

v3.1.71 - 2022-06-02
--------------------

### Bug fix ###

- Fix custom column label

v3.1.70 - 2022-05-27
--------------------

### Bug fix ###

- Operation: fix find change when no OA prefix is set
- Sheet operation: fix display when no 01 prefix is set
- Operation form: fix OA prefix


v3.1.69 - 2022-05-23
--------------------

### Bug fix ###

- Search: fix relation type searches
- Search: fix manual sort with None value


v3.1.64->68 - 2022-05-18
------------------------

### Bug fix ###

- Person: fix raw_name reinit when no name is given
- Account: fix username check on simple user (not only ishtaruser)
- Operation: fix label "in charge" -> "scientific monitor"

v3.1.63 - 2022-05-17
--------------------

### Bug fix ###

- Sheet operation: fix operations relations listing


v3.1.62 - 2022-05-10
--------------------

### Bug fix ###

- Operation: fix operations relations listing

v3.1.61 - 2022-05-09
--------------------

### Bug fix ###

- Sheet container: fix weight display
- Update and fix translation

v3.1.60 - 2022-05-09
--------------------

### Bug fix ###

- Basket: fix permissions for document generation

v3.1.59 - 2022-05-05
--------------------

### Bug fix ###

- Account management - profile: raise an error on profile name duplication

v3.1.58 - 2022-04-22
--------------------

### Features ###

- Templates: add "images" to get_values


v3.1.57 - 2022-04-13
--------------------

### Bug fix ###

- Operation - fix label regeneration on town change

v3.1.55-56 - 2022-04-06
-----------------------

### Features ###

- Base finds: display JSON data

v3.1.54 - 2022-03-25
--------------------

### Bug fix ###

- fix item creation when no last_modified is set

v3.1.53 - 2022-03-24
--------------------

### Bug fix ###

- fix dated field search (modified after/before)



v3.1.52 - 2022-03-23
--------------------

### Bug fix ###

- remove auto update for last_modified - add a _no_last_modified_update


v3.1.51 - 2022-03-09
--------------------

### Features ###

- Do not post treat on qrcode generation
- Disable bulk_update (generate infinite loop and crash)
- Templates: optimize template render for containers


v3.1.50 - 2022-03-08
--------------------

### Features ###
- Templates: improve splitpart filter

v3.1.49 - 2022-02-28
--------------------

### Bug fix ###
- Templates: fix base finds get value from container


v3.1.48 - 2022-02-24
--------------------

### Features ###
- Admin: export/import CSV JSON data field

v3.1.47 - 2022-02-22
--------------------

### Features ###
- CSV export: manage JSON columns

v3.1.46 - 2022-02-20
--------------------

### Bug fix ###
- ooopy: do not crash when z-index is missing

v3.1.45 - 2022-02-09
--------------------

### Features ###
- Document duplicate: add reference field

### Bug fix ###
- File storage: fix file save on broken link

v3.1.44 - 2022-02-08
--------------------

### Bug fix ###
- Document duplication:
  - do not duplicate image
  - fix field name

### Features ###
- Command - ishtar_maintenance: task_check_cached_label
- Document duplication: allow redirection to modify
- More explicit resume on deletion

v3.1.43 - 2021-12-10
--------------------
### Bug fix ###
- Export: fix functional area export

v3.1.42 - 2021-11-16
--------------------
### Bug fix ###
- Container sheet: display image when no main image is selected

v3.1.41 - 2021-11-16
--------------------
### Features ###
- Command - ishtar_maintenance: fix main image

v3.1.39 - 2021-11-09
--------------------
### Bug fix ###
- Container: prevent parent infinite loop

v3.1.38 - 2021-11-01
--------------------
### Bug fix ###
- do not translate type labels on forms

v3.1.37 - 2021-10-20
--------------------
### Features ###
- Finds: add `material_types_recommendations` -> manage hierarchy

v3.1.36 - 2021-10-20
--------------------
### Features ###
- Finds: add `material_types_recommendations` string for templates - update doc with base jinja filters

v3.1.35 - 2021-10-19
--------------------
### Bug fix ###
- Document: fix image with no file attached to

v3.1.34 - 2021-10-18
--------------------
### Bug fix ###
- ID generation: fix custom filters for jinja format

v3.1.33 - 2021-07-29
--------------------
### Bug fix ###
- Fix `history_creator` on duplicate

v3.1.32 - 2021-06-23
--------------------
### Features ###
- Imports: uppercase/lowercase formaters

v3.1.31 - 2021-06-22
--------------------
### Bug fix ###
- Find inside containers: fix permissions

v3.1.30 - 2021-06-21
--------------------
### Features ###
- Context records relations: use cache tables for performance

v3.1.29 - 2021-06-16
--------------------
### Features ###
- Container import: put_document_by_internal_reference

v3.1.28 - 2021-06-04
--------------------
### Bug fix ###
- Stats: fix "#" encoding in CSV

v3.1.27 - 2021-06-03
--------------------
### Bug fix ###
- QAPackaging: fix treatment type filter
- Stats: fix CSV encoding
- Operation - owns: remove end_date constraint

v3.1.26 - 2021-06-02
--------------------
### Features ###
- Find: add functional area field
- Person - bulk update: add person type field

v3.1.25 - 2021-06-01
--------------------
### Features ###
- JSON search: boolean field search
- Import: cleaner layout for match template
- Custom index - document: operation_source_type_code, index by operation and source_type code

### Bug fix ###
- Search vector field always first field

v3.1.24 - 2021-05-25
--------------------

### Features ###
- Search: Container - search by parent

v3.1.23 - 2021-05-11
--------------------

### Bug fix ###
- Search:
	- fix pin for container
	- fix empty search replaced by pin

v3.1.21-22 - 2021-05-10
-----------------------

### Features ###
- More values on the json summary

v3.1.20 - 2021-05-08
--------------------

### Features ###
- Improve autocomplete container sort

v3.1.19 - 2021-05-07
--------------------

### Features ###
- Sheets: display parcel address

### Bug fix ###
- Helpers: fix material_type_label

v3.1.18 - 2021-04-29
--------------------

### Bug fix ###
- Sheet: json display for context records and operations
- Fix autocomplete container and default basket search

v3.1.17 - 2021-04-29
--------------------

### Bug fix ###
- fix default pin

v3.1.16 - 2021-04-29
--------------------

### Bug fix ###
- baskets:
	- pin
	- display of content when pinned

v3.1.15 - 2021-04-28
--------------------

### Features ###
- Better filter on container autocomplete - manage any order in parent types

v3.1.14 - 2021-04-28
--------------------

### Features ###
- Better filter on container autocomplete - manage unaccent - reversed order in parent

v3.1.13 - 2021-04-20
--------------------

### Bug fix ###
- Fix sentry integration of release parameter

v3.1.12 - 2021-04-20
--------------------

### Bug fix ###
- Fix parcel sorting

v3.1.11 - 2021-04-19
--------------------

### Features ###
- sheet find: add container index

### Bug fix ###
- CSV export: deduplicate multi-values

v3.1.10 - 2021-04-12
--------------------

### Features ###
- CSV export: manage "count" - shift get_localisation

v3.1.9 - 2021-04-12
-------------------

### Features ###
- Statistics - modalities:
  - Operation, Site, Context record, Find: modification year

### Bug fix ###
- Fix basket search (bad base query management)

v3.1.8 - 2021-04-02
-------------------

### Features ###
- Statistics - modalities:
  - Context record: operation
  - Find: operation
  - Container: container type

### Bug fix ###
- Forms: fix permissions evaluation on field options

v3.1.7 - 2021-03-30
-------------------

### Features ###
- Operation - Context records: Surface int -> float

### Bug fix ###
- Container: fix index generation when only stationary containers are available

v3.1.6 - 2021-03-23
-------------------

### Bug fix ###
- Operation: fix parcel sorting when add from a file


v3.1.5 - 2021-03-19
--------------------

### Features ###
- sentry: filter disallowed host
- use Black for code source format

### Bug fix ###
- reup CI
- force translations for QA new items
- fix complete_id generation for containers
- fix responsibility field on new container creation

v3.1.4 - 2021-03-09
--------------------

### Bug fix ###
- QA Regen ID: change place and color
- UI: fix display of help button
- Fix: pined search do not interfere with sheet views
- Admin act: signature date not mandatory

### Features ###
- Container: weight and calculated weight

v3.1.3 - 2021-03-04
--------------------

### Bug fix ###
- UI fix on fields of bulk edition

### Features ###
- Bulk edition add fields:
  - operation: finds/doc received
  - organization: grammatical gender
  - finds: remarkabilities, insurance value

v3.1.2 - 2021-03-02
--------------------

### Bug fix ###
- tables: pinned search do not overload click on shortcuts
- gallery: fix pagination
- manage corrupted image - silently fail
- custom forms: use profile type instead of user types for filters - user 
  types usage still works but marked as deprecated -> cannot edit anymore 

v3.1.1 - 2021-02-28
--------------------

### Features ###
- new division management: now a division is a container
- documents: many new fields - document can be inside a container
- templates: better performance - new filters
- labels generation: fixes and new links
- custom index
- custom complete identifier

### Bug fix ###
Many fixes on parcels, documents, files, sheets.

v3.0.17 - 2020-12-02
--------------------

### Bug fix ###
- Archaeological files: fix default labels

v3.0.16 - 2020-11-16
--------------------

### Features ####
- Admin: action to add automatically department towns to an area
- Imports: add "|" separator for CSV

### Bug fix ###
- Operation sheet: fix parcel column on context record table
- Labels generation: fix missing images in templates
- Labels generation: optimize template evaluation

v3.0.15 - 2020-11-04
--------------------

### Bug fix ###
- Operation sheet: fix parcel column on context record table

v3.0.14 - 2020-10-05
--------------------

### Bug fix ###
- Search: fix many town search
- Context records:
  - sheet: fix length display
  - form: fix display of documentation field

v3.0.13 - 2020-09-22
--------------------

### Bug fix ###
- ajax_selects: fix Admin > Import edit

v3.0.12 - 2020-09-17
--------------------

### Bug fix ###
- ajax_selects: fix missing "bootstrap.js" file missing in debian install

v3.0.11 - 2020-08-31
--------------------

### Features ####
- Command - media_find_missing_files: "hard" option

v3.0.10 - 2020-07-26
--------------------

### Features ####
- Admin: Import/export as JSON for types and importers

### Bug fix ###
- Fix modal order for find select form

v3.0.9 - 2020-07-01
-------------------

### Bug fix ###
- Fix bad many to many field renitialization on wizards (refs #4963)

v3.0.8 - 2020-05-26
-------------------

### Features ####
- Search finds/context records: add missing search criteria for datings

### Bug fix ###
- Fix corrupted image
- Operation - Wizard: fix parcel sort
- Find - search: fix dates fields

v3.0.7 - 2020-05-11
-------------------
### Features ####
- Imports:
  - multiple target with different formaters in one column. Warning:
  duplicate fields use the first target
  - manage boolean match for data__ (custom fields)

v3.0.6 - 2020-05-01
-------------------
### Bug fix ###
- Fix database initialization

v3.0.5 - 2020-03-23
-------------------
### Features ###
- Autocomplete widget:
  - tips (display information for the type of item searched)
  - modify action
  - display detail (address) of the current item

### Bug fix ###
- Many fixes on file module


v3.0.4 - 2020-03-11
-------------------
### Features ###
- Quick actions - sheets: add duplicate for site, operation, document and context record
- Quick actions - tables:
  - person: bulk update - organization field
  - document: bulk update - creation date field
  - operation: bulk update - operator field
  - site: add bulk update form (towns field)
  - find: bulk update - appraisal date and treatment emergency fields
- Search - criteria:
  - Containers:
    - find ID, label
    - operation: town, scientist
  - Warehouse:
    - town
  - All document items:
    - has associated file
    - has associated image
    - has associated url
    
### Bug fix ###
- Tables: fix display of links in columns
- Criteria search: manage empty field (but not NULL) multiple level search for file (including images) fields


v3.0.3 - 2020-02-24
-------------------
### Bug fix ###
- Import: fix concatenation when the concat string is empty

v3.0.2 - 2020-02-17
-------------------
### Bug fix ###
- Admin: improve performance
- Sentry integration

v3.0.1 - 2020-02-11
-------------------
### Bug fix ###
- Finds: do not duplicate treatments on duplicate action
- QA basket: fix add to a shared basket


v3.0.0
------
A lot of changes since v2. Main change is python3 and Django 1.11 support.
