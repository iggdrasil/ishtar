v4.0.41 - 2023-01-23
--------------------

### Technique ###
- Correction de filtre javascript sur certains champs

v4.0.40 - 2023-01-22
--------------------

### Technique ###
- Amélioration de certaines traductions

v4.0.39 - 2023-01-20
--------------------

### Fonctionnalités/améliorations ###
- Critères de recherche - Personne : salutation/titre

v4.0.38 - 2023-01-20
--------------------

### Corrections de dysfonctionnements ###
- Fiche lieu de conservation : correction de libellés en double
- Export PDF : correction du caractère nouvelle ligne sur les blocs <pre>.


v4.0.37 - 2023-01-20
--------------------

### Corrections de dysfonctionnements ###
- Recherche mobilier : correction de la recherche sur la date de découverte
- Fiche mobilier/contenant : correction de l'affichage de dates et de nombres à virgule

v4.0.36 - 2023-01-20
--------------------

### Fonctionnalités/améliorations ###
- Fiche mobilier : refonte
- Lieu de conservation : nouveau champ « slug » pour éviter le changement d'ID lorsque le nom est modifié
- Lieu de conservation : ajout de la colonne « commune » dans les tableaux
- Modification de libellés
    - Identifiant musée -> Numéro d'inventaire musée
    - Localisation (fiche) -> Localisation géographique
    - Date de découverte (exacte ou TPQ) -> Date de découverte (exacte ou début)
    - Date de découverte (TAQ) -> Date de découverte (fin)

### Technique ###
- Indexation de la recherche libre :
    - Ajout de la référence entière et fractionnée
    - Amélioration de l'indexation des éléments parents


v4.0.35 - 2023-01-18
--------------------

### Corrections de dysfonctionnements ###
- Correction de certaines recherches de types avec « * »
- Imports : correction de l'import du champ  « attribution culturelle »
- Fiche :
    - Lieu de conservation : correction de la localisation - correction du champ data
    - Contenant : correction de la localisation - correction du champ data

### Technique ###
- Tableau : correction du tri par défaut lorsque la clé primaire n'est pas « id »
- Admin : performances des formulaires contenants et lieux de conservation

v4.0.34 - 2023-01-13
--------------------

### Technique ###
- Syndication :
    - Fiches - correction des permissions par défaut
    - Fiches - correction de l'affichage des détails d'un dictionnaire (affichage du libellé associée)
    - Admin : ApiSearchModel - correction du filtre d'export pour le mobilier
    - Table - sélectionner/désélectionner tout
    - Table - réinitialise les en-têtes au chargement

v4.0.33 - 2023-01-12
--------------------

### Fonctionnalités/améliorations ###
- Tables - export : exportation uniquement des résultats sélectionnés
- Syndication :
    - Export possible des sources externes
    - Interface : meilleure couleur pour les sources externes

### Correction de dysfonctionnements ###
- Correction des recherches géographiques pour les sources externes : ne s'affichent pas
- Correction de la recherche épinglée pour les sources externes
- Correction d'un bug mystérieux sur les statistiques avec critères

### Technique ###
- Recherche : tri par identifiant par défaut
- Commandes admin : gérer la mise à jour du vecteur de recherche avec un script de maintenance
- Utilitaire : adaptation à l'importeur de relations OSM v4
- Interface `bootstrap` pour les statistiques


v4.0.32 - 2022-12-12
--------------------

### Fonctionnalités/améliorations ###
- Fiche - mobile : ne pas afficher les informations sur le contenant si pas de permission sur le contenant
- Formulaire document - champ image : utilisation de l'appareil photo sur appareils mobiles

### Technique ###
- Fiche : définir par défaut toutes les permissions à « False »
- Import - champ formatage : la notation {item} est utilisable
- Recherche libre : ajout de la configuration « simple » pour l'indexation (pas de traitement)

v4.0.31 - 2022-12-05
--------------------

### Fonctionnalités/améliorations ###
- Admin : export de la table « modèles de données »
- Imports : gestion de la notation `__` pour les valeurs par défaut

### Technique ###
- Migration v4 : désactivation du post-traitement M2M

v4.0.30 - 2022-12-02
--------------------

### Technique ###
- Utilisation de « supervisor » au lieu de scripts systemd pour le démon celery
- Formulaires : correction mineure du rendu

v4.0.29 - 2022-11-29
--------------------

### Améliorations ###
- Indexation de recherche - Unité d'enregistrement : ajout du type
- Imports : ajout de « Type d'acte » aux types supportés
- Admin : améliorations de présentation sur la syndication

### Correction de dysfonctionnements ###
- Correction de l'URL de redirection après la déconnexion
- Liste de choix - désactivation de traduction
- Corrections mineures de traduction


v4.0.28 - 2022-11-18
--------------------

### Améliorations ###
- Admin : profil du site - meilleure présentation du formulaire
- Imports : affichage d'une barre de progression sur les traitements post-processus

### Correction de dysfonctionnements ###
- Correction de l'import lorsqu'aucun objet n'est créé et qu'aucune erreur n'est détectée
- Création d'opération depuis un dossier - correction de l'association automatique des communes et de la recopie des parcelles.

### Technique ###
- Refonte du module Dossier
- Suppression de code mort


v4.0.27 - 2022-11-10
--------------------

### Correction de dysfonctionnements ###
- Correction de l'affichage des éléments multi-valués pour les éléments historisés

## Technique ##
- Amélioration de la couverture de tests


v4.0.26 - 2022-11-08
--------------------

### Fonctionnalités ###
- Suppression des champs x, y, z obsolètes sur les formulaires d'éléments principaux

### Correction de dysfonctionnements ###
- Fiche mobilier : correction de l'affichage de la carte
- Formulaire mobilier : correction de l'ordre du champ « décoration »


v4.0.25 - 2022-11-07
--------------------

### Fonctionnalités ###
- Données géographiques : affichage du premier élément associé en tant que nom lorsque aucun nom n'est fourni

### Correction de dysfonctionnements ###
- Import de données géographiques : correction de l'évaluation du nombre total d'éléments lors des imports

### Technique ###
- Modification de panier : correction de la mise à jour du `wizard`


v4.0.24 - 2022-10-27
--------------------

### Technique ###
- Sauvegarde des données géographique : transactions en base de données pour limiter les blocages
