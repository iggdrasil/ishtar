v4.0.23 - 2022-10-26
--------------------

### Fonctionnalités ###
- Données géographiques - affichage carte : bouton de zoom sur l'étendue

### Technique ###
- Post-sauvegarde des données géographiques : transactions en base de données et post-sauvegarde ciblée pour limiter les blocages

v4.0.22 - 2022-10-14
--------------------

### Fonctionnalités ###
- Unité d'enregistrement : la « technique de fouille » devient multi-valuée

v4.0.21 - 2022-10-14
--------------------

### Fonctionnalités ###
- Données géographiques : action rapide d'ajout géo pour les unités d'enregistrement, les sites et le mobilier

### Correction de dysfonctionnements ###
- Améliorations des pages d'administration

### Technique ###
- Données géographiques : gestion des associations « plusieurs à plusieurs »  et de l'association en cascade
- Données géographiques : test d'association `main_geodata` corrigé


v4.0.20 - 2022-10-03
--------------------

### Fonctionnalités ###

- Formulaire géographique - redirection vers la fiche de l'élément source après modification

v4.0.19 - 2022-09-18
--------------------

Reversement des dernières améliorations de la v3.

v4.0.18 - 2022-09-12
--------------------

### Fonctionnalités ###

- Formulaire Import SIG : par défaut 0 saut de lignes (pour geopackage et shapefile cela doit être 0)

### Correction de dysfonctionnements ###

- Données géographiques :
    - Script de migration : plus tolérant avec l'absence de données géo
    - La géométrie principale est polygone lorsqu'un polygone est présent

v4.0.17 - 2022-09-10
--------------------

### Correction de dysfonctionnements ###

- Données géographiques : correction du zoom sur l'étendue - correction du nombre d'éléments

v4.0.16 - 2022-09-09
--------------------

### Fonctionnalités ###

- Données géographiques : affichage des Unités d'enregistrement associées et du mobilier associé sur la fiche de carte
- Données géographiques : possibilité d'affichage des lignes et polygones sur la carte de recherche

### Correction de dysfonctionnements ###
- Correction de la balise pour patron de document `safe_or`

v4.0.15 - 2022-08-30
--------------------

### Fonctionnalités ###

- Critère de recherche : « est verrouillé »

### Correction de dysfonctionnements ###
- Correction de la génération du numéro INSEE en cas d'erreur avec les années
- Correction de la recherche par critère « a du mobilier ».

v4.0.14 - 2022-08-05
--------------------

### Fonctionnalités ###

- Imports : gestion des données géographiques depuis CSV

### Correction de dysfonctionnements ###

- Fiche géo : correction de l'affichage des coordonnées

v4.0.13 - 2022-08-05
--------------------

### Correction de dysfonctionnements ###

- Imports : vérification de CSV, correction de la vérification du codage de caractères

### Technique ###
- Imports : ajout d'actions post import afin de gérer l'importeur de compte

v4.0.12 - 2022-08-05
--------------------

### Correction de dysfonctionnements ###

- Correction d'un dysfonctionnement pour l'installation nue (i.e. : requête sur la base de données avant qu'elle ne soit initialisée)

v4.0.11 - 2022-08-02
--------------------

### Correction de dysfonctionnements ###

- Admin - formulaire personnalisé : correction de l'initialisation du choix du formulaire
- Dossiers :
    - Formulaire parcelle : correction de l'initialisation de la commune
    - Suppression des formulaires parcelle des `wizards`
    - Correction de la modification lorsque l'aménageur est absent

v4.0.10 - 2022-07-22
--------------------

### Caractéristiques ###

- Admin - Champs personnalisés : exports et imports CSV, json

### Correction de dysfonctionnements ###

- Fiche document - correction du contenu du permalien

### Technique ###
- scripts `migrate_to_geo_v4` :
    - Correction de la migration des éléments `multi_polygon_source_item` et `point_source_item`
    - Correction du libellé « Contour »

v4.0.9 - 2022-07-21
-------------------

### Correction de dysfonctionnements ###
- Création d'imports : correction du filtre de type d'importateur

v4.0.8 - 2022-07-18
-------------------

### Fonctionnalités ###
- Mise à jour de traductions

v4.0.7 - 2022-07-18
-------------------

### Technique ###
- Refonte du `wizard` opération

v4.0.6 - 2022-07-17
--------------------

### Correction de dysfonctionnements ###
- Création de site : correction du message final

v4.0.5 - 2022-07-17
--------------------

### Correction de dysfonctionnements ###
- Fiche géo : correction de l'affichage des coordonnées
- Suppression de compte : correction du résumé pré suppression

v4.0.4 - 2022-07-17
--------------------

### Fonctionnalités ###
- Mise à jour des traductions

v4.0.3 - 2022-07-15
--------------------

### Technique ###
- `migrate_to_geo_v4` : correction de l'opération et de l'import de site lorsque le SRID est différent de 4326

v4.0.2 - 2022-06-15
--------------------

### Technique ###
- `migrate_to_geo_v4` : correction de l'import de l'élément principal lorsque le SRID est égal à 4326

v4.0.1 - 2022-06-15
--------------------

### Principales fonctionnalités ###
- Nouvelle gestion des éléments géographiques
- Syndication des bases de données ishtar

### Fonctionnalités ###
- formulaire spécifique pour la gestion des parcelles
- action rapide de tableau : traitement de regroupements
- meilleure interface utilisateur pour les imports

### Correction de dysfonctionnements ###
- Augmentation de la longueur maximale des numéros de téléphone

### Technique ###
- Migration vers Django 2.2
