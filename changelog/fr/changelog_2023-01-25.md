v4.0.80 - 2025-02-19
--------------------

## Correction de dysfonctionnements ###
- formulaires : correction des filtres de champs datations (#4281, #6193)


v4.0.79 - 2025-02-10
--------------------

### Corrections de dysfonctionnements ###
- formulaires : correction de crash lorsque certains filtres sont appliqués (#6166)

v4.0.78 - 2024-09-15
--------------------

### Corrections de dysfonctionnements ###
- correction de la galerie documents (#6022)

v4.0.77 - 2024-06-05
--------------------

### Corrections de dysfonctionnements ###
- correction des formulaires géo - pas de créateur

v4.0.76 - 2024-06-05
--------------------

### Corrections de dysfonctionnements ###
- formulaire document et géographique : correction de la surcharge de l'utilisateur créateur en modification (refs #5964)


v4.0.75 - 2024-04-16
--------------------

### Technique ###
- Mobilier : ajout de l'index à la recherche libre

v4.0.74 - 2024-04-16
--------------------

### Technique ###
- formule JINJA : ajout des filtres de déduplication et de remplissage par 0

v4.0.73 - 2024-03-19
--------------------

### Corrections de dysfonctionnements ###
- correction de la galerie de documents

v4.0.72 - 2024-02-05
--------------------

### Technique ###
- mise à jour des scripts d'import IGN/INSEE


v4.0.71 - 2024-01-17
--------------------

### Corrections de dysfonctionnements ###
- zone géographique : correction de la génération automatique de slugs (#5715)
- tableau des documents : correction de la duplication des lignes pour les auteurs (#5709). **Avertissement** : le décompte dans le tableau n'est plus cohérent, une correction plus robuste sera livrée dans la version v4.1

v4.0.70 - 2023-12-19
--------------------

### Technique ###
- relations entre unité d'enregistrement : forcer une relation un à un lors de la création d'une unité d'enregistrement -> corrige la liste du mobilier sur la fiche d'unité d'enregistrement

v4.0.69 - 2023-12-18
--------------------

### Technique ###
- valeurs associées au mobilier pour les patrons de documents : utiliser le préfixe "base_find_" et le préfixe simple (#5695)
- formulaire de compte : en modification le type de profil est en lecture seule afin d'éviter certaines erreurs lors de la suppression (#5696)

v4.0.68 - 2023-11-30
--------------------

### Corrections de dysfonctionnements ###
- administration contenants : correction de la recherche

### Technique ###
- import : interception des erreurs d'integrité en post-traitement

v4.0.67 - 2023-11-29
--------------------

### Corrections de dysfonctionnements ###
- Formulaire SIG - suppression des champs "horodatage" (#5673)
- Profile d'instance :
    - correction de migration de base de données : valeur par défaut pour l'identifiant complet et le libellé mise en cache (#5674)
    - correction du champ "centre par défaut" (#5675)
- Table documents : correction d'un problème de performance lors du tri (#5667)
- Document - Formulaire de mise à jour groupée : correction de l'ajout de nouveau auteur (#5681)
- Formulaire personalisé : ajout de type de formulaire manquants (#5668)
- Formulaire nouvel élément géographique : widget de carte centré sur le centre de l'élément géographique principal ou à défaut au centre par défaut paramétré pour l'instance (#5676)

v4.0.66 - 2023-11-22
--------------------

### Corrections de dysfonctionnements ###
- patrons - valeur des contenants : correction de l'ordre du listing mobilier (#5661)

v4.0.65 - 2023-11-15
--------------------

### Corrections de dysfonctionnements ###
- recherche : correction des recherches '-type="*"' pour les types sans hiérarchie

v4.0.64 - 2023-11-14
--------------------

Nombreuses améliorations dans les post-traitements. De meilleures performances sont attendues pour les imports.

### Technique ###
- suppression de la mise à jour "bulk" : ne devrait plus être utile et pouvait sévèrement dégrader les performances.
- amélioration des post-traitements des parcelles - ajout d'un horodatage pour éviter des répétition de post traitements geo et de label en cache.


v4.0.63 - 2023-11-08
--------------------

### Corrections de dysfonctionnements ###
- Statistiques: correction de l'affichage image (#5650)
- Expiration du mot de passe : suppression du cache après la réinitialisation du mot de passe

v4.0.62 - 2023-10-27
--------------------

### Corrections de dysfonctionnements ###
- Fiche opération : affichage de champs préventifs manquants, amélioration d'affichage de date
- Fiche d'opération : correction du filtre sur l'opération sur la table de relations entre unités d'enregistrements liés (#5647)

v4.0.61 - 2023-10-18
--------------------

### Corrections de dysfonctionnements ###
- Correction de traduction du composant graphique calendrier (#5642)
- Formulaires personnalisés : correction de crash lorsque l'on enlève des champs déjà filtrés (#5643)

v4.0.60 - 2023-10-10
--------------------

### Corrections de dysfonctionnements ###
- gestion de photos avec quelques "bits" défectueux (#5626)


v4.0.59 - 2023-09-29
--------------------

### Corrections de dysfonctionnements ###
- correction des traductions manquantes
- suppression de "BASE_URL" sur les fiches images (non nécessaire localement et source de dysfonctionnements en accès distant)
- les zones créées automatiquement par les opérations comportant plusieurs villes ne sont pas "disponibles" par défaut

v4.0.58 - 2023-09-26
--------------------

### Corrections de dysfonctionnements ###
- Fédération :
    - au lieu de supprimer les champs exclus, leur attribuer la valeur vide
    - fiches distances : ajout de permissions par défaut manquantes
    - URL absolue pour les ressources

v4.0.57 - 2023-09-13
--------------------

### Technique ###
- div principal plus large sur appareil mobile

### Corrections de dysfonctionnements ###
- correction du sélecteur photo sur appareil mobile


v4.0.56 - 2023-09-06
--------------------

### Fonctionnalités/améliorations ###
- amélioration de la documentation en ligne

### Technique ###
- imports - correspondance : suppression de clés non pertinentes
- imports - geo : correction de l'association aux imports lorsque la clé d'import correspond
- correction de la mise à jour des coordonnées mises en cache en cas de changement de précision du SRS

v4.0.55 - 2023-08-07
--------------------

### Technique ###
- script `ishtar_maintenance` : ajout de l'option "filter" afin de limiter le script à une requête spécifique
- correction de crash quand aucune recherche n'est comptabilisée
- correction de crash lors de mise à jour en cascade du mobilier d'origine
- optimisation des post-traitements : éviter des mises à jour en cascade inutiles (refs #5617)

v4.0.54 - 2023-07-19
--------------------

### Fonctionnalités/améliorations ###
- `wizards` : défilement automatique sur le champ lorsque l'on navigue avec la touche TAB
- amélioration de mise en page : formulaires de mise à jour de mot de passe et d'inscription
- formulaire de mise à jour du mot de passe : redirection vers la page d'accueil
- formulaire d'inscription ouvert : désactivé par défaut
- formulaire de réinitialisation de mot de passe par courriel
- bouton pour montrer le mot de passe sur le formulaire administrateur de changement de mot de passe
- le style de nommage par défaut des noms d'utilisateur est prenom.nom

### Corrections de dysfonctionnements ###
- correction de redirection sur le mobilier après ajout/édition d'élément géographique
- formulaire de compte : correction de la normalisation du nom d'utilisateur
- correction de l'action de conditionnement rapide (#5611)

v4.0.53 - 2023-07-06
--------------------

### Fonctionnalités/améliorations ###
- `wizards` : validation des formulaires avec CTRL+ENTER

### Corrections de dysfonctionnements ###
- correction de l'autofocus du premier champ
- ne pas surcharger les identifiants complets quand ils sont vides

v4.0.52 - 2023-07-06
--------------------

### Corrections de dysfonctionnements ###
- message après création : utilisation de generate_cached_label si aucun label n'est disponible

### Technique ###
- correction d'une boucle infinie sur regenerate_all_id


v4.0.51 - 2023-07-05
--------------------

### Fonctionnalités/améliorations ###
- fiche : ajout d'un bouton de rafraîchissement pour mettre à jour les informations

### Corrections de dysfonctionnements ###
- correction de l'affichage des parcelles et du résumé du `wizard` (localisation inappropriée)
- vérification des identifiants externes lors de la mise à jour des libellés en cache (#5603)


v4.0.50 - 2023-07-03
--------------------

### Fonctionnalités/améliorations ###
- Mise à jour de la documentation
- Mise à jour des traductions
- Amélioration de l'affichage du journal de modifications

### Corrections de dysfonctionnements ###
- Formulaire opération : le champ code opération (Patriarche) n'est plus facultatif
- Correction de la recherche "*" pour les champs texte (#5325)
- Formulaires - champ unité : correction des arrondis des flottants
- Fiche : correction de l'affichage à virgule pour les hectares
- Correction d'erreur lors de la génération de qrcode pour le mobilier d'origine


v4.0.49 - 2023-06-21
--------------------

### Fonctionnalités/améliorations ###
- Amélioration de l'affichage du journal de modifications
- Fiche : affichage du système de référence spatiale avec les coordonnées des points


### Technique ###
- JS : correction TrackPositionControl avec le https


v4.0.48 - 2023-06-08
--------------------

### Fonctionnalités/améliorations ###
- Critères de recherche - Géo - « Z plus petit », « Z plus grand »
- Fiche : affichage des coordonnées des points

### Technique ###
- Arrondi et arrondi en z par défaut pour chaque système de référence spatiale
- Ne pas mettre le journal de débogage dans le répertoire projet sous Debian
- Correction de type mime pour settings.js


v4.0.47 - 2023-06-08
--------------------

### Technique ###
- Amélioration des post-traitements après import
- Nombreuses corrections (ou potentielles corrections) dans les templates (localisation inappropriée) (#5594)


v4.0.46 - 2023-05-26
--------------------

### Fonctionnalités/améliorations ###
- Communes/zones géographiques :
    - attachement de documents
    - mise en place de fiches
    - liens depuis les autres fiches vers les fiches communes/zones géographiques

### Corrections de dysfonctionnements ###
- Recalcul du poids d'un contenant lorsque du mobilier en est retiré (#5470)

### Technique ###
- JS : correction UnitWidget (localisation inappropriée)
- Correction du HTML de la page d'import (localisation inappropriée)


v4.0.45 - 2023-05-15
--------------------

### Fonctionnalités/améliorations ###
- Administration : affichage des sous-catégories amélioré (en couleur)
- Document - modification par lot : modification des étiquettes (#5415)

### Corrections de dysfonctionnements ###
- Correction de la recherche commune pour les lieux de conservation et les organisations
- Correction de la recherche contenant pour les documents

### Technique ###
- Migration de la bibliothèque font-awesome -> fork-awesome
- JS inline migré vers un settings.js dynamique pouvant être mis en cache
- Import contenants : éviter le crash si le type de contenant est vide
- Commande media_find_missing_files: ajout des options set-alt-media-dir pour faire des liens de media manquants depuis un répertoire alternatif et set-empty-image pour créer des liens vers des images factices pour les images manquantes


v4.0.44 - 2023-04-17
--------------------

### Fonctionnalités/améliorations ###
- Affichage d'un journal des modifications avec affichage d'alerte lors de mises à jour
- Contenants : gestion de l'historique
- Sécurité:
    - Gestion de l'expiration des mots de passe
    - Gestion d'une politique de mot de passe fort (possibilité de forcer des classes de caractères).
    - Ajout de validateurs d'authentification par défaut pour les changements de mots de passe
    - Expiration de session configurée à 5 jours par défaut
    - Sécurisation optionnelle (désactivé par défaut) des tentatives de connexion : traçage, blocage de compte après multiples erreurs de connexion.

### Corrections de dysfonctionnements ###
- Champs Json : correction d'une mauvaise sauvegarde de valeurs multiples
- Fiches : correction de l'affichage de l'historique avec la géographie associée
- Contenants : suppression de la recherche par division
- Détail de l'image : ne pas afficher le lien Modifier lorsqu'il n'est pas pertinent (#5438)
- Correction du libellé français pour les types de géographie (#5577)
- Correction des permissions pour les demandes de traitements (#5441)
- Mise à jour en cascade des contenants lors de modifications des lieux de conservation

### Technique ###
- Refactorisation des tâches de chargement - gestion de la régénération de l'identifiant externe avec les tâches
- Mise à jour et correction des traductions (#5578, #5579, #5581)
- Export d'importeur : correction de l'appel au `pre_importer`
- Utilisation d'une longueur de sel de 128 bits pour le hachage des mots de passe
- Marquage de faux problèmes de sécurité détectés par l'outil « bandit »
- Correction de problèmes de sécurité de faible gravité

v4.0.43 - 2023-03-17
--------------------

### Fonctionnalités/améliorations ###
- Modèle : ajout d'un historique pour les documents et les contenants
- Menu :
    - Suppression de l'entrée « Administration »
    - Gestion des comptes déplacé dans l'entrée « Annuaire »
    - Les variables globales sont maintenant seulement éditables dans l'administration Django
- Imports : amélioration de la détection des mauvais codages de caractères
- Critères de recherche :
    - Ajout du critère « créé avant/après »
    - Opération : ajout du critère « ancien code »
    - Opérations et sites : ajout du critère « vestiges »
    - Compte : ajout du critère « type de profil »
    - Unité d'enregistrement : ajout des critères « identification » et « activité »
- Recherche en texte libre :
    - Amélioration de la recherche sur les chaînes accentuées
    - Ajout de la configuration d'un thésaurus dédié `french_archeo`
    - Dossier : indexation de la référence `année-index`
- Fiche :
    - Document : amélioration de l'affichage pour les fichiers
    - Demande de traitement : ajout d'une image associée et d'une liste de documents
- Configuration : ajout de message personnalisé en bas de page
- Message explicite sur la suppression du contenant associé lors de la suppression d'un lieu de conservation
- Acte administratif : ajout d'un avertissement lors de la suppression d'un élément associé
- Utilisation d'une police avec empattement pour les balises `<pre>` (utilisée notamment pour les descriptions)
- Administration Django :
    - Ajout d'en-têtes de sous-section pour clarifier les modules
    - Variable globale : édition depuis le tableau, ajout de l'import/export en CSV/JSON
    - Imports : enregistrer automatiquement les « types de modèles » en export JSON
- Geographie : ajout des tuiles IGN par défaut
- Site : action rapide pour la création d'une opération virtuelle à partir d'un site avec de nombreuses opérations
- Interface utilisateur :
    - En-tête de fiche - ajout d'une flèche pour signifier la possibilité de replier la fiche
    - Bouton pour afficher/masquer le mot de passe lors de la connexion
- Formulaires :
    - Formulaire personnalisé - possibilité d'ajouter un message d'en-tête

### Corrections de dysfonctionnements ###
- Recherche :
    - Correction des recherches par critères (en particulier recherche en exclusion et recherche cumulant plusieurs critères)
    - Document : le type de source est maintenant une recherche hiérarchique
- Formulaire mobilier : suppression de la vérification de cohérence TAQ/TPQ
- Fiche :
    - Ne pas afficher la liste des éléments géographiques lorsque la cartographie est désactivée
    - Traitement, demande de traitement : correction de l'affichage (mauvais lien avec le QR-code)
    - Opération : correction des statistiques sur le nombre de parcelles
    - Panier : affichage des partages par ordre alphabétiques
- Dossier : retrait de l'action d'édition du plan d'intervention lorsque  le module « opérateur préventif » est désactivé dans le profil 

### Technique ###
- Général :
    - Ajout d'une configuration personnalisée de libellé en cache pour chaque élément principal
    - Ajout d'un champ `created` sur les éléments principaux afin de faciliter les requêtes
- Paramètres : ajout de `ISHTAR_SECURE_OPTIONS` pour activer les options sécurisées de Django
- Géographie : nouvelle version openlayers
- Scripts de maintenance : suppression des scripts obsolètes et migration vers `ishtar_maintenance`


v4.0.42 - 2023-01-25
--------------------

### Fonctionnalités/améliorations ###
- Traitement : amélioration de la présentation - ajout de liens détaillés

### Technique ###
- Imports : formatage complexe des formules
- Installation : correction du script d'installation pour permettre la personnalisation du port et de l'hôte de la base de données
