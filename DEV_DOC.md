Attributes use to manage post-process:

- `skip_history_when_saving`: do not record last modified date and creation/update author
- `_post_saved_geo`: do not process geo item attached
- `_cached_label_checked`: do not process cached labels
- `_external_id_changed`: do not check external_id
- `_search_updated`: do not reindex search
- `_no_move`: do not move associated images on save - to be checked
- `no_post_process()`: set to True previous attributes
