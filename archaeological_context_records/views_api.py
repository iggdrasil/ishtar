from ishtar_common.rest import SearchAPIView, FacetAPIView, GetAPIView, ExportAPIView
from archaeological_context_records import models, forms


class FacetContextRecordAPIView(FacetAPIView):
    models = [models.ContextRecord]
    select_forms = [forms.RecordSelect]


class SearchContextRecordAPI(SearchAPIView):
    model = models.ContextRecord


class ExportContextRecordAPI(ExportAPIView):
    model = models.ContextRecord


class GetContextRecordAPI(GetAPIView):
    model = models.ContextRecord
