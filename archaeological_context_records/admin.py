#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2012-2015 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.contrib import admin

from ishtar_common.apps import admin_site
from ishtar_common.admin import HistorizedObjectAdmin, GeneralTypeAdmin, MainGeoDataItem

from . import models


class DatingAdmin(admin.ModelAdmin):
    list_display = ("period", "context_records_lbl", "finds_lbl")
    list_filter = ("period", "dating_type", "quality")
    model = models.Dating
    search_fields = [
        "context_records__cached_label",
        "period__label",
        "find__cached_label",
    ]
    readonly_fields = ["context_records_lbl", "finds_lbl"]


admin_site.register(models.Dating, DatingAdmin)


class ContextRecordAdmin(HistorizedObjectAdmin, MainGeoDataItem):
    list_display = ("label", "operation", "parcel")
    list_filter = ("unit",)
    search_fields = (
        "cached_label",
        "town",
        "parcel__section",
        "parcel__parcel_number",
    )
    autocomplete_fields = ["operation", "parcel", "town", "archaeological_site",
                           "lock_user"]
    model = models.ContextRecord
    readonly_fields = HistorizedObjectAdmin.readonly_fields + [
        "cached_label",
        "datings",
    ]
    exclude = ["documents", "main_image"]


admin_site.register(models.ContextRecord, ContextRecordAdmin)


class RecordRelationsAdmin(admin.ModelAdmin):
    list_display = ("left_record", "relation_type", "right_record")
    list_filter = ("relation_type",)
    model = models.RecordRelations
    autocomplete_fields = ["left_record", "right_record"]


admin_site.register(models.RecordRelations, RecordRelationsAdmin)


class RelationTypeAdmin(GeneralTypeAdmin):
    LIST_DISPLAY = (
        "label",
        "txt_idx",
        "tiny_label",
        "available",
        "symmetrical",
        "logical_relation",
        "inverse_relation",
        "order",
        "comment",
        "logical_relation",
        "inverse_relation",
    )


admin_site.register(models.RelationType, RelationTypeAdmin)


class UnitAdmin(GeneralTypeAdmin):
    list_display = ["label", "txt_idx", "parent", "available", "order", "comment"]


admin_site.register(models.Unit, UnitAdmin)


class IdentificationTypeAdmin(GeneralTypeAdmin):
    list_display = ["label", "txt_idx", "available", "order", "comment"]


admin_site.register(models.IdentificationType, IdentificationTypeAdmin)


general_models = [
    models.DatingType,
    models.DatingQuality,
    models.DocumentationType,
    models.ActivityType,
    models.ExcavationTechnicType,
]
for model in general_models:
    admin_site.register(model, GeneralTypeAdmin)
