#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2010-2016 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.conf.urls import url
from django.urls import path

from ishtar_common.utils import check_rights
from archaeological_context_records import models, views, views_api

# be careful: each check_rights must be relevant with ishtar_menu

# forms
urlpatterns = [
    # Context records
    url(
        r"record_search/(?P<step>.+)?$",
        check_rights(["view_contextrecord", "view_own_contextrecord"])(
            views.record_search_wizard
        ),
        name="record_search",
    ),
    url(
        r"contextrecord_search/(?P<step>.+)?$",
        check_rights(["view_contextrecord", "view_own_contextrecord"])(
            views.record_search_wizard
        ),
        name="contextrecord_search",
    ),
    url(
        r"record_creation/(?P<step>.+)?$",
        check_rights(["add_contextrecord", "add_own_contextrecord"])(
            views.record_creation_wizard
        ),
        name="record_creation",
    ),
    url(
        r"record_modification/(?P<step>.+)?$",
        check_rights(["change_contextrecord", "change_own_contextrecord"])(
            views.record_modification_wizard
        ),
        name="record_modification",
    ),
    url(r"record_modify/(?P<pk>.+)/$", views.record_modify, name="record_modify"),
    url(
        r"record_deletion/(?P<step>.+)?$",
        check_rights(["change_contextrecord", "change_own_contextrecord"])(
            views.record_deletion_wizard
        ),
        name="record_deletion",
    ),
    url(
        r"record_delete/(?P<pk>.+)/$", views.record_delete, name="delete-contextrecord"
    ),
    url(
        r"autocomplete-contextrecord/$",
        views.autocomplete_contextrecord,
        name="autocomplete-contextrecord",
    ),
    url(
        r"show-contextrecord(?:/(?P<pk>.+))?/(?P<type>.+)?$",
        views.show_contextrecord,
        name=models.ContextRecord.SHOW_URL,
    ),
    # show-contextrecordrelation is only a view the id point to a context record
    url(
        r"show-contextrecord(?:/(?P<pk>.+))?/(?P<type>.+)?$",
        views.show_contextrecord,
        name="show-contextrecordrelation",
    ),
    url(
        r"show-historized-contextrecord/(?P<pk>.+)?/(?P<date>.+)?$",
        views.show_contextrecord,
        name="show-historized-contextrecord",
    ),
    url(
        r"revert-contextrecord/(?P<pk>.+)/(?P<date>.+)$",
        views.revert_contextrecord,
        name="revert-contextrecord",
    ),
    url(
        r"generate-relation-image-contextrecord/(?P<pk>.+)/("
        r"?P<type>(below)|(above)|(full))?",
        views.GenerateRelationImage.as_view(),
        name="generate-relation-image-contextrecord",
    ),
    url(
        r"get-contextrecord/own/(?P<type>.+)?$",
        views.get_contextrecord,
        name="get-own-contextrecord",
        kwargs={"force_own": True},
    ),
    url(
        r"get-contextrecord/(?P<type>.+)?$",
        views.get_contextrecord,
        name="get-contextrecord",
    ),
    url(
        r"get-contextrecord-for-ope/own/(?P<type>.+)?$",
        views.get_contextrecord_for_ope,
        name="get-own-contextrecord-for-ope",
        kwargs={"force_own": True},
    ),
    url(
        r"get-contextrecord-for-ope/(?P<type>.+)?$",
        views.get_contextrecord_for_ope,
        name="get-contextrecord-for-ope",
    ),
    url(
        r"get-contextrecord-full/own/(?P<type>.+)?$",
        views.get_contextrecord,
        name="get-own-contextrecord-full",
        kwargs={"full": True, "force_own": True},
    ),
    url(
        r"get-contextrecord-full/(?P<type>.+)?$",
        views.get_contextrecord,
        name="get-contextrecord-full",
        kwargs={"full": True},
    ),
    url(
        r"get-contextrecord-shortcut/(?P<type>.+)?$",
        views.get_contextrecord,
        name="get-contextrecord-shortcut",
        kwargs={"full": "shortcut"},
    ),
    url(
        r"get-contextrecordrelation/(?P<type>.+)?$",
        views.get_contextrecordrelation,
        name="get-contextrecordrelation",
    ),
    url(
        r"get-contextrecordrelationdetail/(?P<type>.+)?$",
        views.get_contextrecordrelationdetail,
        name="get-contextrecordrelationdetail",
    ),
    url(
        r"^operation-qa-contextrecord/(?P<pks>[0-9]+)/$",
        check_rights(["add_contextrecord", "add_own_contextrecord"])(
            views.QAOperationContextRecordView.as_view()
        ),
        name="operation-qa-contextrecord",
    ),
    url(
        r"^contextrecord-qa-lock/(?P<pks>[0-9-]+)?/$",
        views.QAContextRecordLockView.as_view(),
        name="contextrecord-qa-lock",
        kwargs={"model": models.ContextRecord},
    ),
    url(
        r"^contextrecord-qa-duplicate/(?P<pks>[0-9-]+)?/$",
        check_rights(["change_contextrecord", "change_own_contextrecord"])(
            views.QAContextRecordDuplicateFormView.as_view()
        ),
        name="contextrecord-qa-duplicate",
    ),
    url(
        r"^contextrecord-qa-bulk-update/(?P<pks>[0-9-]+)?/$",
        check_rights(["change_contextrecord", "change_own_contextrecord"])(
            views.QAContextRecordForm.as_view()
        ),
        name="contextrecord-qa-bulk-update",
    ),
    url(
        r"^contextrecord-qa-bulk-update/(?P<pks>[0-9-]+)?/confirm/$",
        check_rights(["change_contextrecord", "change_own_contextrecord"])(
            views.QAContextRecordForm.as_view()
        ),
        name="contextrecord-qa-bulk-update-confirm",
        kwargs={"confirm": True},
    ),
    url(
        r"api/facets/contextrecord/$", views_api.FacetContextRecordAPIView.as_view(),
        name="api-facets-contextrecord"
    ),
    url(
        r"api/search/context-record/$", views_api.SearchContextRecordAPI.as_view(),
        name="api-search-contextrecord"
    ),
    path(
        "api/export/contextrecord/<slug:slug>/",
        views_api.ExportContextRecordAPI.as_view(),
        name="api-export-contextrecord"
    ),
    path(
        "api/get/contextrecord/<int:pk>/", views_api.GetContextRecordAPI.as_view(),
        name="api-get-contextrecord"
    ),
]
