#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2010-2016  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

import json

from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import redirect
from django.urls import reverse
from ishtar_common.utils import ugettext_lazy as _
from django.views.generic import RedirectView

from ishtar_common.models import QuickAction
from archaeological_operations.models import Operation
from archaeological_context_records import models

from archaeological_operations.views import site_extra_context
from archaeological_context_records import forms
from ishtar_common.utils import put_session_message

from ishtar_common.views import (
    IshtarMixin,
    LoginRequiredMixin,
    QAItemForm,
    QABaseLockView,
    wizard_is_available,
    QAItemEditForm,
)
from ishtar_common.views_item import display_item, get_item, show_item, revert_item
from archaeological_context_records import wizards

show_contextrecord = show_item(
    models.ContextRecord, "contextrecord", extra_dct=site_extra_context
)
revert_contextrecord = revert_item(models.ContextRecord)


def autocomplete_contextrecord(request):
    if not request.user.has_perm(
        "archaeological_context_records.view_contextrecord", models.ContextRecord
    ) and not request.user.has_perm(
        "archaeological_context_records.view_own_contextrecord",
        models.ArchaeologicalSite,
    ):
        return HttpResponse(content_type="text/plain")
    if not request.GET.get("term"):
        return HttpResponse(content_type="text/plain")
    q = request.GET.get("term")
    query = Q()
    if request.GET.get("operation__pk"):
        query = Q(operation__pk=request.GET.get("operation__pk"))
    for q in q.split(" "):
        qt = (
            Q(parcel__section__icontains=q)
            | Q(parcel__parcel_number__icontains=q)
            | Q(cached_label__icontains=q)
            | Q(label__icontains=q)
        )
        query = query & qt
    limit = 15
    items = models.ContextRecord.objects.filter(query).order_by(
        "parcel__section", "parcel__parcel_number", "label"
    )[:limit]
    data = json.dumps([{"id": item.pk, "value": str(item)[:60]} for item in items])
    return HttpResponse(data, content_type="text/plain")


get_contextrecord = get_item(
    models.ContextRecord,
    "get_contextrecord",
    "contextrecord",
    search_form=forms.RecordSelect,
)
get_contextrecord_for_ope = get_item(
    models.ContextRecord,
    "get_contextrecord",
    "contextrecord",
    own_table_cols=models.ContextRecord.TABLE_COLS_FOR_OPE,
)

get_contextrecordrelation = get_item(
    models.RecordRelationView,
    "get_contextrecordrelation",
    "contextrecordrelation",
    specific_perms=["view_recordrelation"],
    do_not_deduplicate=True,
    extra_request_keys=models.RecordRelationView.EXTRA_REQUEST_KEYS,
)

get_contextrecordrelationdetail = get_item(
    models.RecordRelations,
    "get_contextrecordrelationdetail",
    "contextrecordrelationdetail",
    specific_perms=["view_recordrelation"],
)

record_search_wizard = wizards.RecordSearch.as_view(
    [("general-record_search", forms.RecordFormSelection)],
    label=_("Context record search"),
    url_name="record_search",
)

record_creation_steps = [
    ("selec-record_creation", forms.OperationRecordFormSelection),
    ("general-record_creation", forms.RecordFormGeneral),
    ("datings-record_creation", forms.DatingFormSet),
    ("interpretation-record_creation", forms.RecordFormInterpretation),
    ("relations-record_creation", forms.RecordRelationsFormSet),
    ("final-record_creation", forms.FinalForm),
]

record_creation_wizard = wizards.RecordWizard.as_view(
    record_creation_steps,
    label=_("New context record"),
    url_name="record_creation",
)

record_modification_steps = [
    ("selec-record_modification", forms.RecordFormSelection),
    ("operation-record_modification", forms.OperationFormSelection),
    ("general-record_modification", forms.RecordFormGeneral),
    ("datings-record_modification", forms.DatingFormSet),
    ("interpretation-record_modification", forms.RecordFormInterpretation),
    ("relations-record_modification", forms.RecordRelationsFormSet),
    ("final-record_modification", forms.FinalForm),
]

record_modification_wizard = wizards.RecordModifWizard.as_view(
    record_modification_steps,
    label=_("Context record modification"),
    url_name="record_modification",
)


def record_modify(request, pk):
    if not wizard_is_available(
        record_modification_wizard, request, models.ContextRecord, pk
    ):
        return HttpResponseRedirect("/")

    wizards.RecordModifWizard.session_set_value(
        request, "selec-record_modification", "pk", pk, reset=True
    )
    return redirect(
        reverse("record_modification", kwargs={"step": "operation-record_modification"})
    )


record_deletion_wizard = wizards.RecordDeletionWizard.as_view(
    [
        ("selec-record_deletion", forms.RecordFormMultiSelection),
        ("final-record_deletion", forms.RecordDeletionForm),
    ],
    label=_("Context record deletion"),
    url_name="record_deletion",
)


def record_delete(request, pk):
    if not wizard_is_available(
        record_deletion_wizard, request, models.ContextRecord, pk
    ):
        return HttpResponseRedirect("/")

    wizards.RecordDeletionWizard.session_set_value(
        request, "selec-record_deletion", "pks", pk, reset=True
    )
    return redirect(
        reverse("record_deletion", kwargs={"step": "final-record_deletion"})
    )


def reset_wizards(request):
    for wizard_class, url_name in (
        (wizards.RecordWizard, "record_creation"),
        (wizards.RecordModifWizard, "record_modification"),
        (wizards.RecordDeletionWizard, "record_deletion"),
    ):
        wizard_class.session_reset(request, url_name)


class GenerateRelationImage(IshtarMixin, LoginRequiredMixin, RedirectView):
    upper_model = models.Operation
    model = models.ContextRecord

    def get_redirect_url(self, *args, **kwargs):
        if self.upper_item:
            return (
                reverse(
                    "display-item", args=[self.upper_model.SLUG, self.upper_item.pk]
                )
                + "#cr"
            )
        return (
            reverse("display-item", args=[self.model.SLUG, self.item.pk]) + "#relations"
        )

    def get(self, request, *args, **kwargs):
        self.upper_item = None
        render_type = kwargs.get("type", None)
        if render_type == "full" and self.upper_model:
            try:
                self.upper_item = self.upper_model.objects.get(pk=kwargs["pk"])
            except self.model.DoesNotExist:
                raise Http404()
            q = self.upper_item.context_record_relations_q()
            if not q.count():
                return super(GenerateRelationImage, self).get(request, *args, **kwargs)
            self.item = q.all()[0].left_record
        else:
            try:
                self.item = self.model.objects.get(pk=kwargs["pk"])
            except self.model.DoesNotExist:
                raise Http404()
        render_above, render_below, full = True, True, False
        if render_type == "above":
            render_below = False
        elif render_type == "below":
            render_above = False
        elif render_type == "full":
            full = True
        self.item.generate_relation_image(
            render_below=render_below, render_above=render_above, full=full
        )
        return super(GenerateRelationImage, self).get(request, *args, **kwargs)


class QAOperationContextRecordView(QAItemForm):
    template_name = "ishtar/forms/qa_operation_contextrecord.html"
    model = Operation
    form_class = forms.QAOperationCR
    page_name = _("Context record")

    def get_quick_action(self):
        return QuickAction(
            url="operation-qa-contextrecord",
            icon_class="fa fa-plus",
            text=_("Add context record"),
            target="one",
            rights=["add_contextrecord", "add_own_contextrecord"],
        )

    def form_valid(self, form):
        form.save(self.items)
        return HttpResponseRedirect(reverse("success"))


class QAContextRecordLockView(QABaseLockView):
    model = models.ContextRecord
    base_url = "contextrecord-qa-lock"


class QAContextRecordDuplicateFormView(QAItemForm):
    template_name = "ishtar/forms/qa_contextrecord_duplicate.html"
    model = models.ContextRecord
    page_name = _("Duplicate")
    form_class = forms.QAContextRecordDuplicateForm
    base_url = "contextrecord-qa-duplicate"

    def get_form_kwargs(self):
        kwargs = super(QAContextRecordDuplicateFormView, self).get_form_kwargs()
        kwargs["user"] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(reverse("success"))

    def get_context_data(self, **kwargs):
        data = super(QAContextRecordDuplicateFormView, self).get_context_data(**kwargs)
        data["action_name"] = _("Duplicate")
        data["operation"] = self.items[0].operation
        return data


class QAContextRecordForm(QAItemEditForm):
    model = models.ContextRecord
    form_class = forms.QAContextRecordFormMulti
