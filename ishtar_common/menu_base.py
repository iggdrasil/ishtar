#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2012-2013 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from ishtar_common.models import get_current_profile


class SectionItem:
    def __init__(self, idx, label, childs=None, profile_restriction=None, css=""):
        self.idx = idx
        self._label = label
        self.childs = childs or []
        self.available = False
        self.items = {}
        self.profile_restriction = profile_restriction
        self.css = css

    @property
    def label(self):
        if callable(self._label):
            return self._label()
        return self._label

    def check_profile_restriction(self):
        if self.profile_restriction:
            profile = get_current_profile()
            if not getattr(profile, self.profile_restriction):
                return False
        return True

    def can_be_available(self, user, session=None):
        if not self.check_profile_restriction():
            return False
        if not self.childs:
            return True
        for child in self.childs:
            if child.can_be_available(user, session=session):
                return True
        return False

    def is_available(self, user, obj=None, session=None):
        if not self.childs:
            return True
        for child in self.childs:
            if child.is_available(user, obj, session=session):
                return True
        return False

    def set_items(self, user, items, current_action=None, session=None):
        selected = None
        if user:
            self.available = self.can_be_available(user, session=session)
        for child in self.childs:
            selected = (
                child.set_items(user, items, current_action, session=session)
                or selected
            )
            items[child.idx] = child
        return selected


class MenuItem:
    def __init__(
        self,
        idx,
        label,
        model=None,
        access_controls=None,
        profile_restriction=None,
        css="",
    ):
        self.idx = idx
        self.label = label
        self.model = model
        self.access_controls = access_controls or []
        self.available = False
        self.profile_restriction = profile_restriction
        self.css = css
        if not self.check_profile_restriction():
            return False

    def check_profile_restriction(self):
        if self.profile_restriction:
            profile = get_current_profile()
            if not getattr(profile, self.profile_restriction):
                return False
        return True

    def can_be_available(self, user, session=None):
        if not self.check_profile_restriction():
            return False
        if not self.access_controls:
            return True
        if not hasattr(user, "ishtaruser"):
            return False
        for access_control in self.access_controls:
            # check by profile
            if user.ishtaruser.person.has_right(access_control, session=session):
                return True
        return False

    def is_available(self, user, obj=None, session=None):
        if not self.check_profile_restriction():
            return False
        if not self.access_controls:
            return True
        if not hasattr(user, "ishtaruser"):
            return False
        for access_control in self.access_controls:
            if user.ishtaruser.person.has_right(
                access_control, obj=obj, session=session
            ):
                return True
        return False

    def set_items(self, user, items, current_action=None, session=None):
        if user:
            self.available = self.can_be_available(user, session=session)
        if self.idx == current_action:
            return True
