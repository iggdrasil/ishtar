#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2010-2016 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.urls import path
from django.views.generic import TemplateView

from .menus import Menu

from ishtar_common import views, models, views_item
from ishtar_common.utils import check_rights, get_urls_for_model

# be careful: each check_rights must be relevant with ishtar_menu

# forms
urlpatterns = [
    url(r"^status/$", views.status, name="status"),
    url(r"^raise-error/$", views.raise_error, name="raise-error"),
    url(r"^raise-task-error/$", views.raise_task_error, name="raise-task-error"),
    url(r"^ty/(?P<url_id>[a-zA-Z0-9]+)$", views.tiny_redirect, name="tiny-redirect"),
    url(
        r"^robots\.txt$",
        TemplateView.as_view(template_name="robots.txt", content_type="text/plain"),
    ),
    # internationalization
    url(r"^i18n/", include("django.conf.urls.i18n")),
    url(r"^js/settings.js", views.settings_js, name="settings-js"),
    # General
    url(r"shortcut_menu/", views.shortcut_menu, name="shortcut-menu"),
    url(
        r"display/(?P<item_type>\w+)/(?P<pk>\d+)/",
        views.DisplayItemView.as_view(),
        name="display-item",
    ),
    url(r"qrcode/search/", views.QRCodeForSearchView.as_view(), name="search-qrcode"),
    url(
        r"qrcode/(?P<app>[-a-z]+)/(?P<model_name>[-a-z]+)/(?P<pk>\d+)/",
        views.QRCodeView.as_view(),
        name="qrcode-item",
    ),
    url(
        r"^generate-labels/(?P<template_slug>[-a-z0-9]+)/",
        views.GenerateLabelView.as_view(),
        name="generate-labels",
    ),
    url(
        r"^generate-document/(?P<template_slug>[-a-z0-9]+)/(" r"?P<item_pk>\d+)/",
        views.GenerateView.as_view(),
        name="generate-document",
    ),
    url(
        r"person_search/(?P<step>.+)?$",
        check_rights(["add_person"])(views.person_search_wizard),
        name="person_search",
    ),
    url(
        r"person_creation/(?P<step>.+)?$",
        check_rights(["add_person"])(views.person_creation_wizard),
        name="person_creation",
    ),
    url(
        r"person_modification/(?P<step>.+)?$",
        check_rights(["change_person", "change_own_person"])(
            views.person_modification_wizard
        ),
        name="person_modification",
    ),
    url(r"person_modify/(?P<pk>.+)/$", views.person_modify, name="person_modify"),
    url(
        r"person_deletion/(?P<step>.+)?$",
        check_rights(["change_person", "change_own_person"])(
            views.person_deletion_wizard
        ),
        name="person_deletion",
    ),
    url(r"person_delete/(?P<pk>.+)/$", views.person_delete, name="person_delete"),
    url(
        r"^person-edit/$",
        check_rights(["add_person"])(views.PersonCreate.as_view()),
        name="person_create",
    ),
    url(
        r"^person-edit/(?P<pk>\d+)$",
        check_rights(["change_person", "change_own_person"])(
            views.PersonEdit.as_view()
        ),
        name="person_edit",
    ),
    url(
        r"^person-qa-bulk-update/(?P<pks>[0-9-]+)?/$",
        check_rights(["change_person", "change_own_person"])(
            views.QAPersonForm.as_view()
        ),
        name="person-qa-bulk-update",
    ),
    url(
        r"^person-qa-bulk-update/(?P<pks>[0-9-]+)?/confirm/$",
        check_rights(["change_person", "change_own_person"])(
            views.QAPersonForm.as_view()
        ),
        name="person-qa-bulk-update-confirm",
        kwargs={"confirm": True},
    ),
    url(
        r"organization_search/(?P<step>.+)?$",
        check_rights(["add_organization"])(views.organization_search_wizard),
        name="organization_search",
    ),
    url(
        r"organization_creation/(?P<step>.+)?$",
        check_rights(["add_organization"])(views.organization_creation_wizard),
        name="organization_creation",
    ),
    url(
        r"organization_modification/(?P<step>.+)?$",
        check_rights(["change_organization", "change_own_organization"])(
            views.organization_modification_wizard
        ),
        name="organization_modification",
    ),
    url(
        r"organization_modify/(?P<pk>.+)/$",
        views.organization_modify,
        name="organization_modify",
    ),
    url(
        r"organization_deletion/(?P<step>.+)?$",
        check_rights(["change_organization", "change_own_organization"])(
            views.organization_deletion_wizard
        ),
        name="organization_deletion",
    ),
    url(
        r"organization_delete/(?P<pk>.+)/$",
        views.organization_delete,
        name="delete-organization",
    ),
    url(
        r"organization-edit/$",
        check_rights(["add_organization"])(views.OrganizationCreate.as_view()),
        name="organization_create",
    ),
    url(
        r"organization-edit/(?P<pk>\d+)$",
        check_rights(["change_organization", "change_own_organization"])(
            views.OrganizationEdit.as_view()
        ),
        name="organization_edit",
    ),
    url(
        r"organization-person-edit/$",
        check_rights(["add_organization"])(views.OrganizationPersonCreate.as_view()),
        name="organization_person_create",
    ),
    url(
        r"organization-person-edit/(?P<pk>\d+)$",
        check_rights(["change_organization", "change_own_organization"])(
            views.OrganizationPersonEdit.as_view()
        ),
        name="organization_person_edit",
    ),
    url(
        r"^organization-qa-bulk-update/(?P<pks>[0-9-]+)?/$",
        check_rights(["change_organization", "change_own_organization"])(
            views.QAOrganizationForm.as_view()
        ),
        name="organization-qa-bulk-update",
    ),
    url(
        r"^organization-qa-bulk-update/(?P<pks>[0-9-]+)?/confirm/$",
        check_rights(["change_organization", "change_own_organization"])(
            views.QAOrganizationForm.as_view()
        ),
        name="organization-qa-bulk-update-confirm",
        kwargs={"confirm": True},
    ),
    url(r"get-ishtaruser/(?P<type>.+)?$", views.get_ishtaruser, name="get-ishtaruser"),
    url(
        r"account_management/(?P<step>.+)?$",
        check_rights(["add_ishtaruser"])(views.account_management_wizard),
        name="account_management",
    ),
    url(
        r"account_manage/(?P<pk>\d+)$",
        views.account_manage,
        name="account-manage",
    ),
    url(
        r"account_deletion/(?P<step>.+)?$",
        check_rights(["add_ishtaruser"])(views.account_deletion_wizard),
        name="account_deletion",
    ),
    url(
        r"^import-new/$",
        check_rights(["change_import"])(views.NewImportView.as_view()),
        name="new_import",
    ),
    url(
        r"^import-edit/(?P<pk>[0-9]+)/$",
        check_rights(["change_import"])(views.EditImportView.as_view()),
        name="edit_import",
    ),
    url(
        r"^import-new-gis/$",
        check_rights(["change_import"])(views.NewImportGISView.as_view()),
        name="new_import_gis",
    ),
    url(
        r"^import-list/$",
        check_rights(["change_import"])(views.ImportListView.as_view()),
        name="current_imports",
    ),
    url(
        r"^import-list-table/$",
        check_rights(["change_import"])(views.ImportListTableView.as_view()),
        name="current_imports_table",
    ),
    url(
        r"^import-list-old/$",
        check_rights(["change_import"])(views.ImportOldListView.as_view()),
        name="old_imports",
    ),
    url(
        r"^import-delete/(?P<pk>[0-9]+)/$",
        views.ImportDeleteView.as_view(),
        name="import_delete",
    ),
    url(
        r"^import-link-unmatched/(?P<pk>[0-9]+)/$",
        views.ImportLinkView.as_view(),
        name="import_link_unmatched",
    ),
    url(
        r"^import-step-by-step/all/(?P<pk>[0-9]+)/(?P<line_number>[0-9]+)/$",
        views.ImportStepByStepView.as_view(),
        name="import_step_by_step_all",
        kwargs={"all_pages": True},
    ),
    url(
        r"^import-step-by-step/(?P<pk>[0-9]+)/(?P<line_number>[0-9]+)/$",
        views.ImportStepByStepView.as_view(),
        name="import_step_by_step",
    ),
    url(r"^profile(?:/(?P<pk>[0-9]+))?/$", views.ProfileEdit.as_view(), name="profile"),
    url(
        r"^save-search/(?P<app_label>[a-z-]+)/(?P<model>[a-z-]+)/$",
        views.SearchQueryEdit.as_view(),
        name="save-search-query",
    ),
    url(
        r"^bookmarks/(?P<app_label>[a-z-]+)/(?P<model>[a-z-]+)/$",
        views.BookmarkList.as_view(),
        name="bookmark-list",
    ),
    url(r"^bookmark/(?P<pk>[0-9]+)/$", views.get_bookmark, name="bookmark"),
    url(
        r"^bookmark/delete/(?P<pk>[0-9]+)/$",
        views.SearchQueryDelete.as_view(),
        name="bookmark-delete",
    ),
    url(r"^alerts/$", views.AlertList.as_view(), name="alert-list"),
    url(
        r"^success(?:/(?P<context>[a-z-]+)(?:/(?P<arg>[0-9a-z-|]+))?)?/$",
        TemplateView.as_view(template_name="ishtar/forms/success.html"),
        name="success",
    ),
]

menu = Menu(None)
menu.init()
actions = []
for section in menu.ref_childs:
    for menu_item in section.childs:
        if hasattr(menu_item, "childs"):
            for menu_subitem in menu_item.childs:
                actions.append(menu_subitem.idx)
        else:
            actions.append(menu_item.idx)
actions = r"|".join(actions)

# other views
urlpatterns += [
    # General
    url(
        r"update-current-item/$", views.update_current_item, name="update-current-item"
    ),
    url(
        r"pin/(?P<item_type>[a-z-]+)/(?P<pk>\d+)/$",
        views.update_current_item,
        name="pin",
    ),
    url(r"pin-search/(?P<item_type>[a-z-]+)/$", views.pin_search, name="pin-search"),
    url(r"unpin/(?P<item_type>[a-z-]+)/$", views.unpin, name="unpin"),
    url(
        r"get-by-importer/(?P<slug>[\w-]+)/(?P<type>[a-z-]+)?$",
        views.get_by_importer,
        name="get-by-importer",
    ),
    url(
        r"new-person/(?:(?P<parent_name>[^/]+)/)?(?:(?P<limits>[^/]+)/)?$",
        views.new_person,
        name="new-person",
    ),
    url(
        r"modify-person/(?:(?P<parent_name>[^/]+)/)?(?P<pk>[\d+]+)/$",
        views.modify_person,
        name="modify-person",
    ),
    url(r"detail-person/(?P<pk>[\d+]+)/$", views.detail_person, name="detail-person"),
    url(
        r"modify-organization/(?:(?P<parent_name>[^/]+)/)?(?P<pk>[\d+]+)/$",
        views.modify_organization,
        name="modify-organization",
    ),
    url(
        r"detail-organization/(?P<pk>[\d+]+)/$",
        views.detail_organization,
        name="detail-organization",
    ),
    url(
        r"new-person-noorga/" r"(?:(?P<parent_name>[^/]+)/)?(?:(?P<limits>[^/]+)/)?$",
        views.new_person_noorga,
        name="new-person-noorga",
    ),
    url(r"autocomplete-area/$", views.autocomplete_area, name="autocomplete-area"),
    url(r"autocomplete-user/$", views.autocomplete_user, name="autocomplete-user"),
    url(
        r"autocomplete-ishtaruser/$",
        views.autocomplete_ishtaruser,
        name="autocomplete-ishtaruser",
    ),
    url(
        r"autocomplete-person(?:/([0-9_]+))?(?:/([0-9_]*))?/(user)?$",
        views.autocomplete_person,
        name="autocomplete-person",
    ),
    url(
        r"autocomplete-person-permissive(?:/([0-9_]+))?(?:/([0-9_]*))?" r"/(user)?$",
        views.autocomplete_person_permissive,
        name="autocomplete-person-permissive",
    ),
    url(r"get-person/(?P<type>.+)?$", views.get_person, name="get-person"),
    url(
        r"get-person-full/(?P<type>.+)?$",
        views.get_person,
        name="get-person-full",
        kwargs={"full": True},
    ),
    url(
        r"get-person-for-account/(?P<type>.+)?$",
        views.get_person_for_account,
        name="get-person-for-account",
    ),
    url(
        r"show-person(?:/(?P<pk>.+))?/(?P<type>.+)?$",
        views.show_person,
        name="show-person",
    ),
    url(
        r"department-by-state/(?P<state_id>.+)?$",
        views.department_by_state,
        name="department-by-state",
    ),
    url(
        r"show-area(?:/(?P<pk>.+))?/(?P<type>.+)?$",
        views.show_area,
        name="show-area",
    ),
    url(
        r"show-town(?:/(?P<pk>.+))?/(?P<type>.+)?$",
        views.show_town,
        name="show-town",
    ),
    url(r"autocomplete-town/?$", views.autocomplete_town, name="autocomplete-town"),
    url(
        r"autocomplete-advanced-town/(?P<department_id>[0-9]+[ABab]?)?$",
        views.autocomplete_advanced_town,
        name="autocomplete-advanced-town",
    ),
    url(
        r"autocomplete-department/?$",
        views.autocomplete_department,
        name="autocomplete-department",
    ),
    url(
        r"new-author/(?:(?P<parent_name>[^/]+)/)?(?:(?P<limits>[^/]+)/)?$",
        views.new_author,
        name="new-author",
    ),
    url(
        r"autocomplete-author/$", views.autocomplete_author, name="autocomplete-author"
    ),
    url(
        r"new-organization/(?:(?P<parent_name>[^/]+)/)?" r"(?:(?P<limits>[^/]+)/)?$",
        views.new_organization,
        name="new-organization",
    ),
    url(
        r"get-organization/(?P<type>.+)?$",
        views.get_organization,
        name="get-organization",
    ),
    url(
        r"get-organization-full/(?P<type>.+)?$",
        views.get_organization,
        name="get-organization-full",
        kwargs={"full": True},
    ),
    url(
        r"show-organization(?:/(?P<pk>.+))?/(?P<type>.+)?$",
        views.show_organization,
        name="show-organization",
    ),
    url(
        r"autocomplete-organization/([0-9_]+)?$",
        views.autocomplete_organization,
        name="autocomplete-organization",
    ),
    url(r"changelog/(?:(?P<page>\d+)/)?", views.ChangelogView.as_view(), name="changelog"),
    url(r"person-merge/(?:(?P<page>\d+)/)?$", views.person_merge, name="person_merge"),
    url(
        r"person-manual-merge/$",
        views.PersonManualMerge.as_view(),
        name="person_manual_merge",
    ),
    url(
        r"person-manual-merge-items/(?P<pks>[0-9_]+?)/$",
        views.PersonManualMergeItems.as_view(),
        name="person_manual_merge_items",
    ),
    url(
        r"organization-merge/(?:(?P<page>\d+)/)?$",
        views.organization_merge,
        name="organization_merge",
    ),
    url(
        r"orga-manual-merge/$",
        views.OrgaManualMerge.as_view(),
        name="orga_manual_merge",
    ),
    url(
        r"orga-manual-merge-items/(?P<pks>[0-9_]+?)/$",
        views.OrgaManualMergeItems.as_view(),
        name="orga_manual_merge_items",
    ),
    url(r"reset/$", views.reset_wizards, name="reset_wizards"),
    url(
        r"activate-all-search/$", views.activate_all_search, name="activate-all-search"
    ),
    url(
        r"activate-own-search/$", views.activate_own_search, name="activate-own-search"
    ),
    url(
        r"activate-advanced-menu/$",
        views.activate_advanced_shortcut_menu,
        name="activate-advanced-menu",
    ),
    url(
        r"activate-simple-menu/$",
        views.activate_simple_shortcut_menu,
        name="activate-simple-menu",
    ),
    url(r"hide-shortcut-menu/$", views.hide_shortcut_menu, name="hide-shortcut-menu"),
    url(r"show-shortcut-menu/$", views.show_shortcut_menu, name="show-shortcut-menu"),
    url(
        r"regenerate-external-id/$",
        views.regenerate_external_id,
        name="regenerate-external-id",
    ),
    url(
        r"document/search/(?P<step>.+)?$",
        check_rights(["view_document", "view_own_document"])(
            views.document_search_wizard
        ),
        name="search-document",
    ),
    url(
        r"document/search/(?P<step>.+)?$",
        check_rights(["view_document", "view_own_document"])(
            views.document_search_wizard
        ),
        name="document_search",
    ),
    url(
        r"document/create/$",
        check_rights(["add_document", "add_own_document"])(
            views.DocumentCreateView.as_view()
        ),
        name="create-document",
    ),
    url(
        r"document/edit/$",
        check_rights(["change_document", "change_own_document"])(
            views.DocumentSelectView.as_view()
        ),
        name="edit-document",
    ),
    url(
        r"document/edit/(?P<pk>.+)/$",
        check_rights(["change_document", "change_own_document"])(
            views.DocumentEditView.as_view()
        ),
        name="edit-document",
    ),
    url(
        r"document/delete/(?P<step>.+)?$",
        check_rights(["change_document", "change_own_document"])(
            views.document_deletion_wizard
        ),
        name="document_deletion",
    ),
    url(
        r"autocomplete-document/$",
        views.autocomplete_document,
        name="autocomplete-document",
    ),
    url(
        r"document/shortcut/delete/(?P<pk>.+)/$",
        views.document_delete,
        name="delete-document",
    ),
    url(
        r"^document-qa-bulk-update/(?P<pks>[0-9-]+)?/$",
        check_rights(["change_document", "change_own_document"])(
            views.QADocumentForm.as_view()
        ),
        name="document-qa-bulk-update",
    ),
    url(
        r"^document-qa-bulk-update/(?P<pks>[0-9-]+)?/confirm/$",
        check_rights(["change_document", "change_own_document"])(
            views.QADocumentForm.as_view()
        ),
        name="document-qa-bulk-update-confirm",
        kwargs={"confirm": True},
    ),
    url(
        r"^document-qa-duplicate/(?P<pks>[0-9-]+)?/$",
        check_rights(["change_document", "change_own_document"])(
            views.QADocumentDuplicateFormView.as_view()
        ),
        name="document-qa-duplicate",
    ),
    url(
        r"^document-qa-packaging/(?P<pks>[0-9-]+)?/$",
        check_rights(["change_document", "change_own_document"])(
            views.QADocumentPackagingFormView.as_view()
        ),
        name="document-qa-packaging",
    ),
    url(
        r"autocomplete-documenttag/$",
        views.autocomplete_documenttag,
        name="autocomplete-documenttag",
    ),
    url(
        r"new-documenttag/(?:(?P<parent_name>[^/]+)/)?" r"(?:(?P<limits>[^/]+)/)?$",
        views.new_document_tag,
        name="new-documenttag",
    ),
    url(
        r"geo/create/(?P<app_source>[-\w]+)/(?P<model_source>[-\w]+)/(?P<source_pk>\d+)/$",
        check_rights(["add_geovectordata", "add_own_geovectordata"])(
            views.GeoPreCreateView.as_view()
        ),
        name="create-pre-geo",
    ),
    url(
        r"geo/create/(?P<app_source>[-\w]+)/(?P<model_source>[-\w]+)/(?P<source_pk>\d+)/(?P<geom_type>[-\w]+)/$",
        check_rights(["add_geovectordata", "add_own_geovectordata"])(
            views.GeoCreateView.as_view()
        ),
        name="create-geo",
    ),
    url(
        r"geo/edit/(?P<pk>\d+)/$",
        check_rights(["change_geovectordata", "change_own_geovectordata"])(
            views.GeoEditView.as_view()
        ),
        name="edit-geo",
    ),
    url(
        r"geo/delete/(?P<pk>\d+)/$",
        check_rights(["change_geovectordata", "change_own_geovectordata"])(
            views.GeoDeleteView.as_view()
        ),
        name="delete-geo",
    ),
    url(
        r"^qa-not-available(?:/(?P<context>[0-9a-z-]+))?/$",
        views.QANotAvailable.as_view(),
        name="qa-not-available",
    ),
    path(
        "external-search/<slug:model>/<int:external_source_id>/",
        views_item.get_distant_item,
        name="external-search"
    ),
    path(
        "external-search/<slug:model>/<int:external_source_id>/<slug:data_type>",
        views_item.get_distant_item,
        name="external-search"
    ),
    path(
        "external-export/<int:source_id>/<slug:model_name>/<slug:slug>/",
        views_item.external_export,
        name="external-export"
    ),
]

urlpatterns += get_urls_for_model(models.Document, views, own=True)

urlpatterns += [
    url(r"(?P<action_slug>" + actions + r")/$", views.action, name="action"),
]


if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
