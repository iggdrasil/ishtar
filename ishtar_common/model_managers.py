#!/usr/bin/env python
# -*- coding: utf-8 -*-


from django.contrib.gis.db.models import Manager


class ExternalIdManager(Manager):
    def get_by_natural_key(self, external_id):
        return self.get(external_id=external_id)


class TypeManager(Manager):
    def get_by_natural_key(self, txt_idx):
        return self.get(txt_idx=txt_idx)


class SlugModelManager(Manager):
    def get_by_natural_key(self, slug):
        return self.get(slug=slug)


class UUIDModelManager(Manager):
    def get_by_natural_key(self, uuid):
        return self.get(uuid=uuid)
