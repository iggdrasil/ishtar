from django.conf.urls import url
from django.urls import path
from django.views.generic.base import TemplateView

try:
    from registration import views as registration_views  # debian
except ModuleNotFoundError:
    from django_registration import views as registration_views  # pip

from django.contrib.auth import views as auth_views

from ishtar_common import views

urlpatterns = [
    url(r'^accounts/activate/complete/$',
        TemplateView.as_view(
            template_name='registration/activation_complete.html'
        ),
        name='registration_activation_complete'),
    # Activation keys get matched by \w+ instead of the more specific
    # [a-fA-F0-9]{40} because a bad activation key should still get to
    # the view; that way it can return a sensible "invalid key"
    # message instead of a confusing 404.
    url(r'^accounts/activate/(?P<activation_key>\w+)/$',
        registration_views.ActivationView.as_view(),
        name='registration_activate'),
    url(r'^accounts/register/$',
        views.RegistrationView.as_view(),
        name='registration_register'),
    url(r'^accounts/register/complete/$',
        TemplateView.as_view(
            template_name='registration/registration_complete.html'
        ),
        name='registration_complete'),
    url(r'^accounts/register/closed/$',
        TemplateView.as_view(
            template_name='registration/registration_closed.html'
        ),
        name='registration_disallowed'),
    # url("^accounts/", include('django.contrib.auth.urls')),
    path('accounts/login/', views.LoginView.as_view(), name='login'),
    path('accounts/logout/', views.LogoutView.as_view(), name='logout'),
    path('accounts/password_change/', views.PasswordChangeView.as_view(),
         name='password_change'),
    path('accounts/password_reset/', views.PasswordResetView.as_view(), name='password_reset'),
    path('accounts/reset/<uidb64>/<token>/', views.PasswordResetConfirmView.as_view(),
         name='password_reset_confirm'),
]