#!/usr/bin/env python
# -*- coding: utf-8 -*-


class ConfigDrassm(object):
    LABEL = "DRASSM"

    @classmethod
    def find_administrative_index(cls, find):
        return find.external_id

    @classmethod
    def basefind_complete_id(cls, basefind):
        return basefind.external_id

    @classmethod
    def basefind_short_id(cls, basefind):
        return basefind.external_id


ALTERNATE_CONFIGS = {"DRASSM": ConfigDrassm}

ALTERNATE_CONFIGS_CHOICES = [
    (k, choice.LABEL) for k, choice in ALTERNATE_CONFIGS.items()
]
