{% load replace_underscore %}
var base_source_{{field_id|replace_underscore}} = {{source}};
var source_{{field_id|replace_underscore}} = base_source_{{field_id|replace_underscore}};
var current_label_{{field_id|replace_underscore}};

$(function() {
    $("#id_select_{{field_id}}").autocomplete({
        source: source_{{field_id|replace_underscore}},
        select: function( event, ui ) {
            if(ui.item){
                $('#id_{{field_id}}').val(ui.item.id);
				current_label_{{field_id|replace_underscore}} = ui.item.label;
                $('#id_{{field_id}}').change();
            } else {
                $('#id_{{field_id}}').val(null);
                $('#id_{{field_id}}').change();
            }
        },
        minLength: 2{% if options %},
        {{options}}
        {% endif %}
    });
    {% if autofocus %}autofocus_field = "#id_select_{{field_id}}";
    {% endif %}

    $(document).on("click", '#id_select_{{field_id}}', function(){
        $('#id_{{field_id}}').val(null);
        $('#id_select_{{field_id}}').val(null);
        $('#id_{{field_id}}-modify').addClass("disabled");
		$('#id_{{field_id}}').change();
    });

    $(document).on("click", '#id_{{field_id}}_previous_button', function(){
        $('#id_{{field_id}}').val($('#id_{{field_id}}_previous').val());
        $('#id_select_{{field_id}}').val(
            $('#id_{{field_id}}_previous_label').val()
            );
        $('#id_{{field_id}}').change();
    });

    {% if dynamic_limit %}{% for item_id in dynamic_limit %}
    $('#{{item_id}}').change(function(){
        $("#id_select_{{field_id}}").autocomplete( "option", "source",
            base_source_{{field_id|replace_underscore}} + $('#{{item_id}}').val()
            + '/');
        if ($('#{{item_id}}').val()){
            $("#id_select_{{field_id}}").prop("disabled", false);
        } else {
            $("#id_select_{{field_id}}").prop("disabled", true);
        }
    });
    $('#{{item_id}}').change();
    {% endfor %}{% endif %}

	{% if modify %}
	{{safe_field_id}}_modify = function(){
        var current_val = $('#id_{{field_id}}').val();
        if (current_val){
			dt_qa_open('/modify-{{model_name}}/{{field_id}}/' + current_val + "/",
					   'modal-dynamic-form-{{model_name}}');
	    }
	}
	{% endif %}
    $('#id_{{field_id}}').change(function(){
        $("#id_select_{{field_id}}").attr('title', $('#id_select_{{field_id}}').val());
        var current_val = $('#id_{{field_id}}').val();
		{% if modify %}
        if (current_val){
        	$('#id_{{field_id}}-modify').removeClass("disabled");
        }
        {% endif %}
        {% if detail %}
        if (current_val){
        	var detail_url = "{{detail}}" + current_val + "/";
	        $.get(detail_url, function(data) {
				$("#id_{{field_id}}-detail").html(data);
			});
        } else {
			$("#id_{{field_id}}-detail").html("...");
        }
        {% endif %}

    });

    $('#id_{{field_id}}').change();
});
