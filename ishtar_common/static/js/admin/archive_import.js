ARCHIVE_IMPORT_SUBMIT_MESSAGE = "continue submitting ?";

django.jQuery(document).ready(function(){
    django.jQuery('form').submit(function() {
    	var action_value = django.jQuery("select[name='action']").val();
    	if (action_value && action_value != "launch_import_action") return True;
        var c = confirm(ARCHIVE_IMPORT_SUBMIT_MESSAGE);
        return c;
    });
})
