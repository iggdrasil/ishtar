/* CSRFToken management */

$.ajaxSetup({
beforeSend: function(xhr, settings) {
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
        // Only send the token to relative URLs i.e. locally.
        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
    }
}});

if (typeof String.prototype.endsWith !== 'function') {
    String.prototype.endsWith = function(suffix) {
        return this.indexOf(suffix, this.length - suffix.length) !== -1;
    };
}

if (typeof String.prototype.format !== 'function') {
    String.prototype.format = function() {
        var formatted = this;
        for (var arg in arguments) {
            formatted = formatted.replace("{" + arg + "}", arguments[arg]);
        }
        return formatted;
    };
}

$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null) {
       return null;
    }
    return decodeURI(results[1]) || 0;
}

function manage_async_link(event){
    event.preventDefault();
    var url = $(this).attr('href');
    var target = $(this).attr('data-target');
    $.get(url, function(data) {
        $(target).html(data);
    });
    var modal_open = $(this).attr('data-modal-open');
    if (modal_open) $(modal_open).modal('show');
}

text_truncate = function(str, length) {
    var ending = "...";
    if (str.length > length) {
        return str.substring(0, length - ending.length) + ending;
    } else {
        return str;
    }
};

/* default function to prevent undefined */
function get_next_table_id(){}
function get_previous_table_id(){}

var datatable_submit_search = function() {};

var current_tab = "table";
var shortcut_url = '';
var show_shortcut_menu = false;
var alert_url = '';
var debug = false;
var datatables_i18n;
var current_modal;
var default_search_vector;
var pin_search_url;
var static_path = '/static/';
var autofocus_field;
var form_changed = false;

var datatables_default = {
    "processing": true,
    "serverSide": true,
    "scrollX": true,
    "scrollY": true,
    "searching": false,
    "scrollCollapse": true,
    "pageLength": 10,
    "lengthMenu": [ 5, 10, 25, 50, 100 ]
};

var datatables_static_default = {
    "searching": false,
    "scrollCollapse": true,
    "pageLength": 10,
    "lengthMenu": [ 5, 10, 25, 50, 100 ]
};

var show_msg = "Show";
var info_show_msg = "Showing"
var entries_msg = "entries";
var info_entries_msg = "entrie(s)";
var table_to_msg = "to";
var table_of_msg = "of";
var no_data_gallery_msg = "No data available in the gallery";
var activate_all_search_msg = "Searches in the shortcut menu deals with all items.";
var activate_own_search_msg = "Searches in the shortcut menu deals with only your items.";
var added_message = " items added.";
var select_only_one_msg = "Select only one item.";
var search_pinned_msg = "";
var form_changed_msg = "The form has changed. If you don't validate it, all your changes will be lost.";

var advanced_menu = false;
var shortcut_menu_hide = false;
var activate_all_search_url = '/activate-all-search/';
var activate_own_search_url = '/activate-own-search/';
var activate_advanced_url = '/activate-advanced-menu/';
var activate_simple_url = '/activate-simple-menu/';
var shortcut_menu_hide_url = '/hide-shortcut-menu/'
var shortcut_menu_show_url = '/show-shortcut-menu/'

function init_shortcut_menu(html){
    close_wait();
    $("#context-menu").html(html);
    $("#shortcut-menu .chosen-select").chosen();
    $("#shortcut-menu .chosen-container").css('width', '100%');
    if (advanced_menu) {
        init_advanced_shortcut_fields();
    } else {
        init_shortcut_fields();
    }
    $("#short-menu-advanced").click(function(){
        $.get(url_path + activate_advanced_url,
               load_opened_shortcut_menu
        );
    });
    $("#short-menu-simple").click(function(){
        $.get(url_path + activate_simple_url,
               load_opened_shortcut_menu
        );
    });

    /*
    $(".short-menu-close").click(function(){
        $('#shortcut-menu div').hide();
        $('#shortcut-menu table').hide();
        $(".short-menu-close").hide();
        $(".short-menu-open").show();
        $.get(shortcut_menu_hide_url);
    });
    $(".short-menu-open").click(function(){
        $('#shortcut-menu div').show();
        $('#shortcut-menu table').show();
        $(".short-menu-open").hide();
        $(".short-menu-close").show();
        $.get(shortcut_menu_show_url);
    });
    if (shortcut_menu_hide){
        $('#shortcut-menu div').hide();
        $('#shortcut-menu table').hide();
        $(".short-menu-close").hide();
        $(".short-menu-open").show();
    }
    */
}

function init_shortcut_fields(){
    $("#current_file").change(function(){
        $.post('/' + url_path + 'update-current-item/',
               {item:'file', value:$("#current_file").val()},
               load_opened_shortcut_menu
        );
    });
    $("#current_operation").change(function(){
        $.post('/' + url_path + 'update-current-item/',
               {item:'operation', value:$("#current_operation").val()},
               load_opened_shortcut_menu
        );
    });
    $("#current_site").change(function(){
        $.post('/' + url_path + 'update-current-item/',
               {item:'site', value:$("#current_site").val()},
               load_opened_shortcut_menu
        );
    });
    $("#current_contextrecord").change(function(){
        $.post('/' + url_path + 'update-current-item/',
               {item:'contextrecord', value:$("#current_contextrecord").val()},
               load_opened_shortcut_menu
        );
    });
    $("#current_find").change(function(){
        $.post('/' + url_path + 'update-current-item/',
               {item:'find', value:$("#current_find").val()},
               load_opened_shortcut_menu
        );
    });
    $("#current_warehouse").change(function(){
        $.post('/' + url_path + 'update-current-item/',
               {item:'warehouse', value:$("#current_warehouse").val()},
               load_opened_shortcut_menu
        );
    });
    $("#current_treatment").change(function(){
        $.post('/' + url_path + 'update-current-item/',
               {item:'treatment', value:$("#current_treatment").val()},
               load_opened_shortcut_menu
        );
    });
    $("#current_treatmentfile").change(function(){
        $.post('/' + url_path + 'update-current-item/',
               {item:'treatmentfile', value:$("#current_treatmentfile").val()},
               load_opened_shortcut_menu
        );
    });
}

function init_advanced_shortcut_fields(){
    $('#id_file-shortcut').change(function(){
        $("#id_select_file-shortcut").attr(
            'title', $('#id_select_file-shortcut').val());
        $.post('/' + url_path + 'update-current-item/',
               {item: "file", value:$("#id_file-shortcut").val()},
               load_opened_shortcut_menu
        );
    });
    $('#id_operation-shortcut').change(function(){
        $("#id_select_operation-shortcut").attr(
            'title', $('#id_select_operation-shortcut').val());
        $.post('/' + url_path + 'update-current-item/',
               {item: "operation", value:$("#id_operation-shortcut").val()},
               load_opened_shortcut_menu
        );
    });
    $('#id_site-shortcut').change(function(){
        $("#id_select_site-shortcut").attr(
            'title', $('#id_select_site-shortcut').val());
        $.post('/' + url_path + 'update-current-item/',
               {item: "site", value:$("#id_site-shortcut").val()},
               load_opened_shortcut_menu
        );
    });
    $('#id_contextrecord-shortcut').change(function(){
        $("#id_select_contextrecord-shortcut").attr(
            'title', $('#id_select_contextrecord-shortcut').val());
        $.post('/' + url_path + 'update-current-item/',
               {item: "contextrecord", value:$("#id_contextrecord-shortcut").val()},
               load_opened_shortcut_menu
        );
    });
    $('#id_find-shortcut').change(function(){
        $("#id_select_find-shortcut").attr(
            'title', $('#id_select_find-shortcut').val());
        $.post('/' + url_path + 'update-current-item/',
               {item: "find", value:$("#id_find-shortcut").val()},
               load_opened_shortcut_menu
        );
    });
    $('#id_warehouse-shortcut').change(function(){
        $("#id_select_warehouse-shortcut").attr(
            'title', $('#id_select_warehouse-shortcut').val());
        $.post('/' + url_path + 'update-current-item/',
               {item: "warehouse", value:$("#id_warehouse-shortcut").val()},
               load_opened_shortcut_menu
        );
    });
    $('#id_treatment-shortcut').change(function(){
        $("#id_select_treatment-shortcut").attr(
            'title', $('#id_select_treatment-shortcut').val());
        $.post('/' + url_path + 'update-current-item/',
               {item: "treatment", value:$("#id_treatment-shortcut").val()},
               load_opened_shortcut_menu
        );
    });
    $('#id_treatmentfile-shortcut').change(function(){
        $("#id_select_treatmentfile-shortcut").attr(
            'title', $('#id_select_treatmentfile-shortcut').val());
        $.post('/' + url_path + 'update-current-item/',
               {item: "treatmentfile", value:$("#id_treatmentfile-shortcut").val()},
               load_opened_shortcut_menu
        );
    });
}

function display_info(msg){
    $('#message .information .content').html(msg);
    $('#message').fadeIn('slow');
    $('#message .information').fadeIn('slow');
    setTimeout(
        function(){
            $('#message .information').fadeOut('slow');
            $('#message').fadeOut('slow');
        }, 5000);
}

function add_message(message, message_type, target, clear, dismiss){
    if (!message_type) message_type = 'info';
    if (!target) target = "#message_list";
    if (typeof dismiss == "undefined") dismiss = true;

    var html = "";
    if (!clear) html = $(target).html();
    html += '<div class="alert alert-' + message_type + ' alert-dismissible fade show"';
    html += '   role="alert">';
    html += message;
    if (dismiss){
        html += '     <button type="button" class="close" data-dismiss="alert"';
        html += '             aria-label="Close">';
        html += '         <span aria-hidden="true">&times;</span>';
        html += '     </button>';
    }
    html += ' </div>';
    $(target).html(html);
}

function load_opened_shortcut_menu(){
    load_shortcut_menu(true);
}

function load_shortcut_menu(opened){
    if (show_shortcut_menu && shortcut_url){
        $('.modal-progress').modal('show');
        $.ajax({
            url: shortcut_url,
            cache: false,
            success:function(html){
                init_shortcut_menu(html);
                load_alerts();
                if(opened){
                    $("#dropdown-toggle-shortcut-menu").click();
                }
            },
            error:function(XMLHttpRequest, textStatus, errorThrows){
                close_wait();
            }
        });
    } else {
        load_alerts();
    }
}

var load_alerts = function(){
    if (!alert_url) return;
    $('.modal-progress').modal('show');
    $.ajax({
        url: alert_url,
        cache: false,
        success:function(json){
            $('.modal-progress').modal('hide');
            var html = "";
            for (idx in json["alerts"]){
                var b = json["alerts"][idx];
                var url = "/bookmark/" + b["query_id"] + "/" ;
                html += '<a class="badge badge-secondary" href="' + url + '">';
                html += b["label"];
                html += ' <span class="badge badge-light">';
                html += b["number"];
                html += '</span>';
                html += '</a> ';
            }
            $("#alert-list").html(html);
            if (autofocus_field) {
                $(autofocus_field).focus();
                autofocus_field = null;
            }
        },
        error:function(XMLHttpRequest, textStatus, errorThrows){
            close_wait();
        },
        dataType: 'json'
    });
}

function dynamic_load(url, target){
    $.ajax({
        url: url,
        success: function(data, textStatus, jqXHR) {
            $(target).html(jqXHR.responseText);
            var response = $(jqXHR.responseText);
            var responseScript = response.find("script");
            $.each(responseScript, function(idx, val) {
                eval(val.text);
            } );
            responseScript = response.filter("script");
            $.each(responseScript, function(idx, val) {
                eval(val.text);
            } );
   }
    });
}

$(document).ready(function(){
    $("#main_menu > ul > li > ul").hide();
    $("#main_menu ul ul .selected").parents().show();
    var items = new Array('file', 'operation');
    if ($(document).height() < 1.5*$(window).height()){
        $('#to_bottom_arrow').hide();
        $('#to_top_arrow').hide();
    }
    $('#language-selector a').click(function(){
        $('#language-selector input[name="language"]').val(
            $(this).attr("data-lang"));
        $('#language-form').submit();
    });
    load_shortcut_menu();
    $('#current_items select').change(function(){
        $(this).attr(
            'class',
            $(this).children("option:selected").attr('class')
        );
    });
    $("a.async-link").click(manage_async_link);
    $(".chosen-select").chosen();
    $("#clear-search-button").click(function(){
        $(this).parent().parent().children('input').prop("value", "");
        $("#id_search_vector").addClass('input-progress');
        enable_save();
    });

    $("#pin-search-button").click(function(){
        if (!pin_search_url){
            return;
        }
        var url = pin_search_url.replace(
            'item', $(this).attr('data-item-type')
        );
        var current_search =
            $(this).parent().parent().children('input').prop("value");

        ajax_post(
            url, {'value': current_search}, "",
            function(){
                display_info(search_pinned_msg);
            }
        );
    });

    $("#submit-search").click(function(){
        $(".search_button").click();
    });
    register_wait_button();
});

var register_wait_button = function(){
    $(".wait-button").click(function(){short_wait()});
};

$(document).on("click", '#to_bottom_arrow', function(){
  $("html, body").animate({ scrollTop: $(document).height() }, 1000);
});

$(document).on("click", '#to_top_arrow', function(){
  $("html, body").animate({ scrollTop: 0}, 1000);
});

var bookmark_url = "";
var bookmark_delete_url = "/bookmark/delete/";

var load_bookmark_list = function(){
    if (!bookmark_url) return;
    $.get(bookmark_url, function(data) {
        var bookmark_list = "";
        for (idx in data['bookmarks']){
            var bookmark = data['bookmarks'][idx];
            bookmark_list += '<span class="dropdown-item d-flex">' +
                '<a data-query="' + bookmark['query'].replace(/"/g, "''") +
                '" href="#" class="flex-grow-1 input-link">' + bookmark['label'] +
                '</a>' +
                '<a class="close" href="#" data-id="' + bookmark['id'] + '">' +
                '<i class="fa fa-times text-danger" aria-hidden="true"></i>' +
                '</a></span>';
        }
        $('#bookmark-list').html(bookmark_list);
        if (!bookmark_list){
            $('#load-bookmark').addClass('disabled');
        } else {
            $('#load-bookmark').removeClass('disabled');
        }
        $("#bookmark-list span a.input-link").click(function(){
            $("#id_search_vector").val(
                $(this).attr('data-query').replace(/''/g, '"'));
            enable_save();
            $(".search_button").click();
            return false;
        });
        $("#bookmark-list span a.close").click(function(){
            var id = $(this).attr('data-id');
            dt_qa_open(bookmark_delete_url + id + '/');
            return false;
        });
    }, 'json');
}


var autorefresh = false;
var autorefresh_message_start = "";
var autorefresh_message_end = "";

function startRefresh() {
    if (!autorefresh) return;
    autorefresh_link = $('#autorefreshpage').attr("data-link");
    autorefresh_div_id =  $('#autorefreshpage').attr("data-div");
    $.get(autorefresh_link, function(data) {
        $('#' + autorefresh_div_id).html(data);
    });
    setTimeout(startRefresh, 10000);
}

$(document).on("click", '#autorefreshpage', function(){
    autorefresh_div_id =  $('#autorefreshpage').attr("data-div");
    if (!$(this).hasClass('disabled')) {
        $(this).addClass('disabled');
        autorefresh = false;
        display_info(autorefresh_message_end);
        $("#" + autorefresh_div_id).find('select').prop('disabled', false);
        $("#" + autorefresh_div_id).find('input').prop('disabled', false);
    } else {
        $(this).removeClass('disabled');
        autorefresh = true;
        setTimeout(startRefresh, 10000);
        display_info(autorefresh_message_start);
        $("#" + autorefresh_div_id).find('select').prop('disabled', true);
        $("#" + autorefresh_div_id).find('input').prop('disabled', true);

    }
});

$(document).on("click", '.check-all', function(){
  $(this).closest('table'
        ).find('input:checkbox'
        ).attr('checked', $(this).is(':checked'));
});

$(document).on("click", '#main_menu > ul > li', function(){
    var current_id = $(this).attr('id');
    $("#main_menu ul ul").not($(this).parents('ul')).not($(this).find('ul')
                        ).hide('slow');
    $(this).find('ul').show('slow');
});

/* manage help texts */
$(document).on("click", '.help_display', function(){
    var help_text_id = $(this).attr("href") + "_help";
    $(help_text_id).toggle();
});

$(document).on("click", '#progress-content', function(){
    $('.modal-progress').hide();
});


var closed_wait = true;

function short_wait(check_wait){
    if (check_wait && closed_wait){
        return
    }
    closed_wait = false;
    $('.modal-progress').modal('show');
    $('.progress-1').show('slow');
}

function long_wait(check_wait){
    if (check_wait && closed_wait){
        return
    }
    closed_wait = false;
    $('.modal-progress').modal('show');
    $('.progress-1').show('slow');
    setTimeout(function(){
        $('.progress-1').hide('slow');
        $('.progress-2').show('slow');
    }, 60000);
    setTimeout(function(){
        $('.progress-2').hide('slow');
        $('.progress-3').show('slow');
    }, 120000);
    setTimeout(function(){
        $('.progress-3').hide('slow');
        $('.progress-4').show('slow');
    }, 180000);
    setTimeout(function(){
        $('.progress-4').hide('slow');
        long_wait(true);
    }, 240000);
}

function close_wait(){
    closed_wait = true;
    $('.modal-progress').modal('hide');
    if (current_modal){
        $('body').addClass('modal-open');
    }
}

var last_window;

function load_window(url, speed, on_success, no_jump){
    $('.modal-progress').modal('show');
    $.ajax({
        url: url,
        cache: false,
        success:function(html){
            close_wait();
            $(".previous_page").remove();
            $(".next_page").remove();
            var register_id = $(html).find(".card-header").attr("data-sheet-id");
            var current_sheet = $('[data-sheet-id="' + register_id + '"]');
            if (current_sheet.length){
                current_sheet.parent().remove();
            }
            var current_id = $(html).find(".card-header").parent().attr("id");
            var current_label = $(html).find(".card-label").html();
            var new_sheet_list = new Array();
            for (idx in sheet_list){
                if (sheet_list[idx]['register_id'] != register_id){
                    new_sheet_list.push(sheet_list[idx]);
                }
            }
            sheet_list = new_sheet_list;
            sheet_list.push({'id': current_id, 'label': current_label,
                             'register_id': register_id});
            $("#window").append(html);
            $("#" + last_window).show();
            update_window_menu();
            $(".lg-close").click(); // close a potential lightgallery view
            if (!no_jump){
                // jump to this window
                var url = location.href;
                location.href = "#" + last_window;
                history.replaceState(null, null, url);
            }
            register_wait_button();
            if (on_success) on_success();
        },
        error:function(XMLHttpRequest, textStatus, errorThrows){
            close_wait();
        }
    });
}

function load_current_window(url, model_name){
    var id;
    if (advanced_menu){
        id = $("#id_" + model_name + "-shortcut").val();
    } else {
        id = $("#current_" + model_name).val();
    }
    if (!id) return;
    url = url.split('/');
    url[url.length - 1] = id;
    url.push('');
    return load_window(url.join('/'));
}

function hide_window(window_id){
    $("#" + window_id).remove();
    splitted = window_id.split('-');
    register_id = "";
    for (idx=0 ; idx < splitted.length - 1 ; idx++){
        if (idx > 0) register_id += "-";
        register_id += splitted[idx]
    }
    var new_sheet_list = new Array();
    for (idx in sheet_list){
        if (sheet_list[idx]['register_id'] != register_id){
            new_sheet_list.push(sheet_list[idx]);
        }
    }
    sheet_list = new_sheet_list;
    update_window_menu();
}

function load_url(url, callback){
    $('.modal-progress').modal('show');
    $.ajax({
        url: url,
        cache: false,
        success:function(html){
            close_wait();
            if (callback) callback();
        },
        error:function(XMLHttpRequest, textStatus, errorThrows){
            close_wait();
        }
    });
}

function open_window(url){
    var newwindow = window.open(
        url, '_blank', 'height=500,width=600,scrollbars=yes');
    if (window.focus) {newwindow.focus()}
    return false;
}

function save_and_close_window(name_label, name_pk, item_name, item_pk){
    var main_page = opener.document;
    save_and_close_window_data(main_page, name_label,
                               name_pk, item_name, item_pk);
    opener.focus();
    self.close();
}

function save_and_close_window_data(main_page, name_label,
                                    name_pk, item_name, item_pk){
    var cur_item;
    var cur_item_pk;
    var id_item;

    if (main_page){
        cur_item = $(main_page).find("#" + name_label);
        cur_item_pk = $(main_page).find("#" + name_pk);
        id_item = $(main_page).find("#id_" + name_label);
    } else {
        cur_item = $("#" + name_label);
        cur_item_pk = $("#" + name_pk);
        id_item = $("#id_" + name_label);
    }
    if (name_label != name_pk){  // custom
		cur_item.val(item_name);
		cur_item.change();
		cur_item_pk.val(item_pk);
		cur_item_pk.change();
		id_item.change();
    } else {  // select2
    	var data = [{
    		"id": item_pk, "value": item_name
    	}];

		var new_option = new Option(item_name, item_pk, true, true);
		$('#' + name_pk).append(new_option).trigger('change');

		$('#' + name_pk).trigger({
        	type: 'select2:select',
	        params: {
    	        data: data
	        }
	    });
    }
}

function save_and_close_window_many(name_label, name_pk, item_name, item_pk){
    var main_page = opener.document;
    save_and_close_window_many_data(main_page, name_label,
                                    name_pk, item_name, item_pk);
    opener.focus();
    self.close();
}

function save_and_close_window_many_data(main_page, name_label,
                                         name_pk, item_name, item_pk){
    var cur_item;
    var cur_item_pk;
    if (main_page){
        cur_item = $(main_page).find("#"+name_label);
        cur_item_pk = $(main_page).find("#"+name_pk);
    } else {
        cur_item = $("#" + name_label);
        cur_item_pk = $("#" + name_pk);
    }
    var lbl_ = cur_item;
    var val_ = cur_item_pk;
    if (val_.val()){
        var v = lbl_.val();
        v = v.slice(0, v.lastIndexOf(","));
        lbl_.val(v + ", " + item_name + ", ");
        val_.val(val_.val() + ", " + item_pk);
        lbl_.change();
    } else {
        cur_item.val(item_name);
        cur_item_pk.val(item_pk);
    }
}

function multiRemoveItem(selItems, name, idx){
    for(id in selItems){
        if(selItems[id] == idx){
            selItems.splice(id, 1);
        }
    }
    jQuery("#selected_"+name+"_"+idx).remove();
}

function closeAllWindows(){
    jQuery("#window > div").hide("slow");
    jQuery("#window").html("");
}

function show_hide_flex(id){
    if ($(id).is(':hidden')){
        $(id).css('display', 'flex');
    } else {
        $(id).hide();
    }
}

function activate_all_search(){
    $('.activate_all_search').removeClass('disabled');
    $('.activate_own_search').addClass('disabled');
    $.get(activate_all_search_url, function(data) {
        display_info(activate_all_search_msg);
    });
    return false;
}

function activate_own_search(){
    $('.activate_own_search').removeClass('disabled');
    $('.activate_all_search').addClass('disabled');
    $.get(activate_own_search_url, function(data) {
        display_info(activate_own_search_msg);
    });
    return false;
}

function get_label_from_input(input){
    if (input.parent().filter('.input-group').length){
        input = input.parent();
    }
    return input.parent().attr('data-alt-name');
}

var enable_save = function(){
    if ($(".search-widget input").val()){
        $("#save-search-button").removeClass('disabled');
        $("#clear-search-button").removeClass('disabled');
        $("#generate-qrcode").removeClass('disabled');
    } else {
        $("#save-search-button").addClass('disabled');
        $("#clear-search-button").addClass('disabled');
        $("#generate-qrcode").addClass('disabled');
    }
}

function clear_search_field(){
    $("#id_search_vector").val("");
    add_message("-", 'info', "#advanced-search-info", true, false);
    enable_save();
}

function _clear_search_criteria_fields(query){
    var datatables_length = $(".dataTables_length select").val();
    document.getElementById('wizard-form').reset();
    // hidden input are not cleared
    $("#modal-advanced-search input[type='hidden']").each(
        function(){
            if($(this).attr("name") != 'csrfmiddlewaretoken'){
                $(this).val("")
            }
        }
    );
    $(".dataTables_length select").val(datatables_length);
    $('.dataTables_length select option[value="' + datatables_length + '"]'
        ).prop('selected', true);
    if(query) $("#id_search_vector").val(query);
    filter_search_fields();
}

var get_search_parameters_from_url = function(){
    var stored_search = $.urlParam('stored_search');
    if(!stored_search) return;
    $("#id_search_vector").val(decodeURIComponent(stored_search));
    enable_save();
};

function update_search_field(){
    var query = $("#id_search_vector").val();
    if (!query){
        query = "";
    }
    var inputs = $('#wizard-form input').map(
        function(){
            var v = "";
            var item_for_label = $(this);
            if ($(this).filter(":checkbox:checked").length){
                var check = $(this).val();
                if (check == "on"){
                    v = YES;
                } else {
                    v = $(this).parent().first().contents().filter(function() {
                        return this.nodeType == 3;
                    }).text();
                    item_for_label = $(this).parent().parent().parent();
                }
            } else if ($(this).filter(":text").length){
                v = $(this).val();
                if (v.match( new RegExp(' > ') )){
                    matches = v.split(' > ');
                    v = matches[matches.length - 1];
                }

            } else if ($(this).filter('input[type="number"]').length){
                v = $(this).val();
            }
            if (v == "") return;
            var term = get_label_from_input(item_for_label);
            if(!term || typeof term == "undefined") return;
            if (query) query += " ";
            query += term + '="' + v + '"';
        }
    ).get();
    var selects = $('#wizard-form select').each(
        function(){
            $(this).find('option').not(':first').filter(':selected').map(
                function(){
                    var item_for_label = $(this).parent();
                    if(item_for_label.filter('optgroup').length){
                        item_for_label = item_for_label.parent();
                    }
                    var term = get_label_from_input(item_for_label);
                    if(!term || typeof term == "undefined") return;
                    if (query) query += " ";
                    query += get_label_from_input(item_for_label) + '="' +
                        $(this).text() + '"';
            });
        }
    );
    _clear_search_criteria_fields(query);
    if (query){
        add_message(query, 'secondary', "#advanced-search-info", true, false);
    } else {
        add_message("-", 'secondary', "#advanced-search-info", true, false);
    }
    enable_save();
}

var sheet_list = new Array();

function update_window_menu(){
    if (!sheet_list.length){
        $("#window-fixed-menu").hide();
        return
    }
    var html = "";
    for (idx in sheet_list){
        var sheet = sheet_list[idx];
        html += '<li class="nav-item">';
        html += '<a class="nav-link" href="#' + sheet['id'] + '">' +
            sheet['label'] +'</a>';
        html += '</li>';
    }
    $("#window-fixed-menu-list").html(html);
    $("#window-fixed-menu").show();
}

function toggle_window_menu(){
    $("#window-fixed-menu").toggleClass('hidden');
    return false;
}

var register_qa_on_sheet = function(){
    $(".btn-qa").click(function(){
        var target = $(this).attr('data-target');
        dt_qa_open(target);
    });
};


var search_field_list;

var register_search_field_list = function(){
    search_field_list = new Object();
    $(".search-fields .form-group").each(function(){
        var key_val = $(this).attr('data-alt-name');
        if (key_val) {
            search_field_list[key_val] = $(this).children('label').html();
        }
    });
};

var filter_search_fields = function(){
    var current_filter = $("#search-input-filter").val();
    if (!current_filter){
        $(".search-fields .form-group").show();
        $("#clear-filter-button").addClass('disabled');
        return
    }
    $("#clear-filter-button").removeClass('disabled');
    $.each(search_field_list, function(key, value) {
            if (
                !key.match( new RegExp(current_filter, 'i') ) &&
                !value.match( new RegExp(current_filter, 'i') )
            ) {
                $("[data-alt-name=" + key + "]").hide();
            } else {
                $("[data-alt-name=" + key + "]").show();
            }
        }

    );
};


function register_advanced_search(){
    $(".advanced-search-reset").click(
        function(){
            _clear_search_criteria_fields();
        }
    );
    $('#modal-advanced-search input').keypress(function(e) {
        var key = e.key;
        if (key === "Enter") {
            $(".advanced-search-valid").click();
            e.stopPropagation();
            return false;
        }
    });
    register_search_field_list();
    $("#search-input-filter").on('input', filter_search_fields);
    $("#clear-filter-button").click(function(){
        $("#search-input-filter").val("");
        filter_search_fields();
    });

    $(".advanced-search-valid").click(update_search_field);
    $(".advanced-search-clear").click(clear_search_field);
    $('#modal-advanced-search').on('hidden.bs.modal', function (e) {
        var base_query = $("#id_search_vector").val();
        // reset all criteria
        _clear_search_criteria_fields(base_query);
        $(".search_button").click();
        setTimeout(function(){
            $("#id_search_vector").focus();
        }, 200);  // the hidden should be fire after anything is hide but...
    });
    $('#modal-advanced-search').on('shown.bs.modal', function (e) {
        update_search_field();
        $("#search-input-filter").focus();
    });
}


var _pinned_search_init = false;

function manage_pinned_search(name, data){
    $('#pinned_search_content_' + name).html('');
    for (idx in data){
        if (idx == 'pinned-search' && data[idx] != ''){
           $("#id_search_vector").val(data[idx]);
           if(!_pinned_search_init){
               update_search_field();
               _pinned_search_init = true;
           }
        }
    }
}

var dt_generate_qa_url = function (table, url){
    var data = table.rows( { selected: true } ).data();
    var value = "";
    for (k in data){
        if (!data[k]['id']) continue;
        if (k > 0) value += "-";
        value += data[k]['id'];
    }
    url += value + "/";
    return url;
};

var dt_qa_open = function (url, modal_id){
	if (!modal_id) modal_id = "modal-dynamic-form";
    short_wait();
    $.ajax({
        url: url,
        cache: false,
        success: function(data, textStatus, jqXHR) {
            close_wait();
            var text = jqXHR.responseText;
            $('#' + modal_id).html(text);
            var response = $(text);
            var responseScript = response.find("script");
            $.each(responseScript, function(idx, val){
                    eval(val.text);
                }
            );
            $('#' + modal_id).modal("show");
        },
        error: function() {
            close_wait();
        }
    });
    return false;
};

var ajax_post = function(url, data, target, callback, error_callback){
    $.ajax({
        url : url,
        type : "POST",
        data : data,
        success : function(data) {
            close_wait();
            if(target) $(target).html(data);
            if(callback) callback();
        },
        error : function(xhr,errmsg,err) {
            close_wait();
            if (target) {
                $(target).html("<div class='alert-box alert'>Oops! We have encountered an error: "
                    + errmsg + "</div>");
            }
            console.log(xhr.status + ": " + xhr.responseText);
            if (error_callback) error_callback();
        }
    });
};

var qa_action_register = function(url, slug) {
    var qa_id;
    var modal_id;
    if (!slug){
        qa_id = "qa-action";
        modal_id = "modal-dynamic-form";
    } else {
        qa_id = "qa-new-" + slug;
        modal_id = "modal-dynamic-form-" + slug;
    }
    $("#" + qa_id).on('submit', function(event){
        event.preventDefault();
        var fn = function(){
            $('#' + modal_id).modal("show");
        } ;
        $('#' + modal_id).modal("hide");
        short_wait();
        ajax_post(
            url, $(this).serialize(), "#" + modal_id, fn, fn
        );
    });
};


var update_export_urls = function(dt, source_cls, sname, source, source_full, extra_sources, extra_tpl){
    let rows = dt.rows( { selected: true } ).data();
    let data = "selected_ids=";
    for (k in rows){
        if (!rows[k]['id']) continue;
        if (k > 0) data += "-";
        data += rows[k]['id'];
    }
    let extra = "?submited=1&" + data;
    let csv_url = source + "csv" + extra;
    $("." + sname + "-csv").attr("href", csv_url);
    let csv_full_url = source_full + "csv?submited=1&" + data;
    $("." + sname + "-csv-full").attr("href", csv_full_url);

    $("." + source_cls + " ." + sname + "-csv-external").each(function(){
        let url = $(this).attr("href").split('?')[0] + extra;
        $(this).attr("href", url);
    });

    for (k in extra_sources){
        let src = extra_sources[k];
        let slug = src[0];
        let name = src[1];
        let extra_source = src[2];
        $("." + slug + "-csv-full").attr("href", extra_source + "csv?submited=1&" + data);
    }
    for (k in extra_tpl){
        let tpl = extra_tpl[k];
        let slug = tpl[0];
        let lnk = tpl[1];
        $("." + slug + "-labels").attr("href", lnk + "?submited=1&" + data);
    }
    return false;
};

var dt_single_enable_disable_submit_button = function(e, dt, type, indexes){
    var rows = dt.rows( { selected: true } ).count();
    if (rows == 1) {
        $("#validation-bar #submit_form").prop('title', "");
        $("#validation-bar #submit_form").prop('disabled', false);
    } else {
        $("#validation-bar #submit_form").prop('title', select_only_one_msg);
        $("#validation-bar #submit_form").prop('disabled', true);
    }
};

var dt_multi_enable_disable_submit_button = function(e, dt, type, indexes){
    var rows = dt.rows( { selected: true } ).count();
    if (rows >= 1) {
        $("#validation-bar #submit_form").prop('title', "");
        $("#validation-bar #submit_form").prop('disabled', false);
    } else {
        $("#validation-bar #submit_form").prop('title', select_only_one_msg);
        $("#validation-bar #submit_form").prop('disabled', true);
    }
};

var dt_update_badge = function(source_id, number){
	$("#source_badge_" + source_id).html(number);
};

var number_with_commas = function(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


var render_paginate_select = function(table_name, name, nb_select){
    var html = '<label>' + show_msg;
    var id_select = table_name + '-length_' + name;
    html += ' <select class="form-control form-control-sm" name="' + id_select + '" id="id_' + id_select + '">';
    var nb_rows = [5, 10, 25, 50, 100];
    for (idx in nb_rows){
        var nb = nb_rows[idx];
        html += '<option value="' + nb + '"';
        if (nb == nb_select) html += " selected='selected'";
        html += '>' + nb + '</option>';
    }
    html += '</select> ' + entries_msg + '</label>';
    return html
};

var get_hover_div = function(table_cols, data){
    var hover = "";
    for (idx_tc in table_cols){
        var key = table_cols[idx_tc];
        if (idx_tc < 7 && key in data && data[key]){
            if (idx_tc == 3 || idx_tc == 6){
                hover += "<br>";
            } else if (hover){
                hover += " - ";
            }
            if (!data[key]) data[key] = "";
            hover += String(data[key]).replace(/"/g, '&quot;');
        }
    }
    return hover;
};

var _gallery_submit_search = function(image_page, query_vars, name, source, extra){
	if (!extra) extra = "";
    if (image_page) {
        current_image_page = image_page;
    } else {
        current_image_page = 1;
    }
    $('.modal-progress').modal('show');
    var data = search_get_query_data(query_vars, name);
    var nb_select = jQuery("#id_" + name + "-length_image" + extra).val();
    if (!nb_select) nb_select = 10;

    var url = source + "json-image?length=" + nb_select + "&submited=1&" + data;
    $.getJSON(url, function(data) {
        var timestamp = Math.floor(Date.now() / 1000);
        var gallery_id = "gallery-" + timestamp + extra;
        $("#content-gallery-" + name + extra).html(
            render_gallery(data, name, nb_select, gallery_id));
        $("#id_" + name + "-length_image" + extra).change(gallery_submit_search);
        register_image_gallery(gallery_id);
        $('.card[data-toggle="tooltip"]').tooltip();
        if ($('.modal-progress').length > 0){
            $('.modal-progress').modal('hide');
        }
    });
    return false;
}

var render_gallery = function(data_table, table_name, nb_select, gallery_id){
    var html = '<div class="ishtar-gallery-wrapper container-fluid">';
    html += render_paginate_select(table_name, 'image', nb_select);
    var page_total = data_table['total'];
    var page_current = data_table['page'];
    var recordsTotal = data_table['recordsTotal'];
    html += "<span class='ishtar-gallery-info'>";
    var page_min = ((page_current - 1) * nb_select + 1);
    if (page_min > recordsTotal) page_min = recordsTotal;
    html += info_show_msg + " " + page_min + " ";
    var page_max = page_current * nb_select;
    if (page_max > recordsTotal) page_max = recordsTotal;
    html += table_to_msg + " " + page_max;

    var recordsTotal = number_with_commas(recordsTotal);
    html += " " + table_of_msg + " " + recordsTotal + " ";
    html += info_entries_msg + "</span>";

    html += "<div class='ishtar-gallery d-flex flex-wrap' id='" + gallery_id + "'>";
    var captions = '<div class="lightgallery-captions">';
    var table_cols = data_table["table-cols"];

    if(!data_table["rows"].length){
        html += "<div class='no-data'>" + no_data_gallery_msg + "</div>";
    }

    $.each(data_table["rows"], function(idx, data){
        var thumb = "";
        if ("main_image__thumbnail" in data){
            thumb = data["main_image__thumbnail"];
        } else {
            thumb = static_path + "media/images/empty-image.png";
        }
        var image = "";
        if ("main_image__image" in data){
            image = data["main_image__image"];
        } else {
            image = static_path + "media/images/empty-image.png";
        }
        var link = data["link"];
        var name = '';
        if ("cached_label" in data){
            name = data["cached_label"];
        }
        if ("name" in data){
            name = data["name"];
        }
        var hover = get_hover_div(table_cols, data);
        var caption_id = 'lightgallery-' + gallery_id +'-caption-' + data["id"];
        html += '<div class="card m-2" data-toggle="tooltip" data-html="true" title="' + hover + '">';
        html += '<a data-sub-html="#'+ caption_id + '" class="thumb-image" href="' + image + '">';
        html += '<img class="card-img-top" src="' + thumb + '">';
        html += '</a>';
        html += '<div class="card-body">';
        html += '<h4 class="card-title">' + link + " " + name + '</h4>';
        html += "</div></div>";
        captions += '<div id="' +  caption_id + '">';
        captions += '<span class="close">&times</span>';
        captions += name;
        captions += '</div>';
    });
    html += "</div>";
    captions += '</div >';

    /* pagination */
    html += "<div class='dataTables_wrapper'><div class='dataTables_paginate'>";
    html += '<ul class="image-pagination">';

    var disabled = page_current == 1;
    html += render_paginate_button(page_current - 1, page_current, "Previous",
                                   disabled, " previous");

    var idx_page = 1;
    if (page_current < 5){
        while (idx_page <= 5){
            if (idx_page <= page_total){
                html += render_paginate_button(idx_page, page_current);
            }
            idx_page += 1;
        }
        if (page_total > 5){
            html += render_paginate_button('...');
            html += render_paginate_button(page_total, page_current);
        } else if (idx_page == page_total) {
            html += render_paginate_button(idx_page, page_current);
        }
    } else {
        html += render_paginate_button('...');
        if (page_total < page_current + 2 &&
            page_total > (page_current - 2)){
            html += render_paginate_button(page_current - 3, page_current);
        }
        if (page_total < page_current + 3 &&
            page_total > (page_current - 3)){
            html += render_paginate_button(page_current - 2, page_current);
        }
        html += render_paginate_button(page_current - 1, page_current);
        html += render_paginate_button(page_current, page_current);
        if (page_total > page_current)
            html += render_paginate_button(page_current + 1, page_current);
        if (page_total < page_current + 3){
            idx_page = page_current + 2;
            while (idx_page <= page_total){
                html += render_paginate_button(idx_page, page_current);
                idx_page += 1;
            }
        } else {
            html += render_paginate_button('...');
            html += render_paginate_button(page_total, page_current);
        }
    }

    disabled = page_current == page_total;
    html += render_paginate_button(
        page_current + 1, page_current, "Next", disabled, " next");

    html += '</ul></div></div>';
    return captions + html;
};

var render_paginate_button = function(nb, current, label, disabled, extra_class){
    if (!extra_class) extra_class = '';
    if (nb == '...'){
        disabled = true;
    } else if (current == nb) {
        extra_class += ' active';
    }
    if (disabled) extra_class += ' disabled';
    if (!label) label = nb;

    var html = '<li class="paginate_button page-item' + extra_class + '">';
    html += '<a href="#" data-dt-idx="' + nb + '" tabindex="0" class="page-link">';
    html += label + '</a></li>';
    return html;
};

var current_image_page = 1;

var register_image_gallery = function(gallery_id){
    $(".image-pagination .paginate_button a").click(function(){
        gallery_submit_search($(this).attr('data-dt-idx'));
    });
    lightGallery(document.getElementById(gallery_id),
                 {selector: '.thumb-image'});
    $("#" + gallery_id).on('onBeforeOpen.lg', function(event){
        $('.tooltip').tooltip('hide');
    });
    $('.tooltip').tooltip('hide');
};

var item_list_msg = "Item list";

var render_map_list_modal = function(map_id){
    var html = "";
    html += '<div id="ishtar-map-window-' + map_id + '" class="ishtar-map-window simple-window" tabindex="-1" aria-hidden="true">';
    html += '<div class="simple-window-content">';
    html += '<div class="modal-header">';
    html += item_list_msg;
    html += '<button type="button" class="close" ';
    html += ' onclick="$(\'#ishtar-map-window-' + map_id + '\').hide()" aria-label="Close"> ';
    html += '<span aria-hidden="true">&times;</span> </button> </div>';
    html += '<div class="modal-body"> </div> </div> </div>';
    return html;
};

var limit_map_msg = "Limit to {0} items";
var display_poly_map_msg = "Display lines, polygons";
var limit_map_help_msg = "Unchecking this limit on a poorly performing device may result in web browser freeze";
var limit_map_nb = 50000;
var current_map_limit = limit_map_nb;
var display_polygon_on_map = false;
var displayed_map_msg = "{0} items displayed on the map";
var non_displayed_map_msg = "{0} items not displayed";
var non_displayed_map_link_msg = "(list)";

var render_map = function(map_id, use_map_limit, hide_limit, display_polygons){
    var html = "";
    if (!hide_limit){
        html += "<div class='ishtar-map-top row'>";
        html += "<div class='ishtar-map-info col-sm' id='ishtar-map-info-" + map_id + "'></div>";
        html += "<div class='ishtar-map-limit col-sm form-check'>";
        html += "<input class='form-check-input' type='checkbox' id='ishtar-map-limit-" + map_id + "' ";
        if (use_map_limit) html += " checked='checked'";
        html += "/> ";
        html += "<label class='form-check-label' for='ishtar-map-limit-" + map_id + "'>";
        html += limit_map_msg.format(number_with_commas(limit_map_nb));
        html += " <i class='fa fa-question-circle' title=\""+  limit_map_help_msg  +"\" aria-hidden='true'></i></label>";
        html += "</div>";
        html += "<div class='ishtar-map-limit col-sm form-check'>";
        html += "<input class='form-check-input' type='checkbox' id='ishtar-map-poly-" + map_id + "' ";
        if (display_polygons) html += " checked='checked'";
        html += "/> ";
        html += "<label class='form-check-label' for='ishtar-map-poly-" + map_id + "'>";
        html += display_poly_map_msg;
        html += " </label>";
        html += "</div></div>";
    }

    html += "<div class='ishtar-table-map' id='" + map_id + "'></div>";
    html += "<div class='ishtar-map-popup' id='ishtar-map-popup-" + map_id + "'></div>";
    html += render_map_list_modal(map_id);
    current_map_limit = limit_map_nb;
    return html;
};

var no_geo_window_content = "";

var register_map = function(map_id, result){
    let points = {};
    Object.assign(points, result);
    points["features"] = new Array();
    let line_and_polys = {};
    Object.assign(line_and_polys, result);
    line_and_polys["features"] = new Array();
    for (const feature of result["features"]){
        if (feature["geometry"] && feature["geometry"]["type"] === "Point"){
            points["features"].push(feature);
        } else {
            line_and_polys["features"].push(feature);
        }
    }
    display_map(map_id, points, line_and_polys);
    $('#ishtar-map-limit-' + map_id).change(function() {
        if ($(this).prop('checked')) {
            current_map_limit = limit_map_nb;
        } else {
            current_map_limit = 0;
        }
        $(".search_button").click();
    });
    $('#ishtar-map-poly-' + map_id).change(function() {
        if ($(this).prop('checked')) {
            display_polygon_on_map = true;
        } else {
            display_polygon_on_map = false;
        }
        $(".search_button").click();
    });
    if (points || line_and_polys){
        let lbl = "";
        let feature_nb = 0;
        if (points && points['features'].length) feature_nb = points['features'].length;
        if (line_and_polys && line_and_polys['features'].length) feature_nb += line_and_polys['features'].length;

        if (feature_nb > 0) lbl += displayed_map_msg.format(number_with_commas(feature_nb));
        if (result['no-geo'].length){
            if (lbl != ""){
                lbl += "<br>";
            }
            lbl += non_displayed_map_msg.format(number_with_commas(result['no-geo'].length));
            lbl += " <a href='#' id='no-geo-" + map_id + "'>" + non_displayed_map_link_msg + "</a>";
        }
        $("#ishtar-map-info-" + map_id).html(lbl);

        no_geo_window_content = "<ul>";
        for (const idx in result['no-geo']){
            no_geo = result['no-geo'][idx];
            let link = link_template[map_id].replace("<pk>", no_geo["id"]);
            let txt = "<li>" + link + " " + no_geo['name'] + "</li>";
            no_geo_window_content += txt;
        }
        no_geo_window_content += "</ul>";
        $("#no-geo-" + map_id).click(function(){
            $(popup_item[map_id]).hide();
            $('#ishtar-map-window-' + map_id).hide();
            $("#ishtar-map-window-" + map_id + " .modal-body").html(
                no_geo_window_content
            );
            $('#ishtar-map-window-' + map_id).show();
        });
    }
};

var main_submit_search = function(){
    if (current_tab == "table") datatable_submit_search();
    if (current_tab == "gallery") gallery_submit_search();
    if (current_tab == "stats") stats_submit_search();
    if (current_tab == "map") map_submit_search();
};

var search_get_query_data = function(query_vars, table_name){
    $("#id_search_vector").removeClass('input-progress');
    var data = "";
    for (idx in query_vars){
        var key = query_vars[idx];
        var item = jQuery("#id_" + key);
        var val = null;
        if (item.prop('type') == 'checkbox'){
            if (item.prop('checked')){
                var val = item.val();
            }
        } else {
            var val = item.val();
        }
        if (val){
            if (data) data += "&";
            data += key + "=" + encodeURIComponent(val);
        }
    }
    if (current_image_page && current_tab == "gallery"){
        var id_select = "#id_" + table_name + '-length_image';
        if (data) data += "&";
        data += "start=" + ((current_image_page - 1) * $(id_select).val());
    }
    if (current_tab == "map"){
        if (!current_map_limit){
            if (data) data += "&";
            data += "no_limit=true";
        }
        if (display_polygon_on_map){
            if (data) data += "&";
            data += "display_polygon=true";
        }
    }


    return data;
};

var register_wizard_form = function(){
    $('form :input').focus(function(){
      let item_bottom = $(this).offset().top + $(this).height();
      let footer_top = $("#footer").offset().top;
      if (item_bottom > footer_top) {
        $('html, body').animate({
          scrollTop: $(document).scrollTop() + (item_bottom - footer_top) + 25
        }, 800);
      }
      let label_margin = 30;
      if ($(document).scrollTop() > ($(this).offset().top - label_margin)){
        $('html, body').animate({
          scrollTop: $(this).offset().top - label_margin
        }, 800);
      }
    });

    $('form :input').change(function(){form_changed=true;});

    $('.change_step').click(function(){
        if(!form_changed ||
            confirm(form_changed_msg)){
            return true;
        }
        return false;
    });

    if ($("[autofocus]").length > 0) autofocus_field = "#" + $($("[autofocus]")[0]).attr('id');

    $(".wizard-form").keydown(function(e){
      if (e.ctrlKey && e.keyCode == 13) {
        $("#submit_form").click();
      }
    });
}

var registered_stats = false;
var registered_stats_img = new Array();
var extra_list = new Array();
var sources = new Array();

var register_stats = function(query_vars, name){
    if (registered_stats) return;
    registered_stats = true;
    $("#stats-form-" + name + " select").on('change',
    	function() {
            for (idx in extra_list){
            	_stats_submit_search(query_vars, name, sources[idx], extra_list[idx]);
            }
        }
    );
};

var register_stats_img = function(query_vars, name, extra){
    if (registered_stats_img.includes(extra)) return;
    registered_stats_img.push(extra);
    if (extra != "") name += "-" + extra;
    $('#chart-img-display-' + name).click(function(){
        $('#chart-img-' + name).hide();
        $('#img-' + name).html(
        $('<img/>').attr(
                'src', $('#chart-' + name).jqplotToImageStr({})
        )
        );
        $('#chart-img-' + name).show('slow');
    });
};

var _stats_submit_search = function(query_vars, name, source, extra){
	if (!extra) extra = "";
    $('.modal-progress').modal('show');

    var data = search_get_query_data(query_vars, name);
    register_stats(query_vars, name);
    register_stats_img(query_vars, name, extra);

    var url = source + "json-stats?submited=1&" + data;
    var modality_1 = $("#stats_modality_1-" + name).val();
    if (modality_1) url += '&stats_modality_1=' + modality_1;
    var modality_2 = $("#stats_modality_2-" + name).val();
    if (modality_2) url += '&stats_modality_2=' + modality_2;
    var stats_sum_variable = $("#stats_sum-" + name).val();
    if (stats_sum_variable) url += '&stats_sum_variable=' + stats_sum_variable;
    $.getJSON(url, function(data) {
        var timestamp = Math.floor(Date.now() / 1000);
        var stats_id = "stats-" + timestamp;
        $("#tab-content-stats-" + name).html(
            render_stats(data, name, extra)
        );
        if ($('.modal-progress').length > 0){
            $('.modal-progress').modal('hide');
        }
    });
    return false;
}

var stats_showmarker = false;
var stats_incompatible_modality = "This graph type accept only one modality.";
var stats_current_graph = new Array();

var _render_stats_empty = function(stats_values, name){
    $("#charts-" + name).hide();
    $("#stats-table-" + name).hide();
    $("#stats-error-" + name).hide();
    $("#stats-empty-" + name).show();
};

var _render_stats_too_many_values = function(name){
    $("#charts-" + name).hide();
    $("#stats-empty-" + name).hide();
    $("#stats-table-" + name).hide();
    $("#stats-error-" + name).show();
}

var _render_stats_table = function(stats_values, name, extra){
    var modality_1 = $("#stats_modality_1-" + name).val();
    var modality_2 = $("#stats_modality_2-" + name).val();
    var rows = new Array();

    var current_row = new Array();
    var html = "";
    html += "<table class='table w-75 mt-4 pt-4 mx-auto'><thead>";
    html += "<tr><th>";
    var modality_1_lbl = $('#stats_modality_1-' + name + ' option:selected').text();
    current_row.push(modality_1_lbl);
    html += modality_1_lbl;
    if (modality_2 && modality_2 != modality_1){
        html += "</th><th>";
        var modality_2_lbl = $('#stats_modality_2-' + name + ' option:selected').text();
        html += modality_2_lbl;
        current_row.push(modality_2_lbl);
    }
    html += "</th><th>";
    var sum_lbl = $('#stats_sum-' + name + ' option:selected').text();
    html += sum_lbl;
    current_row.push(sum_lbl);
    html += "</th></tr></thead><tbody>";
    rows.push(current_row);

    if (modality_2 && modality_2 != modality_1){
        for (idx in stats_values){
            var start_row = "";
            row_content = "";
            for (inner_idx in stats_values[idx][1]){
                current_row = new Array();
                var colspan = parseInt(inner_idx) + 1;
                start_row = "<tr><td rowspan='" + colspan + "'>" + stats_values[idx][0] + "</td>";
                current_row.push(stats_values[idx][0]);
                if (inner_idx > 0) row_content += "<tr>";
                row_content += "<td>" + stats_values[idx][1][inner_idx][0] + "</td>";
                current_row.push(stats_values[idx][1][inner_idx][0]);
                var cvalue = stats_values[idx][1][inner_idx][1];
                current_row.push(cvalue);
                rows.push(current_row);
                if (typeof cvalue.toLocaleString !== "undefined")
                    cvalue = cvalue.toLocaleString();
                row_content += "<td class='text-right'>" + cvalue + "</td></tr>";
            }
            html += start_row + row_content;
        }
    } else {
        for (idx in stats_values){
            current_row = new Array();
            html += "<tr><td>" + stats_values[idx][0] + "</td>";
            current_row.push(stats_values[idx][0]);
            var cvalue = stats_values[idx][1];
            if (cvalue == null) cvalue = 0;
            current_row.push(cvalue);
            rows.push(current_row);
            if (typeof cvalue.toLocaleString !== "undefined")
                cvalue = cvalue.toLocaleString();
            html += "<td class='text-right'>" + cvalue + "</td></tr>";
        }
    }

    html += "</tbody></table>";
	if (!extra) extra = "default";
    $("#charts-" + name + "-" + extra).hide();
    $("#stats-empty-" + name + "-" + extra).hide();
    $("#stats-error-" + name + "-" + extra).hide();
    $("#stats-table-content-" + name + "-" + extra).html(html);
    $("#stats-table-" + name + "-" + extra).show();

    var encoded_uri = encodeURI("data:text/csv;charset=utf-8,") + encodeURIComponent(Papa.unparse(rows));
    $("#stats-table-csv-" + name + "-" + extra).attr("href", encoded_uri);
    $("#stats-table-csv-" + name + "-" + extra).attr("download", "ishtar-stats.csv");
};

var MAX_STATS_GRAPH_VALUES = 100;
var MAX_STATS_GRAPH_MODALITY_VALUES = 100;
var jqvalues = new Array();

var render_stats = function(stats_values, name, extra){
    var stats_type = $("#stats_renderer-" + name).val();

    if (!stats_values || !stats_values['data']
        || stats_values['data'].length == 0){
        return _render_stats_empty(stats_values, name);
    }

    stats_values = stats_values['data'];

	if (!extra) extra = "default";
    if (stats_current_graph[extra]){
        stats_current_graph[extra].destroy();
    }
    if (stats_type == "table"){
        return _render_stats_table(stats_values, name, extra);
    }

	if (stats_values.length > MAX_STATS_GRAPH_VALUES){
		_render_stats_too_many_values(name);
		return;
	}
	for (idx in stats_values){
		if (stats_values[idx][1].length > MAX_STATS_GRAPH_MODALITY_VALUES){
			_render_stats_too_many_values(name);
			return;
		}
	}

    $("#stats-table-" + name + "-" + extra).hide();
    $("#stats-empty-" + name + "-" + extra).hide();
    $("#stats-error-" + name + "-" + extra).hide();
    $("#charts-" + name + "-" + extra).show();
    var modality_1 = $("#stats_modality_1-" + name).val();
    var modality_2 = $("#stats_modality_2-" + name).val();
    stats_xaxis = {
        label: $('#stats_modality_1-' + name + ' option:selected').text()
    };
    
    var stats_help = $("#stats-zoom-help-" + name);

    if (stats_type == "pie"){
        stats_help.hide();
        if (modality_2){
            display_info(stats_incompatible_modality);
            return;
        }
    } else {
        stats_help.show();
        var stats_xaxis_tickoptions;
        if (modality_1 == "year"){
            stats_xaxis_tickoptions = {formatString: "%d", angle: -30};
        } else {
            stats_xaxis_tickoptions = {angle: -30};
        }
        stats_xaxis["tickOptions"] = stats_xaxis_tickoptions
    }
    
    var stats_showmarker = false;
    if (stats_values.length < 25){
        stats_showmarker = true;
    }
    jqvalues[extra] = new Array();
    var ticks = new Array();
    var series = new Array();
    var series_conf = new Array();
    var legend_conf = {
            show:true, location: 'ne', placement: 'outside'
    };
    if (modality_2 && modality_2 != modality_1){
        for (idx in stats_values){
            ticks.push(text_truncate(stats_values[idx][0], 40));
            var serie_values = stats_values[idx][1];
            var current_serie_values = new Array();
            for (inner_idx in serie_values){
                var serie_value = text_truncate(serie_values[inner_idx][0], 40);
                if (series.indexOf(serie_value) == -1){
                    series.push(serie_value);
                    var fill_values = new Array();
                    if (idx > 0){ // put 0 for previous items
                        for (var fill_idx = 0 ; fill_idx < idx ; fill_idx++ ){
                            fill_values.push(0);
                        }
                    }
                    jqvalues[extra].push(fill_values);
                }
                current_serie_values.push(serie_value);
                var current_value = serie_values[inner_idx][1];
                jqvalues[extra][series.indexOf(serie_value)].push(current_value);
            }
            // put 0 for missing series for this item
            for (idx_serie in series){
                if (current_serie_values.indexOf(series[idx_serie]) == -1){
                    jqvalues[extra][series.indexOf(series[idx_serie])].push(0);
                }
            }
        }
        stats_xaxis['ticks'] = ticks;
    } else {
        for (idx in stats_values){
            stats_values[idx][0] = text_truncate(stats_values[idx][0], 40);
        }
        jqvalues[extra] = [stats_values];
        series.push("Total");
        if (stats_type != "pie"){
            legend_conf = {};
        }
    }
    if (stats_type != "pie"){
        stats_xaxis['renderer'] = $.jqplot.CategoryAxisRenderer;
        stats_xaxis['labelRenderer'] = $.jqplot.CanvasAxisLabelRenderer;
        stats_xaxis['tickRenderer'] = $.jqplot.CanvasAxisTickRenderer;
    }

    var stats_renderer;
    if (stats_type == "bar"){
        stats_renderer = $.jqplot.BarRenderer;
    } else if (stats_type == "pie"){
        stats_renderer = $.jqplot.PieRenderer;
    }
    for (idx in series){
        var serie_conf = {
            label: series[idx],
            showmarker: stats_showmarker
        };
        if (stats_renderer){
            serie_conf["renderer"] = stats_renderer;
        }
        serie_conf["rendererOptions"] = {
            padding: 8, showDataLabels: true,
            dataLabels: 'value'
        };
        series_conf.push(serie_conf);
    }
    
    var stats_options = {
            axes: {
                xaxis: stats_xaxis,
                yaxis: {
                    label: $('#stats_sum-' + name + ' option:selected').text(),
                    min: 0
                }
            },
            highlighter: {
                show: true,
                tooltipAxes: 'y',
                sizeAdjust: 7.5
            },
            series: series_conf,
            legend: legend_conf
    };
    if (stats_type != "pie"){
        stats_options["cursor"] = {
			show: true,
			zoom: true,
			showTooltip: false
        }
    }
    stats_current_graph[extra] = $.jqplot(
    	'chart-' + name + "-" + extra,
        jqvalues[extra], stats_options
    );
};

var is_valid_isbn = function(str) {
    var sum, weight, digit, check, i;

    str = str.replace(/[^0-9X]/gi, '');

    if (str.length != 10 && str.length != 13) {
        return false;
    }

    if (str.length == 13) {
        sum = 0;
        for (i = 0; i < 12; i++) {
            digit = parseInt(str[i]);
            if (i % 2 == 1) {
                sum += 3*digit;
            } else {
                sum += digit;
            }
        }
        check = (10 - (sum % 10)) % 10;
        return (check == str[str.length-1]);
    }

    if (str.length == 10) {
        weight = 10;
        sum = 0;
        for (i = 0; i < 9; i++) {
            digit = parseInt(str[i]);
            sum += weight*digit;
            weight--;
        }
        check = (11 - (sum % 11)) % 11
        if (check == 10) {
            check = 'X';
        }
        return (check == str[str.length-1].toUpperCase());
    }
}

var is_valid_issn = function(str) {
    var sum, weight, digit, check, i;

    str = str.replace(/[^0-9X]/gi, '');

    if (str.length != 8) {
        return false;
    }

	sum = 0;
	for (i = 0; i < 7; i++) {
		digit = parseInt(str[i]);
		sum += digit * (8 - i)
	}
	check = 11 - sum % 11;
	if (check == 10) {
		check = 'X';
	}
	return (check == str[str.length-1].toUpperCase());
}

var update_select_widget = function(input_name, values, only_values, excluded_values){
    cvalue = $("#id_" + input_name).val();
    var options = "";
    for (var idx in values){
        var option = values[idx];
        if (!(option[0] && option[0] != cvalue
                && ((excluded_values &&
                     excluded_values.indexOf(option[0]) != -1) ||
                    (only_values && only_values.indexOf(option[0]) == -1)
                    ))){
            var selected = "";
            if (option[0] == cvalue) selected = " selected";
            options += "<option value='" + option[0] + "'" + selected + ">";
            options += option[1] + "</option>";
		}
	}
	$("#id_" + input_name).html(options);
};

var inline_register_add_button = function(slug){
    let inline_form = document.querySelectorAll(".form-" + slug);
    let inline_container = document.querySelector("#formset-container-" + slug);
    let inline_add_button = document.querySelector("#add-form-" + slug);
    let inline_div_add_button = document.querySelector("#div-add-form-" + slug);
    let inline_total_forms = document.querySelector("#id_" + slug + "-TOTAL_FORMS");
    let inline_form_num = inline_form.length - 1;
    inline_add_button.addEventListener(
        'click', function(e){
            e.preventDefault();
            let new_form = inline_form[0].cloneNode(true);
            let form_regex_var = slug + '(\\d){1}-';
            let form_regex = new RegExp(form_regex_var, 'g');
            inline_form_num++;
            new_form.innerHTML = new_form.innerHTML.replace(
                form_regex, slug + `-${inline_form_num}-`);
            inline_container.insertBefore(new_form, inline_div_add_button);
            inline_total_forms.setAttribute('value', `${inline_form_num+1}`);
        }
    );
};

var redraw_plots = function(name, key){
	if (stats_current_graph && stats_current_graph[key]){
	 	$('#chart-' + name + "-" + key).height($('#charts-' + name + "-" + key).height()*0.96 - 100);
	 	$('#chart-' + name + "-" + key).width($('#charts-' + name + "-" + key).width()* 0.96);
		stats_current_graph[key].replot({resetAxes: true});
	}
};

var mobile_check = function() {
  let check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};

var preview_input_image = function(input, input_id){
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#img-' + input_id).attr('src', e.target.result);
            $('#img-' + input_id).parent().attr('href', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        let gallery = document.getElementById('lightgallery-' + input_id);
        if (gallery && gallery.getAttribute('lg-uid') && window.lgData) {
            window.lgData[gallery.getAttribute('lg-uid')].destroy(true);
        }
        lightGallery(document.getElementById('lightgallery-' + input_id));
     }
};


var register_preview_input_image = function(input_id){
    $('#img-' + input_id).on('load',function() {
         let css;
         let ratio = $(this).width() / $(this).height();
         let pratio = $(this).parent().parent().width() / $(this).parent().parent().height();
         if (ratio < pratio) {
             css = {width:'auto', height:'100%'};
         } else {
             css = {width:'100%', height:'auto'};
         }
         $(this).css(css);
     });
     $('#mobile-'+ input_id).change(function (){
        if($(this).is(":checked")) {
            $("#input-" + input_id).attr("capture", "camera");
            $("#input-" + input_id).attr("accept", "image/*");
        } else {
            $("#input-" + input_id).removeAttr("capture");
            $("#input-" + input_id).removeAttr("accept");
        }
     });
};

var bs_expand_table = function(name){
    $("#grid_" + name + "_wrapper").appendTo(
        "#modal_grid_" + name + " .modal-body .current-grid");
    $("#window").appendTo(
        "#modal_grid_" + name + " .modal-body .current-sheets");
    $("#grid_" + name).DataTable().clear().draw();
    current_modal = $("#modal_grid_"+ name);
}

var bs_hide_table = function(name){
    $("#grid_" + name + "_wrapper").appendTo(
        "#grid_" + name + "_meta_wrapper");
    $("#window").appendTo("#window_wrapper");
    $("#grid_" + name).DataTable().clear().draw();
    current_modal = null;
}
