#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.template import Library
from django.utils.translation import ugettext_lazy as _

register = Library()


@register.inclusion_tag('blocks/bs_form_snippet.html', takes_context=True)
def bs_form(context, form, position=0, no_error=False):
    user = context['user']
    show_field_number = user.ishtaruser and user.ishtaruser.show_field_number()
    return {'form': form, 'odd': position % 2,
            'show_field_number': show_field_number,
            'no_error': no_error}


@register.inclusion_tag('blocks/bs_formset_snippet.html', takes_context=True)
def bs_formset(context, formset, position=0, no_error=False):
    user = context['user']
    show_field_number = user.ishtaruser and user.ishtaruser.show_field_number()
    return {'formset': formset, 'odd': position % 2,
            'show_field_number': show_field_number,
            'no_error': no_error}


@register.inclusion_tag('blocks/bs_compact_form_snippet.html',
                        takes_context=True)
def bs_compact_form(context, form, position=0):
    user = context['user']
    show_field_number = user.ishtaruser and user.ishtaruser.show_field_number()
    return {'form': form, 'odd': position % 2,
            'show_field_number': show_field_number}


@register.inclusion_tag('blocks/table_form_snippet.html')
def table_form(form):
    return {'form': form}


@register.inclusion_tag('blocks/bs_field_snippet.html', takes_context=True)
def bs_field(context, field, required=False, label=None):
    user = context['user']
    show_field_number = user.ishtaruser and user.ishtaruser.show_field_number()
    if label:
        label = _(label)
    return {'field': field, 'required': required, 'label': label,
            'show_field_number': show_field_number}


@register.inclusion_tag('blocks/table_field.html')
def table_field(field, required=False, label=None):
    if label:
        label = _(label)
    return {'field': field, 'required': required, 'label': label}
