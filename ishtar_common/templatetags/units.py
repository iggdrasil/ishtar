#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.template import Library

register = Library()


@register.filter
def m2_to_ha(value):
    try:
        value = float(value)/10000
    except (ValueError, TypeError):
        return ""
    return "%.2f" % round(value, 2)

