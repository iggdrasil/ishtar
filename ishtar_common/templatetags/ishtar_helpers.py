#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from django.template import Library
from django.utils.safestring import mark_safe

register = Library()


@register.filter()
def is_numeric(value):
    if isinstance(value, (int, float)):
        return True
    elif not isinstance(value, str):
        return False
    return value.isdigit()


@register.filter
def bs_field(input_field):
    input_field = str(input_field)
    if not (input_field.startswith("<input") or input_field.startswith("<select")) or \
            "form-control" in input_field or "<script" in input_field:
        return input_field
    soup = BeautifulSoup(str(input_field), "lxml")
    for tag in ["input", "select"]:
        field = soup.find(tag)
        if not field:
            continue
        if "class" in field.attrs.keys():
            cls = field["class"]
            cls.append("form-control")
            cls = " ".join(field["class"])
        else:
            cls = "form-control"
        field["class"] = cls
        input_field = str(field)
        break
    return input_field


@register.filter
def or_(value1, value2):
    return value1 or value2


@register.filter
def and_(value1, value2):
    return value1 and value2


@register.filter
def safe_or(item, args):
    if not item:
        return False
    result = False
    for arg in args.split("|"):
        current_item = item
        for sub in arg.split("."):
            if isinstance(current_item, dict):
                if sub not in current_item:
                    result = False
                    break
                current_item = current_item[sub]
            else:
                if not hasattr(current_item, sub) or not getattr(current_item, sub):
                    result = False
                    break
                current_item = getattr(current_item, sub)
        if callable(current_item):
            result = current_item()
        if result:
            return True
    return False


@register.filter
def file_content(value):
    if value:
        try:
            return mark_safe(value.read())
        except FileNotFoundError:
            pass
    return ""


@register.filter
def is_locked(item, user):
    if not hasattr(item, "is_locked"):
        return False
    return item.is_locked(user)


@register.filter
def can_edit_item(item, context):
    if hasattr(context, "request"):  # WSGIRequest
        request = context.request
    elif "request" in context:  # RequestContext
        request = context['request']
    else:
        request = context
    if item.can_edit(request):
        return True
