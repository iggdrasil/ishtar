import os

from django import template
from django.template import loader
from django.utils.translation import ugettext as _
from django.utils.safestring import mark_safe

from ishtar_common.models import HistoryModel
from ishtar_common.templatetags.link_to_window import link_to_window

register = template.Library()


@register.inclusion_tag('ishtar/blocks/window_field.html')
def field(caption, data, pre_data='', post_data='', li=False, size=None):
    if data is True:
        data = mark_safe(
            '<i class="text-success fa fa-check" title="{}"></i>'.format(
                _("Yes"))
        )
    elif data is False:
        data = mark_safe(
            '<i class="text-danger fa fa-times" title="{}"></i>'.format(
                _("No"))
        )
    # fix bad conversion
    if data == 'None':
        data = ""
    return {'caption': caption, 'data': data, "pre_data": pre_data,
            'post_data': post_data, 'li': li, "size": size}


@register.inclusion_tag('ishtar/blocks/window_field.html')
def field_li(caption, data, pre_data='', post_data=''):
    return field(caption, data, pre_data, post_data, li=True)


@register.inclusion_tag('ishtar/blocks/window_field_flex.html')
def field_flex(caption, data, pre_data='', post_data='', small=False):
    size = None
    if small:
        size = 2
    return field(caption, data, pre_data, post_data, size=size)


@register.inclusion_tag('ishtar/blocks/window_field_flex.html')
def field_flex_2(caption, data, pre_data='', post_data=''):
    return field(caption, data, pre_data, post_data, size=2)


@register.inclusion_tag('ishtar/blocks/window_field_flex_full.html')
def field_flex_full(caption, data, pre_data='', post_data='', small=False):
    size = None
    if small:
        size = 2
    return field(caption, data, pre_data, post_data, size=size)


@register.inclusion_tag('ishtar/blocks/window_field_url.html')
def field_url(caption, link, link_name='', li=False, get_base_url=False):
    if link:
        link = link.strip()
        if not link.startswith('http://') and not link.startswith('https://'):
            link = 'http://' + link
    return {'caption': caption, 'link': link, "link_name": link_name, 'li': li,
            'get_base_url': get_base_url}


@register.inclusion_tag('ishtar/blocks/window_field_url.html')
def field_li_url(caption, link, link_name=''):
    return field_url(caption, link, link_name, li=True)


@register.inclusion_tag('ishtar/blocks/window_field_flex_url.html')
def field_flex_url(caption, link, link_name=''):
    return field_url(caption, link, link_name)


@register.inclusion_tag('ishtar/blocks/window_field_url.html',
                        takes_context=True)
def field_file(context, caption, link):
    link_name = ""
    if link and getattr(link, 'path', None):
        link = context['BASE_URL'] + link.url
        link_name = link.split(os.sep)[-1]
    return field_url(caption, link, link_name, get_base_url=True)


@register.inclusion_tag('ishtar/blocks/window_field_flex_file.html',
                        takes_context=True)
def field_flex_file(context, caption, link):
    return field_file(context, caption, link)


@register.inclusion_tag('ishtar/blocks/window_field_multiple.html')
def field_multiple(caption, data, li=False, size=None):
    return {'caption': caption, 'data': data, 'li': li, "size": size}


@register.simple_tag
def field_multiple_obj(caption, item, attr, li=False, size=None):
    data, external = "", False
    if isinstance(item, dict):
        if attr in item:
            data = " ; ".join(item[attr])
            if data:
                external = True
    else:
        data = getattr(item, attr) if hasattr(item, attr) else ""
    if not hasattr(item, '_step') or attr not in item.history_m2m \
            or not item.history_m2m[attr]:
        t = loader.get_template('ishtar/blocks/window_field_flex_multiple.html')
        return t.render(
            {'caption': caption, 'data': data, 'li': li, "size": size,
             "external": external}
        )
    field = getattr(item.instance.__class__, attr)
    if hasattr(field, "remote_field"):
        rel_model = field.remote_field.model
    else:
        rel_model = field.rel.model
    data = rel_model.history_decompress(item.history_m2m[attr])
    t = loader.get_template(
        'ishtar/blocks/window_field_flex_historized_multiple.html')
    return t.render(
        {'caption': caption, 'data': data, 'li': li, "size": size}
    )


@register.inclusion_tag('ishtar/blocks/window_field_multiple.html')
def field_li_multiple(caption, data):
    return field_multiple(caption, data, li=True)


@register.inclusion_tag('ishtar/blocks/window_field_flex_multiple.html')
def field_flex_multiple(caption, data, small=False):
    size = None
    if small:
        size = 2
    return field_multiple(caption, data, size=size)


@register.simple_tag
def field_flex_multiple_obj(caption, item, attr, small=False):
    size = None
    if small:
        size = 2
    return field_multiple_obj(caption, item, attr, size=size)


@register.inclusion_tag('ishtar/blocks/window_field_flex_multiple_full.html')
def field_flex_multiple_full(caption, data, small=False):
    size = None
    if small:
        size = 2
    return field_multiple(caption, data, size=size)


@register.inclusion_tag('ishtar/blocks/window_field_detail.html',
                        takes_context=True)
def field_detail(context, caption, item, li=False, size=None):
    if isinstance(item, dict):
        for k in ("cached_label", "label", "common_name"):
            if k in item:
                item = item[k]
                break
    return {'caption': caption, 'item': item,
            'link': link_to_window(item, context),
            'li': li, 'size': size}


@register.inclusion_tag('ishtar/blocks/window_field_detail.html',
                        takes_context=True)
def field_li_detail(context, caption, item):
    return field_detail(context, caption, item, li=True)


@register.inclusion_tag('ishtar/blocks/window_field_flex_detail.html',
                        takes_context=True)
def field_flex_detail(context, caption, item, size=None):
    if size == "large":
        size = "full"
    elif size:
        size = 2
    return field_detail(context, caption, item, size=size)


@register.inclusion_tag('ishtar/blocks/window_field_flex_detail.html',
                        takes_context=True)
def field_flex_detail_full(context, caption, item, small=False):
    size = "full"
    return field_detail(context, caption, item, size=size)


@register.inclusion_tag('ishtar/blocks/window_field_flex_detail_multiple.html',
                        takes_context=True)
def field_flex_detail_multiple(context, caption, items, small=False, size=None):
    if small:
        size = 2
    data = []
    if hasattr(items, "all"):
        items = items.distinct().all()
    for item in items:
        link = link_to_window(item, context)
        data.append({"item": item, "link": link})
    dct = {'caption': caption, 'data': data, "size": size}
    return dct


@register.inclusion_tag(
    'ishtar/blocks/window_field_flex_detail_multiple_full.html',
    takes_context=True)
def field_flex_detail_multiple_full(context, caption, items):
    size = "full"
    return field_flex_detail_multiple(context, caption, items, size=size)


@register.filter
def m2m_listing(item, key):
    if isinstance(item, HistoryModel):
        return item.m2m_listing(key)
    if hasattr(item, "m2m_listing_" + key):
        return getattr(item, "m2m_listing_" + key)()
    if isinstance(item, dict):
        if key in item:
            return item[key]
        return []
    return item.m2m_listing(key)
