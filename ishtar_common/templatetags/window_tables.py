import json
import time

from django import template
from django.conf import settings
from django.template.defaultfilters import slugify
from django.template.loader import get_template
from django.urls import resolve
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

from ishtar_common.forms import reverse_lazy
from ishtar_common.widgets import DataTable

from ishtar_common.models import Document
from archaeological_files.models import File
from archaeological_operations.models import Operation, ArchaeologicalSite, \
    AdministrativeAct
from archaeological_context_records.models import ContextRecord, \
    RecordRelationView, RecordRelations as CRRecordRelations
from archaeological_finds.models import Find, FindUpstreamTreatments, \
    FindDownstreamTreatments, FindTreatments, TreatmentFile, Treatment, \
    FindInsideContainer
from archaeological_warehouse.models import Container, Warehouse


register = template.Library()


@register.inclusion_tag('ishtar/blocks/window_tables/documents.html')
def table_document(caption, data):
    return {'caption': caption, 'data': data}


ASSOCIATED_MODELS = {}
ASSOCIATED_MODELS['files'] = (File, 'get-file', '')
ASSOCIATED_MODELS['operations'] = (Operation, 'get-operation', '')
ASSOCIATED_MODELS['admin_acts'] = (AdministrativeAct,
                                   'get-administrativeact', '')
ASSOCIATED_MODELS['context_records'] = (ContextRecord, 'get-contextrecord',
                                        'get-contextrecord-full')
ASSOCIATED_MODELS['context_records_for_ope'] = (
    ContextRecord,
    'get-contextrecord-for-ope', 'get-contextrecord-full')
ASSOCIATED_MODELS['context_records_relations'] = (
    RecordRelationView, 'get-contextrecordrelation', '')
ASSOCIATED_MODELS['context_records_relations_detail'] = (
    CRRecordRelations, 'get-contextrecordrelationdetail', '')

ASSOCIATED_MODELS['finds'] = (Find, 'get-find', 'get-find-full')
ASSOCIATED_MODELS['sites'] = (ArchaeologicalSite, 'get-site', '')
ASSOCIATED_MODELS['finds_for_ope'] = (
    Find, 'get-find-for-ope', 'get-find-full')
ASSOCIATED_MODELS['finds_for_cr'] = (
    Find, 'get-find-for-cr', 'get-find-full')
ASSOCIATED_MODELS['finds_for_treatment'] = (
    Find, 'get-find-for-treatment', 'get-find-full')
ASSOCIATED_MODELS['finds_upstreamtreatments'] = (
    FindUpstreamTreatments, 'get-upstreamtreatment', '')
ASSOCIATED_MODELS['finds_downstreamtreatments'] = (
    FindDownstreamTreatments, 'get-downstreamtreatment', '')
ASSOCIATED_MODELS['finds_inside_container'] = (
    FindInsideContainer, 'get-find-inside-container', '')
ASSOCIATED_MODELS['treatments'] = (
    FindTreatments, 'get-treatment', '')
ASSOCIATED_MODELS['base_treatments'] = (
    Treatment, 'get-treatment', '')
ASSOCIATED_MODELS['treatment_files'] = (TreatmentFile, 'get-treatmentfile', '')
ASSOCIATED_MODELS['containers'] = (Container, 'get-container', '')
ASSOCIATED_MODELS['divisions'] = (Container, 'get-divisions-container', '')
ASSOCIATED_MODELS['non-divisions'] = (
    Container, 'get-non-divisions-container', '')
ASSOCIATED_MODELS['warehouses'] = (Warehouse, 'get-warehouse', '')

ASSOCIATED_MODELS['documents'] = (Document, 'get-document', '')
ASSOCIATED_MODELS['documents_inside_container'] = (Document, 'get-document', '')


@register.simple_tag(takes_context=True)
def dynamic_table_document(
        context, caption, associated_model, key, value,
        table_cols='TABLE_COLS', output='html', large=True,
        col_prefix=''):
    if not table_cols:
        table_cols = 'TABLE_COLS'
    model, url, url_full = ASSOCIATED_MODELS[associated_model]
    grid = DataTable(None, None, model, table_cols=table_cols,
                     col_prefix=col_prefix)
    source = str(reverse_lazy(url))
    source_full = str(reverse_lazy(url_full)) if url_full else ''
    source_attrs = mark_safe('?submited=1&{}={}'.format(key, value))
    if output == 'html':
        col_names, extra_cols = grid.get_cols()
        t = get_template('ishtar/blocks/window_tables/dynamic_documents.html')
        context = {
            'caption': caption,
            'name':  '{}{}{}'.format(slugify(caption), key,
                                     int(time.time() * 1000)),
            'source': source + source_attrs,
            'source_full': source_full,
            'simple_source': source,
            'source_attrs': source_attrs,
            'col_names': col_names,
            'extra_cols': extra_cols,
            'no_result': str(_("No results")),
            'loading': str(_("Loading...")),
            'encoding': settings.ENCODING or 'utf-8',
            'large': large
        }
        return t.render(context)
    else:
        col_names, extra_cols = grid.get_cols(python=True)
        view, args, kwargs = resolve(source)
        request = context['request']
        if source_attrs and source_attrs.startswith('?'):
            source_attrs = source_attrs[1:]
            # force to not paginate up to 100000 - should be enough
            dct = {'length': 100000}
            for attr in source_attrs.split('&'):
                if '=' in attr:
                    key, val = attr.split('=')
                    dct[key] = val
            request.GET = dct
        kwargs['request'] = request
        page = view(*args, **kwargs)
        data = []
        if page.content:
            res = json.loads(page.content.decode())
            if "rows" in res:
                for r in res["rows"]:
                    d = []
                    for col in extra_cols:
                        if col in r:
                            d.append(r[col])
                        else:
                            d.append('')
                    data.append(d)
        t = get_template('ishtar/blocks/window_tables/static_documents.html')
        context = {
            'caption': caption,
            'col_names': col_names,
            'data': data
        }
        return t.render(context)


@register.simple_tag(takes_context=True)
def dynamic_table_document_small(
        context, caption, associated_model, key,
        value, table_cols='TABLE_COLS', output='html'):
    return dynamic_table_document(
        context, caption, associated_model, key,
        value, table_cols, output, large=False)
