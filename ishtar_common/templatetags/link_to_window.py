#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.urls import reverse
from django.template import Library
from django.utils.safestring import mark_safe

register = Library()


@register.filter
def simple_link_to_window(item, caption=""):
    if not hasattr(item, 'SLUG'):
        return ""
    if caption:
        caption = " <small>{}</small>".format(caption)
    lock = ""
    if getattr(item, "locked", None):
        lock = '&nbsp;<i class="fa fa-lock text-danger" aria-hidden="true"></i>'
    return mark_safe(
        '&nbsp;<a class="display_details" href="#" '
        'onclick="load_window(\'{}\')">'
        '<i class="fa fa-info-circle" aria-hidden="true"></i>{}{}</a>'.format(
            reverse("show-" + item.SLUG, args=[item.pk, '']), lock, caption))


@register.filter
def link_to_window(item, context):
    if not hasattr(item, 'can_view'):  # no permission check
        return simple_link_to_window(item)
    if hasattr(context, "request"):  # WSGIRequest
        request = context.request
    elif "request" in context:  # RequestContext
        request = context['request']
    else:
        return ""
    if not item.can_view(request):
        return ""
    return simple_link_to_window(item)


@register.filter
def link_to_odt(item):
    return reverse(item.SHOW_URL, args=[item.pk, 'odt'])


@register.filter
def link_to_pdf(item):
    return reverse(item.SHOW_URL, args=[item.pk, 'pdf'])


@register.filter
def link_to_modify(item):
    if not hasattr(item, 'MODIFY_URL') or not item:
        return ""
    return reverse(item.MODIFY_URL, args=[item.pk])


@register.filter
def add_links(items, extra_attr=''):
    html = []
    if hasattr(items, 'all'):
        items = list(items.all())
    if not items:
        return []
    for item in items:
        item_lnk = item
        if extra_attr:
            item_lnk = getattr(item, extra_attr)
        lbl = ''
        if hasattr(item, 'fancy_str'):
            lbl = item.fancy_str()
        else:
            lbl = str(item)
        html.append("{} {}".format(lbl, simple_link_to_window(item_lnk)))
    return mark_safe("<br/>".join(html))


@register.inclusion_tag('ishtar/blocks/modify_toolbar.html',
                        takes_context=True)
def modify_toolbar(context, item, action):
    request = context.get('request')
    menu = context.get('MENU', None)
    print("TODO: link_to_window - check")
    if not menu:
        return {}
    items_by_idx = menu.items_by_idx.keys()
    if action not in items_by_idx:
        return {}
    action = menu.items_by_idx[action]
    user = request.user
    if not hasattr(user, 'ishtaruser') or \
            not action.is_available(user.ishtaruser, item):
        return {}
    return {'item': item}
