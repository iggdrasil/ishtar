#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django import template

register = template.Library()


@register.inclusion_tag('blocks/inline_formset.html')
def inline_formset(caption, formset, header=True, skip=False):
    """
    Render a formset as an inline table.
    For i18n of the caption be careful to add manually the caption label to
    the translated fields
    """
    return {'caption': caption, 'formset': formset, 'header': header,
            'skip': skip}
