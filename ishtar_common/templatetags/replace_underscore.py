#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.template import Library

register = Library()


@register.filter
def replace_underscore(value):
    return value.replace('-', '_')
