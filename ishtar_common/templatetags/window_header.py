from django import template
from django.utils.safestring import mark_safe

register = template.Library()


def sorted_actions(extra_actions):
    return [
        actions[:6]
        for actions in sorted(
            extra_actions,
            key=lambda x: len(x) > 6 and x[6] or 1)
    ]


@register.inclusion_tag('ishtar/blocks/window_nav.html', takes_context=True)
def window_nav(context, item, window_id, show_url, modify_url='', histo_url='',
               revert_url='', previous=None, nxt=None, pin_action=False):
    extra_actions = []
    if hasattr(item, 'get_extra_actions'):
        extra_actions = sorted_actions(
            item.get_extra_actions(context['request']))
    extra_templates = []
    if hasattr(item, 'get_extra_templates'):
        extra_templates = item.get_extra_templates(context['request'])

    slug = None
    if hasattr(item, "LONG_SLUG"):
        slug = item.LONG_SLUG
    elif hasattr(item, "SLUG"):
        slug = item.SLUG
    modify_url_value, delete_url = None, None
    can_edit = hasattr(item, 'can_do') and slug and \
            item.can_do(context['request'], 'change_' + slug)
    if can_edit:
        modify_url_value = modify_url
        delete_url = getattr(item, "DELETE_URL", False)
    return {
        'current_user': context['request'].user,
        'show_url': show_url,
        'modify_url': modify_url_value,
        'delete_url': delete_url,
        'histo_url': histo_url,
        'revert_url': revert_url,
        'item': item,
        'window_id': window_id,
        'window_id_underscore': window_id.replace("-", "_"),
        'previous': previous,
        'next': nxt,
        'extra_actions': extra_actions,
        'pin_action': pin_action,
        'extra_templates': extra_templates,
    }


@register.inclusion_tag('ishtar/blocks/window_find_nav.html',
                        takes_context=True)
def window_find_nav(context, item, window_id, show_url, modify_url='',
                    histo_url='', revert_url='', previous=None, nxt=None,
                    pin_action=False, baskets=None):
    dct = window_nav(context, item, window_id, show_url, modify_url=modify_url,
                     histo_url=histo_url, revert_url=revert_url,
                     previous=previous, nxt=nxt, pin_action=pin_action)
    dct['baskets'] = baskets
    return dct


@register.inclusion_tag('ishtar/blocks/window_file_nav.html',
                        takes_context=True)
def window_file_nav(context, item, window_id, previous=None, nxt=None):
    extra_actions = []
    if hasattr(item, 'get_extra_actions'):
        extra_actions = sorted_actions(
            item.get_extra_actions(context['request']))
    show_url = 'show-file'
    modify_url = 'file_modify'
    histo_url = 'show-historized-file'
    revert_url = 'revert-file'
    add_operation = ""
    modify_url_value, delete_url = None, None
    can_edit = hasattr(item, 'can_do') and  \
               item.can_do(context['request'], 'change_file')
    if can_edit:
        modify_url_value = modify_url
        delete_url = getattr(item, "DELETE_URL", False)
    return {
        'show_url': show_url,
        'modify_url': modify_url_value,
        'delete_url': delete_url,
        'histo_url': histo_url,
        'revert_url': revert_url,
        'item': item,
        'extra_action': mark_safe(add_operation),
        'window_id': window_id,
        'window_id_underscore': window_id.replace("-", "_"),
        'previous': previous,
        'next': nxt,
        'extra_actions': extra_actions,
        'pin_action': True}
