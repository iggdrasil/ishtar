#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import datetime
import sys
import json

from django.core.management.base import BaseCommand

from django.contrib.auth.models import User
from archaeological_operations.models import Operation, ArchaeologicalSite
from archaeological_context_records.models import ContextRecord
from archaeological_finds.models import Find
from archaeological_warehouse.models import Warehouse, Container
from ishtar_common.models import Document


class Command(BaseCommand):
    args = ""
    help = "Json export of sums"

    def handle(self, *args, **options):
        data = {
            "date": datetime.datetime.now().isoformat(),
            "finds": Find.objects.count(),
            "operations": Operation.objects.count(),
            "operations_with_finds": Operation.objects.filter(
                context_record__base_finds__pk__isnull=False
            ).values("id").distinct().count(),
            "context_records": ContextRecord.objects.count(),
            "sites": ArchaeologicalSite.objects.count(),
            "warehouses": Warehouse.objects.count(),
            "containers": Container.objects.filter(
                container_type__stationary=False).count(),
            "users": User.objects.filter(is_active=True).count(),
            "documents": Document.objects.count(),
            "documents_with_images":
                Document.objects.filter(image__isnull=False).exclude(image='').count(),
        }
        sys.stdout.write(json.dumps(data))

