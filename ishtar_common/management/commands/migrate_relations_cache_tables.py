#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2021 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

import sys

from django.core.management.base import BaseCommand

from django.apps import apps


CACHE_TABLES = [
    "archaeological_context_records.ContextRecordTree"
]


class Command(BaseCommand):
    args = ''
    help = 'Update all relations for cache tables'

    def add_arguments(self, parser):
        parser.add_argument('table', nargs='?', default=None,
                            choices=CACHE_TABLES)
        parser.add_argument(
            '--quiet', dest='quiet', action='store_true',
            help='Quiet output')

    def handle(self, *args, **options):
        quiet = options['quiet']
        tables = CACHE_TABLES
        if options.get("table", None):
            table = options.get("table", None)
            if table not in CACHE_TABLES:
                sys.stdout.write("{} not a valid cache table\n".format(table))
                return
            tables = [table]
        for table in tables:
            if not quiet:
                print("* table: {}".format(table))
            app, tablename = table.split(".")
            model = apps.get_app_config(app).get_model(tablename)
            model.regenerate_all(quiet=quiet)
            if not quiet:
                sys.stdout.write("\n")
