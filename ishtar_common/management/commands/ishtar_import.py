#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError

from ishtar_common import models, models_imports


class Command(BaseCommand):
    help = "./manage.py ishtar_import <command> <import_id>\n\n"\
           "Launch the importation a configured import.\n"\
           "<command> must be: \"list\", \"analyse\", \"import\" or " \
           "\"archive\"."

    def add_arguments(self, parser):
        parser.add_argument('command', choices=["list", "analyse", "import",
                                                "archive"])
        parser.add_argument('import_id', nargs='?', default=None)

    def handle(self, *args, **options):
        command = options['command']
        import_id = options['import_id']
        if command != "list" and not import_id:
            raise CommandError("With {} <import_id> is mandatory".format(
                command))
        if command == 'list':
            state = dict(models_imports.IMPORT_STATE)
            self.stdout.write("*" * 80 + "\n")
            self.stdout.write(
                "|  pk  |               type               |     state    "
                "|        name\n")
            self.stdout.write("*" * 80 + "\n")
            for imp in models.Import.objects.exclude(state="AC").all():
                self.stdout.write("|{: ^6}| {: ^32} | {: ^12} | {}\n".format(
                    imp.pk, str(imp.importer_type)[:32],
                    state[imp.state][:12],
                    imp.name[:128]))
            self.stdout.write("*" * 80 + "\n")
            self.stdout.flush()
            return
        try:
            imp = models.Import.objects.get(pk=args[1])
        except (ValueError, models.Import.DoesNotExist):
            raise CommandError("{} is not a valid import ID".format(args[0]))
        if command == 'analyse':
            imp.initialize()
            self.stdout.write("* {} analysed\n".format(imp))
            self.stdout.flush()
        elif command == 'import':
            self.stdout.write("* import {}\n".format(imp))
            imp.importation()
            self.stdout.write("* {} imported\n".format(imp))
            self.stdout.flush()
        elif command == 'archive':
            imp.archive()
            self.stdout.write("*{} archived\n".format(imp))
            self.stdout.flush()
