#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2014  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

import sys

from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ObjectDoesNotExist

import ishtar_common.models as models


class Command(BaseCommand):
    args = ''
    help = 'Regenerate merge candidates'

    def handle(self, *args, **options):
        for model in [models.Person, models.Organization]:
            sys.stdout.write('\n* %s treatment\n' % str(model))
            q = model.objects
            total = q.count()
            for idx, item in enumerate(q.all()):
                sys.stdout.write('\r\t %d/%d' % (idx, total))
                sys.stdout.flush()
                item.generate_merge_candidate()
        sys.stdout.write('\nSuccessfully generation of merge candidates\n')
