#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2022  Étienne Loks <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.core.management.base import BaseCommand
from optparse import make_option
import sys

from ishtar_common.models import GeoVectorData, IshtarSiteProfile
from ishtar_common.utils import BColors


class Command(BaseCommand):
    help = "Update SRID - change from profile"
    option_list = BaseCommand.option_list + (
        make_option('--quiet',
                    action='store_true',
                    dest='quiet',
                    default=False,
                    help='Proceed silently with no interactive input.'),
    )

    def interactive_start(self):
        sys.stdout.write(
            BColors.HEADER + BColors.BOLD +
            "Update to new SRID\n")
        sys.stdout.write(BColors.ENDC)
        yes = None
        while yes != "yes":
            sys.stdout.write(
                "Are you sure you want to proceed? (yes/[n])\n")
            yes = input()
            if not yes or yes == "n":
                sys.stdout.write(BColors.FAIL + "Aborting\n")
                sys.stdout.write(BColors.ENDC)
                sys.exit()

    def handle(self, *args, **options):
        quiet = options['quiet']
        if not quiet:
            self.interactive_start()
        profile = IshtarSiteProfile.get_current_profile()
        if not profile.srs or not profile.srs.srid:
            sys.stdout.write(BColors.FAIL + "No SRS set\n")
            sys.stdout.write(BColors.ENDC)
        GeoVectorData.migrate_srid(profile.srs.srid)

