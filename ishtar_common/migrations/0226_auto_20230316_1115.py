# Generated by Django 2.2.24 on 2023-03-16 11:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0225_migrate_created'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='customformjsonfield',
            options={'verbose_name': 'Custom forms - Json data field', 'verbose_name_plural': 'Custom forms - Json data fields'},
        ),
        migrations.AlterModelOptions(
            name='importermodel',
            options={'ordering': ('name',), 'verbose_name': 'Data model', 'verbose_name_plural': 'Data models'},
        ),
        migrations.AlterModelOptions(
            name='ishtaruser',
            options={'ordering': ('person',), 'verbose_name': 'Ishtar user', 'verbose_name_plural': 'Ishtar users'},
        ),
        migrations.AlterModelOptions(
            name='itemkey',
            options={'verbose_name': 'Importer - Item key', 'verbose_name_plural': 'Imports - Item keys'},
        ),
        migrations.AddField(
            model_name='customform',
            name='header',
            field=models.TextField(default='', help_text='You can use markdown syntax.', verbose_name='Header text'),
        ),
        migrations.AddField(
            model_name='ishtarsiteprofile',
            name='footer',
            field=models.TextField(default='', help_text='You can use markdown syntax.', verbose_name='Footer text'),
        ),
        migrations.AlterField(
            model_name='ishtarsiteprofile',
            name='basefind_cached_label',
            field=models.TextField(blank=True, default='', help_text='Formula to manage cached label. If not set a default formula is used.', verbose_name='Base find cached label'),
        ),
        migrations.AlterField(
            model_name='ishtarsiteprofile',
            name='container_cached_label',
            field=models.TextField(blank=True, default='', help_text='Formula to manage cached label. If not set a default formula is used.', verbose_name='Container cached label'),
        ),
        migrations.AlterField(
            model_name='ishtarsiteprofile',
            name='contextrecord_cached_label',
            field=models.TextField(blank=True, default='', help_text='Formula to manage cached label. If not set a default formula is used.', verbose_name='Context record cached label'),
        ),
        migrations.AlterField(
            model_name='ishtarsiteprofile',
            name='document_cached_label',
            field=models.TextField(blank=True, default='', help_text='Formula to manage cached label. If not set a default formula is used.', verbose_name='Document cached label'),
        ),
        migrations.AlterField(
            model_name='ishtarsiteprofile',
            name='file_cached_label',
            field=models.TextField(blank=True, default='', help_text='Formula to manage cached label. If not set a default formula is used.', verbose_name='File cached label'),
        ),
        migrations.AlterField(
            model_name='ishtarsiteprofile',
            name='find_cached_label',
            field=models.TextField(blank=True, default='', help_text='Formula to manage cached label. If not set a default formula is used.', verbose_name='Find cached label'),
        ),
        migrations.AlterField(
            model_name='ishtarsiteprofile',
            name='operation_cached_label',
            field=models.TextField(blank=True, default='', help_text='Formula to manage cached label. If not set a default formula is used.', verbose_name='Operation cached label'),
        ),
        migrations.AlterField(
            model_name='ishtarsiteprofile',
            name='parcel_cached_label',
            field=models.TextField(blank=True, default='', help_text='Formula to manage cached label. If not set a default formula is used.', verbose_name='Parcel cached label'),
        ),
        migrations.AlterField(
            model_name='ishtarsiteprofile',
            name='parent_relations_engine',
            field=models.CharField(choices=[('V', 'SQL views'), ('T', 'Cache tables')], default='V', help_text='If you experience performance problems with complex relations (for instance: complex statigraphic relations), set it to "Cache tables" in order to use static cache tables. Do not forget to update theses table with the "migrate_relations_cache_tables" manage.py command.', max_length=1, verbose_name='Parent relations engine'),
        ),
        migrations.AlterField(
            model_name='ishtarsiteprofile',
            name='site_cached_label',
            field=models.TextField(blank=True, default='', help_text='Formula to manage cached label. If not set a default formula is used.', verbose_name='Site cached label'),
        ),
        migrations.AlterField(
            model_name='ishtarsiteprofile',
            name='srs',
            field=models.ForeignKey(blank=True, help_text='Set it to the most used spatial reference system. Warning: after change launch the migrate_srid script.', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='profile_srs', to='ishtar_common.SpatialReferenceSystem', verbose_name='Spatial Reference System in database'),
        ),
        migrations.AlterField(
            model_name='ishtarsiteprofile',
            name='warehouse_cached_label',
            field=models.TextField(blank=True, default='', help_text='Formula to manage cached label. If not set a default formula is used.', verbose_name='Warehouse cached label'),
        ),
    ]
