# -*- coding: utf-8 -*-
# Generated by Django 1.11.27 on 2020-12-07 23:37
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0206_auto_20201206_2316'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='persontype',
            name='grammatical_gender',
        ),
        migrations.AddField(
            model_name='titletype',
            name='grammatical_gender',
            field=models.CharField(blank=True, choices=[('M', 'Male'), ('F', 'Female'), ('N', 'Neutral')], default='', max_length=1, verbose_name='Grammatical gender'),
        ),
    ]
