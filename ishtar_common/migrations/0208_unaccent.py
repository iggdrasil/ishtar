# -*- coding: utf-8 -*-
# Generated by Django 1.11.27 on 2020-12-29 11:50
from django.db import migrations

from django.contrib.postgres.operations import UnaccentExtension

class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0207_auto_20201207_2337'),
    ]

    operations = [
        UnaccentExtension()
    ]
