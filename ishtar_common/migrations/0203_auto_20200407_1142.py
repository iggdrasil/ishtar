# -*- coding: utf-8 -*-
# Generated by Django 1.11.27 on 2020-04-07 11:42
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0202_auto_20200129_1941'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ishtarsiteprofile',
            name='container_external_id',
            field=models.TextField(default='{parent_external_id}-{container_type__txt_idx}-{reference}', help_text='Formula to manage container external ID. Change this with care. With incorrect formula, the application might be unusable and import of external data can be destructive.', verbose_name='Container external id'),
        ),
    ]
