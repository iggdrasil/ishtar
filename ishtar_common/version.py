# 4.0.80
VERSION = (4, 0, 80)


def get_version():
    return ".".join((str(num) for num in VERSION))


__version__ = get_version()
