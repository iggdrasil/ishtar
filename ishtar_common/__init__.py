#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2014-2015  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

from django.utils.translation import ugettext_lazy as _

# overload of translation of registration module
_("username")
_("email address")
_("Related item")

default_app_config = "ishtar_common.apps.IshtarCommonConfig"
