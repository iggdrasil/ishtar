Ishtar
======

|build status| |doc status|

Description
-----------

Ishtar is a database project for managing archaeological data and
documentation (including archaeological finds) from archaeological
operations, published as a free software under AGPL 3.0 or any later
version license.

The aim is to ensure maximum traceability of the information, in order
to make this documentation live and make it even possibly accessible to
the public or to user groups.

This software is intended to be installed on a web server but can
operate locally, at the scale of an excavation site, a town or an entire
region.

Designed to enable inter-database communication, the Ishtar project aims
rather to a distributed information model than a centralized one:
communication between the databases is favored.

It is organized around a common core associated with modules linked to
specific professional needs: administration of operations and
inventories, archaeological warehouses, treatments related to
restoration laboratories, advanced stratigraphic analysis, QR-code
labeling, etc.

Multiple levels of users are possible, from a public access (or not) to
access for researchers, operations managers, warehouses managers, GIS
connection, etc.

A full description of this project and news about Ishtar are available
on this `website <http://www.ishtar-archeo.net>`__.

Contribution
------------

Ishtar is in constant development. Ishtar is a participatory project, so
do not hesitate to share your needs on the
`forum <http://forum.ishtar-archeo.net>`__ and we'll see what we can do!

Docs
----

Documentation is not yet available but help can be provided on the
`forum <http://forum.ishtar-archeo.net>`__.

.. |build status| image:: https://gitlab.com/iggdrasil/ishtar/badges/main/build.svg
   :target: https://gitlab.com/iggdrasil/ishtar/commits/main
.. |doc status| image:: https://readthedocs.org/projects/ishtar/badge/?version=latest
   :target: http://ishtar.readthedocs.io/fr/main/
