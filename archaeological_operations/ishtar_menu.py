#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2012-2014 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from ishtar_common.utils import ugettext_lazy as _, pgettext_lazy

from ishtar_common.menu_base import SectionItem, MenuItem

from ishtar_common.models import IshtarSiteProfile
from archaeological_operations import models

# be careful: each access_controls must be relevant with check_rights in urls


MENU_SECTIONS = [
    (
        30,
        SectionItem(
            "operation_management",
            _("Operation"),
            css="menu-operation",
            childs=[
                MenuItem(
                    "operation_search",
                    _("Search"),
                    model=models.Operation,
                    access_controls=["view_operation", "view_own_operation"],
                ),
                MenuItem(
                    "operation_creation",
                    _("Creation"),
                    model=models.Operation,
                    access_controls=["add_operation", "add_own_operation"],
                ),
                MenuItem(
                    "operation_modification",
                    _("Modification"),
                    model=models.Operation,
                    access_controls=["change_operation", "change_own_operation"],
                ),
                MenuItem(
                    "operation_closing",
                    _("Closing"),
                    model=models.Operation,
                    access_controls=["close_operation"],
                ),
                MenuItem(
                    "operation_deletion",
                    _("Deletion"),
                    model=models.Operation,
                    access_controls=["change_operation", "change_own_operation"],
                ),
                SectionItem(
                    "admin_act_operations",
                    _("Administrative act"),
                    profile_restriction="files",
                    childs=[
                        MenuItem(
                            "operation_administrativeactop_search",
                            _("Search"),
                            model=models.AdministrativeAct,
                            access_controls=["change_administrativeact"],
                        ),
                        MenuItem(
                            "operation_administrativeactop",
                            _("Creation"),
                            model=models.AdministrativeAct,
                            access_controls=["change_administrativeact"],
                        ),
                        MenuItem(
                            "operation_administrativeactop_modification",
                            _("Modification"),
                            model=models.AdministrativeAct,
                            access_controls=["change_administrativeact"],
                        ),
                        MenuItem(
                            "operation_administrativeactop_deletion",
                            _("Deletion"),
                            model=models.AdministrativeAct,
                            access_controls=["change_administrativeact"],
                        ),
                    ],
                ),
            ],
        ),
    ),
    (
        35,
        SectionItem(
            "administrativact_management",
            _("Administrative Act"),
            profile_restriction="files",
            css="menu-file",
            childs=[
                MenuItem(
                    "administrativact_register",
                    pgettext_lazy("admin act register", "Register"),
                    model=models.AdministrativeAct,
                    access_controls=[
                        "view_administrativeact",
                        "view_own_administrativeact",
                    ],
                ),
            ],
        ),
    ),
    (
        37,
        SectionItem(
            "site_management",
            IshtarSiteProfile.get_default_site_label,
            css="menu-site",
            profile_restriction="archaeological_site",
            childs=[
                MenuItem(
                    "site_search",
                    _("Search"),
                    model=models.ArchaeologicalSite,
                    access_controls=[
                        "view_archaeologicalsite",
                        "view_own_archaeologicalsite",
                    ],
                ),
                MenuItem(
                    "site_creation",
                    _("Creation"),
                    model=models.ArchaeologicalSite,
                    access_controls=[
                        "add_archaeologicalsite",
                        "add_own_archaeologicalsite",
                    ],
                ),
                MenuItem(
                    "site_modification",
                    _("Modification"),
                    model=models.ArchaeologicalSite,
                    access_controls=[
                        "change_archaeologicalsite",
                        "change_own_archaeologicalsite",
                    ],
                ),
                MenuItem(
                    "site_deletion",
                    _("Deletion"),
                    model=models.ArchaeologicalSite,
                    access_controls=["change_archaeologicalsite"],
                ),
            ],
        ),
    ),
]
