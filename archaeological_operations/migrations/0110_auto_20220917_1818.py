# Generated by Django 2.2.24 on 2022-09-17 18:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_operations', '0109_auto_20220711_1025'),
    ]

    operations = [
        migrations.AlterField(
            model_name='administrativeact',
            name='ref_sra',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='Other reference'),
        ),
        migrations.AlterField(
            model_name='historicaladministrativeact',
            name='ref_sra',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='Other reference'),
        ),
    ]
