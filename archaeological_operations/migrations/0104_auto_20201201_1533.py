# -*- coding: utf-8 -*-
# Generated by Django 1.11.27 on 2020-12-01 15:33
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_operations', '0103_auto_20200129_1941'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='recordrelations',
            options={'ordering': ('left_record__cached_label', 'relation_type', 'right_record__cached_label'), 'permissions': [('view_operationrelation', 'Can view all Operation relations')], 'verbose_name': 'Operation record relation', 'verbose_name_plural': 'Operation record relations'},
        ),
        migrations.AddField(
            model_name='archaeologicalsite',
            name='complete_identifier',
            field=models.TextField(blank=True, null=True, verbose_name='Complete identifier'),
        ),
        migrations.AddField(
            model_name='archaeologicalsite',
            name='custom_index',
            field=models.IntegerField(blank=True, null=True, verbose_name='Custom index'),
        ),
        migrations.AddField(
            model_name='historicalarchaeologicalsite',
            name='complete_identifier',
            field=models.TextField(blank=True, null=True, verbose_name='Complete identifier'),
        ),
        migrations.AddField(
            model_name='historicalarchaeologicalsite',
            name='custom_index',
            field=models.IntegerField(blank=True, null=True, verbose_name='Custom index'),
        ),
        migrations.AddField(
            model_name='historicaloperation',
            name='complete_identifier',
            field=models.TextField(blank=True, null=True, verbose_name='Complete identifier'),
        ),
        migrations.AddField(
            model_name='historicaloperation',
            name='custom_index',
            field=models.IntegerField(blank=True, null=True, verbose_name='Custom index'),
        ),
        migrations.AddField(
            model_name='operation',
            name='complete_identifier',
            field=models.TextField(blank=True, null=True, verbose_name='Complete identifier'),
        ),
        migrations.AddField(
            model_name='operation',
            name='custom_index',
            field=models.IntegerField(blank=True, null=True, verbose_name='Custom index'),
        ),
        migrations.AlterField(
            model_name='relationtype',
            name='logical_relation',
            field=models.CharField(blank=True, choices=[('above', 'Above'), ('below', 'Below'), ('equal', 'Equal'), ('include', 'Include'), ('included', 'Is included')], max_length=10, null=True, verbose_name='Logical relation'),
        ),
    ]
