# -*- coding: utf-8 -*-
# Generated by Django 1.11.27 on 2020-12-04 15:57
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0205_auto_20201203_1453'),
        ('archaeological_operations', '0105_auto_20201203_1453'),
    ]

    operations = [
        migrations.AddField(
            model_name='administrativeact',
            name='documents',
            field=models.ManyToManyField(blank=True, related_name='administrativeacts', to='ishtar_common.Document', verbose_name='Documents'),
        ),
        migrations.AddField(
            model_name='administrativeact',
            name='main_image',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='main_image_administrativeacts', to='ishtar_common.Document', verbose_name='Main image'),
        ),
        migrations.AddField(
            model_name='historicaladministrativeact',
            name='main_image',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='ishtar_common.Document', verbose_name='Main image'),
        ),
    ]
