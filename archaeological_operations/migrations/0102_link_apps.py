# -*- coding: utf-8 -*-
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_operations', '0101_squashed'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicaladministrativeact',
            name='treatment',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='archaeological_finds.Treatment'),
        ),
        migrations.AddField(
            model_name='historicaladministrativeact',
            name='treatment_file',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='archaeological_finds.TreatmentFile'),
        ),
        migrations.AddField(
            model_name='administrativeact',
            name='treatment',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='administrative_act', to='archaeological_finds.Treatment', verbose_name='Treatment'),
        ),
        migrations.AddField(
            model_name='administrativeact',
            name='treatment_file',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='administrative_act', to='archaeological_finds.TreatmentFile', verbose_name='Treatment request'),
        ),
    ]
