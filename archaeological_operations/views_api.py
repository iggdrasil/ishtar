from ishtar_common.rest import SearchAPIView, FacetAPIView, GetAPIView, ExportAPIView
from archaeological_operations import models, forms


class FacetOperationAPIView(FacetAPIView):
    models = [models.Operation, models.ArchaeologicalSite]
    select_forms = [forms.OperationSelect, forms.SiteSelect]


class SearchOperationAPI(SearchAPIView):
    model = models.Operation


class SearchSiteAPI(SearchAPIView):
    model = models.ArchaeologicalSite


class ExportOperationAPI(ExportAPIView):
    model = models.Operation


class ExportSiteAPI(ExportAPIView):
    model = models.ArchaeologicalSite


class GetOperationAPI(GetAPIView):
    model = models.Operation


class GetSiteAPI(GetAPIView):
    model = models.ArchaeologicalSite
