#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2013-2016 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django import forms
from django.forms import widgets
from django.forms.utils import flatatt
from django.template import loader
from django.utils.safestring import mark_safe
from ishtar_common.utils import ugettext_lazy as _, get_current_profile


class ParcelWidget(widgets.MultiWidget):
    def __init__(self, attrs=None):
        if not attrs:
            attrs = {"class": "widget-parcel"}
        elif "class" not in attrs:
            attrs["class"] = "widget-parcel"
        else:
            attrs["class"] += " widget-parcel"
        _widgets = (
            widgets.TextInput(attrs=attrs),
            widgets.TextInput(attrs=attrs),
            widgets.CheckboxInput(),
        )
        super(ParcelWidget, self).__init__(_widgets, attrs)

    def decompress(self, value):
        if value:
            return value
        return [None, None]

    def format_output(self, rendered_widgets):
        return " / ".join(rendered_widgets)


class SelectParcelWidget(widgets.TextInput):
    def render(self, *args, **kwargs):
        render = super(SelectParcelWidget, self).render(*args, **kwargs)
        html = """{}
    <div class="input-group-append">
        <button class='input-group-text btn btn-success' name='formset_add'
            value='add'>{}</button>
    </div>""".format(
            render, _("Add")
        )
        return mark_safe(html)


class OAWidget(forms.TextInput):
    def render(self, name, value, attrs=None, renderer=None):
        if not value:
            value = ""
        attrs.update(self.attrs)
        if "class" not in attrs:
            attrs["class"] = "widget-oa"
        else:
            attrs["class"] += " widget-oa"
        final_attrs = flatatt(self.build_attrs(attrs, {"name": name, "value": value}))
        oa_prefix = get_current_profile(force=False).operation_prefix or ""
        dct = {
            "final_attrs": final_attrs,
            "id": attrs["id"],
            "safe_id": attrs["id"].replace("-", "_"),
            "oa_prefix": oa_prefix
        }
        t = loader.get_template("ishtar/blocks/OAWidget.html")
        rendered = t.render(dct)
        return mark_safe(rendered)
