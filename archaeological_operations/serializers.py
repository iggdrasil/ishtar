from django.db.models import Q

from ishtar_common.serializers_utils import generic_get_results, archive_serialization
from archaeological_operations import models


OPERATION_MODEL_LIST = [
    models.ArchaeologicalSite,
    models.Operation,
    models.RecordRelations,
    models.Parcel,
    models.ParcelOwner,
]

# TODO: administrativ acts, associated documents


def generate_warehouse_queryset(ids):
    base_query_key = "context_record__base_finds__find"

    q_archaeological_site, q_operation, q_record_relation = None, None, None

    for container_key in ("container", "container_ref"):
        for warehouse_key in ("location", "responsible"):
            q_s = Q(
                **{
                    "operations__{}__{}__{}__id__in".format(
                        base_query_key, container_key, warehouse_key
                    ): ids
                }
            )
            q_o = Q(
                **{
                    "{}__{}__{}__id__in".format(
                        base_query_key, container_key, warehouse_key
                    ): ids
                }
            )
            q_r = Q(
                **{
                    "left_record__{}__{}__{}__id__in".format(
                        base_query_key, container_key, warehouse_key
                    ): ids,
                    "right_record__{}__{}__{}__id__in".format(
                        base_query_key, container_key, warehouse_key
                    ): ids,
                }
            )
        if not q_archaeological_site:
            q_archaeological_site = q_s
            q_operation = q_o
            q_record_relation = q_r
        else:
            q_archaeological_site |= q_s
            q_operation |= q_o
            q_record_relation |= q_r

    result_queryset = {
        models.ArchaeologicalSite.__name__: models.ArchaeologicalSite.objects.filter(
            q_archaeological_site
        ),
        models.Operation.__name__: models.Operation.objects.filter(q_operation),
        models.RecordRelations.__name__: models.RecordRelations.objects.filter(
            q_record_relation
        ),
    }
    return result_queryset


def operation_serialization(
    archive=False,
    return_empty_types=False,
    archive_name=None,
    operation_queryset=None,
    site_queryset=None,
    cr_queryset=None,
    find_queryset=None,
    warehouse_queryset=None,
    get_queryset=False,
    no_geo=True,
    put_locks=False,
    lock_user=None,
):
    result_queryset = {}
    if operation_queryset:
        operation_ids = operation_queryset.values_list("id", flat=True)
        result_queryset = {
            models.Operation.__name__: operation_queryset,
            models.RecordRelations.__name__: models.RecordRelations.objects.filter(
                left_record__id__in=operation_ids,
                right_record__id__in=operation_ids,
            ),
            models.ArchaeologicalSite.__name__: models.ArchaeologicalSite.objects.filter(
                operations__id__in=operation_ids
            ),
        }
    elif site_queryset:
        site_ids = site_queryset.values_list("id", flat=True)
        result_queryset = {
            models.ArchaeologicalSite.__name__: site_queryset,
            models.Operation.__name__: models.Operation.objects.filter(
                archaeological_sites__id__in=site_ids
            ),
            models.RecordRelations.__name__: models.RecordRelations.objects.filter(
                left_record__archaeological_sites__id__in=site_ids,
                right_record__archaeological_sites__id__in=site_ids,
            ),
        }
    elif cr_queryset:
        cr_ids = cr_queryset.values_list("id", flat=True)
        result_queryset = {
            models.ArchaeologicalSite.__name__: models.ArchaeologicalSite.objects.filter(
                operations__context_record__id__in=cr_ids
            ),
            models.Operation.__name__: models.Operation.objects.filter(
                context_record__id__in=cr_ids
            ),
            models.RecordRelations.__name__: models.RecordRelations.objects.filter(
                left_record__context_record__id__in=cr_ids,
                right_record__context_record__id__in=cr_ids,
            ),
        }
    elif find_queryset:
        find_ids = find_queryset.values_list("id", flat=True)
        result_queryset = {
            models.ArchaeologicalSite.__name__: models.ArchaeologicalSite.objects.filter(
                operations__context_record__base_finds__find__id__in=find_ids
            ),
            models.Operation.__name__: models.Operation.objects.filter(
                context_record__base_finds__find__id__in=find_ids
            ),
            models.RecordRelations.__name__: models.RecordRelations.objects.filter(
                left_record__context_record__base_finds__find__id__in=find_ids,
                right_record__context_record__base_finds__find__id__in=find_ids,
            ),
        }
    elif warehouse_queryset:
        warehouse_ids = warehouse_queryset.values_list("id", flat=True)
        result_queryset = generate_warehouse_queryset(warehouse_ids)
    if get_queryset:
        return result_queryset

    result = generic_get_results(
        OPERATION_MODEL_LIST,
        "operations",
        result_queryset=result_queryset,
        no_geo=no_geo,
    )

    if put_locks:
        for model in OPERATION_MODEL_LIST:
            if not hasattr(model, "locked"):
                continue
            q = model.objects
            if result_queryset and model.__name__ in result_queryset:
                q = result_queryset[model.__name__]
            q.update(locked=True, lock_user=lock_user)

    full_archive = archive_serialization(
        result,
        archive_dir="operations",
        archive=archive,
        return_empty_types=return_empty_types,
        archive_name=archive_name,
    )
    return full_archive
