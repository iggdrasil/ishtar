from django import template

register = template.Library()


@register.inclusion_tag('ishtar/blocks/window_tables/administrativacts.html')
def table_administrativact(caption, data):
    return {'caption': caption, 'data': data}


@register.inclusion_tag('ishtar/blocks/window_tables/archaeologicalsites.html')
def table_archaeologicalsites(caption, data):
    return {'caption': caption, 'data': data}
