#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2010-2016 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.conf.urls import url
from django.urls import path

from ishtar_common.utils import check_rights
from archaeological_operations import views
from archaeological_operations import views_api
from archaeological_operations import models

# be carreful: each check_rights must be relevant with ishtar_menu

# forms
urlpatterns = [
    url(
        r"operation_administrativeactop_search/(?P<step>.+)?$",
        check_rights(["change_administrativeact"])(
            views.operation_administrativeactop_search_wizard
        ),
        name="operation_administrativeactop_search",
    ),
    url(
        r"operation_administrativeactop/(?P<step>.+)?$",
        check_rights(["change_administrativeact"])(
            views.operation_administrativeactop_wizard
        ),
        name="operation_administrativeactop",
    ),
    url(
        r"operation_administrativeactop_modification/(?P<step>.+)?$",
        check_rights(["change_administrativeact"])(
            views.operation_administrativeactop_modification_wizard
        ),
        name="operation_administrativeactop_modification",
    ),
    url(
        r"operation_administrativeactop_modify/(?P<pk>.+)/$",
        views.operation_administrativeactop_modify,
        name="operation_administrativeactop_modify",
    ),
    url(
        r"operation_administrativeactop_deletion/(?P<step>.+)?$",
        check_rights(["change_administrativeact"])(
            views.operation_administrativeactop_deletion_wizard
        ),
        name="operation_administrativeactop_deletion",
    ),
    url(
        r"operation_administrativeactop_delete/(?P<pk>.+)/$",
        views.operation_administrativeactop_delete,
        name="delete-administrativeact-operation",
    ),
    url(
        r"operation_search/(?P<step>.+)?$",
        check_rights(["view_operation", "view_own_operation"])(
            views.operation_search_wizard
        ),
        name="operation_search",
    ),
    url(
        r"operation_creation/(?P<step>.+)?$",
        check_rights(["add_operation", "add_own_operation"])(
            views.operation_creation_wizard
        ),
        name="operation_creation",
    ),
    url(r"operation_add/(?P<file_id>\d+)$", views.operation_add, name="operation_add"),
    url(
        r"operation_modification/(?P<step>.+)?$",
        check_rights(["change_operation", "change_own_operation"])(
            views.operation_modification_wizard
        ),
        name="operation_modification",
    ),
    url(
        r"operation_modify/(?P<pk>.+)/$",
        views.operation_modify,
        name="operation_modify",
    ),
    url(
        r"operation_closing/(?P<step>.+)?$",
        check_rights(["change_operation"])(views.operation_closing_wizard),
        name="operation_closing",
    ),
    url(
        r"operation_deletion/(?P<step>.+)?$",
        check_rights(["change_operation", "change_own_operation"])(
            views.operation_deletion_wizard
        ),
        name="operation_deletion",
    ),
    url(
        r"operation_delete/(?P<pk>.+)/$",
        views.operation_delete,
        name="delete-operation",
    ),
    url(
        r"administrativact_register/(?P<step>.+)?$",
        check_rights(["view_administrativeact", "view_own_administrativeact"])(
            views.administrativact_register_wizard
        ),
        name="administrativact_register",
    ),
    url(
        r"autocomplete-operation/$",
        views.autocomplete_operation,
        name="autocomplete-operation",
    ),
    url(
        r"get-operation/own/(?P<type>.+)?$",
        views.get_operation,
        name="get-own-operation",
        kwargs={"force_own": True},
    ),
    url(r"get-operation/(?P<type>.+)?$", views.get_operation, name="get-operation"),
    url(
        r"get-operation-full/own/(?P<type>.+)?$",
        views.get_operation,
        name="get-own-operation-full",
        kwargs={"full": True, "force_own": True},
    ),
    url(
        r"get-operation-full/(?P<type>.+)?$",
        views.get_operation,
        name="get-operation-full",
        kwargs={"full": True},
    ),
    url(
        r"get-operation-shortcut/(?P<type>.+)?$",
        views.get_operation,
        name="get-operation-shortcut",
        kwargs={"full": "shortcut"},
    ),
    url(
        r"get-available-operation-code/(?P<year>.+)?$",
        views.get_available_operation_code,
        name="get_available_operation_code",
    ),
    url(
        r"revert-operation/(?P<pk>.+)/(?P<date>.+)$",
        views.revert_operation,
        name="revert-operation",
    ),
    url(
        r"show-operation(?:/(?P<pk>.+))?/(?P<type>.+)?$",
        views.show_operation,
        name=models.Operation.SHOW_URL,
    ),
    url(
        r"show-historized-operation/(?P<pk>.+)?/(?P<date>.+)?$",
        views.show_operation,
        name="show-historized-operation",
    ),
    url(
        r"get-administrativeactop/(?P<type>.+)?$",
        views.get_administrativeactop,
        name="get-administrativeactop",
    ),
    url(
        r"get-administrativeact/(?P<type>.+)?$",
        views.get_administrativeact,
        name="get-administrativeact",
    ),
    url(
        r"get-administrativeact-full/(?P<type>.+)?$",
        views.get_administrativeact,
        name="get-administrativeact-full",
        kwargs={"full": True},
    ),
    url(
        r"show-administrativeact(?:/(?P<pk>.+))?/(?P<type>.+)?$",
        views.show_administrativeact,
        name="show-administrativeact",
    ),
    # allow specialization for operations
    url(
        r"show-administrativeact(?:/(?P<pk>.+))?/(?P<type>.+)?$",
        views.show_administrativeact,
        name="show-administrativeactop",
    ),
    # allow specialization for files, treatment, treatment request
    url(
        r"show-administrativeact(?:/(?P<pk>.+))?/(?P<type>.+)?$",
        views.show_administrativeact,
        name="show-administrativeactfile",
    ),
    url(
        r"show-administrativeact(?:/(?P<pk>.+))?/(?P<type>.+)?$",
        views.show_administrativeact,
        name="show-administrativeacttreatment",
    ),
    url(
        r"show-administrativeact(?:/(?P<pk>.+))?/(?P<type>.+)?$",
        views.show_administrativeact,
        name="show-administrativeacttreatmentfile",
    ),
    url(
        r"generatedoc-administrativeactop/(?P<pk>.+)?/(?P<template_pk>.+)?$",
        views.generatedoc_administrativeactop,
        name="generatedoc-administrativeactop",
    ),
    url(
        r"autocomplete-administrativeact/$",
        views.autocomplete_administrativeact,
        name="autocomplete-administrativeact",
    ),
    url(
        r"autocomplete-archaeologicalsite/$",
        views.autocomplete_archaeologicalsite,
        name="autocomplete-archaeologicalsite",
    ),
    url(
        r"new-archaeologicalsite/(?:(?P<parent_name>[^/]+)/)?"
        r"(?:(?P<limits>[^/]+)/)?$",
        views.new_archaeologicalsite,
        name="new-archaeologicalsite",
    ),
    url(r"get-site/(?P<type>.+)?$", views.get_site, name="get-site"),
    url(
        r"get-site-full/(?P<type>.+)?$",
        views.get_site,
        name="get-site-full",
        kwargs={"full": True},
    ),
    url(
        r"get-site-shortcut/(?P<type>.+)?$",
        views.get_site,
        name="get-site-shortcut",
        kwargs={"full": "shortcut"},
    ),
    url(r"revert-site/(?P<pk>.+)/(?P<date>.+)$", views.revert_site, name="revert-site"),
    url(
        r"show-site(?:/(?P<pk>.+))?/(?P<type>.+)?$",
        views.show_site,
        name=models.ArchaeologicalSite.SHOW_URL,
    ),
    url(
        r"show-historized-site/(?P<pk>.+)?/(?P<date>.+)?$",
        views.show_site,
        name="show-historized-site",
    ),
    url(
        r"site_search/(?P<step>.+)?$",
        check_rights(["view_archaeologicalsite", "view_own_archaeologicalsite"])(
            views.site_search_wizard
        ),
        name="site_search",
    ),
    url(
        r"site_creation/(?P<step>.+)?$",
        check_rights(["add_archaeologicalsite", "add_own_archaeologicalsite"])(
            views.site_creation_wizard
        ),
        name="site_creation",
    ),
    url(
        r"site_modification/(?P<step>.+)?$",
        check_rights(["change_archaeologicalsite", "change_own_archaeologicalsite"])(
            views.site_modification_wizard
        ),
        name="site_modification",
    ),
    url(r"site_modify/(?P<pk>.+)/$", views.site_modify, name="site_modify"),
    url(
        r"site_deletion/(?P<step>.+)?$",
        check_rights(["change_archaeologicalsite"])(views.site_deletion_wizard),
        name="site_deletion",
    ),
    url(r"site_delete/(?P<pk>.+)/$", views.site_delete, name="delete-site"),
    url(
        r"autocomplete-patriarche/$",
        views.autocomplete_patriarche,
        name="autocomplete-patriarche",
    ),
    url(
        r"operation_administrativeact_document/$",
        views.administrativeactfile_document,
        name="operation-administrativeact-document",
    ),
    url(
        r"^operation-parcels-modify/(?P<pk>.+)/$",
        views.operation_modify_parcels,
        name="operation-parcels-modify",
    ),
    url(
        r"^operation-qa-bulk-update/(?P<pks>[0-9-]+)?/$",
        check_rights(["change_operation", "change_own_operation"])(
            views.QAOperationForm.as_view()
        ),
        name="operation-qa-bulk-update",
    ),
    url(
        r"^operation-qa-bulk-update/(?P<pks>[0-9-]+)?/confirm/$",
        check_rights(["change_operation", "change_own_operation"])(
            views.QAOperationForm.as_view()
        ),
        name="operation-qa-bulk-update-confirm",
        kwargs={"confirm": True},
    ),
    url(
        r"^operation-qa-duplicate/(?P<pks>[0-9-]+)?/$",
        check_rights(["change_operation", "change_own_operation"])(
            views.QAOperationdDuplicateFormView.as_view()
        ),
        name="operation-qa-duplicate",
    ),
    url(
        r"^operation-qa-lock/(?P<pks>[0-9-]+)?/$",
        views.QAOperationLockView.as_view(),
        name="operation-qa-lock",
        kwargs={"model": models.Operation},
    ),
    url(
        r"^site-qa-duplicate/(?P<pks>[0-9-]+)?/$",
        check_rights(["change_archaeologicalsite", "change_own_archaeologicalsite"])(
            views.QAArchaeologicalSiteDuplicateFormView.as_view()
        ),
        name="site-qa-duplicate",
    ),
    url(
        r"^site-qa-lock/(?P<pks>[0-9-]+)?/$",
        views.QASiteLockView.as_view(),
        name="site-qa-lock",
        kwargs={"model": models.ArchaeologicalSite},
    ),
    url(
        r"^site-qa-bulk-update/(?P<pks>[0-9-]+)?/$",
        check_rights(["change_archaeologicalsite", "change_own_archaeologicalsite"])(
            views.QAArchaeologicalSiteForm.as_view()
        ),
        name="site-qa-bulk-update",
    ),
    url(
        r"^site-qa-bulk-update/(?P<pks>[0-9-]+)?/confirm/$",
        check_rights(["change_archaeologicalsite", "change_own_archaeologicalsite"])(
            views.QAArchaeologicalSiteForm.as_view()
        ),
        name="site-qa-bulk-update-confirm",
        kwargs={"confirm": True},
    ),
    url(
        r"^site-add-operation/(?P<pks>[0-9-]+)?/$",
        check_rights(["change_operation"])(
            views.site_add_operation
        ),
        name="site-add-operation",
    ),
    url(
        r"^site-add-top-operation/(?P<pks>\d+)?/$",
        check_rights(["change_operation"])(
            views.site_add_top_operation
        ),
        name="site-add-top-operation",
    ),
    url(
        r"generate-stats-operation/(?P<pk>.+)/",
        views.GenerateStatsOperation.as_view(),
        name="generate-stats-operation",
    ),
    url(
        r"api/facets/operation/$", views_api.FacetOperationAPIView.as_view(),
        name="api-facets-operation"
    ),
    url(
        r"api/search/operation/$", views_api.SearchOperationAPI.as_view(),
        name="api-search-operation"
    ),
    url(
        r"api/search/archaeologicalsite/$", views_api.SearchSiteAPI.as_view(),
        name="api-search-archaeologicalsite"
    ),
    path(
        "api/export/operation/<slug:slug>/",
        views_api.ExportOperationAPI.as_view(),
        name="api-export-operation"
    ),
    path(
        "api/export/archaeologicalsite/<slug:slug>/",
        views_api.ExportSiteAPI.as_view(),
        name="api-export-archaeologicalsite"
    ),
    path(
        "api/get/operation/<int:pk>/", views_api.GetOperationAPI.as_view(),
        name="api-get-operation"
    ),
    path(
        "api/get/site/<int:pk>/", views_api.GetSiteAPI.as_view(),
        name="api-get-archaeologicalsite"
    ),
]
