#!/bin/bash

INSTANCES="formation_v3 prod_v3"

DEST="/srv/www/rabbitmq-queues.json"
CDATE=`date --iso-8601=seconds`
echo '{"date": "'$CDATE'",' > $DEST
echo -n ' "instances": [' >> $DEST

PREV=""
for INSTANCE in $INSTANCES; do
	echo $PREV"" >> $DEST
	echo '    {"name": "'$INSTANCE'",' >> $DEST
	values=`rabbitmqctl list_queues -p /ishtar$INSTANCE -q messages`
	total=0
	for value in $values; do
		(( total += value ))
	done
	echo -n '     "queue": '$total'}' >> $DEST
	PREV=","
done
echo "" >> $DEST
echo ' ]}' >> $DEST
