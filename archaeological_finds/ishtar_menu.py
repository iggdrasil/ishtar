#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2012-2016 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from ishtar_common.utils import ugettext_lazy as _

from ishtar_common.menu_base import SectionItem, MenuItem

from archaeological_operations.models import AdministrativeAct
from . import models

# be careful: each access_controls must be relevant with check_rights in urls

MENU_SECTIONS = [
    (
        50,
        SectionItem(
            "find_management",
            _("Find"),
            profile_restriction="find",
            css="menu-find",
            childs=[
                MenuItem(
                    "find_search",
                    _("Search"),
                    model=models.Find,
                    access_controls=["view_find", "view_own_find"],
                ),
                MenuItem(
                    "find_creation",
                    _("Creation"),
                    model=models.Find,
                    access_controls=["add_find", "add_own_find"],
                ),
                MenuItem(
                    "find_modification",
                    _("Modification"),
                    model=models.Find,
                    access_controls=["change_find", "change_own_find"],
                ),
                MenuItem(
                    "find_deletion",
                    _("Deletion"),
                    model=models.Find,
                    access_controls=["change_find", "change_own_find"],
                ),
                SectionItem(
                    "find_basket",
                    _("Basket"),
                    childs=[
                        MenuItem(
                            "find_basket_search",
                            _("Search"),
                            model=models.FindBasket,
                            access_controls=["view_find", "view_own_find"],
                        ),
                        MenuItem(
                            "find_basket_creation",
                            _("Creation"),
                            model=models.FindBasket,
                            access_controls=["view_find", "view_own_find"],
                        ),
                        MenuItem(
                            "find_basket_modification",
                            _("Modification"),
                            model=models.FindBasket,
                            access_controls=["view_find", "view_own_find"],
                        ),
                        MenuItem(
                            "find_basket_modification_add",
                            _("Manage items"),
                            model=models.FindBasket,
                            access_controls=["view_find", "view_own_find"],
                        ),
                        MenuItem(
                            "find_basket_deletion",
                            _("Deletion"),
                            model=models.FindBasket,
                            access_controls=["view_find", "view_own_find"],
                        ),
                    ],
                ),
                # MenuItem(
                #     'treatment_creation', _("Add a treatment"),
                #     model=models.Treatment,
                #     access_controls=['change_find',
                #                      'change_own_find']),
            ],
        ),
    ),
    (
        60,
        SectionItem(
            "treatmentfle_management",
            _("Treatment request"),
            profile_restriction="warehouse",
            css="menu-warehouse",
            childs=[
                MenuItem(
                    "treatmentfle_search",
                    _("Search"),
                    model=models.TreatmentFile,
                    access_controls=["view_treatmentfile", "view_own_treatmentfile"],
                ),
                MenuItem(
                    "treatmentfle_creation",
                    _("Creation"),
                    model=models.TreatmentFile,
                    access_controls=[
                        "change_treatmentfile",
                        "change_own_treatmentfile",
                    ],
                ),
                MenuItem(
                    "treatmentfle_modification",
                    _("Modification"),
                    model=models.TreatmentFile,
                    access_controls=[
                        "change_treatmentfile",
                        "change_own_treatmentfile",
                    ],
                ),
                MenuItem(
                    "treatmentfle_deletion",
                    _("Deletion"),
                    model=models.TreatmentFile,
                    access_controls=[
                        "change_treatmentfile",
                        "change_own_treatmentfile",
                    ],
                ),
                SectionItem(
                    "admin_act_fletreatments",
                    _("Administrative act"),
                    childs=[
                        MenuItem(
                            "treatmentfle_admacttreatmentfle_search",
                            _("Search"),
                            model=AdministrativeAct,
                            access_controls=["change_administrativeact"],
                        ),
                        MenuItem(
                            "treatmentfle_admacttreatmentfle",
                            _("Creation"),
                            model=AdministrativeAct,
                            access_controls=["change_administrativeact"],
                        ),
                        MenuItem(
                            "treatmentfle_admacttreatmentfle_modification",
                            _("Modification"),
                            model=AdministrativeAct,
                            access_controls=["change_administrativeact"],
                        ),
                        MenuItem(
                            "treatmentfle_admacttreatmentfle_deletion",
                            _("Deletion"),
                            model=AdministrativeAct,
                            access_controls=["change_administrativeact"],
                        ),
                    ],
                ),
            ],
        ),
    ),
    (
        70,
        SectionItem(
            "treatment_management",
            _("Treatment"),
            profile_restriction="warehouse",
            css="menu-warehouse",
            childs=[
                MenuItem(
                    "treatment_search",
                    _("Search"),
                    model=models.Treatment,
                    access_controls=["view_treatment", "view_own_treatment"],
                ),
                MenuItem(
                    "treatment_creation",
                    _("Simple treatment - creation"),
                    model=models.Treatment,
                    access_controls=["change_find", "change_own_find"],
                ),
                MenuItem(
                    "treatment_creation_n1",
                    _("Treatment many to one - creation"),
                    model=models.Treatment,
                    access_controls=["change_find", "change_own_find"],
                ),
                MenuItem(
                    "treatment_creation_1n",
                    _("Treatment one to many - creation"),
                    model=models.Treatment,
                    access_controls=["change_find", "change_own_find"],
                ),
                MenuItem(
                    "treatment_modification",
                    _("Modification"),
                    model=models.Treatment,
                    access_controls=["change_treatment", "change_own_treatment"],
                ),
                MenuItem(
                    "treatment_deletion",
                    _("Deletion"),
                    model=models.Treatment,
                    access_controls=["change_treatment", "change_own_treatment"],
                ),
                SectionItem(
                    "admin_act_treatments",
                    _("Administrative act"),
                    childs=[
                        MenuItem(
                            "treatment_admacttreatment_search",
                            _("Search"),
                            model=AdministrativeAct,
                            access_controls=["view_administrativeact"],
                        ),
                        MenuItem(
                            "treatment_admacttreatment",
                            _("Creation"),
                            model=AdministrativeAct,
                            access_controls=["add_administrativeact"],
                        ),
                        MenuItem(
                            "treatment_admacttreatment_modification",
                            _("Modification"),
                            model=AdministrativeAct,
                            access_controls=["change_administrativeact"],
                        ),
                        MenuItem(
                            "treatment_admacttreatment_deletion",
                            _("Deletion"),
                            model=AdministrativeAct,
                            access_controls=["change_administrativeact"],
                        ),
                    ],
                ),
            ],
        ),
    ),
]
