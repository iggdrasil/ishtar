from rest_framework import authentication, permissions
from rest_framework.views import APIView
from rest_framework.response import Response

from ishtar_common.rest import SearchAPIView, FacetAPIView, GetAPIView, ExportAPIView
from ishtar_common.serializers import PublicSerializer

from archaeological_finds import models, forms


class PublicFindAPI(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        empty = models.Find.objects.filter(pk=None)
        basket_slug = self.request.GET.get("basket", None)
        if not basket_slug:
            return empty
        try:
            basket = models.FindBasket.objects.get(slug=basket_slug, public=True)
        except models.FindBasket.DoesNotExist:
            return empty
        q = (
            models.FindBasket.items.through.objects.filter(findbasket_id=basket.id)
                .values("find_id")
                .order_by("id")
        )
        id_list = [bi["find_id"] for bi in q]
        clauses = " ".join(
            "WHEN id=%s THEN %s" % (pk, i) for i, pk in enumerate(id_list)
        )

        ordering = "CASE {} END".format(clauses)
        # nosec: extra clauses uses only find id from a FindBasket query no injection possible
        return models.Find.objects.filter(id__in=id_list).extra(  # nosec
            select={"ordering": ordering}, order_by=("ordering",)
        )

    def get(self, request, format=None):
        serializer = PublicSerializer(self.get_queryset(), many=True)
        return Response(serializer.data)


class FacetFindAPIView(FacetAPIView):
    models = [models.Find]
    select_forms = [forms.FindSelect]


class SearchFindAPI(SearchAPIView):
    model = models.Find


class ExportFindAPI(ExportAPIView):
    model = models.Find


class GetFindAPI(GetAPIView):
    model = models.Find
