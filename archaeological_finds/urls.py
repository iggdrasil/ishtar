#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2010-2016 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.conf.urls import url
from django.urls import path

from ishtar_common.utils import check_rights, get_urls_for_model

from archaeological_finds import views
from archaeological_finds import views_api
from archaeological_operations.views import administrativeactfile_document
from archaeological_finds import models

# be careful: each check_rights must be relevant with ishtar_menu

# forms
urlpatterns = [
    url(
        r"find_search/(?P<step>.+)?$",
        check_rights(["view_find", "view_own_find"])(views.find_search_wizard),
        name="find_search",
    ),
    url(
        r"find_creation/(?P<step>.+)?$",
        check_rights(["add_find", "add_own_find"])(views.find_creation_wizard),
        name="find_creation",
    ),
    url(
        r"find_modification/(?P<step>.+)?$",
        check_rights(["change_find", "change_own_find"])(
            views.find_modification_wizard
        ),
        name="find_modification",
    ),
    url(r"find_modify/(?P<pk>.+)/$", views.find_modify, name="find_modify"),
    url(r"find_create/(?P<pk>.+)/$", views.find_create, name="find_create"),
    url(
        r"find_deletion/(?P<step>.+)?$",
        check_rights(["change_find", "change_own_find"])(views.find_deletion_wizard),
        name="find_deletion",
    ),
    url(r"find_delete/(?P<pk>.+)/$", views.find_delete, name="delete-find"),
    url(
        r"^find-qa-duplicate/(?P<pks>[0-9-]+)?/$",
        check_rights(["change_find", "change_own_find"])(
            views.QAFindDuplicateFormView.as_view()
        ),
        name="find-qa-duplicate",
    ),
    url(r"get-findbasket/$", views.get_find_basket, name="get-findbasket"),
    url(
        r"get-findbasket-write/$",
        views.get_find_basket_for_write,
        name="get-findbasket-write",
    ),
    url(
        r"find_basket_search/(?P<step>.+)?$",
        check_rights(["view_find", "view_own_find"])(views.basket_search_wizard),
        name="find_basket_search",
    ),
    url(
        r"^find_basket_creation/$",
        check_rights(["view_find", "view_own_find"])(views.NewFindBasketView.as_view()),
        name="new_findbasket",
    ),
    url(
        r"^find_basket_modification/(?P<step>.+)?$",
        check_rights(["view_find", "view_own_find"])(views.basket_modify_wizard),
        name="find_basket_modification",
    ),
    url(
        r"find_basket_modify/(?P<pk>.+)/$",
        views.find_basket_modify,
        name="find_basket_modify",
    ),
    url(
        r"^find_basket_modification_add/$",
        check_rights(["view_find", "view_own_find"])(
            views.SelectBasketForManagement.as_view()
        ),
        name="select_findbasketforadd",
    ),
    url(
        r"^find_basket_modification_add/(?P<pk>[0-9]+)?/$",
        check_rights(["view_find", "view_own_find"])(
            views.SelectItemsInBasket.as_view()
        ),
        name="select_itemsinbasket",
    ),
    url(
        r"^find_basket_modification_add_item/$",
        check_rights(["view_find", "view_own_find"])(
            views.FindBasketAddItemView.as_view()
        ),
        name="add_iteminbasket",
    ),
    url(
        r"^find_basket_modification_delete_item/(?P<basket>[0-9]+)?"
        r"/(?P<find_pk>[0-9]+)?/$",
        check_rights(["view_find", "view_own_find"])(
            views.FindBasketDeleteItemView.as_view()
        ),
        name="delete_iteminbasket",
    ),
    url(
        r"^find_basket_list/(?P<pk>[0-9]+)?/$",
        check_rights(["view_find", "view_own_find"])(
            views.FindBasketListView.as_view()
        ),
        name="list_iteminbasket",
    ),
    url(
        r"^find_basket_deletion/(?P<step>.+)?$",
        check_rights(["view_find", "view_own_find"])(views.basket_delete_wizard),
        name="find_basket_deletion",
    ),
    url(
        r"^findbasket-qa-duplicate/(?P<pks>[0-9-]+)?/$",
        check_rights(["view_find", "view_own_find"])(
            views.QAFindbasketDuplicateFormView.as_view()
        ),
        name="findbasket-qa-duplicate",
    ),
    url(
        r"^findbasket-add-treatment/(?P<pk>[0-9-]+)/$",
        check_rights(["change_find", "change_own_find"])(
            views.findbasket_treatment_add
        ),
        name="findbasket-add-treatment",
    ),
    url(
        r"^findbasket-add-treatmentfile/(?P<pk>[0-9-]+)/$",
        check_rights(["add_treatmentfile", "add_own_treatmentfile"])(
            views.findbasket_treatmentfile_add
        ),
        name="findbasket-add-treatmentfile",
    ),
    url(
        r"^find-add-treatment/(?P<pk>[0-9-]+)/$",
        check_rights(["change_find", "change_own_find"])(views.find_treatment_add),
        name="find-add-treatment",
    ),
    url(
        r"^find-add-divide-treatment/(?P<pk>[0-9-]+)/$",
        check_rights(["change_find", "change_own_find"])(
            views.find_divide_treatment_add
        ),
        name="find-add-divide-treatment",
    ),
    url(
        r"^treatmentfile-add-treatment/(?P<pk>[0-9-]+)/$",
        check_rights(["change_find", "change_own_find"])(
            views.treatmentfile_treatment_add
        ),
        name="treatmentfile-add-treatment",
    ),
    url(
        r"^treatment-add-adminact/(?P<pk>[0-9-]+)/$",
        check_rights(["add_administrativeact"])(views.treatment_adminact_add),
        name="treatment-add-adminact",
    ),
    url(
        r"^treatmentfile-add-adminact/(?P<pk>[0-9-]+)/$",
        check_rights(["add_administrativeact"])(views.treatmentfile_adminact_add),
        name="treatmentfile-add-adminact",
    ),
    url(
        r"^find-qa-bulk-update/(?P<pks>[0-9-]+)?/$",
        check_rights(["change_find", "change_own_find"])(views.QAFindForm.as_view()),
        name="find-qa-bulk-update",
    ),
    url(
        r"^find-qa-bulk-update/(?P<pks>[0-9-]+)?/confirm/$",
        check_rights(["change_find", "change_own_find"])(views.QAFindForm.as_view()),
        name="find-qa-bulk-update-confirm",
        kwargs={"confirm": True},
    ),
    url(
        r"^find-qa-basket/(?P<pks>[0-9-]+)?/$",
        check_rights(["change_find", "change_own_find"])(
            views.QAFindBasketFormView.as_view()
        ),
        name="find-qa-basket",
    ),
    url(
        r"findbasket-qa-bulk-update/(?P<pks>[0-9-]+)?/$",
        check_rights([
            "change_find",
            "change_own_find",
        ])(
            views.QAFindBasketModifyView.as_view()
        ),
        name="findbasket-qa-bulk-update",
    ),
    url(
        r"findbasket-qa-bulk-update/(?P<pks>[0-9-]+)?/confirm/$",
        check_rights([
            "change_find",
            "change_own_find",
        ])(
            views.QAFindBasketModifyView.as_view()
        ),
        name="findbasket-qa-bulk-update-confirm",
        kwargs={"confirm": True},
    ),
    url(
        r"^find-qa-packaging/(?P<pks>[0-9-]+)?/$",
        check_rights(["change_find", "change_own_find"])(
            views.QAFindTreatmentFormView.as_view()
        ),
        name="find-qa-packaging",
    ),
    url(
        r"^find-qa-lock/(?P<pks>[0-9-]+)?/$",
        views.QAFindLockView.as_view(),
        name="find-qa-lock",
        kwargs={"model": models.Find},
    ),
    url(
        r"^treatment_creation/(?P<step>.+)?$",
        check_rights(["change_find", "change_own_find"])(
            views.treatment_creation_wizard
        ),
        name="treatment_creation",
    ),
    url(
        r"^treatment_creation_n1/(?P<step>.+)?$",
        check_rights(["change_find", "change_own_find"])(
            views.treatment_creation_n1_wizard
        ),
        name="treatment_creation_n1",
    ),
    url(
        r"treatment_n1_create/(?P<pks>[-0-9]+)/$",
        views.treatment_n1_create,
        name="treatment-n1-create",
    ),
    url(
        r"^treatment_creation_1n/(?P<step>.+)?$",
        check_rights(["change_find", "change_own_find"])(
            views.treatment_creation_1n_wizard
        ),
        name="treatment_creation_1n",
    ),
    url(
        r"^treatment_modification/(?P<step>.+)?$",
        check_rights(["change_find", "change_own_find"])(
            views.treatment_modification_wizard
        ),
        name="treatment_modification",
    ),
    url(
        r"^treatment_modify/(?P<pk>.+)/$",
        views.treatment_modify,
        name="treatment_modify",
    ),
    url(
        r"^treatment_search/(?P<step>.+)?$",
        check_rights(["view_find", "view_own_find"])(views.treatment_search_wizard),
        name="treatment_search",
    ),
    url(
        r"^treatment_deletion/(?P<step>.+)?$",
        check_rights(["change_treatmentfile", "change_own_treatmentfile"])(
            views.treatment_deletion_wizard
        ),
        name="treatment_deletion",
    ),
    url(
        r"^treatment_delete/(?P<pk>.+)/$",
        views.treatment_delete,
        name="delete-treatment",
    ),
    url(
        r"^treatment_admacttreatment_search/(?P<step>.+)?$",
        check_rights(["change_administrativeact"])(
            views.treatment_administrativeact_search_wizard
        ),
        name="treatment_admacttreatment_search",
    ),
    url(
        r"^treatment_admacttreatment/(?P<step>.+)?$",
        check_rights(["change_administrativeact"])(
            views.treatment_administrativeact_wizard
        ),
        name="treatment_admacttreatment",
    ),
    url(
        r"^treatment_admacttreatment_modification/(?P<step>.+)?$",
        check_rights(["change_administrativeact"])(
            views.treatment_administrativeact_modification_wizard
        ),
        name="treatment_admacttreatment_modification",
    ),
    url(
        r"^treatment_administrativeacttreatment_modify/(?P<pk>.+)/$",
        views.treatment_administrativeacttreatment_modify,
        name="treatment_administrativeacttreatment_modify",
    ),
    url(
        r"^treatment_admacttreatment_deletion/(?P<step>.+)?$",
        check_rights(["change_administrativeact"])(
            views.treatment_admacttreatment_deletion_wizard
        ),
        name="treatment_admacttreatment_deletion",
    ),
    url(
        r"^get-administrativeacttreatment/(?P<type>.+)?$",
        views.get_administrativeacttreatment,
        name="get-administrativeacttreatment",
    ),
    url(
        r"^treatment_administrativeacttreatment_delete/(?P<pk>.+)/$",
        views.treatment_administrativeacttreatment_delete,
        name="delete-administrativeact-treatment",
    ),
    url(
        r"^treatmentfle_admacttreatmentfle_search/(?P<step>.+)?$",
        check_rights(["change_administrativeact"])(
            views.treatmentfile_admacttreatmentfile_search_wizard
        ),
        name="treatmentfle_admacttreatmentfle_search",
    ),
    url(
        r"^treatmentfle_admacttreatmentfle_modification/(?P<step>.+)?$",
        check_rights(["change_administrativeact"])(
            views.treatmentfile_admacttreatmentfile_modification_wizard
        ),
        name="treatmentfle_admacttreatmentfle_modification",
    ),
    url(
        r"^treatmentfle_admacttreatmentfle/(?P<step>.+)?$",
        check_rights(["change_administrativeact"])(
            views.treatmentfile_admacttreatmentfile_wizard
        ),
        name="treatmentfle_admacttreatmentfle",
    ),
    url(
        r"^treatmentfile_administrativeacttreatmentfile_modify/(?P<pk>.+)/$",
        views.treatmentfile_administrativeacttreatmentfile_modify,
        name="treatmentfile_administrativeacttreatmentfile_modify",
    ),
    url(
        r"^treatmentfle_admacttreatmentfle_deletion/(?P<step>.+)?$",
        check_rights(["change_administrativeact"])(
            views.treatmentfile_admacttreatmentfile_deletion_wizard
        ),
        name="treatmentfle_admacttreatmentfle_deletion",
    ),
    url(
        r"^treatmentfile_administrativeacttreatmentfile_delete/(?P<pk>.+)/$",
        views.treatmentfile_administrativeacttreatmentfile_delete,
        name="delete-administrativeact-treatmentfile",
    ),
    url(
        r"^treatmentfle_search/(?P<step>.+)?$",
        check_rights(["view_treatmentfile", "view_own_treatmentfile"])(
            views.treatmentfile_search_wizard
        ),
        name="treatmentfile_search",
    ),
    url(
        r"treatmentfle_creation/(?P<step>.+)?$",
        check_rights(["change_treatmentfile", "change_own_treatmentfile"])(
            views.treatmentfile_creation_wizard
        ),
        name="treatmentfile_creation",
    ),
    url(
        r"treatmentfle_modification/(?P<step>.+)?$",
        check_rights(["change_treatmentfile", "change_own_treatmentfile"])(
            views.treatmentfile_modification_wizard
        ),
        name="treatmentfile_modification",
    ),
    url(
        r"^treatmentfile_modify/(?P<pk>.+)/$",
        views.treatmentfile_modify,
        name="treatmentfile_modify",
    ),
    url(
        r"^treatmentfle_deletion/(?P<step>.+)?$",
        check_rights(["change_find", "change_own_find"])(
            views.treatmentfile_deletion_wizard
        ),
        name="treatmentfile_deletion",
    ),
    url(
        r"^treatmentfle_delete/(?P<pk>.+)/$",
        views.treatmentfile_delete,
        name="delete-treatmentfile",
    ),
    url(
        r"get-administrativeacttreatmentfile/(?P<type>.+)?$",
        views.get_administrativeacttreatmentfile,
        name="get-administrativeacttreatmentfile",
    ),
    url(
        r"get-upstreamtreatment/(?P<type>.+)?$",
        views.get_upstreamtreatment,
        name="get-upstreamtreatment",
    ),
    url(
        r"get-downstreamtreatment/(?P<type>.+)?$",
        views.get_downstreamtreatment,
        name="get-downstreamtreatment",
    ),
    url(
        r"autocomplete-objecttype/$",
        views.autocomplete_objecttype,
        name="autocomplete-objecttype",
    ),
    url(
        r"autocomplete-functionalarea/$",
        views.autocomplete_functionalarea,
        name="autocomplete-functionalarea",
    ),
    url(
        r"autocomplete-materialtype/$",
        views.autocomplete_materialtype,
        name="autocomplete-materialtype",
    ),
    url(
        r"autocomplete-treatmenttype/$",
        views.autocomplete_treatmenttype,
        name="autocomplete-treatmenttype",
    ),
    url(
        r"autocomplete-integritytype/$",
        views.autocomplete_integritytype,
        name="autocomplete-integritytype",
    ),
    url(
        r"autocomplete-treatmentfile/$",
        views.autocomplete_treatmentfile,
        name="autocomplete-treatmentfile",
    ),
    url(
        r"get-find-for-ope/own/(?P<type>.+)?$",
        views.get_find_for_ope,
        name="get-own-find-for-ope",
        kwargs={"force_own": True},
    ),
    url(
        r"get-find-for-ope/(?P<type>.+)?$",
        views.get_find_for_ope,
        name="get-find-for-ope",
    ),
    url(
        r"get-find-for-cr/cr/(?P<type>.+)?$",
        views.get_find_for_cr,
        name="get-own-find-for-cr",
        kwargs={"force_own": True},
    ),
    url(
        r"get-find-for-cr/(?P<type>.+)?$", views.get_find_for_cr, name="get-find-for-cr"
    ),
    url(
        r"get-find-for-treatment/own/(?P<type>.+)?$",
        views.get_find_for_treatment,
        name="get-own-find-for-treatment",
        kwargs={"force_own": True},
    ),
    url(
        r"get-find-for-treatment/(?P<type>.+)?$",
        views.get_find_for_treatment,
        name="get-find-for-treatment",
    ),
    url(
        r"get-find-inside-container/own/(?P<type>.+)?$",
        views.get_find_inside_container,
        name="get-find-inside-container",
        kwargs={"force_own": True},
    ),
    url(
        r"get-find-inside-container/(?P<type>.+)?$",
        views.get_find_inside_container,
        name="get-find-inside-container",
    ),
    url(
        r"get-find-full/own/(?P<type>.+)?$",
        views.get_find,
        name="get-own-find-full",
        kwargs={"full": True, "force_own": True},
    ),
    url(
        r"get-find-full/(?P<type>.+)?$",
        views.get_find,
        name="get-find-full",
        kwargs={"full": True},
    ),
    url(
        r"get-find-shortcut/(?P<type>.+)?$",
        views.get_find,
        name="get-find-shortcut",
        kwargs={"full": "shortcut"},
    ),
    url(
        r"^show-find-basket/(?:(?P<pk>.+)/(?P<type>.+)?)?$",
        views.show_findbasket,
        name="show-findbasket",
    ),
    url(
        r"^show-basefind/(?:(?P<pk>.+)/(?P<type>.+)?)?$",
        views.show_basefind,
        name="show-basefind",
    ),
    url(
        r"^display-find/basket-(?P<pk>.+)/$",
        views.display_findbasket,
        name="display-findbasket",
    ),
    url(
        r"^show-historized-find/(?P<pk>.+)?/(?P<date>.+)?$",
        views.show_find,
        name="show-historized-find",
    ),
    url(
        r"^revert-find/(?P<pk>.+)/(?P<date>.+)$", views.revert_find, name="revert-find"
    ),
    url(
        r"get-treatment-shortcut/(?P<type>.+)?$",
        views.get_treatment,
        name="get-treatment-shortcut",
        kwargs={"full": "shortcut"},
    ),
    url(
        r"show-historized-treatment/(?P<pk>.+)?/(?P<date>.+)?$",
        views.show_treatment,
        name="show-historized-treatment",
    ),
    url(
        r"^revert-treatment/(?P<pk>.+)/(?P<date>.+)$",
        views.revert_treatment,
        name="revert-treatment",
    ),
    url(
        r"get-treatmentfile/(?P<type>.+)?$",
        views.get_treatmentfile,
        name="get-treatmentfile",
    ),
    url(
        r"get-treatmentfile-shortcut/(?P<type>.+)?$",
        views.get_treatmentfile,
        name="get-treatmentfile-shortcut",
        kwargs={"full": "shortcut"},
    ),
    url(
        r"^show-treatmentfile(?:/(?P<pk>.+))?/(?P<type>.+)?$",
        views.show_treatmentfile,
        name=models.TreatmentFile.SHOW_URL,
    ),
    url(
        r"show-historized-treatmentfile/(?P<pk>.+)?/(?P<date>.+)?$",
        views.show_treatmentfile,
        name="show-historized-treatmentfile",
    ),
    url(
        r"^revert-treatmentfile/(?P<pk>.+)/(?P<date>.+)$",
        views.revert_treatmentfile,
        name="revert-treatmentfile",
    ),
    url(
        r"^treatment_administrativeact_document/$",
        administrativeactfile_document,
        name="treatment-administrativeact-document",
        kwargs={"treatment": True},
    ),
    url(
        r"^treatmentfle_administrativeact_document/$",
        administrativeactfile_document,
        name="treatmentfle-administrativeact-document",
        kwargs={"treatment_file": True},
    ),
    url(
        r"autocomplete-findbasket/$",
        check_rights(["change_find", "change_own_find"])(views.autocomplete_findbasket),
        name="autocomplete-findbasket",
    ),
    url(
        r"autocomplete-findbasket-write/$",
        check_rights(["change_find", "change_own_find"])(
            views.autocomplete_findbasket_write
        ),
        name="autocomplete-findbasket-write",
    ),
    url(
        r"api/public/find/$", views_api.PublicFindAPI.as_view(), name="api-public-find"
    ),
    url(
        r"api/ishtar/base-finds/get-geo$",
        check_rights(["view_find", "view_own_find"])(
            views.get_geo_items,
        ),
        name="api-get-geo",
    ),
    url(
        r"api/facets/find/$", views_api.FacetFindAPIView.as_view(),
        name="api-facets-find"
    ),
    url(
        r"api/search/find/$", views_api.SearchFindAPI.as_view(),
        name="api-search-find"
    ),
    path(
        "api/export/find/<slug:slug>/",
        views_api.ExportFindAPI.as_view(),
        name="api-export-find"
    ),
    path(
        "api/get/find/<int:pk>/", views_api.GetFindAPI.as_view(),
        name="api-get-find"
    ),
    url(
        r"autocomplete-basefind/$",
        check_rights(["view_basefind", "view_own_basefind"])(
            views.autocomplete_basefind
        ),
        name="autocomplete-basefind",
    ),
]

urlpatterns += get_urls_for_model(models.Find, views, own=True, autocomplete=True)
urlpatterns += get_urls_for_model(models.Treatment, views, autocomplete=True)
