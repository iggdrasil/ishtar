#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012-2016 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.contrib import admin

from ishtar_common.apps import admin_site
from ishtar_common.admin import HistorizedObjectAdmin, GeneralTypeAdmin, MainGeoDataItem

from . import models


class BaseFindAdmin(HistorizedObjectAdmin, MainGeoDataItem):
    list_display = ('label', 'context_record', 'index')
    search_fields = ('label', 'cache_complete_id',)
    exclude = ["line"]
    model = models.BaseFind
    autocomplete_fields = HistorizedObjectAdmin.autocomplete_fields + \
                          MainGeoDataItem.autocomplete_fields + ["context_record"]
    readonly_fields = HistorizedObjectAdmin.readonly_fields + [
        'cache_short_id', 'cache_complete_id',
    ]


admin_site.register(models.BaseFind, BaseFindAdmin)


class FindBasketAdmin(admin.ModelAdmin):
    class Meta:
        model = models.FindBasket
        exclude = []
    list_display = ["label", "user"]
    search_fields = ('label', 'user__person__raw_name',)
    readonly_fields = ["search_vector"]
    autocomplete_fields = (
        'user',
        'shared_with',
        'shared_write_with',
        'items',
    )


admin_site.register(models.FindBasket, FindBasketAdmin)


class FindAdmin(HistorizedObjectAdmin):
    list_display = ('label', 'operations_lbl', 'context_records_lbl', 'index',
                    'dating', 'materials')
    list_filter = ('datings__period', 'material_types')
    search_fields = ('cached_label', "base_finds__cache_complete_id")
    model = models.Find
    autocomplete_fields = HistorizedObjectAdmin.autocomplete_fields + [
        'base_finds',
        'container',
        'container_ref',
        'material_types',
        'upstream_treatment',
        'downstream_treatment',
        'treatments',
        'main_image',
        'documents',
    ]
    readonly_fields = HistorizedObjectAdmin.readonly_fields + [
        'datings', 'cached_label'
    ]


admin_site.register(models.Find, FindAdmin)


class PropertyAdmin(HistorizedObjectAdmin):
    list_display = ['find', 'person', 'start_date', 'end_date']
    search_fields = ('find__label', 'person__name')
    model = models.Property
    autocomplete_fields = ("find", "person")
    readonly_fields = HistorizedObjectAdmin.readonly_fields + [
        'administrative_act']

    def has_add_permission(self, request):
        return False


admin_site.register(models.Property, PropertyAdmin)


class TreatmentAdmin(HistorizedObjectAdmin):
    list_display = ('year', 'index', 'label', 'treatment_types_lbl', 'location',
                    'downstream_lbl', 'upstream_lbl', 'container', 'person')
    list_filter = ('treatment_types', 'treatment_state', 'year')
    model = models.Treatment
    readonly_fields = HistorizedObjectAdmin.readonly_fields + [
        'cached_label', 'downstream_lbl', 'upstream_lbl',
    ]
    exclude = ["documents", "main_image"]
    search_fields = ("cached_label",)
    autocomplete_fields = HistorizedObjectAdmin.autocomplete_fields + [
        "person",
        "organization",
        "file",
        "location",
        "container",
        "scientific_monitoring_manager",
    ]

    def has_add_permission(self, request):
        return False


admin_site.register(models.Treatment, TreatmentAdmin)


class TreatmentFileAdmin(HistorizedObjectAdmin):
    list_display = ('type', 'year', 'index', 'name',
                    'applicant', 'in_charge', 'internal_reference')
    list_filter = ('type', 'year')
    search_fields = ('name', 'applicant__name', 'applicant__surname',
                     'applicant__raw_name', 'applicant_organisation__name',
                     'cached_label')
    model = models.TreatmentFile
    autocomplete_fields = HistorizedObjectAdmin.autocomplete_fields + [
        'in_charge',
        'applicant',
        'applicant_organisation',
    ]
    readonly_fields = HistorizedObjectAdmin.readonly_fields + [
        'cached_label',
    ]
    exclude = ["documents", "main_image"]


admin_site.register(models.TreatmentFile, TreatmentFileAdmin)


class ObjectTypeAdmin(GeneralTypeAdmin):
    model = models.ObjectType
    autocomplete_fields = ("parent",)


admin_site.register(models.ObjectType, ObjectTypeAdmin)


class FunctionalAreaAdmin(GeneralTypeAdmin):
    model = models.FunctionalArea
    autocomplete_fields = ("parent",)


admin_site.register(models.FunctionalArea, FunctionalAreaAdmin)


class MaterialTypeAdmin(GeneralTypeAdmin):
    search_fields = ('label', 'parent__label', 'comment',)
    model = models.MaterialType
    autocomplete_fields = ("parent",)
    extra_list_display = ['recommendation']


admin_site.register(models.MaterialType, MaterialTypeAdmin)


admin_site.register(models.CommunicabilityType, GeneralTypeAdmin)


class TreatmentTypeAdmin(GeneralTypeAdmin):
    list_filter = [
        'virtual', 'destructive', 'create_new_find', 'upstream_is_many',
        'downstream_is_many', 'destructive', 'change_reference_location',
        'change_current_location', 'restore_reference_location'
    ]
    model = models.TreatmentType
    CSV_FIELD_ORDER = [
        "virtual",
        "destructive",
        "create_new_find",
        "upstream_is_many",
        "downstream_is_many",
        "change_reference_location",
        "change_current_location",
        "restore_reference_location",
    ]

    def get_list_display(self, request):
        list_display = super(TreatmentTypeAdmin, self).get_list_display(request)
        return list_display[:-1] + ['order'] + [list_display[-1]]


admin_site.register(models.TreatmentType, TreatmentTypeAdmin)


class CheckedTypeAdmin(GeneralTypeAdmin):
    extra_list_display = ["order"]
    model = models.CheckedType


admin_site.register(models.CheckedType, CheckedTypeAdmin)


@admin.register(models.ConservatoryState, site=admin_site)
class ConservatoryStateAdmin(GeneralTypeAdmin):
    extra_list_display = ["order"]


@admin.register(models.TreatmentFileType, site=admin_site)
class TreatmentFileType(GeneralTypeAdmin):
    extra_list_display = ["treatment_type"]


@admin.register(models.TreatmentState, site=admin_site)
class TreatmentState(GeneralTypeAdmin):
    extra_list_display = ["order", "executed"]


general_models = [
    models.RemarkabilityType,
    models.IntegrityType,
    models.BatchType, models.AlterationCauseType, models.AlterationType,
    models.TreatmentEmergencyType, models.ObjectTypeQualityType,
    models.MaterialTypeQualityType
]

for model in general_models:
    admin_site.register(model, GeneralTypeAdmin)
