# backup database and media
sudo vim /etc/apt/sources.list.d/iggdrasil.list
# buster -> bullseye
sudo apt update
sudo apt upgrade
screen  # data migration is long...
# for each instance in /etc/ishtar/instances
cd /srv/ishtar/{instance_name}
# update fixtures
./manage.py loaddata /usr/share/python3-django-ishtar/fixtures/initial_data-auth-fr.json 
./manage.py loaddata /usr/lib/python3/dist-packages/archaeological_files/fixtures/initial_data-fr.json
# migrate to new geo management
./manage.py migrate_to_geo_v4 --process {number_of_thread}  # number_of_thread depend on your CPU
# edit profile type permissions for geovectordata
-> http(s)://{my-ihstar}/admin/ishtar_common/profiletypesummary/