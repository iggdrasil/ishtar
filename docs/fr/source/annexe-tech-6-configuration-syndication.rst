.. -*- coding: utf-8 -*-

.. _annexe-tech-6-configuration-syndication:

====================================================
Annexe technique 6 - Configuration de la fédération
====================================================

:Auteurs: Étienne Loks
:Date: 2023-02-16
:Copyright: CC-BY 3.0
:Ishtar Version: v4.0

La fédération de données entre instances Ishtar est une fonctionnalité
puissante mais assez complexe à mettre en place. La documentation
ci-dessous détaille pas à pas les différentes étapes pour mettre en
place cela.

Selon que l’on soit l’instance qui partage des données ou celle qui
consulte des données les enjeux sont différents :

L’instance qui partage des données (dite « source ») doit paramétrer :

-  les types de données à partager ;
-  les sous-ensembles de ces données à partager ;
-  les attributs que l’on souhaite exposer / cacher ;
-  à qui l’on va partager les données (nécessitant la création d’un
   compte système avec un jeton d’accès et le paramétrage de l’adresse
   IP du serveur vers lequel les données vont être envoyées).

L’instance source est configurée en administration via les entrées « API -
Accès distant ».

L’instance qui consulte les données (dite « destination ») doit paramétrer :

-  le serveur « source » qui fournit les données, son adresse web d’Ishtar
   et un jeton d’accès ;
-  la correspondance entre les listes de vocabulaire contrôlé de son
   instance et les listes de vocabulaire contrôlé de l’instance source.
   Cette correspondance est nécessaire pour traduire les recherches par
   critères de manière pertinente.

L’instance destination est configurée en administration via les entrées «
API - Recherche ».

Pour illustrer une fédération de données, nous allons prendre l’exemple
suivant :

L’instance Charente-Maritime veut partager un ensemble de données avec
l’instance Nouvelle-Aquitaine.

Le serveur dit « source » est celui de Charente-Maritime.

Le serveur dit « destination » est celui de Nouvelle-Aquitaine.

Configuration du serveur source
===============================

Cette configuration est à faire si vous souhaitez donner un accès en
lecture à vos données depuis une autre instance Ishtar. Dans notre
exemple, il s’agit du serveur Charente-Maritime.

Préalablement à cette configuration vous avez besoin de l’adresse IP du
serveur de l’autre instance Ishtar.

Création d’un utilisateur système
---------------------------------

Contrairement à la création d’utilisateur classique, on crée un
utilisateur système. Celui-ci n’aura pas accès à l’instance Ishtar
directement. Créer cet utilisateur en interface « Administration de base
de données » : « Authentification et autorisation > Utilisateurs > Ajout
». Utiliser un identifiant qui n’existe pas encore en base de données,
exemple « federation-nouvelle-aquitaine ». Le mot de passe ne sera pas
utilisé : en choisir un particulièrement complexe.

|image0|

Associer un jeton d’authentification à un utilisateur système
-------------------------------------------------------------

Associer ensuite à ce compte un jeton d’authentification. Depuis
l’interface « Administration de base de données » : « Accueil > Jeton
d’authentification > Jetons > Ajout ». Le compte est retrouvé via
l’icône loupe.

Une fois le jeton créé, aller à la liste des jetons puis mettre de côté
la clé du jeton.

|image1|

Par exemple sur la capture d’écran, la clé est
``a14b32f31029216da11621ba5ddb0431997ca61d``. Une fois la configuration
terminée, cette clé sera à transmettre à l’administrateur de l'instance Ishtar destination.

Associer l’adresse IP du serveur destination à l’utilisateur système
--------------------------------------------------------------------

Depuis « Ishtar - Commun > API - Accès distant - Utilisateurs > Ajout »,
on associe l’utilisateur système à l’adresse IP du serveur.

|image2|

Pour notre exemple, le serveur de Nouvelle-Aquitaine a l’adresse
``8.8.8.8``.

Ouvrir l’accès à un ou plusieurs types de contenu
-------------------------------------------------

Pour chaque type de contenu que l’on souhaite ouvrir (Opération, Unité
d’Enregistrement, Mobilier, …), ajouter une entrée via : « Ishtar -
Commun > API - Accès distant - Modèles de recherche > Ajout ».

|image3|

On sélectionne notre utilisateur système, le type de contenu, ainsi
qu’éventuellement une requête filtrante. Cette requête correspond à une
chaîne de caractères correspondant à recherche. Seuls les éléments
correspondants à cette requête seront partagés. Si l’on ne renseigne pas
cette requête filtrante tous les éléments seront renvoyés.

Filtrer la fiche de contenu
---------------------------

Il est possible de cacher certains champs sur la fiche que l’on partage.
Pour chaque champ que l’on souhaite cacher, ajouter une entrée via «
Ishtar - Commun > API - Accès distant - Filtres de fiche > Ajout ». Le
formulaire doit rempli en deux fois. On se contente d’abord de choisir
le type de contenu que l’on veut filtrer.

|image4|

Valider avec le bouton « Enregistrer et continuer les modifications ».

Une fois ce premier enregistrement, l’aide disponible sous le champ «
Clé » liste les différentes clés correspondant au contenu que l’on
souhaite filtrer.

|image5|

Sur cet exemple, on souhaite filtrer l’affichage de l’image principale.
Note : si l’on souhaite qu’aucune image ne soit affichée, il faut
ajouter chaque champ « image ».

Configuration du serveur destination
====================================

Cette configuration est à faire si vous souhaitez accéder à des données
d’une autre instance Ishtar depuis votre propre instance. Dans notre
exemple, il s’agit du serveur Nouvelle-Aquitaine.

Préalablement à cette configuration vous avez besoin du jeton
d’authentification donné par le serveur source ainsi que l’adresse web
pour accéder à celui-ci.

Créer la source externe
-----------------------

Cette source externe se crée en interface « Administration de base de
données » : « Ishtar - Commun > API - Recherche - Sources externes >
Ajout ».

|image6|

Sur le formulaire reprendre l’adresse web du serveur source ainsi que le
jeton d’authentification qui vous a été fourni. Le nom renseigné doit
être explicite, il apparaîtra sur les écrans de recherche de
l’interface. On renseigne explicitement chaque utilisateur qui aura
accès à cette source externe.

Synchroniser les listes de vocabulaire contrôlées depuis la source
------------------------------------------------------------------

Une fois la source externe créée il est nécessaire de récupérer les
listes de vocabulaire contrôlées de cette source (le serveur source). Pour
cela, on se rend sur la page listant les sources externes : « Ishtar -
Commun > API - Recherche - Sources externes ». Sélectionner la source
externe concernée, l’action « Mettre à jour les types depuis la source »
et valider (le bouton « Aller »).

|image7|

Cette action peut prendre un certain temps. Si des changements sont
faits sur les listes de vocabulaire contrôlé sur la source cette étape
ainsi que les suivantes sont à effectuer de nouveau.

Mettre à jour les correspondances
---------------------------------

Pour chaque liste de vocabulaire contrôlée, des correspondances sont
faites automatiquement entre les termes identiques. Lorsque deux
instances partagent la même langue et ont des cas d’utilisation
semblable, une bonne partie des listes de vocabulaire sont mises en
correspondance automatiquement. Afin de visualiser les correspondances
et de compléter les correspondances manquantes, on génère un document de
correspondance. Celui-ci se présente sous la forme d’un document tableur
LibreOffice. Pour cela, on reste sur la page listant les sources
externes : « Ishtar - Commun > API - Recherche - Sources externes ».
Sélectionner la source externe concernée et l’action « Générer le document
de correspondance », valider (bouton « Aller »).

|image8|

Ouvrir le document généré avec LibreOffice et parcourir les différents
onglets pour effectuer les éventuelles correspondances manquantes. Là
où des cellules sur la troisième colonne « Local » sont vides, choisir
l’élément manquant dans la liste.

|image9|

Une fois ces modifications faites, enregistrer le document et
le déposer sur le serveur sur la source externe concernée.

|image10|

Enregistrer la source externe puis sélectionner la source externe
concernée et l’action « Mettre à jour les associations depuis un document
de correspondance », valider.

|image11|

.. |image0| image:: _static/annexe-technique-06-01.png
.. |image1| image:: _static/annexe-technique-06-02.png
.. |image2| image:: _static/annexe-technique-06-03.png
.. |image3| image:: _static/annexe-technique-06-04.png
.. |image4| image:: _static/annexe-technique-06-05.png
.. |image5| image:: _static/annexe-technique-06-06.png
.. |image6| image:: _static/annexe-technique-06-07.png
.. |image7| image:: _static/annexe-technique-06-08.png
.. |image8| image:: _static/annexe-technique-06-09.png
.. |image9| image:: _static/annexe-technique-06-10.png
.. |image10| image:: _static/annexe-technique-06-11.png
.. |image11| image:: _static/annexe-technique-06-12.png
