.. -*- coding: utf-8 -*-

.. _import-communes-ign:

============================================
Annexe technique 2 - Import des communes IGN
============================================

:Auteur: Étienne Loks
:Date: 2020-09-23
:Copyright: CC-BY 3.0

----------------------------------


`L'IGN <http://www.ign.fr/>`_ met à disposition sous licence libre (`Licence ouverte <https://www.etalab.gouv.fr/wp-content/uploads/2014/05/Licence_Ouverte.pdf>`_) les géométries du découpage administratif du territoire métropolitain français avec une réactualisation annuelle. Au sein d'Ishtar, il est opportun de récupérer le découpage des communes avec leur géométrie associée. On réalise cela en deux temps : récupération des données et export au format CSV, puis import au sein d'Ishtar. Il est nécessaire pour faire cet import d'un accès en ligne de commande au serveur sur lequel est hébergé Ishtar.


Récupération des données
------------------------

Le jeu de données qui nous intéresse est distribué sous le nom ADMIN EXPRESS. Ce jeu de données est disponible en ligne directement sur le site de l'IGN : `ADMIN EXPRESS <https://geoservices.ign.fr/documentation/diffusion/telechargement-donnees-libres.html#admin-express>`_.

Une fois le fichier récupéré, il doit être décompressé (il faut disposer d'un utilitaire à même de décompresser les archives 7z). Les fichiers qui nous intéressent concernent le shapefile COMMUNE ou COMMUNE_CARTO (géométrie généralisée - moins précise et de fait moins volumineuse). Celui se trouve dans le répertoire `ADMIN-EXPRESS-COG/1_DONNEES_LIVRAISON_<date de livraison>/<zone concernée>/`.

Ce shapefile doit ensuite être ouvert dans QGIS puis exporté au format CSV en n'oubliant pas d'inclure la géométrie au format WKT.

.. image:: _static/annexe-technique-1-export-qgis.png


Import dans Ishtar
------------------

Il ne reste plus qu'à importer dans Ishtar. Pour cela, se connecter sur le serveur hébergeant Ishtar, déposer le fichier CSV que l'on a généré, puis se rendre dans le répertoire d'installation de l'instance concernée. Enfin, lancer la commande suivante (en remplaçant <année> et <chemin vers le fichier CSV> par les valeurs adéquates) :

.. code-block:: bash

    root@ishtar-server:# cd <chemin vers l instance> # exemple : /srv/ishtar/prod_deb
    root@ishtar-server:# python3 ./manage.py import_geofla_csv --year <année> \
                                  --srid <code epsg de la projection> <chemin vers le fichier CSV>
    root@ishtar-server:#  # exemple
    root@ishtar-server:# python3 ./manage.py import_geofla_csv --year 2019 --srid 4326 /tmp/2019.csv

.. warning:: L'année attendue est celle correspondant à l'année de création pour des communes nouvelles. Le jeu de données d'une année concerne les communes créées l'année précédente ainsi pour le jeu de données 2020, il faut indiquer 2019.
.. warning:: Si l'on souhaite garder un suivi des regroupements de communes, il est important de procéder à l'import des nouvelles communes (cf. :ref:`import-regroupements-communes-insee`) de l'année concernée depuis les sources INSEE avant de faire l'import des géométries depuis les sources IGN.

