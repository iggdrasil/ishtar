.. -*- coding: utf-8 -*-

==================================================================
Annexe 4 - Correspondance des champs Ishtar - Normes documentaires
==================================================================

:Auteur: Étienne Loks
:Date: 2020-05-27
:Copyright: CC-BY 3.0

----------------------------------

Ishtar - Dublin Core
--------------------

Issu d'un consensus international et multidisciplinaire, le
`Dublin Core <https://fr.wikipedia.org/wiki/Dublin_Core>`_ a été
développé pour décrire des documents de manière simple et standardisée.

.. figure:: _static/ishtar-dublincore.png
	:align: center

Ishtar - COinS
--------------

Le `ContextObjects in Spans (COinS) <https://fr.wikipedia.org/wiki/ContextObjects_in_Spans>`_ est une
méthode pour inclure des métadonnées bibliographiques dans le code HTML de
pages web.

L’utilisation de COinS permet à un logiciel de gestion bibliographique de
récupérer les méta-données de l’ouvrage de référence.

.. figure:: _static/ishtar-coins.png
	:align: center
