.. -*- coding: utf-8 -*-

.. _annexe-technique-3-variables:

==============================
Annexe technique 3 - Variables
==============================

:Auteurs: Étienne Loks, Ishtar team
:Date: 2023-08-28
:Copyright: CC-BY 3.0
:Ishtar Version: v4.0.55

----------------------------------


Ces variables sont utilisées pour les configurations des imports, les patrons de documents et la configuration des identifiants, des index personnalisés.

Ces variables correspondent aux noms des champs utilisés en base de données (exemple : ``code_patriarche`` pour accéder au code patriarche d'une opération) ainsi que des « facilitateurs » qui permettent de disposer de champs plus évolués (exemple : ``get_next_index`` pour accéder au prochain numéro d'index).

On peut passer d'un élément lié à un autre (par exemple, accéder à l'opération d'une unité d'enregistrement) avec la notation double tiret `__` et ensuite accéder aux variables de l'élément lié (exemple : ``operation__code_patriarche`` permet d'accéder au code patriarche de l'opération ).

Ci-dessous la liste des variables pour chaque type d'éléments.

..warning:: Cette documentation liste tous les champs en base de données (moins quelques champs de gestion interne). Certains sont des champs historiques obsolètes voués à disparaître, d'autres sont des champs mis en place pour des futures fonctionnalités et peuvent encore faire l'objet de modifications. Tout champ listé ci-dessous qui ne correspond pas à un champ accessible depuis l'interface d'Ishtar ne doit normalement pas être utilisé.

.. _valeurs-champs-adresse:


Modèles génériques
------------------

Champs personnalisés
********************

Les éléments principaux (`Operation`, `Unité d'Enregistrement`, `Mobilier`, etc.) permettent d'utiliser des :ref:`champs personnalisés <champ-personnalises>`.
Ceux-ci sont accessibles via l'attribut `data__`. Ainsi on accède à une clé `ma_clef` via `data__ma_clef` ou à une autre clé sous une catégorie `ma_categorie__ma_sousclef` via `data__ma_categorie__ma_sousclef`.

Champs adresse
**************

Les champs adresse sont une liste de variables partagées par plusieurs éléments :

- **address** : *Texte* - Adresse
- **address_complement** : *Texte* - Complément d'adresse
- **alt_address** : *Texte* - Autre adresse : adresse
- **alt_address_complement** : *Texte* - Autre adresse : complément d'adresse
- **alt_address_is_prefered** : *Booléen* - L'adresse alternative est préférée
- **alt_country** : *Chaîne de caractères (30)* - Autre adresse : pays
- **alt_postal_code** : *Chaîne de caractères (10)* - Autre adresse : code postal
- **alt_town** : *Chaîne de caractères (70)* - Autre adresse : ville
- **country** : *Chaîne de caractères (30)* - Pays
- **email** : *Courriel (300)* - Courriel
- **mobile_phone** : *Chaîne de caractères (32)* - Téléphone mobile
- **phone** : *Chaîne de caractères (32)* - Téléphone
- **phone2** : *Chaîne de caractères (32)* - Type de téléphone 2
- **phone3** : *Chaîne de caractères (32)* - Téléphone 3
- **phone_desc** : *Chaîne de caractères (300)* - Type de téléphone
- **phone_desc2** : *Chaîne de caractères (300)* - Type de téléphone 2
- **phone_desc3** : *Chaîne de caractères (300)* - Type de téléphone 3
- **postal_code** : *Chaîne de caractères (10)* - Code postal
- **precise_town_id** : *Entier positif* - Commune (précis)
- **raw_phone** : *Texte* - Téléphone brut
- **town** : *Chaîne de caractères (150)* - Commune (saisie libre)

Personne
********

Chaque personne dispose des :ref:`champs adresse <valeurs-champs-adresse>`, ainsi que des champs suivants :

- **adminact_operation_in_charge__** : *→ Actes administratifs (responsable de suivi scientifique)*
- **adminact_scientist__** : *→ Actes administratifs (responsable d'opération)*
- **attached_to__** : *→ Organisation* - Est rattaché à 
- **author__** : *→ Auteurs (personne)*
- **cached_label** : *Texte* - Nom en cache
- **cira_rapporteur__** : *→ Opérations (rapporteur ctra/cira)*
- **comment** : *Texte* - Commentaire
- **contact_type** : *Chaîne de caractères (300)* - Type de contact
- **file_responsability__** : *→ Dossiers archéologiques (personne responsable)*
- **general_contractor_files__** : *→ Dossiers archéologiques (aménageur)*
- **manage_treatments__** : *→ Traitements (responsable de suivi scientifique)*
- **minutes_writer__** : *→ Opérations (rédacteur du procès verbal)*
- **name** : *Chaîne de caractères (200)* - Nom
- **old_title** : *Chaîne de caractères (100)* - Titre
- **operation_collaborator__** : *→ Opérations (collaborateurs)*
- **operation_monitoring__** : *→ Opérations (responsable de suivi scientifique)*
- **operation_protagonist__** : *→ Opérations (nom du protagoniste)*
- **operation_scientist_responsability__** : *→ Opérations (responsable de suivi scientifique)*
- **person_types__** : *→ Type de personne* (**label** Dénomination, **txt_idx** Identifiant textuel) - Types 
- **raw_name** : *Chaîne de caractères (300)* - Nom brut
- **responsible_town_planning_service_files__** : *→ Dossiers archéologiques (responsable pour le service instructeur)*
- **salutation** : *Chaîne de caractères (200)* - Formule d'appel
- **scientist__** : *→ Dossiers archéologiques (responsable d'opération)*
- **signatory__** : *→ Actes administratifs (signataire)*
- **site_collaborator__** : *→ Entités archéologiques (collaborateurs)*
- **surname** : *Chaîne de caractères (50)* - Prénom - Attention, résidu historique et malheureux dans le code d'une erreur de traduction initiale (surname correspond en anglais au nom de famille, pas au prénom).
- **title__** : *→ Type de titre* (**label** Dénomination, **txt_idx** Identifiant textuel, **grammatical_gender** Genre grammatical - "M": Masculin ; "F": Féminin ; "N": Neutre, **long_title** Titre long) - Titre 
- **treatmentfile_applicant__** : *→ Demandes de traitement (demandeur)*
- **treatmentfile_responsability__** : *→ Demandes de traitement (personne responsable)*
- **treatments__** : *→ Traitements (responsable)*
- **warehouse_in_charge__** : *→ Lieux de conservation (personne responsable)*


Auteur
******

- **author_type__** : *→ Type d'auteur* (**label** Dénomination, **txt_idx** Identifiant textuel) - Type d'auteur 
- **cached_label** : *Texte* - Nom en cache
- **documents__** : *→ Documents (auteurs)*
- **person__** : *→ Personne* - Personne 

Organisation
************

Chaque organisation dispose des :ref:`champs adresse <valeurs-champs-adresse>`, ainsi que des champs suivants :

- **adminact_operator__** : *→ Actes administratifs (opérateur d'archéologie préventive)*
- **cached_label** : *Texte* - Nom en cache
- **files__** : *→ Dossiers archéologiques (organisation)*
- **general_contractor_files__** : *→ Dossiers archéologiques (organisation de l'aménageur)*
- **grammatical_gender** : *Chaîne de caractères (1)* - Genre grammatical -  "M": Masculin ; "F": Féminin ; "N": Neutre
- **members__** : *→ Personnes (est rattaché à)*
- **name** : *Chaîne de caractères (500)* - Nom
- **operation_applicant_authority__** : *→ Opérations (autorité requérante)*
- **operator__** : *→ Opérations (opérateur)*
- **organization_type__** : *→ Type d'organisation* (**label** Dénomination, **txt_idx** Identifiant textuel, **grammatical_gender** Genre grammatical - "M": Masculin ; "F": Féminin ; "N": Neutre) - Type 
- **planning_service_files__** : *→ Dossiers archéologiques (service instructeur)*
- **publish__** : *→ Documents (éditions)*
- **treatmentfile_applicant__** : *→ Demandes de traitement (organisation demandeuse)*
- **treatments__** : *→ Traitements (organisation)*
- **url** : *Adresse web (200)* - Adresse web
- **warehouses__** : *→ Lieux de conservation (organisation)*


Documentation
*************


- **additional_information** : *Texte* - Information supplémentaire
- **administrativeacts__** : *→ Actes administratifs (documents)*
- **areas__** : *→ Zones* (documents - **label** Dénomination, **txt_idx** Identifiant textuel)
- **associated_file** : *Image (255)* - Dossier associé - La taille maximale supportée pour le fichier est de 100 Mo.
- **associated_links** : *Texte* - Liens symboliques
- **associated_url** : *Adresse web (1000)* - Ressource numérique (adresse web)
- **authors__** : *→ Auteur* - Auteurs 
- **authors_raw** : *Chaîne de caractères (250)* - Auteurs (brut)
- **cache_related_label** : *Texte* - Lié - Valeur en cache - ne pas éditer
- **cached_label** : *Texte* - Nom en cache - Généré automatiquement - ne pas éditer
- **children__** : *→ Documents (source)*
- **comment** : *Texte* - Commentaire
- **complete_identifier** : *Texte* - Identifiant complet
- **container_id** : *Entier positif* - ID contenant
- **container_ref_id** : *Entier positif* - ID contenant de référence
- **containers__** : *→ Contenants (documents)*
- **context_records__** : *→ Unité d'Enregistrement (documents)*
- **creation_date** : *Date* - Date de création
- **custom_index** : *Entier* - Custom index
- **description** : *Texte* - Description
- **duplicate** : *Booléen* - Existe en doublon
- **external_id** : *Texte* - Identifiant
- **files__** : *→ Dossiers archéologiques (documents)*
- **finds__** : *→ Mobilier (documents)*
- **format_type__** : *→ Type de format* (**label** Dénomination, **txt_idx** Identifiant textuel) - Format 
- **image** : *Image (255)* - image - La taille maximale supportée pour le fichier est de 100 Mo.
- **index** : *Entier* - Index
- **internal_reference** : *Texte* - Réf. interne
- **isbn** : *Chaîne de caractères (17)* - ISBN
- **issn** : *Chaîne de caractères (10)* - ISSN
- **item_number** : *Entier* - Nombre d'éléments
- **language__** : *→ Langue* (**label** Dénomination, **txt_idx** Identifiant textuel) - Langue 
- **licenses__** : *→ Type de licence* (**label** Dénomination, **txt_idx** Identifiant textuel) - Licence 
- **main_image_administrativeacts__** : *→ Actes administratifs (image principale)*
- **main_image_areas__** : *→ Zones* (image principale - **label** Dénomination, **txt_idx** Identifiant textuel)
- **main_image_containers__** : *→ Contenants (image principale)*
- **main_image_context_records__** : *→ Unité d'Enregistrement (image principale)*
- **main_image_finds__** : *→ Mobilier (image principale)*
- **main_image_operations__** : *→ Opérations (image principale)*
- **main_image_sites__** : *→ Entités archéologiques (image principale)*
- **main_image_towns__** : *→ Communes* (image principale - **name** Nom, **numero_insee** Code commune (numéro INSEE), **cached_label** Nom en cache)
- **main_image_treatment_files__** : *→ Demandes de traitement (image principale)*
- **main_image_treatments__** : *→ Traitements (image principale)*
- **main_image_warehouses__** : *→ Lieux de conservation (image principale)*
- **operations__** : *→ Opérations (documents)*
- **publisher__** : *→ Organisation* - Éditions 
- **publishing_year** : *Entier positif* - Année de publication
- **qrcode** : *Image (255)* - qrcode
- **receipt_date** : *Date* - Date de réception
- **receipt_date_in_documentation** : *Date* - Date de réception en documentation
- **reference** : *Texte* - Réf.
- **scale** : *Chaîne de caractères (30)* - Échelle
- **sites__** : *→ Entités archéologiques (documents)*
- **source__** : *→ Document* - Source 
- **source_free_input** : *Chaîne de caractères (500)* - Source - saisie libre
- **source_page_range** : *Chaîne de caractères (500)* - Source - intervalle de pages
- **source_type__** : *→ Type de document* (**label** Dénomination, **txt_idx** Identifiant textuel) - Type 
- **support_type__** : *→ Type de support* (**label** Dénomination, **txt_idx** Identifiant textuel) - Support 
- **tags__** : *→ Mot-clé du document* (**label** Dénomination, **txt_idx** Identifiant textuel) - Mots-clés 
- **thumbnail** : *Image (255)* - thumbnail - La taille maximale supportée pour le fichier est de 100 Mo.
- **title** : *Texte* - Titre
- **towns__** : *→ Communes* (documents - **name** Nom, **numero_insee** Code commune (numéro INSEE), **cached_label** Nom en cache)
- **treatment_files__** : *→ Demandes de traitement (documents)*
- **treatments__** : *→ Traitements (documents)*
- **warehouses__** : *→ Lieux de conservation (documents)*


Élément géographique
********************


- **buffer** : *Nombre à virgule* - Tampon
- **buffer_type__** : *→ Géographie - Type de tampon* (**label** Dénomination, **txt_idx** Identifiant textuel) - buffer type 
- **comment** : *Texte* - Commentaire
- **data_type__** : *→ Géographie - Type de données* (**label** Dénomination, **txt_idx** Identifiant textuel) - Type de donnée  - Par exemple : contour, Z-supérieur, ... 
- **main_related_items_archaeological_context_records_contextrecord__** : *→ Unité d'Enregistrement (élément géographique principal)*
- **main_related_items_archaeological_finds_basefind__** : *→ Mobilier d'origine (élément géographique principal)*
- **main_related_items_archaeological_operations_archaeologicalsite__** : *→ Entités archéologiques (élément géographique principal)*
- **main_related_items_archaeological_operations_operation__** : *→ Opérations (élément géographique principal)*
- **main_related_items_archaeological_warehouse_container__** : *→ Contenants (élément géographique principal)*
- **main_related_items_archaeological_warehouse_warehouse__** : *→ Lieux de conservation (élément géographique principal)*
- **main_related_items_ishtar_common_town__** : *→ Communes* (élément géographique principal - **name** Nom, **numero_insee** Code commune (numéro INSEE), **cached_label** Nom en cache)
- **multi_line** : *Multi-lignes* - Lignes multiples
- **multi_points** : *Multi-points* - Points multiples
- **name** : *Texte* - Nom
- **origin__** : *→ Géographie - Type d'origine* (**label** Dénomination, **txt_idx** Identifiant textuel) - Origine  - Par exemple : levé topographiques, géoréférencement, ... 
- **point_3d** : *Point* - Point (3D)
- **provider__** : *→ Géographie - Type de fournisseur* (**label** Dénomination, **txt_idx** Identifiant textuel) - Fournisseur  - Fournisseur de données 
- **related_items_archaeological_context_records_contextrecord__** : *→ Unité d'Enregistrement (élément géographique)*
- **related_items_archaeological_finds_basefind__** : *→ Mobilier d'origine (élément géographique)*
- **related_items_archaeological_operations_archaeologicalsite__** : *→ Entités archéologiques (élément géographique)*
- **related_items_archaeological_operations_operation__** : *→ Opérations (élément géographique)*
- **related_items_archaeological_warehouse_container__** : *→ Contenants (élément géographique)*
- **related_items_archaeological_warehouse_warehouse__** : *→ Lieux de conservation (élément géographique)*
- **related_items_ishtar_common_town__** : *→ Communes* (élément géographique - **name** Nom, **numero_insee** Code commune (numéro INSEE), **cached_label** Nom en cache)
- **source_content_type__** : *→ type de contenu* - source content type 
- **source_id** : *Entier positif* - source id
- **spatial_reference_system__** : *→ Géographie - Système de référence spatiale* (**label** Dénomination, **txt_idx** Identifiant textuel, **srid** SRID, **auth_name** Registre) - Système de référence spatiale 

Acte administratif
******************


- **act_object** : *Texte* - Objet
- **act_type__** : *→ Type d'acte* (**label** Dénomination, **txt_idx** Identifiant textuel) - Type d'acte 
- **associated_file__** : *→ Dossier archéologique* - Dossier archéologique 
- **comment** : *Texte* - Commentaire
- **departments_label** : *Texte* - Départements - Valeur en cache des départements associés
- **documents__** : *→ Document* - Documents 
- **in_charge__** : *→ Personne* - Responsable de suivi scientifique 
- **index** : *Entier* - Index
- **main_image__** : *→ Document* - Image principale 
- **operation__** : *→ Opération* - Opération 
- **operator__** : *→ Organisation* - Opérateur d'archéologie préventive 
- **property__** : *→ Propriétés (acte administratif)*
- **ref_sra** : *Chaîne de caractères (200)* - Autre référence
- **scientist__** : *→ Personne* - Responsable d'opération 
- **signatory__** : *→ Personne* - Signataire 
- **signature_date** : *Date* - Date de signature
- **towns_label** : *Texte* - Communes - Valeur en cache des communes associées
- **treatment__** : *→ Traitement* - Traitement 
- **treatment_file__** : *→ Demande de traitement* - Demande de traitement 
- **year** : *Entier* - Année

Commune
*******


- **areas__** : *→ Zones* (communes - **label** Dénomination, **txt_idx** Identifiant textuel)
- **cached_label** : *Chaîne de caractères (500)* - Nom en cache
- **center** : *Point* - Localisation
- **children__** : *→ Commune* (**name** Nom, **numero_insee** Code commune (numéro INSEE), **cached_label** Nom en cache) - Communes enfants 
- **context_record__** : *→ Unité d'Enregistrement (commune)*
- **documents__** : *→ Document* - Documents 
- **file__** : *→ Dossiers archéologiques (communes)*
- **file_main__** : *→ Dossiers archéologiques (commune principale)*
- **geodata__** : *→ Géographie - Donnée vectorielle* - Élément géographique 
- **limit** : *Multi-polygones* - Limite
- **main_geodata__** : *→ Géographie - Donnée vectorielle* - Élément géographique principal 
- **main_image__** : *→ Document* - Image principale 
- **name** : *Chaîne de caractères (100)* - Nom
- **numero_insee** : *Chaîne de caractères (120)* - Code commune (numéro INSEE)
- **operations__** : *→ Opérations (communes)*
- **parcels__** : *→ Parcelles (commune)*
- **parents__** : *→ Communes* (communes enfants - **name** Nom, **numero_insee** Code commune (numéro INSEE), **cached_label** Nom en cache)
- **sites__** : *→ Entités archéologiques (communes)*
- **surface** : *Entier* - Surface (m2)
- **year** : *Entier* - Année de création - Remplir ce champ est nécessaire pour distinguer les anciennes communes des nouvelles communes.

Zone géographique
*****************


- **available** : *Booléen* - Disponible
- **children__** : *→ Zones* (parent - **label** Dénomination, **txt_idx** Identifiant textuel)
- **comment** : *Texte* - Commentaire
- **documents__** : *→ Document* - Documents 
- **label** : *Texte* - Dénomination
- **main_image__** : *→ Document* - Image principale 
- **parent__** : *→ Zone* (**label** Dénomination, **txt_idx** Identifiant textuel) - Parent  - Seulement quatre niveaux de parents sont gérés. 
- **reference** : *Texte* - Référence
- **towns__** : *→ Commune* (**name** Nom, **numero_insee** Code commune (numéro INSEE), **cached_label** Nom en cache) - Communes 
- **txt_idx** : *Texte* - Identifiant textuel - Le "slug" est une version standardisée du nom. Il ne contient que des lettres en minuscule, des nombres et des tirets (-). Chaque "slug" doit être unique dans la typologie.

Parcelle
********


- **associated_file__** : *→ Dossier archéologique* - Dossier 
- **cached_label** : *Texte* - Nom en cache
- **context_record__** : *→ Unité d'Enregistrement (parcelle)*
- **external_id** : *Chaîne de caractères (100)* - Identifiant
- **operation__** : *→ Opération* - Opération 
- **owners__** : *→ Propriétaires de parcelle (parcelle)*
- **parcel_number** : *Chaîne de caractères (6)* - Numéro de parcelle
- **public_domain** : *Booléen* - Domaine public
- **section** : *Chaîne de caractères (4)* - Section
- **year** : *Entier* - Année

Datation
********


- **context_records__** : *→ Unité d'Enregistrement (datings)*
- **dating_type__** : *→ Type de datation* (**label** Dénomination, **txt_idx** Identifiant textuel) - Type de datation 
- **end_date** : *Entier* - Date de fin
- **find__** : *→ Mobilier (datation)*
- **period__** : *→ Type de période* (**label** Dénomination, **txt_idx** Identifiant textuel) - Période chronologique 
- **precise_dating** : *Texte* - Précision sur la datation
- **quality__** : *→ Type de qualité de datation* (**label** Dénomination, **txt_idx** Identifiant textuel) - Qualité 
- **start_date** : *Entier* - Date de début

Modèles des éléments principaux
-------------------------------

Opération
*********

Chaque opération dispose du champ `data__` ainsi que des champs suivants :


- **abstract** : *Texte* - Résumé
- **administrative_act__** : *→ Actes administratifs (opération)*
- **applicant_authority__** : *→ Organisation* - Autorité requérante 
- **archaeological_sites__** : *→ Entité archéologique* - Entités archéologiques 
- **associated_file__** : *→ Dossier archéologique* - Dossier 
- **cached_label** : *Texte* - Nom en cache - Généré automatiquement - ne pas éditer
- **cached_periods** : *Texte* - Nom en cache des périodes - Généré automatiquement - ne pas éditer
- **cached_remains** : *Texte* - Nom en cache des vestiges - Généré automatiquement - ne pas éditer
- **cached_towns_label** : *Texte* - Nom en cache des communes - Généré automatiquement - ne pas éditer
- **cira_date** : *Date* - Date avis CTRA/CIRA
- **cira_rapporteur__** : *→ Personne* - Rapporteur CTRA/CIRA 
- **code_patriarche** : *Texte* - Code PATRIARCHE
- **collaborators__** : *→ Personne* - Collaborateurs 
- **comment** : *Texte* - Commentaire
- **common_name** : *Texte* - Nom générique
- **complete_identifier** : *Texte* - Identifiant complet
- **containers** : Liste des contenants associés - **Champ non disponible pour les imports**
- **context_record__** : *→ Unité d'Enregistrement (opération)*
- **context_records** : Liste des unités d'enregistrement associées - **Champ non disponible pour les imports**
- **cost** : *Entier* - Coût (euros)
- **creation_date** : *Date* - Date de création
- **custom_index** : *Entier* - Custom index
- **documentation_deadline** : *Date* - Date limite de versement de la documentation
- **documentation_received** : *Booléen* - Documentation versée
- **documents__** : *→ Document* - Documents 
- **drassm_code** : *Chaîne de caractères (100)* - Code DRASSM
- **eas_number** : *Chaîne de caractères (20)* - Numéro de l'EA
- **effective_man_days** : *Entier* - Jours-hommes effectifs
- **end_date** : *Date* - Date de clôture
- **excavation_end_date** : *Date* - Date de fin de chantier
- **finds_deadline** : *Date* - Date limite de versement du mobilier
- **finds_received** : *Booléen* - Mobilier versé
- **fnap_cost** : *Entier* - Financement FNAP (€)
- **fnap_financing** : *Nombre à virgule* - Financement FNAP (%)
- **geoarchaeological_context_prescription** : *Booléen* - Prescription sur un contexte géoarchéologique
- **geodata__** : *→ Géographie - Donnée vectorielle* - Élément géographique 
- **in_charge__** : *→ Personne* - Responsable de suivi scientifique 
- **large_area_prescription** : *Booléen* - Prescription sur une vaste surface
- **left_relations__** : *→ Relations entre opérations (right record)*
- **main_geodata__** : *→ Géographie - Donnée vectorielle* - Élément géographique principal 
- **main_image__** : *→ Document* - Image principale 
- **minutes_writer__** : *→ Personne* - Rédacteur du procès verbal 
- **negative_result** : *Booléen* - Résultat considéré comme négatif
- **official_report_number** : *Texte* - Numéro de procès-verbal ou de saisine
- **old_code** : *Chaîne de caractères (200)* - Ancien code
- **operation_code** : *Entier* - Identifiant numérique
- **operation_type__** : *→ Type d'opération* (**label** Dénomination, **txt_idx** Identifiant textuel) - Type d'opération 
- **operationbydepartment__** : *→ operation by departments (opération)*
- **operator__** : *→ Organisation* - Opérateur 
- **operator_reference** : *Chaîne de caractères (20)* - Référence de l'opérateur
- **optional_man_days** : *Entier* - Jours-hommes optionnels
- **parcels__** : *→ Parcelles (opération)*
- **periods__** : *→ Type de période* (**label** Dénomination, **txt_idx** Identifiant textuel) - Périodes 
- **protagonist__** : *→ Personne* - Nom du protagoniste 
- **qrcode** : *Image (255)* - qrcode
- **record_quality_type__** : *→ Type de qualité d'enregistrement* (**label** Dénomination, **txt_idx** Identifiant textuel) - Qualité d'enregistrement 
- **relation_bitmap_image** : *Image (100)* - Image des relations (PNG généré) - La taille maximale supportée pour le fichier est de 100 Mo.
- **relation_image** : *Image (100)* - Image des relations (SVG généré) - La taille maximale supportée pour le fichier est de 100 Mo.
- **remains__** : *→ Type de vestige* (**label** Dénomination, **txt_idx** Identifiant textuel) - Vestiges 
- **report_delivery_date** : *Date* - Date de livraison du rapport
- **report_processing__** : *→ Type d'état de rapport* (**label** Dénomination, **txt_idx** Identifiant textuel) - Traitement du rapport 
- **right_relations__** : *→ Relations entre opérations (left record)*
- **scheduled_man_days** : *Entier* - Jours-hommes prévus
- **scientific_documentation_comment** : *Texte* - Commentaire relatif à la documentation scientifique
- **scientist__** : *→ Personne* - Responsable de suivi scientifique 
- **seizure_name** : *Texte* - Nom de la saisie
- **spatial_reference_system__** : *→ Géographie - Système de référence spatiale* (**label** Dénomination, **txt_idx** Identifiant textuel, **srid** SRID, **auth_name** Registre) - Système de référence spatiale 
- **start_date** : *Date* - Date de début
- **surface** : *Nombre à virgule* - Surface (m2)
- **top_sites__** : *→ Entité archéologique* - Sites pour lesquels cette opération est une opération chapeau 
- **towns__** : *→ Commune* (**name** Nom, **numero_insee** Code commune (numéro INSEE), **cached_label** Nom en cache) - Communes 
- **virtual_operation** : *Booléen* - Opération virtuelle - Si coché, cela signifie que cette opération n'a pas été officiellement enregistrée.
- **year** : *Entier* - Année
- **zoning_prescription** : *Booléen* - Prescription sur zonage

Site/Entité archéologique
*************************

Chaque site/entité archéologique dispose du champ `data__` ainsi que des champs suivants :


- **affmar_number** : *Chaîne de caractères (100)* - Numéro AffMar
- **cached_label** : *Texte* - Nom en cache - Généré automatiquement - ne pas éditer
- **cached_periods** : *Texte* - Nom en cache des périodes - Généré automatiquement - ne pas éditer
- **cached_remains** : *Texte* - Nom en cache des vestiges - Généré automatiquement - ne pas éditer
- **cached_towns_label** : *Texte* - Nom en cache des communes - Généré automatiquement - ne pas éditer
- **collaborators__** : *→ Personne* - Collaborateurs 
- **comment** : *Texte* - Commentaire
- **complete_identifier** : *Texte* - Identifiant complet
- **context_records__** : *→ Unité d'Enregistrement (entité archéologique)*
- **cultural_attributions__** : *→ Type d'attribution culturelle* (**label** Dénomination, **txt_idx** Identifiant textuel) - Attribution culturelle 
- **custom_index** : *Entier* - Custom index
- **discovery_area** : *Texte* - Zone de découverte
- **documents__** : *→ Document* - Documents 
- **drassm_number** : *Chaîne de caractères (100)* - Numéro DRASSM
- **geodata__** : *→ Géographie - Donnée vectorielle* - Élément géographique 
- **locality_cadastral** : *Texte* - Lieu-dit cadastre
- **locality_ngi** : *Texte* - Lieu-dit IGN
- **main_geodata__** : *→ Géographie - Donnée vectorielle* - Élément géographique principal 
- **main_image__** : *→ Document* - Image principale 
- **name** : *Chaîne de caractères (200)* - Nom
- **oceanographic_service_localisation** : *Texte* - Localisation SHOM
- **operations__** : *→ Opérations (entités archéologiques)*
- **other_reference** : *Texte* - Autre référence
- **periods__** : *→ Type de période* (**label** Dénomination, **txt_idx** Identifiant textuel) - Périodes 
- **qrcode** : *Image (255)* - qrcode
- **reference** : *Chaîne de caractères (200)* - Référence
- **remains__** : *→ Type de vestige* (**label** Dénomination, **txt_idx** Identifiant textuel) - Vestiges 
- **shipwreck_code** : *Texte* - Code épave
- **shipwreck_name** : *Texte* - Nom de l'épave
- **sinking_date** : *Date* - Date de naufrage
- **spatial_reference_system__** : *→ Géographie - Système de référence spatiale* (**label** Dénomination, **txt_idx** Identifiant textuel, **srid** SRID, **auth_name** Registre) - Système de référence spatiale 
- **top_operations__** : *→ Opérations (sites pour lesquels cette opération est une opération chapeau)*
- **towns__** : *→ Commune* (**name** Nom, **numero_insee** Code commune (numéro INSEE), **cached_label** Nom en cache) - Communes 

Dossier archéologique
*********************

Chaque dossier archéologique dispose du champ `data__` ainsi que des champs suivants :


- **administrative_act__** : *→ Actes administratifs (dossier archéologique)*
- **cached_label** : *Texte* - Nom en cache - Généré automatiquement - ne pas éditer
- **cira_advised** : *Booléen* - Passage en CIRA
- **classified_area** : *Booléen* - Au sein d'un site classé
- **comment** : *Texte* - Commentaire
- **complete_identifier** : *Texte* - Identifiant complet
- **corporation_general_contractor__** : *→ Organisation* - Organisation de l'aménageur 
- **creation_date** : *Date* - Date de création
- **custom_index** : *Entier* - Custom index
- **departments__** : *→ Département* - Départements 
- **documents__** : *→ Document* - Documents 
- **end_date** : *Date* - Date de fin
- **equipment_costs__** : *→ preventive file equipment service costs (file)*
- **execution_report_date** : *Date* - Date du rapport d'exécution
- **external_id** : *Chaîne de caractères (120)* - Identifiant
- **file__** : *→ Dossiers archéologiques (dossier associé)*
- **file_type__** : *→ Type de dossier archéologique* (**label** Dénomination, **txt_idx** Identifiant textuel) - Type de dossier 
- **filebydepartment__** : *→ file by departments (dossier)*
- **general_contractor__** : *→ Personne* - Aménageur 
- **ground_end_date** : *Date* - Date de fin de la fouille
- **ground_jobs__** : *→ preventive file ground jobs (file)*
- **ground_start_date** : *Date* - Date de début de la fouille
- **imported_line** : *Texte* - Ligne importée
- **in_charge__** : *→ Personne* - Personne responsable 
- **instruction_deadline** : *Date* - Date limite d'instruction
- **internal_reference** : *Chaîne de caractères (60)* - Référence interne
- **intervention_period** : *Chaîne de caractères (200)* - Période d'intervention
- **jobs__** : *→ preventive file jobs (file)*
- **linear_meter** : *Entier* - Mètre linéaire
- **locality** : *Chaîne de caractères (100)* - Lieu-dit
- **main_town__** : *→ Commune* (**name** Nom, **numero_insee** Code commune (numéro INSEE), **cached_label** Nom en cache) - Commune principale 
- **mh_listing** : *Booléen* - Sur Monument Historique inscrit
- **mh_register** : *Booléen* - Sur Monument Historique classé
- **name** : *Texte* - Nom
- **numeric_reference** : *Entier* - Identifiant numérique
- **operation_type_for_royalties__** : *→ Type d'opération pour les redevances - France* (**label** Dénomination, **txt_idx** Identifiant textuel) - Type d'opération pour la redevance 
- **operations__** : *→ Opérations (dossier)*
- **organization__** : *→ Organisation* - Organisation 
- **parcels__** : *→ Parcelles (dossier)*
- **permit_reference** : *Texte* - Référence du permis / de l'arrêté
- **permit_type__** : *→ Type de permis* (**label** Dénomination, **txt_idx** Identifiant textuel) - Type de permis 
- **planning_service__** : *→ Organisation* - Service instructeur 
- **planning_service_date** : *Date* - Date du dossier du service instructeur
- **price_agreement__** : *→ Contrat tarifaire* (**label** Dénomination, **txt_idx** Identifiant textuel) - Contrat tarifaire 
- **protected_area** : *Booléen* - Au sein d'un secteur sauvegardé
- **qrcode** : *Image (255)* - qrcode
- **raw_general_contractor** : *Chaîne de caractères (200)* - Aménageur (brut)
- **raw_town_planning_service** : *Chaîne de caractères (200)* - Service instructeur (brut)
- **reception_date** : *Date* - Date de réception
- **related_file__** : *→ Dossier archéologique* - Dossier associé 
- **report_due_period** : *Chaîne de caractères (200)* - Période de remise du rapport
- **requested_operation_type__** : *→ Type d'opération* (**label** Dénomination, **txt_idx** Identifiant textuel) - Type d'opération demandée 
- **research_comment** : *Texte* - Commentaire relatif à l'archéologie programmée
- **responsible_town_planning_service__** : *→ Personne* - Responsable pour le service instructeur 
- **saisine_type__** : *→ Type de saisine* (**label** Dénomination, **txt_idx** Identifiant textuel) - Type de saisine 
- **scientist__** : *→ Personne* - Responsable d'opération 
- **start_date** : *Date* - Date de début
- **study_period** : *Chaîne de caractères (200)* - Période d'étude
- **total_developed_surface** : *Nombre à virgule* - Surface totale aménagée (m2)
- **total_surface** : *Nombre à virgule* - Surface totale (m2)
- **towns__** : *→ Commune* (**name** Nom, **numero_insee** Code commune (numéro INSEE), **cached_label** Nom en cache) - Communes 
- **type_of_agreement__** : *→ Type de convention - France* (**label** Dénomination, **txt_idx** Identifiant textuel) - Type de convention 
- **year** : *Entier* - Année

Unité d'enregistrement
**********************

Chaque unité d'enregistrement dispose du champ `data__` ainsi que des champs suivants :


- **activity__** : *→ Type d'activité* (**label** Dénomination, **txt_idx** Identifiant textuel) - Activité 
- **archaeological_site__** : *→ Entité archéologique* - Entité archéologique 
- **base_finds** : Liste du mobilier de base associé - **Champ non disponible pour les imports**
- **base_finds__** : *→ Mobilier d'origine (unité d'enregistrement)*
- **cached_label** : *Texte* - Nom en cache - Généré automatiquement - ne pas éditer
- **cached_periods** : *Texte* - Nom en cache des périodes - Généré automatiquement - ne pas éditer
- **cached_related_context_records** : *Texte* - Nom en cache des unités d'enregistrements liées - Généré automatiquement - ne pas éditer
- **closing_date** : *Date* - Date de clôture
- **comment** : *Texte* - Commentaire général
- **complete_identifier** : *Texte* - Identifiant complet
- **context_record_tree_child__** : *→ context record trees (unité d'enregistrement parente)*
- **context_record_tree_parent__** : *→ context record trees (unité d'enregistrement)*
- **contextrecord__** : *→ Unité d'Enregistrement (related context records)*
- **custom_index** : *Entier* - Custom index
- **datings__** : *→ Datation* - datings 
- **datings_comment** : *Texte* - Commentaire relatif aux datations
- **depth** : *Nombre à virgule* - Profondeur (m)
- **depth_of_appearance** : *Nombre à virgule* - Profondeur d'apparition (m)
- **description** : *Texte* - Description
- **diameter** : *Nombre à virgule* - Diamètre (m)
- **documentations__** : *→ Type de documentation* (**label** Dénomination, **txt_idx** Identifiant textuel) - documentations 
- **documents__** : *→ Document* - Documents 
- **excavation_technics__** : *→ Type de méthode de fouille* (**label** Dénomination, **txt_idx** Identifiant textuel) - Méthodes de fouille 
- **external_id** : *Texte* - Identifiant
- **filling** : *Texte* - Remplissage
- **geodata__** : *→ Géographie - Donnée vectorielle* - Élément géographique 
- **identification__** : *→ Type d'identification* (**label** Dénomination, **txt_idx** Identifiant textuel) - Identification 
- **interpretation** : *Texte* - Interprétation
- **label** : *Chaîne de caractères (200)* - Identifiant
- **left_relations__** : *→ Relations entre Unités d'Enregistrement (right record)*
- **length** : *Nombre à virgule* - Taille (m)
- **location** : *Texte* - Localisation - Une courte description de la localisation de l'Unité d'Enregistrement
- **main_geodata__** : *→ Géographie - Donnée vectorielle* - Élément géographique principal 
- **main_image__** : *→ Document* - Image principale 
- **opening_date** : *Date* - Date d'ouverture
- **operation__** : *→ Opération* - Opération 
- **parcel__** : *→ Parcelle* - Parcelle 
- **qrcode** : *Image (255)* - qrcode
- **related_context_records__** : *→ Unité d'Enregistrement* - related context records 
- **relation_bitmap_image** : *Image (100)* - Image des relations (PNG généré) - La taille maximale supportée pour le fichier est de 100 Mo.
- **relation_image** : *Image (100)* - Image des relations (SVG généré) - La taille maximale supportée pour le fichier est de 100 Mo.
- **right_relations__** : *→ Relations entre Unités d'Enregistrement (left record)*
- **spatial_reference_system__** : *→ Géographie - Système de référence spatiale* (**label** Dénomination, **txt_idx** Identifiant textuel, **srid** SRID, **auth_name** Registre) - Système de référence spatiale 
- **surface** : *Nombre à virgule* - Surface (m2)
- **taq** : *Entier* - TAQ - « Terminus Ante Quem ». L'Unité d'Enregistrement ne peut avoir été créée après cette date
- **taq_estimated** : *Entier* - TAQ estimé - Estimation d'un « Terminus Ante Quem »
- **thickness** : *Nombre à virgule* - Épaisseur (m)
- **tpq** : *Entier* - TPQ - « Terminus Post Quem ». L'Unité d'Enregistrement ne peut avoir été créée avant cette date
- **tpq_estimated** : *Entier* - TPQ estimé - Estimation d'un « Terminus Post Quem »
- **unit__** : *→ Type d'Unité d'Enregistrement* (**label** Dénomination, **txt_idx** Identifiant textuel) - Type d'Unité d'Enregistrement 
- **width** : *Nombre à virgule* - Largeur (m)

Mobilier d'origine
******************

Chaque mobilier d'origine dispose du champ `data__` ainsi que des champs suivants :


- **batch__** : *→ Type de lot* (**label** Dénomination, **txt_idx** Identifiant textuel) - Lot/objet 
- **cache_complete_id** : *Texte* - Identifiant complet - Valeur en cache - ne pas éditer
- **cache_short_id** : *Texte* - Identifiant court - Valeur en cache - ne pas éditer
- **cached_label** : *Texte* - Nom en cache - Généré automatiquement - ne pas éditer
- **comment** : *Texte* - Commentaire
- **complete_identifier** : *Texte* - Identifiant complet
- **context_record__** : *→ Unité d'Enregistrement* - Unité d'Enregistrement 
- **custom_index** : *Entier* - Custom index
- **description** : *Texte* - Description
- **discovery_date** : *Date* - Date de découverte (exacte ou début)
- **discovery_date_taq** : *Date* - Date de découverte (fin)
- **excavation_id** : *Texte* - Identifiant  fouille
- **external_id** : *Texte* - Identifiant
- **find__** : *→ Mobilier (mobilier d'origine)*
- **geodata__** : *→ Géographie - Donnée vectorielle* - Élément géographique 
- **index** : *Entier* - Index
- **label** : *Texte* - Identifiant libre
- **line** : *Ligne* - Ligne
- **main_geodata__** : *→ Géographie - Donnée vectorielle* - Élément géographique principal 
- **material_index** : *Entier* - Index matériel
- **material_type_label** : Concaténation des types de matériaux associés - **Champ non disponible pour les imports**
- **qrcode** : *Image (255)* - qrcode
- **spatial_reference_system__** : *→ Géographie - Système de référence spatiale* (**label** Dénomination, **txt_idx** Identifiant textuel, **srid** SRID, **auth_name** Registre) - Système de référence spatiale 
- **special_interest** : *Chaîne de caractères (120)* - Intérêt spécifique
- **topographic_localisation** : *Chaîne de caractères (120)* - Point topographique

Mobilier
********

Chaque élément mobilier dispose du champ `data__` ainsi que des champs suivants :


- **alteration_causes__** : *→ Type de cause d'altération* (**label** Dénomination, **txt_idx** Identifiant textuel) - Cause d'altération 
- **alterations__** : *→ Type d'altération* (**label** Dénomination, **txt_idx** Identifiant textuel) - Altération 
- **appraisal_date** : *Date* - Date d'évaluation
- **base_finds** : Liste du mobilier de base associé - **Champ non disponible pour les imports**
- **base_finds__** : *→ Mobilier d'origine* - Mobilier d'origine 
- **basket__** : *→ Paniers (items)*
- **cached_label** : *Texte* - Nom en cache - Généré automatiquement - ne pas éditer
- **cached_materials** : *Texte* - Nom en cache des types de matériaux - Généré automatiquement - ne pas éditer
- **cached_object_types** : *Texte* - Nom en cache des types d'objet - Généré automatiquement - ne pas éditer
- **cached_periods** : *Texte* - Nom en cache des périodes - Généré automatiquement - ne pas éditer
- **check_date** : *Date* - Date de vérification
- **checked_type__** : *→ Type de vérification* (**label** Dénomination, **txt_idx** Identifiant textuel) - Vérification 
- **circumference** : *Nombre à virgule* - Circonférence (cm)
- **clutter_height** : *Nombre à virgule* - Encombrement - hauteur (cm)
- **clutter_long_side** : *Nombre à virgule* - Encombrement - grand côté (cm)
- **clutter_short_side** : *Nombre à virgule* - Encombrement - petit côté (cm)
- **collection__** : *→ Lieu de conservation* - Collection  - Ne pas utiliser - Nécessite des évolutions 
- **comment** : *Texte* - Commentaire
- **communicabilities__** : *→ Type de communicabilité* (**label** Dénomination, **txt_idx** Identifiant textuel) - Communicabilité 
- **complete_id** : Identifiant complet du mobilier d'origine associé - **Champ non disponible pour les imports**
- **complete_identifier** : *Texte* - Identifiant complet
- **conservatory_comment** : *Texte* - Commentaire relatif à la conservation
- **conservatory_state__** : *→ Type d'état de conservation* (**label** Dénomination, **txt_idx** Identifiant textuel) - État de conservation 
- **container__** : *→ Contenant* - Contenant 
- **container_ref__** : *→ Contenant* - Contenant de référence 
- **context_record_label** : Dénomination des unités d'enregistrement associées - **Champ non disponible pour les imports**
- **cultural_attributions__** : *→ Type d'attribution culturelle* (**label** Dénomination, **txt_idx** Identifiant textuel) - Attribution culturelle 
- **custom_index** : *Entier* - Custom index
- **dating_comment** : *Texte* - Commentaire relatif aux datations
- **datings__** : *→ Datation* - Datation 
- **decoration** : *Texte* - Décor
- **denomination** : *Texte* - Dénomination
- **description** : *Texte* - Description
- **diameter** : *Nombre à virgule* - Diamètre (cm)
- **dimensions_comment** : *Texte* - Commentaire relatif aux dimensions
- **documents__** : *→ Document* - Documents 
- **downstream_treatment__** : *→ Traitement* - Traitement aval 
- **estimated_value** : *Nombre à virgule* - Valeur estimée
- **external_id** : *Texte* - Identifiant
- **find_number** : *Entier* - Nombre de restes (NR)
- **finddownstreamtreatments_related__** : *→ find downstream treatmentss (mobilier)*
- **findnonmodiftreatments_related__** : *→ find non modif treatmentss (mobilier)*
- **findtreatments_related__** : *→ find treatmentss (mobilier)*
- **findupstreamtreatments_related__** : *→ find upstream treatmentss (mobilier)*
- **functional_areas__** : *→ Domaine fonctionnel* (**label** Dénomination, **txt_idx** Identifiant textuel) - Domaine fonctionnel 
- **height** : *Nombre à virgule* - Hauteur (cm)
- **index** : *Entier* - Index
- **inscription** : *Texte* - Inscription
- **inside_container__** : *→ find inside container (mobilier)*
- **insurance_value** : *Nombre à virgule* - Valeur d'assurance
- **integrities__** : *→ Type d'intégrité / intérêt* (**label** Dénomination, **txt_idx** Identifiant textuel) - Intégrité / intérêt 
- **is_complete** : *Booléen* - Est complet ?
- **label** : *Texte* - Identifiant libre
- **laboratory_id** : *Texte* - Numéro de laboratoire
- **length** : *Nombre à virgule* - Longueur (cm)
- **main_image__** : *→ Document* - Image principale 
- **manufacturing_place** : *Texte* - Lieu de fabrication
- **mark** : *Texte* - Marquage
- **material_comment** : *Texte* - Commentaire relatif au matériau
- **material_type_quality__** : *→ Type de qualité du type de matériaux* (**label** Dénomination, **txt_idx** Identifiant textuel) - Qualité du type de matériaux 
- **material_types__** : *→ Type de matériau* (**label** Dénomination, **txt_idx** Identifiant textuel) - Types de matériau 
- **material_types_code** : Code des types de matériau - **Champ non disponible pour les imports**
- **material_types_label** : Chaîne de caractère des types de matériau - **Champ non disponible pour les imports**
- **material_types_recommendations** : Chaîne de caractères - recommandations pour le matériau - **Champ non disponible pour les imports**
- **min_number_of_individuals** : *Entier* - Nombre minimum d'individus (NMI)
- **museum_id** : *Texte* - Numéro d'inventaire musée
- **object_type_quality__** : *→ Type de qualité du type d'objet* (**label** Dénomination, **txt_idx** Identifiant textuel) - Qualité du type d'objet 
- **object_types__** : *→ Type d'objet* (**label** Dénomination, **txt_idx** Identifiant textuel) - Types d'objet 
- **order** : *Entier* - Ordre
- **preservation_to_considers__** : *→ Type de traitement* (**label** Dénomination, **txt_idx** Identifiant textuel) - Traitements recommandés 
- **previous_id** : *Texte* - Identifiant précédent
- **property__** : *→ Propriétés (mobilier)*
- **public_description** : *Texte* - Description publique
- **qrcode** : *Image (255)* - qrcode
- **remarkabilities__** : *→ Type de remarquabilité* (**label** Dénomination, **txt_idx** Identifiant textuel) - Remarquabilité 
- **seal_number** : *Texte* - Numéro de scellé
- **thickness** : *Nombre à virgule* - Épaisseur (cm)
- **treatment_emergency__** : *→ Type d'urgence de traitement* (**label** Dénomination, **txt_idx** Identifiant textuel) - Urgence du traitement 
- **treatments__** : *→ Traitement* - Traitements  - Traitements associés quand il n'y a pas de création de nouveau mobilier 
- **upstream_treatment__** : *→ Traitement* - Traitement amont 
- **volume** : *Nombre à virgule* - Volume (l)
- **weight** : *Nombre à virgule* - Poids
- **weight_unit** : *Chaîne de caractères (4)* - Unité de poids
- **width** : *Nombre à virgule* - Largeur (cm)

Traitement
**********

Chaque traitement dispose du champ `data__` ainsi que des champs suivants :


- **administrative_act__** : *→ Actes administratifs (traitement)*
- **cached_label** : *Texte* - Nom en cache - Généré automatiquement - ne pas éditer
- **comment** : *Texte* - Commentaire
- **complete_identifier** : *Texte* - Identifiant complet
- **container__** : *→ Contenant* - Contenant 
- **creation_date** : *Date/heure* - creation date
- **custom_index** : *Entier* - Custom index
- **description** : *Texte* - Description
- **documents__** : *→ Document* - Documents 
- **downstream__** : *→ Mobilier (traitement amont)*
- **end_date** : *Date* - Date de clôture
- **estimated_cost** : *Nombre à virgule* - Coût estimé
- **executed** : *Booléen* - Le traitement a été réalisé
- **external_id** : *Chaîne de caractères (200)* - Identifiant
- **file__** : *→ Demande de traitement* - Demande associée 
- **finddownstreamtreatments__** : *→ find downstream treatments (traitement)*
- **findnonmodiftreatments__** : *→ find non modif treatments (traitement)*
- **finds__** : *→ Mobilier (traitements)*
- **findtreatments__** : *→ find treatments (traitement)*
- **findupstreamtreatments__** : *→ find upstream treatments (traitement)*
- **goal** : *Texte* - But
- **image** : *Image (255)* - image - La taille maximale supportée pour le fichier est de 100 Mo.
- **index** : *Entier* - Index
- **insurance_cost** : *Nombre à virgule* - Coût d'assurance
- **label** : *Chaîne de caractères (200)* - Dénomination
- **location__** : *→ Lieu de conservation* - Localisation  - Endroit où le traitement est réalisé. Renseignez le lieu de conservation de destination pour un déplacement. 
- **main_image__** : *→ Document* - Image principale 
- **organization__** : *→ Organisation* - Organisation 
- **other_reference** : *Chaîne de caractères (200)* - Autre réf.
- **person__** : *→ Personne* - Responsable 
- **qrcode** : *Image (255)* - qrcode
- **quoted_cost** : *Nombre à virgule* - Coût devisé
- **realized_cost** : *Nombre à virgule* - Coût réalisé
- **scientific_monitoring_manager__** : *→ Personne* - Responsable de suivi scientifique 
- **start_date** : *Date* - Date de début
- **thumbnail** : *Image (255)* - thumbnail - La taille maximale supportée pour le fichier est de 100 Mo.
- **treatment_state__** : *→ Type d'état de traitement* (**label** Dénomination, **txt_idx** Identifiant textuel) - État 
- **treatment_types__** : *→ Type de traitement* (**label** Dénomination, **txt_idx** Identifiant textuel) - Type de traitement 
- **upstream__** : *→ Mobilier (traitement aval)*
- **year** : *Entier* - Année

Demande de traitement
*********************

Chaque demande de traitement dispose du champ `data__` ainsi que des champs suivants :


- **administrative_act__** : *→ Actes administratifs (demande de traitement)*
- **applicant__** : *→ Personne* - Demandeur 
- **applicant_organisation__** : *→ Organisation* - Organisation demandeuse 
- **associated_basket__** : *→ Panier* - associated basket 
- **cached_label** : *Texte* - Nom en cache - Généré automatiquement - ne pas éditer
- **comment** : *Texte* - Commentaire
- **complete_identifier** : *Texte* - Identifiant complet
- **creation_date** : *Date* - Date de création
- **custom_index** : *Entier* - Custom index
- **documents__** : *→ Document* - Documents 
- **end_date** : *Date* - Date de clôture
- **exhibition_end_date** : *Date* - Date de fin de l'exposition
- **exhibition_name** : *Texte* - Nom d'exposition
- **exhibition_start_date** : *Date* - Date de commencement de l'exposition
- **external_id** : *Chaîne de caractères (200)* - Identifiant
- **in_charge__** : *→ Personne* - Personne responsable 
- **index** : *Entier* - Index
- **internal_reference** : *Chaîne de caractères (200)* - Référence interne
- **main_image__** : *→ Document* - Image principale 
- **name** : *Texte* - Nom
- **qrcode** : *Image (255)* - qrcode
- **reception_date** : *Date* - Date de réception
- **treatments__** : *→ Traitements (demande associée)*
- **type__** : *→ Type de demande de traitement* (**label** Dénomination, **txt_idx** Identifiant textuel) - Type de demande de traitement 
- **year** : *Entier* - Année

Dépot
*****

Chaque dépot dispose des :ref:`champs adresse <valeurs-champs-adresse>`, du champ `data__` ainsi que des champs suivants :


- **associated_divisions__** : *→ Type de division de lieu de conservation* (**label** Dénomination, **txt_idx** Identifiant textuel) - Divisions 
- **cached_label** : *Texte* - Nom en cache - Généré automatiquement - ne pas éditer
- **cached_town_label** : *Texte* - Nom en cache des communes - Généré automatiquement - ne pas éditer
- **comment** : *Texte* - Commentaire
- **complete_identifier** : *Texte* - Identifiant complet
- **containers__** : *→ Contenants (lieu de conservation)*
- **custom_index** : *Entier* - Custom index
- **divisions__** : *→ warehouse division links (warehouse)*
- **documents__** : *→ Document* - Documents 
- **external_id** : *Texte* - Identifiant
- **finds__** : *→ Mobilier (collection)*
- **geodata__** : *→ Géographie - Donnée vectorielle* - Élément géographique 
- **main_geodata__** : *→ Géographie - Donnée vectorielle* - Élément géographique principal 
- **main_image__** : *→ Document* - Image principale 
- **max_division_number** : *Entier* - Nombre maximum de divisions - Généré automatiquement
- **name** : *Chaîne de caractères (200)* - Nom
- **organization__** : *→ Organisation* - Organisation 
- **owned_containers__** : *→ Contenants (lieu de conservation responsable)*
- **person_in_charge__** : *→ Personne* - Personne responsable 
- **qrcode** : *Image (255)* - qrcode
- **responsibilities__** : *→ Contenants (responsabilité)*
- **slug** : * (200)* - Identifiant textuel
- **spatial_reference_system__** : *→ Géographie - Système de référence spatiale* (**label** Dénomination, **txt_idx** Identifiant textuel, **srid** SRID, **auth_name** Registre) - Système de référence spatiale 
- **treatment__** : *→ Traitements (localisation)*
- **warehouse_type__** : *→ Type de lieu de conservation* (**label** Dénomination, **txt_idx** Identifiant textuel) - Type de lieu de conservation 

Contenant
*********

Chaque contenant dispose du champ `data__` ainsi que des champs suivants :


- **cached_division** : *Texte* - Division mise en cache
- **cached_label** : *Texte* - Nom en cache - Généré automatiquement - ne pas éditer
- **cached_location** : *Texte* - Localisation - en cache
- **cached_weight** : *Nombre à virgule* - Poids mis en cache (g) - Poids saisi si disponible sinon poids calculé.
- **calculated_weight** : *Nombre à virgule* - Poids calculé (g)
- **children__** : *→ Contenants (contenant parent)*
- **comment** : *Texte* - Commentaire
- **complete_identifier** : *Texte* - Identifiant complet
- **container_content__** : *→ find inside containers (contenant)*
- **container_tree_child__** : *→ container tree (contenant)*
- **container_tree_parent__** : *→ container trees (contenant parent)*
- **container_type__** : *→ Type de contenant* (**label** Dénomination, **txt_idx** Identifiant textuel) - Type de contenant 
- **context_record_** : Unité d'Enregistrement associée - à utiliser avec précaution seule la première unité d'enregistrement trouvée est retournée - **Champ non disponible pour les imports**
- **custom_index** : *Entier* - Custom index
- **division__** : *→ Localisations de contenant (contenant)*
- **documents__** : *→ Document* - Documents 
- **external_id** : *Texte* - Identifiant
- **finds** : Liste du mobilier associé - **Champ non disponible pour les imports**
- **finds__** : *→ Mobilier (contenant)*
- **finds_ref__** : *→ Mobilier (contenant de référence)*
- **geodata__** : *→ Géographie - Donnée vectorielle* - Élément géographique 
- **history_date_deprecated** : *Date/heure* - history date deprecated
- **index** : *Entier* - ID contenant
- **location__** : *→ Lieu de conservation* - Lieu de conservation 
- **main_geodata__** : *→ Géographie - Donnée vectorielle* - Élément géographique principal 
- **main_image__** : *→ Document* - Image principale 
- **material_types** : Types de matériaux dans le contenant - chaîne de caractères - **Champ non disponible pour les imports**
- **material_types_code** : Code des types de matériau - chaîne de caractères - **Champ non disponible pour les imports**
- **old_reference** : *Texte* - Ancienne référence
- **operation_** : Opération associée - à utiliser avec précaution seule la première opération trouvée est retournée - **Champ non disponible pour les imports**
- **parent__** : *→ Contenant* - Contenant parent 
- **qrcode** : *Image (255)* - qrcode
- **reference** : *Texte* - Réf. du contenant
- **responsibility__** : *→ Lieu de conservation* - Responsabilité  - Lieu de conservation auquel le contenant appartient 
- **responsible__** : *→ Lieu de conservation* - Lieu de conservation responsable  - Obsolète - ne pas utiliser 
- **spatial_reference_system__** : *→ Géographie - Système de référence spatiale* (**label** Dénomination, **txt_idx** Identifiant textuel, **srid** SRID, **auth_name** Registre) - Système de référence spatiale 
- **treatment__** : *→ Traitements (contenant)*
- **weight** : *Nombre à virgule* - Poids mesuré (g)
