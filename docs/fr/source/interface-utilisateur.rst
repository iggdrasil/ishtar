.. -*- coding: utf-8 -*-

=====================
Interface utilisateur
=====================

:Auteur: Étienne Loks
:Date: 2018-10-02
:Copyright: CC-BY 3.0


----------------------------


Interface générale
==================

L'interface générale se découpe en 4 zones :

1. zone de profil
2. menu d'actions
3. zone centrale
4. zone d'alerte et de sélection épinglée

.. image:: _static/interface-generale.png

La capture d'écran ci-dessous reprend l'interface « bureau ». L'interface mobile reprend les mêmes éléments dans une version compacte : la zone de profil est accessible via l'icône « hamburger » à droite, le menu d'action est repris en haut en position centrale.


Zone de profil
--------------

La zone de profil reprend, dans l'ordre :

- le nom de l'instance Ishtar (ici « Démo »),
- le nom de l'utilisateur et le nom du profil en cours d'utilisation - en cliquant sur ces noms, on accède à un sous-menu permettant de modifier son profil, changer son mot de passe et se déconnecter,
- si plusieurs langues sont disponibles, une icône « drapeau » qui permet de changer la langue,
- si on a les droits administrateur, une icône « engrenage » qui permet à accéder aux pages spécifiques d'administration (cf. documentation administrateur).

Menu d'actions
--------------

Ce menu reprend les différentes pages disponibles en fonction des droits dont dispose l'utilisateur et des modules activés dans Ishtar.
Visible à tout moment il permet visuellement d'identifier immédiatement le contexte.
Ce menu est organisé de manière hiérarchique :

- le premier niveau reprend les éléments généraux : « Opération », « Unité d'Enregistrement », « Mobilier », etc. ainsi que des sections génériques telles que « Imports » ou « Annuaire ».
- le second niveau fournit le détail des actions disponibles en fonction du premier niveau sélectionné. Classiquement pour les éléments généraux (si l'on dispose des droits adéquats), il y des actions de « Recherche », « Ajout », « Modification » et « Suppression ». Si des sous-élements sont disponibles (par exemple, les « Contenants » dans le menu « Lieu de conservation »), ils sont accessibles après ces actions de base.
- le troisième niveau rend disponible les actions des sous-éléments.

La sélection d'un élément du premier niveau charge automatiquement la page de la première action du second niveau. De même la sélection d'un sous-menu dans le second niveau charge automatiquement la première action du troisième niveau correspondant.

Zone centrale
-------------

La zone centrale est totalement contextuelle. Néanmoins si certaines pages d'action ont une logique particulière, la plupart des pages d'action sur les éléments généraux s'ouvrent sur une « :ref:`page de recherche <page-de-recherche>` » et les actions d'ajout, modification et suppression suivent ensuite une logique de « :ref:`page d'assistant logiciel <wizard>` » (ou wizard). Le détail de ces pages est décrit dans les sections suivantes de cette documentation.

Zone d'alerte et de sélection épinglée
--------------------------------------

Cette zone reprend les éventuelles alertes (cf. :ref:`Page de recherche > Marques-pages et alertes <bookmarks>`), mise en place par l'utilisateur ainsi que (si l'affichage de ceux-ci est activé) les éléments actuellement épinglés (cf. :ref:`Utilisation avancée > Éléments épinglés <pinned>`).

.. _page-de-recherche:

Page de recherche
=================

La page de recherche comporte une barre de recherche et d'un tableau de résultat.


Recherche textuelle et par critères
-----------------------------------

La barre de recherche est composée d'une zone de saisie et d'une série d'icônes d'outils de recherche. Dans l'ordre ces icônes permettent de :

- icône « loupe » : lancer la recherche,
- icône « engrenage » : accéder à la recherche par critère,
- icône « croix » : vider la zone de recherche,
- icône « épingle » : épingler la recherche actuelle pour qu'elle devienne la recherche par défaut pour cette session,
- icône « étoile » : enregistrer une alerte, un marque-page,
- icône « marque-page » : accéder aux marques-pages (les supprimer).

Lorsque aucune recherche n'est active par défaut et que la zone de saisie est vide tous les éléments de la base de données sont listés.

La zone de recherche permet deux types de recherches distinctes : une recherche libre (sur l'ensemble des champs indexés) et une recherche par critère (champ par champ, pour l'ensemble des champs de la base).

Recherche libre
+++++++++++++++

Chaque élément de la base de données est indexé afin de pouvoir permettre ce type de recherche de manière performante. Les propriétés et descriptions rattachées aux éléments sont indexées. De même chaque élément parent est compris dans l'index de l'élément enfant.

Plus précisément :

- l'index de recherche d'une opération comprend les propriétés des sites(entités) archéologiques lié(e)s ;
- l'index de recherche d'un(e) site(entité) archéologique comprend les propriétés des opérations liées ;
- l'index de recherche d'une Unité d'Enregistrement comprend les propriétés des opérations et des sites(entités) archéologiques lié(e)s ;
- l'index de recherche du mobilier comprend les propriétés des opérations, des sites(entités) archéologiques et des Unités d'Enregistrement lié(e)s.

En revanche tous les champs de la base ne sont pas indexés, ceci afin que les résultats restent cohérents.

Les index de recherche permettent de faire des recherches en s'affranchissant des pluriels et de la casse.

**Exemple** : des recherches sur les termes « AMPHORE », « AMPHORES » et « amphores » renverront bien tous les éléments concernant des amphores.

.. note:: Par choix de ne pas intégrer des résultats trop éloignés de la recherche initiale, les fautes de frappe et d'orthographe ne sont pas prises en charge. Les recherches par orthographe approximative sont plus adaptées à des interfaces grand public qu'à des bases de données métier. Concrètement lorsque l'on cherche avec le terme « amphore », les résultats deviennent peu pertinents si par exemple nous sont renvoyés des éléments évoquant un « phare » dans sa description. En revanche la recherche libre comprend tout terme de recherche avec le sens « Commence par ». Ainsi une recherche sur ``amp`` renverra autant les amphore que les ampoules.

Adjoindre plusieurs termes correspond à faire une recherche avec l'opérateur logique *ET*.

**Exemple** : une recherche avec le terme « ``amphore dressel 1B`` » retournera tous les éléments concernant des amphores Dressel de type 1B.

..
 TODO: Une recherche avec l'opérateur OU... pour l'instant cette recherche pose trop de problèmes


Préfixer un terme par un « moins » : « ``-`` » permet d'exclure des termes de notre recherche.

**Exemple** : une recherche avec « ``ancre -amphore`` » permet d'obtenir la liste des ancres en excluant les lots de mobilier comprenant des amphores.

Recherche par critère
+++++++++++++++++++++

En cliquant sur l'icône engrenage, on accède à un formulaire permettant de construire simplement sa recherche par critère. Le formulaire de construction de requête dépend bien entendu du type d'élément recherché. Par ailleurs comme les autres formulaires ce formulaire peut avoir été personnalisé sur votre installation Ishtar, permettant de cacher certains champs inutiles ou ajouter d'autres champs personnalisés.

Après sélection d'une ou plusieurs contraintes dans le formulaire, en cliquant sur Ajouter, on les ajoute de manière textuelle sous la forme : « ``attribut="valeur"`` ». Cette forme permet de facilement retoucher une requête de manière textuelle sans passer par le formulaire.

**Exemple** : « ``annee="2018"`` » recherchera les éléments de l'année 2018.

.. note:: Dans la recherche par critère, le moteur recherche exactement la valeur entrée. Si l'on souhaite faire une recherche ouverte du type « contient la valeur », il faut ajouter un astérisque ``*`` à la valeur.

**Exemple** : « ``denomination="éclat"`` » retournera uniquement les éléments dont la dénomination est exactement Éclat, tandis que « ``denomination="éclat*"`` » renverra tous les éléments dont la dénomination contient le mot éclat, donc par exemple Lots d'éclats, éclat retouché, etc. De la même manière pour les nombres, « ``patriarche="1012"`` » donnera uniquement l'OA1012, alors que ``patriarche="1012*"`` renverra toutes les OA contenant les chiffres 1012, donc par exemple 101201 ou 1010125.

.. warning:: Contrairement à la recherche libre, la juxtaposition des termes concernant un même attribut est comprise comme un opérateur *OU*. Ainsi « ``annee="2018" annee="2017"`` » listera les éléments de l'année 2017 ou 2018. Néanmoins pour les attributs différents, cela reste à comprendre comme un opérateur *ET*. Ainsi « ``annee="2018" annee="2017" type-objet="Ancre et corps-mort"`` » listera le mobilier avec un type d'objet « Ancre et corps-mort » et rattaché à une des années 2017 ou 2018.

.. _bookmarks:

Marques-pages et alertes
------------------------

Les marques-pages permettent de stocker une recherche pour la retrouver plus aisément plus tard. Ceux-ci sont alors directement disponibles depuis le menu qui s'ouvre lorsque l'on clique sur l'icône marque-page de la barre de recherche.

Techniquement le contenu d'un marque-page correspond à la chaîne de texte utilisé dans la zone de recherche. Ainsi (contrairement aux :ref:`paniers <basket>`) c'est la requête qui est stockée et donc la liste d'éléments est à même de varier au cours du temps.

On peut stocker indifféremment des requêtes en recherche libre ou en recherche par critère.

Une alerte est un marque-page mis en évidence : il est toujours disponible en haut à gauche de l'interface (à gauche de la zone 4 sur l'image de l'interface générale) avec un badge contenant le nombre d'éléments contenu. En cliquant sur cette alerte, on accède directement au tableau de cette recherche.  

Tableaux
--------

Les tableaux se présentent de la même manière que les tableaux web « classiques » avec possibilité d'afficher plus ou moins de lignes de tableaux (entre 10 et 100), possibilité de tri par colonne et une pagination.

Dans Ishtar, la première colonne est systématiquement une icône permettant d'accéder à la :ref:`fiche <sheet>` associé à l'élément de cette ligne du tableau.

En tant qu'administrateur, sur certains tableaux, des actions rapides en haut à droite permettant des actions sur les éléments actuellement sélectionnés.

En bas du tableau, sur la gauche un bouton permet d'afficher le tableau en pleine page pour une meilleure lisibilité.

À la suite de ce bouton différents boutons d'export en CSV du tableau courant sont disponibles. Ces exports peuvent être configurés en administration.

.. _sheet:

Fiches
------

En s'ouvrant, les fiches se placent au-dessus de la page courante (généralement au dessus d'un tableau).

Entête
++++++

Une fiche se compose tout d'abord d'une entête. Cette entête est composée de :

- un titre : ce titre reprend la dénomination précise de l'élément. En cliquant sur ce titre, la fiche se replie.
- de flèches de navigations (si pertinent) : dans le cadre d'une fiche ouverte depuis un tableau, ces flèches permettent de naviguer entre éléments du tableau. Il n'est possible pour l'instant que de naviguer parmi les éléments actuellement affichés dans le tableau.
- d'une flèche de fermeture de la fiche.

Barre d'outil
+++++++++++++

Juste en dessous de l'entête une barre d'outil est présente.

Tout à gauche, lorsque plusieurs versions d'une même fiche sont disponibles, il est possible de naviguer parmi les différentes versions de cette fiche.

Ensuite sur la droite, différentes actions sont disponibles. Ces actions dépendent du type d'élément sélectionné. Généralement on y trouve :

- une icône « épingle » : elle permet d'épingler l'élément de la fiche (cf. :ref:`épinglage <pinned>`).
- une icône « crayon » : si l'on dispose des droits adéquats, elle permet d'éditer cette fiche. On accède au premier volet des pages d'assistants dédié à l'édition de ce type d'élément.
- des icônes « + » : ces icônes permettent d'associer facilement un type particulier d'élément à l'élément de la fiche courante. Par exemple « + doc./image » permet d'associer un document/une image à l'élément de la fiche.
- un menu « exporter » : les éléments de ce menu permettent un export dans différement format de la fiche actuelle.

Contenu de la fiche
+++++++++++++++++++

Les fiches présentent globalement les informations relatives à un élément.

Lorsque ces informations sont en relations avec d'autres éléments qui disposent eux aussi des fiches une icône « i » d'information est affiché à côté permettant ainsi de sauter de fiche en fiche.

Navigation entre fiches
+++++++++++++++++++++++

Un rappel des fiches ouvertes est disponible sur la droite. En cliquant sur un élément de cette liste, la page scrolle jusque la fiche correspondante.

.. _wizard:

Page d'assistant logiciel (wizard)
==================================

Dans Ishtar, pour la plupart des éléments complexes, les pages d'édition se présentent sous la forme d'« assistant logiciel ». Un assistant logiciel, découpe la saisie en plusieurs pages. Ces pages permettent une structuration logique de l'information. Ainsi en fonction des modules activés et des informations renseignées le contenu et l'affichage des panneaux est dynamique.

**Exemple** : les panneaux disponibles lors de la saisie d'une opération préventive, d'une opération programmée, d'une saisie judiciaire, les panneaux affichés sont différents.

Les panneaux enchaînent les formulaires jusqu'au dernier panneau de récapitulatif qui demandera confirmation des changements opérés. Tant que la validation finale n'est pas faite aucune donnée n'est enregistrée en base de données.

Jusqu'à la validation finale, les données de formulaire sont enregistrées dans la session de l'utilisateur. L'utilisateur peut tout à fait avoir plusieurs saisies d'éléments de nature différente en parallèle. Attention à la déconnexion ou à expiration de la session ces données de formulaires sont effacées. 

Fil d'Ariane
------------

Tout en haut des pages d'assistant logiciel, un fil d'Ariane est affiché. Celui-ci permet de naviguer rapidement entre les différents panneaux. En modification la navigation est libre. En saisie, certains panneaux requérant des informations obligatoires ne peuvent pas être passés : le fil d'Ariane s'adapte automatiquement et affiche les étapes jusqu'au prochain panneau obligatoire.

Tableau / Zone de formulaire
----------------------------

En saisie, en général, deux types de panneau sont disponibles : un type « tableau » et un type « zone de formulaire ».

Les types de page « tableau » permettent de sélectionner un élément tout en profitant des possibilités de recherche, filtre, tri évoqués précédemment. Ces types de pages permettent de sélectionner l'élément que l'on souhaite modifier ou l'élément parent de l'élément que l'on souhaite créer.

Les types de page « zone de formulaire » sont des formulaires web classiques. Ils offrent quelques facilitées tel que la saisie de dates en cliquant dans un calendrier, des conversions d'unités dynamiques, etc. Certains contrôles de formulaire sont réalisés de manière dynamique (par exemple une saisie textuelle dans un champ qui attend un chiffre), d'autres sont réalisés après envoi au serveur (par exemple, utilisation d'un index déjà utilisé par ailleurs).

Zone de validation
------------------

La zone de validation se situe tout en bas de la page.
Les boutons « Valider » et « Annuler » sont toujours disponibles.

« Valider » permet de valider le panneau en cours et de passer au panneau suivant. Si c'est le dernier panneau, le panneau de récapitulatif, la création/modification/suppression est validée. En création/modification, l'utilisateur est redirigé vers la fiche de l'élément qui vient d'être créé/modifié.

« Annuler » permet d'annuler toute la saisie en cours : les données de session seront effacées.

« Valider et confirmer » (qui s'insère entre « Valider » et « Annuler ») permet de valider le panneau actuel et d'aller directement au panneau récapitulatif.

Utilisation avancée
===================

.. _basket:

Paniers
-------

Les paniers sont un autre type de sélection d'éléments. Contrairement aux marques-pages, ils concernent une liste d'éléments fixe.

Pour effectuer certaines actions, il est nécessaire de préalablement constituer un panier (notamment pour ce qui concerne les traitements).

Ce regroupement virtuel peut être aussi un outil de travail, par exemple en constituant une liste d'éléments « à traiter ». Un marque-page (voire une alerte) pouvant être faite sur le contenu d'un panier, cela permet de conserver sous la main cette sélection d'éléments.

Pour l'heure les paniers ne concernent que le mobilier.

.. _pinned:

Éléments épinglés
-----------------

Les éléments épinglés constituent les éléments « par défaut » en terme de recherche.
L'épinglage d'élément est un outil de travail permettant de travailler dans un contexte de travail donné.

**Exemple** : je souhaite travailler sur le mobilier d'une opération précise. En épinglant cette opération, par défaut, toutes les recherches de mobilier se feront sur cette opération.

L'épinglage est disponible directement dans la barre d'outil des fiches.
L'épinglage est aussi disponible au niveau de la barre de recherche (cette recherche épinglée ne concerne que le tableau d'éléments en cours sans gestion hiérarchique).
Si celui-ci est activé dans son profil, l'épinglage est aussi disponible dans le menu de gestion des sélections épinglées (à droite de la zone 4 sur l'image de l'interface générale).
Si on a activé cette option dans son profil, un élément créé ou modifié est automatiquement épinglé.

Une gestion hiérarchique des épingles est faite : le fait d'épingler un élément épingle automatiquement ses parents directs.

**Exemple** : le fait d'épingler un mobilier épingle automatiquement l'Unité d'Enregistrement et l'opération archéologique associée à ce mobilier.

Menu de gestion des sélections épinglées
++++++++++++++++++++++++++++++++++++++++

Ce menu n'est visible que s'il est activé dans son profil. Il propose de sélectionner les éléments que l'on souhaite épingler. Différentes versions de ce menu sont disponibles :

- « simple » : des menus déroulants permettent d'épingler les éléments qui sont directement rattachés à notre compte utilisateur (en particulier les éléments que l'on a créé) ;
- « avancé - mes éléments » : on épingle ici seulement les éléments rattachés à notre compte utilisateur mais avec des champs fonctionnant en autocomplétion ;
- « avancé - tous les éléments » : on peut épingler tous les éléments avec des champs en autocomplétion.

Sur chaque menu, il est possible d'accéder aux fiches des différents éléments épinglés (icône « i ») et de détacher l'épingle sur ces éléments (croix rouge).

Action rapide
-------------

Pour les administrateurs, des actions rapides sont directement accessibles depuis les tableaux. Ces actions se situent en haut à droite du tableau. Elles concernent les éléments actuellement sélectionnés dans le tableau. Les icônes d'action rapide ne sont actives que lorsque cela est pertinent : si une action n'est applicable qu'à un seul élément, en sélectionner aucun ou plusieurs désactive l'icône de l'action en question.

Ces actions rapides permettent notamment de faire de l'édition de groupe, de l'empaquetage, etc.


..
  TODO: expliciter les droits ? les notions de propriétés ?
