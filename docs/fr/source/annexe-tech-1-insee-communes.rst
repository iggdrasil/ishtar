.. -*- coding: utf-8 -*-

.. _import-regroupements-communes-insee:

==============================================================
Annexe technique 1 - Import des regroupements de commune INSEE
==============================================================

:Auteur: Étienne Loks
:Date: 2020-09-23
:Copyright: CC-BY 3.0

----------------------------------

`L'INSEE <https://www.insee.fr>`_ met à disposition un fichier permettant de suivre l'évolution des « communes nouvelles » (procédure de regroupement de communes). Au sein de sa base de données, Ishtar gère ces évolutions permettant à la fois d'insérer de nouvelles données pour ces communes nouvelles mais aussi de conserver voire ajouter des données relatives à des communes anciennes maintenant regroupées. Une page sur le site de l'INSEE permet de télécharger ces données au format CSV : `communes nouvelles <https://www.insee.fr/fr/information/2549968>`_.

Ce fichier est importable au sein d'Ishtar.

Pour cela, se connecter sur le serveur hébergeant Ishtar, déposer le fichier CSV que l'on a téléchargé, puis se rendre dans le répertoire d'installation de l'instance concernée. Enfin, lancer la commande suivante (en remplaçant <année> et <chemin vers le fichier CSV> par les valeurs adéquates) :

.. code-block:: bash

    root@ishtar-server:# cd <chemin vers l instance> # exemple : /srv/ishtar/prod_deb
    root@ishtar-server:# python3 ./manage.py import_insee_comm_csv --year <année> <chemin vers le fichier CSV>

.. note:: L'année attendue est celle de l'année de création des communes nouvelles.

