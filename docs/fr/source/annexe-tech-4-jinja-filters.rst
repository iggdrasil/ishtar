.. -*- coding: utf-8 -*-

.. _annexe-technique-4-jinja-filters:

==========================================================
Annexe technique 4 - Filtres pour les patrons de documents
==========================================================

:Auteurs: Étienne Loks, Valérie-Emma Leroux
:Date: 2022-03-08
:Copyright: CC-BY 3.0
:Ishtar Version: v3.1.0

----------------------------------


Les patrons de documents permettent d'utiliser des filtres sur les données de la base de données. Cela permet essentiellement de mettre en forme les champs. Certains de ces filtres sont directement disponibles via la bibliothèque Jinja utilisée dans Ishtar, d'autres ont été développés au sein d'Ishtar.

Pour utiliser un filtre, à la suite de la variable, il faut utiliser le
caractère `|`, par exemple : `{{variable|capfirst}}`.

.. note:: Les filtres peuvent se chaîner. On peut donc écrire : `{{variable|human_date|capitalize}}.`

Pour les différents exemples nous utilisons directement une chaîne de caractères pour illustrer, en utilisation réelle on utilise un nom de variable.

La bibliothèque logicielle Jinja utilisée pour la génération des patrons met à disposition nativement un certain nombre de filtres : `documentation des filtres de base (anglais) <https://jinja.palletsprojects.com/en/2.11.x/templates/#list-of-builtin-filters>`_.

Ishtar met a disposition des filtres supplémentaires :

Formatage des chaînes de caractères
-----------------------------------

- **capfirt**

Ce filtre met la première lettre en majuscule et ne touche pas au reste de la
chaîne.

  - `{{\"saint georges d'oléron\"|capfisrt}}` -> `Saint georges d'oléron`


- **lowerfirst**

Ce filtre met la première lettre en minuscule et ne touche pas au reste de la
chaîne

  - `{{\"SAINT-GEORGES-D'OLÉRON\"|lowerfirst}}` -> `sAINT-GEORGES-D'OLÉRON`


- **capitalize**

Ce filtre met la première lettre de chaque mot en majuscule et le reste de la
chaîne en minuscule.

  - `{{\"SAINT-GEORGES-D'OLÉRON\"|capitalize}}` -> `Saint-Georges-d'Oléron`


- **human_date**

Ce filtre permet d'afficher une date en toutes lettres.

  - `{{\"2020-03-03\"|human_date}}` -> `3 mars 2020`

- **int**

Pour afficher un nombre sans décimales.

  - `{{\"600.0\"|int}}` -> `600`


Manipulation des chaînes de caractères
--------------------------------------


- **splitpart**

Ce filtre permet d'extraire un élément depuis une chaîne de
caractères en prenant en compte un séparateur. Par exemple depuis la chaîne
`"2,3,10"`, accéder au troisième élément : `"10"`.

Ce filtre nécessite au minimum un argument, le numéro de l'élément souhaité (en
commençant le compte à 0) : pour avoir le second élément, il faut indiquer en argument
`"1"`.

Le second argument correspond à la borne de fin non incluse (en commençant le
compte à 0) : ainsi pour avoir jusqu'au deuxième élément il faut indiquer en argument
`"2"`. Si l'on ne souhaite avoir qu'un seul élément, on indique `"0"`.

Par défaut le séparateur `","` est utilisé si l'on souhaite un autre
séparateur, on spécifie celui-ci en troisème argument.

On peut associer un dernier argument qui permet de spécifier le(s) caractère(s) de
concaténation que l'on souhaite utiliser pour la chaîne en retour.


  - `{{\"9,2,10\"|splitpart(1)}}` -> `2`
  - `{{\"chaise;bureau;papier;paragraphe\"|splitpart(0,0,\";\")}}` -> `chaise`
  - `{{\"182025_C001\"|splitpart(1,0,\"_\")}}` -> `C001`
  - `{{\"chaise;bureau;papier;paragraphe\"|splitpart(1,3,\";\")}}` -> `bureau;papier`
  - `{{\"chaise;bureau;papier;paragraphe\"|splitpart(1,4,\";\",\"|\")}}` -> `bureau|papier|paragraphe`

