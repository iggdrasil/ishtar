.. -*- coding: utf-8 -*-

.. _annexe-tech-5-manage-commands:

================================================
Annexe technique 5 - Commandes de gestion en CLI
================================================

:Auteurs: Étienne Loks
:Date: 2021-06-21
:Copyright: CC-BY 3.0
:Ishtar Version: v3.1.0

----------------------------------


Les commandes de gestion en CLI nécessite un accès en ligne de commande au serveur.
Une fois connecté en SSH à celui-ci, on se rend dans le répertoire de l'instance sur
laquelle on souhaite intervenir (par défaut : `/srv/ishtar/<nom-de-l'instance>`) et
l'on lance la commande souhaité via un appel au de ce répertoire `manage.py` suivi du
nom de la commande. Exemple :

.. code-block:: bash

    root@ishtar-server:# cd /srv/ishtar/prod_deb
    root@ishtar-server:# python3 ./manage.py relations_update_cache_tables


import_geofla_csv
-----------------

Import des communes depuis les données IGN (cf. :ref:`Annexe technique 2<import-communes-ign>`).

import_insee_comm_csv
---------------------

Import des communes depuis les données INSEE (cf. :ref:`Annexe technique 1<import-regroupements-communes-insee>`).

relations_update_cache_tables
-----------------------------

Cette commande met à jour les tables de cache des relations. Cette commande doit être
impérativement lancée lorsque l'on change le type de gestion des relations depuis le
profil afin de mettre à jour les tables correpondantes.

