.. -*- coding: utf-8 -*-

==================================
Configuration de l'instance Ishtar
==================================

:Auteur: Étienne Loks
:Date: 2018-12-04
:Copyright: CC-BY 3.0

----------------------------------

.. _interface-administrateur:

La configuration d'Ishtar se fait essentiellement par le biais de « l'interface d'administration ». Cette interface propose une vue brute des tables en base de données. Même si cette interface propose une ergonomie simple et pratique, utiliser cette interface peut nécessiter d'avoir connaissance de notions de base d'une base de données pour pouvoir opérer des choix pertinents.

L'interface d'administration nécessite d'avoir un compte administrateur pour y accéder. Les utilisateurs disposant de ce droit ont une icône « roue dentée » sur la barre de menu à l'extrémité droite. Sauf configuration spécifique, cette interface est aussi disponible via l'adresse « https://monsite-ishtar.net/admin/ » (ajouter `/admin/` à l'adresse de base).

L'interface d'administration présente la liste des différentes tables par ordre alphabétique regroupées par application.

.. warning:: Pour des questions de performance, Ishtar utilise intensément un système de cache. Il arrive parfois que celui-ci tarde à se mettre à jour. Il peut arriver que des modifications en administration prennent plusieurs minutes à être prises en compte.

Listes de types
---------------

Ishtar fournit par défaut des données pour chaque liste de choix. Chacune de ces listes est paramétrable en administration.

Dans les pages d'administration, les listes de types se retrouvent sous les dénominations « Types de ... », rangées par application :

- **Ishtar - Commun** : contient les listes de types transversales utilisées par plusieurs types d'éléments ;
- **Ishtar - Opération** : contient les listes de types concernant les opérations et les entités archéologiques/sites ;
- **Ishtar - Unités d'enregistrement** : contient les listes de types relatives aux Unités d'enregistrement et aux datations ;
- **Ishtar - Mobilier** : contient les listes de types relatives au mobilier et aux traitements ;
- **Ishtar - Lieu de conservation** : contient les listes de types relatives aux lieux de conservation et aux contenants ;
- **Ishtar - Dossier** : contient les listes de types relatives aux dossiers administratifs.

Dans Ishtar, chaque type est défini au minimum par les champs suivants :

- **Dénomination**
- **Identifiant textuel** : L'identifiant textuel est une version standardisée du nom. Il ne contient que des lettres en minuscules non accentuées, des nombres et des tirets (-). Chaque identifiant textuel doit être unique dans la liste de types. Ces identifiants sont des clés permettant les échanges entre bases de données et pour des traductions. Ces identifiants peuvent être utilisés dans le code source de l'application. Une fois créés il ne faut a priori pas changer ces identifiants textuels.
- **Commentaire** : Le contenu du commentaire est affiché dans l'aide en ligne sur les formulaires.
- **Disponibilité** : Décocher ce champ rend indisponible ce type dans les formulaires, sans détruire l'information pour ce qui est déjà présent en base ; l'information est toujours visible dans les fiches.
- **Ordre** : Dans les listes les champs sont ordonnés par ce numéro d'ordre et en cas d'égalité par ordre alphabétique.

Certains types permettent de mettre en place une hiérarchie. Pour cela le champ **parent** est disponible. Pour chaque type enfant, ce champ est renseigné avec le type parent adéquat. 

Certains types disposent aussi d'autres champs spécifiques ; ceux-ci sont explicites ou disposent d'une aide en ligne.

.. note:: Pour travailler sur ces listes de types ou les transmettre à des tiers, la possibilité est offerte d'exporter ces listes de types via l'interface d'administration. On sélectionne les éléments à exporter (ou tous les éléments) puis on utilise l'action « Exporter les éléments sélectionnés en fichier CSV ». Le fichier peut alors être édité dans un tableur. Pour une mise à jour, il est important de ne pas modifier les identifiants textuels qui sont la clé de rapprochement pour le ré-import. L'action d'import est disponible en haut à droite : « Import depuis un CSV ».

.. _champ-personnalises:

Champs personnalisés
--------------------

Ishtar propose un certain nombre de champs standards et génériques permettant de disposer dès le départ d'une base solide et aussi pour assurer un certain degré d'inter-opérabilité entre les différentes installations d'Ishtar. Néanmoins nul ne peut prétendre à l'exhaustivité et, notamment dans le cadre d'une utilisation d'Ishtar axée recherche, le besoin se fera probablement sentir d'ajouter des champs.

Dans Ishtar ces champs sont appelés : **Champs personnalisés**. Le format JSON étant utilisé pour stocker ces données, le nom **Données JSON** est aussi utilisé.

Ajouter un champ personnalisé se fait via l'interface d'administration au niveau de la rubrique : *Ishtar - Commun › Données JSON - Champs*.

Les différentes données à rentrer sont :

* **Nom** : Ce nom sera repris dans les formulaires et la fiche.
* **Type de contenu** : Le type d'objet auquel sera rattaché le champ - Opération, Site, Unité d'Enregistrement, Mobilier, ...
* **Clé** : Valeur de la clé dans le format JSON. Cette clé ne doit impérativement comporter que des lettres minuscules, sans accent et des tirets bas « _ » (ne pas commencer la clé par le tiret bas et ne pas mettre plusieurs tirets bas d'affilée dans la clé de base). On peut structurer les données personnalisée de manière hiérarchique. Pour les clés hiérarchiques on utilise « __ » entre les sections. Par exemple pour la clé « ma_sousclef » dans la catégorie « ma_categorie », la clé sera notée : *ma_categorie__ma_sousclef*.
* **Type** : Les types de données disponibles sont les suivants :

  * Texte,
  * Texte long : le composant de saisie sera une zone de texte,
  * Entier : nombre entier positif ou négatif,
  * Nombre à virgule,
  * Booléen : case à cocher - Vrai ou Faux,
  * Date : un composant permettant le choix de date depuis un calendrier est proposé,
  * Choix : un composant en autocomplétion sur les valeurs existantes est proposé. L'utilisateur a la possibilité de rentrer librement de nouvelles valeurs.

* **Ordre** : Le numéro saisi permet d'ordonner par défaut ce champ par rapport aux autres champs.
* **Section** : La section correspond à un titre de section pour présenter ce champ sur la fiche et permettre des regroupements.
* **Utiliser dans les index de recherche** : Si cette case est cochée, la recherche libre indexera le contenu de ce champ.
* **Afficher** : Si cette case n'est pas cochée, ce champ ne sera pas affiché sur la fiche.

Sauf si un champ personnalisé est uniquement destiné à des données importées et à un affichage sur la fiche, un champ personnalisé sera dans la plupart des cas intégré à un :ref:`formulaire personnalisé <formulaire-personnalise>`.

.. _formulaire-personnalise:

Formulaires personnalisés
-------------------------

La plupart des formulaires peuvent être personnalisés dans Ishtar, notamment tous les formulaires de création/modification ainsi que les formulaires de recherche. À ce jour, il n'est pas encore possible d'ajouter de nouveaux formulaires.

Les formulaires personnalisés permettent deux choses : soustraire de l'affichage du formulaire les champs disponibles par défaut et ajouter des champs JSON dans le formulaire. Chaque formulaire personnalisé peut être mis à disposition pour tous ou seulement certains utilisateurs.

La configuration de ces champs se fait en administration via : *Ishtar - Commun › Formulaires personnalisés*.

La création se fait en deux temps, d'abord un paramétrage des champs de base puis une définition des champs à exclure et des champs JSON à ajouter.

Le paramétrage de base demande les champs suivants :

* **Nom** : le nom correspondant au formulaire personnalisé. Ce nom ne sera visible qu'en administration mais pour s'y retrouver, il doit à la fois reprendre le nom du formulaire ainsi que le contexte pour lequel il a été défini. Par exemple : « Mobilier - 020 - Général - Tout utilisateur » ou « Mobilier - 030 - Conservation - Saisie terrain ».
* **Formulaire** : le formulaire à personnaliser. Le nom utilisé permet d'identifier assez simplement le formulaire correspondant, car il correspond dans l'ordre (séparé par des tirets) au :

  * type d'objet concerné (par exemple : « Mobilier »),
  * éventuellement, le numéro d'ordre dans les formulaires successifs,
  * nom du formulaire.
* **À qui s'applique ce formulaire**, cela peut être au choix :

  * à tous les utilisateurs,
  * à certains utilisateurs en particulier,
  * à certains types d'utilisateurs.

Une fois ce paramétrage de base enregistré, la configuration précise du formulaire peut se faire :

* **champs à exclure** : chaque champ de base présent dans le formulaire actuel peut être sélectionné dans la liste pour être écarté de la saisie.
* **champs JSON** : tous les champs JSON préalablement paramétrés concernant l'élément courant (mobilier, OA...) peuvent être sélectionnés. La dénomination permet éventuellement de surcharger le nom par défaut du champ JSON. L'ordre permet de placer le champ dans le formulaire. L'aide permet éventuellement d'ajouter un texte pour aider à la saisie.

.. warning:: Sur les formulaires de création, il est impératif de ne pas exclure des champs obligatoires sans quoi la création devient impossible.

.. note:: En tant qu'administrateur, en modifiant son profil depuis les pages d'administration (*Ishtar - Commun › Profils d'utilisateurs*) et en cochant « Afficher les numéros des champs », les numéros des champs actuels des formulaires s'affichent sur l'interface, cela permet ainsi de placer plus aisément les champs personnalisés.


Gestion des permissions
-----------------------

Gestion des comptes
*******************

(Action administrateur en interface utilisateur) 

Dans Ishtar, un compte doit être associé à une personne. Si la personne n'existe pas dans la base, il faut l'ajouter via l'interface principale d'Ishtar : *Annuaire › Personne › Ajout*.

Ensuite on crée un compte associé à cette personne, toujours via l'interface d'Ishtar : *Annuaire › Compte › Ajout/modification*. Dans les différents panneaux, il est demandé : l'identifiant du compte, le courriel rattaché, le mot de passe puis le type de profil.

Il est obligatoire de renseigner un mot de passe lors de la création. En modification, on laisse ces champs vides si l'on ne souhaite pas modifier le mot de passe actuel.

Les types de profil définiront les types de permissions auxquelles aura accès l'utilisateur. Sur ces types de profil, des zones peuvent être définies afin de permettre des règles de rattachement spécifique (cf. :ref:`permissions dans Ishtar<permissions-ishtar>`).

Cette interface de création de compte permet aussi de modifier le mot de passe de comptes existants.

.. _permissions-ishtar:

Permissions dans Ishtar
***********************

Les permissions dans Ishtar sont essentiellement gérées par « Type de profil ». Chaque compte a un ou plusieurs « types de profil » associés à son compte. Chaque type de profil donne accès à des actions et des accès sur des types d'objets.

La création/configuration d'un type de profil se fait via : *Ishtar - Commun › Types de profil* :

- **dénomination** : ce champ doit être explicite, car il va être retrouvé au niveau de l'interface utilisateur.
- **identifiant textuel** : rempli selon les règles habituelles des identifiants textuels.
- **groupes** : listes des groupes auxquels le profil est rattaché.

Chaque groupe correspond à un type de permission pour un élément précis de la base de données :

- droit de lecture ;
- droit d'ajout ;
- droit de modification/suppression.

Chacun de ces droits est décliné en deux modalités :

- droits sur tous les éléments ;
- droit sur les éléments rattachés.

Un élément est dit « rattaché » à une personne en fonction de règles précises spécifiées dans l':ref:`annexe 1 - règles de rattachement à un élément <annexe-1-rattachement>`. La notion de rattachement permet de spécifier finement les permissions pour des personnes qui sont directement associées à l'élément. Par exemple cela permet donner les droits de modification du mobilier d'une opération au responsable scientifique de cette opération.

En pratique, globalement, les groupes de droits permettent d'accéder à certaines actions :

- le droit de lecture permet une ouverture de la fiche correspondant à l'élément ;
- le droit d'ajout permet d'accéder aux actions d'ajout d'un nouvel élément ;
- le droit de modification/suppression permet d'accéder aux actions concernant la modification/suppression des éléments.

Dans le détail, il y a certaines actions qui sont ouvertes en fonction d'appartenance à des groupes en particulier. Tout cela est détaillé dans l':ref:`annexe 2 - permissions nécessaires pour les actions <annexe-2-permission-action>`.

.. note:: La page *Ishtar - Commun › Résumés des types de profil* permet d'accéder à un tableau qui reprend et rend explicites toutes les permissions associées aux types de profil.

Permissions dans les pages d'administration
*******************************************

Les permissions des pages d'administration sont gérées différemment. Elles utilisent le système de permission du framework Django, framework (cadre de développement logiciel) utilisé par Ishtar.
Une fois le compte créé, les droits se spécifient dans les pages d'administration : *Authentification et autorisation › Utilisateurs*.

L'ouverture de l'accès aux pages d'administration se fait en cochant le « Statut équipe ». Si l'on souhaite n'ouvrir l'accès qu'à certaines pages spécifiques, on ajoute les « Permissions de l'utilisateur » correspondant aux tables que l'on souhaite ouvrir, si l'on souhaite ouvrir l'accès à toutes les tables, il suffit de cocher le « Statut super-utilisateur ».


Patrons de documents
--------------------

Principes de base
*****************

Ishtar propose une génération automatisée de document. Depuis un patron au format LibreOffice (ODT), les données relatives à un élément (acte administratif, mobilier...) remplacent les variables du patron pour obtenir le document désiré.

On crée le patron au format ODT avec un contenu adapté, puis depuis l'interface d'administration, sous l'entrée *Ishtar - Commun › Patrons de document › Document de référence*, on créé un patron de document avec ce fichier ODT associé au type d'élément pour lequel il est destiné.

Le document peut alors être généré depuis n'importe quelle fiche de l'élément concerné (en haut à droite sous « Documents »).

Un premier patron
*****************

Pour créer un patron, la première étape est de récupérer toutes les variables disponibles pour l'élément à partir duquel on veut générer un document. Il existe pour cela un `document de référence`_ que l'on peut attacher à l'élément pour lequel on souhaite éditer un nouveau document.

.. _document de référence: https://gitlab.com/iggdrasil/ishtar/raw/main/archaeological_operations/tests/document_reference.odt

.. note:: En cas d'indisponibilité du lien pour ce document, ce document est très simple et peut être recréé facilement, il suffit d'insérer : ``{{VALUES}}`` dans un document ODT vide et de sauvegarder le document.

Depuis *Ishtar - Commun › Patrons de document › Document de référence*, on ajoute un nouveau patron de document : « Document de référence » auquel on associe le document de référence téléchargé et le type d'élément pour lequel on souhaite créer un patron de document.

Ensuite, il faut récupérer un document de référence généré depuis la fiche d'un élément contenant tous les champs que l'on souhaite exploiter.

On ouvre ce document sous LibreOffice. Le document produit contient une liste de clé avec la valeur associée concernant l'élément que l'on a choisi.

Les différentes clés vont permettre de constituer un patron répondant à ce qui est attendu. Pour cela reprendre un exemple du document que l'on souhaite générer (toujours au format ODT) et remplacer chaque occurence d'une valeur par la clé en reprenant la :ref:`syntaxe Jinja <formules-syntaxe-jinja>` (Jinja est le nom de la bibliothèque utilisée).
Une fois quelques substitutions faites, on peut l'enregistrer et créer le patron dans l'interface d'administration Ishtar. Ce premier patron est alors disponible depuis la fiche des éléments.


.. _configuration-instance-ishtar:

Configuration du profil d'instance Ishtar
-----------------------------------------

*En cours de rédaction...*

Identifiants et index personnalisés
***********************************

Pour chaque type d'élément principal, il est possible de configurer le profil Ishtar pour personnaliser :

- l'identifiant externe : c'est un identifiant textuel unique dans la base qui permet de faire des rapprochements de manière non ambiguë. Il est souvent utilisé pour les imports. Par exemple, un identifiant externe pour les unités d'enregistrement peut être "code patriarche de l'opération-identifiant de l'unité d'enregistrement" ou alors "code patriarche de l'opération-section parcelle-numéro parcelle-identifiant de l'unité d'enregistrement". Des identifiants externes sont paramétrés par défaut pour chaque type d'élément principale. Note : l'identifiant externe de l'opération est toujours le code patriarche et n'est pas paramétrable.
- l'identifiant complet (optionnel) : cet identifiant est un identifiant de gestion paramétrable. Cet identifiant peut par exemple se distinguer de l'identifiant externe pour incorporer des codes matières.
- clés pour index personnalisé : un index personnalisé peut être défini en fonction d'une ou plusieurs clés reprenant les champs. Par exemple une clé opération peut être utilisée pour générer un index numérotant de 1 à n le mobilier sur une opération.

Pour définir identifiant externes et identifiant complet, on utilise des formules. Deux types de syntaxes sont utilisées : une :ref:`syntaxe simple <formules-syntaxe-simple>` et une :ref:`syntaxe Jinja <formules-syntaxe-jinja>` (nom de la bibliothèque utilisée). Ces deux syntaxes utilisent des variables relatives à l'élément. L'utilisation de ces variables est explicitée dans l':ref:`annexe technique 3 - variables <annexe-technique-3-variables>`.



Formules
--------

.. _formules-syntaxe-simple:

Syntaxe simple
**************

Cette syntaxe permet d'utiliser directement les variables en utilisant la notation accolade simple ``{}``. Par exemple : ::

   OA-{code_patriarche}

Cette notation se traduira ainsi par des rendus comme : `OA-061234` ou `OA-44789`.


.. _formules-syntaxe-jinja:

Syntaxe Jinja
*************

Cette syntaxe permet de faire de faire des substitutions de manière plus fine qu'avec la syntaxe simple. Cette syntaxe est utilisée systématiquement pour les patrons de document, un sous-ensemble de cette syntaxe (variables et structures conditionnelles) peut-être utilisée optionnellement pour les identifiants personnalisés.

Variables
+++++++++

L'accès aux `variables <annexe-technique-3-variables>` se fait par la notation double accolade ``{{ }}``.
Ainsi par exemple, pour accéder aux variables `nom_de_ma_clef_1` et `nom_de_ma_clef_2` : ::

        Je, soussigné, {{nom_de_ma_clef_1}}, vous accorde un prêt de {{nom_ma_clef_2}}.


Conditions
++++++++++

Des structures conditionnelles peuvent être mises en place. Cela permet notamment de tester si une valeur a été renseignée et de permettre de contextualiser l'affichage en fonction de cela. Exemple : ::

        Ce traitement sous
        {% if responsable %}la responsabilité de {{responsable}}
        {% else %}une responsabilité à définir
        {% endif %} se tiendra...

Les structures conditionnelles se structurent autour des mots clés ``if``, ``else`` et ``endif``. On utilise ``{%`` et ``%}`` autour de ces mots clés. La section ``else`` est facultative.


Parcours de liste
+++++++++++++++++

Certaines clés peuvent parfois renvoyer à des listes d'éléments, chacun ayant des attributs. On peut alors parcourir cette liste d'élément de cette manière : ::

        {% for element in liste_elements %}
           {{element.nom}} - {{element.prenom}}
        {% endfor %}

Cela se structure autour des mots clés ``for``, ``in`` et ``endfor``. Au lieu de doubles accolades, ``{%`` et ``%}`` encadrent ces mots clés.

.. 
  Filtres
  +++++++
  {{|filtre}}

..
  Images
  ++++++
  Il est possible d'utiliser des images dans les patrons. Pour cela il faut placer une image dans le document du patron et modifier son attribut nom sous LibreOffice (clic droit sur l'image, « Propriétés... » et l'onglet « Options ») par une chaîne sous la forme ``{{ mon_image|image }}``. ``mon_image`` correspond au champ image



.. 
  TODO: Profil d'instance Ishtar
