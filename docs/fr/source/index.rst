.. Ishtar documentation master file, created by
   sphinx-quickstart on Wed Oct 12 01:01:02 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenue dans la documentation d'Ishtar !
==========================================

Contents:

.. toctree::
   :maxdepth: 3

   installation
   principes
   interface-utilisateur
   interface-administrateur
   annexe-1-rattachement
   annexe-2-permission-action
   annexe-3-ex-flux-ope
   annexe-4-doc-normes
   annexe-tech-1-insee-communes
   annexe-tech-2-ign-communes
   annexe-tech-3-variables-gen
   annexe-tech-4-jinja-filters
   annexe-tech-5-manage-commands
   annexe-tech-6-configuration-syndication
