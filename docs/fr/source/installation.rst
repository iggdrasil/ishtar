.. -*- coding: utf-8 -*-

============
Installation
============

:Auteur: Étienne Loks
:Date: 2023-06-12
:Copyright: CC-BY 3.0


Ce document présente les instructions d'installation d'Ishtar.

Seule l'installation sous Debian GNU/linux via le paquet Debian est décrite. Cet environnement est le seul sous lequel un suivi de sécurité et des dysfonctionnements est assuré, installer Ishtar hors de cet environnement est déconseillé.

Un nom de domaine dédié est nécessaire pour chaque instance (une instance Ishtar n'est pas installable dans un sous-répertoire). Par contre un sous-domaine est tout à fait utilisable (par exemple : *ishtar.mon-domaine.net*).

.. note:: Sauf mention explicite, chaque commande est exécutée en tant qu'utilisateur root. Les utilisateurs de sudo l'ajouteront à chaque commande.

Installation sur serveur Debian Bullseye
----------------------------------------

Un dépôt a été mis en place pour installer sous Debian Bullseye.
Ce dépôt est signé, pour ajouter la clé du dépôt à votre gestionnaire de paquet, lancez la commande ::

    wget -O - http://deb.iggdrasil.net/iggdrasil.gpg.key | apt-key add -

Puis, au choix, ajoutez le dépôt à votre /etc/apt/sources.list ::

    deb http://deb.iggdrasil.net/apt/debian/ bullseye main
    deb-src http://deb.iggdrasil.net/apt/debian/ bullseye main

Ou sauvegardez le fichier `iggdrasil.list`_ dans votre répertoire **/etc/apt/sources.list.d/** ::

    wget -O - http://deb.iggdrasil.net/apt/debian/dists/bullseye/iggdrasil.list > /etc/apt/sources.list.d/iggdrasil.list

.. _iggdrasil.list: http://deb.iggdrasil.net/apt/debian/dists/bullseye/iggdrasil.list

Ensuite mettez à jour la base de données de votre gestionnaire de paquet et installez le paquet ::

    apt update
    apt install python3-django-ishtar

Deux paquets optionnels peuvent être installés :

* **ishtar-tasks** : installe un service de tâche pour gérer en tâche de fond les opérations longues (par exemple les imports). L'installation de ce paquet est vivement conseillée sauf si votre serveur a une mémoire vive limitée (par exemple nano-ordinateur).
* **ishtar-libreoffice** : installe libreoffice en mode serveur pour faciliter des imports / exports aux formats bureautique. Ce paquet est nécessaire pour générer les tableaux depuis les types d'import. Il dépend de la version libreoffice-nogui : la couche graphique n'est pas installée sur le serveur. 

Enfin pour créer une nouvelle instance d'Ishtar ::

    ishtar-prepare-instance

Un ensemble de questions vous sera posé afin de déterminer les paramètres qui concernent cette instance.


.. note:: Le nom de domaine doit bien entendu pointer vers l'adresse IP du serveur. Si à l'issue de l'installation, le service n'est pas joignable, verifiez bien votre configuration DNS ou le cas échéant verifez bien auprès du gestionnaire de nom de domaine que c'est le cas.

.. warning::
    En termes de serveur Web, l'installateur fonctionne avec la configuration que nous considérons comme la plus optimisée qui est le couple nginx / uwsgi. Si vous avez des services tournant sous Apache ou sous un autre serveur web, plusieurs options se présentent à vous :
 
    - faire fonctionner nginx sur un autre port ;
    - faire fonctionner vos autres services avec nginx (je vous laisse découvrir l'abondante documentation en ligne en cherchant « nginx + le nom de mon service ») ;
    - configurer Ishtar pour fonctionner avec votre serveur web (référez-vous à la `documentation de Django`_).

.. _`documentation de Django`: https://docs.djangoproject.com/fr/2.2/howto/deployment/wsgi/

L'installateur vous demandera un identifiant / mot de passe pour le compte administrateur.
Une fois l'instance préparée, une base de données a été créée avec un nom du type ishtar-le_nom_de_mon_instance (le nom que vous avez donné), Ishtar est joignable à l'adresse donnée par la variable URL et les données de cette instance sont stockées dans le répertoire /srv/ishtar/le_nom_de_mon_instance.


Initialisation de la base de données
------------------------------------

Ishtar dispose de nombreuses tables de paramétrage permettant d'avoir un logiciel au plus proche de vos besoins.
Remplir toutes ces tables est fastidieux, c'est pour cela que des jeux de données de base sont disponibles.
Lors de l'installation du paquet, à l'exception des communes (trop lourdes pour être incluses par défaut), cette initialisation est faite.


Initialisation des communes
***************************

Une liste des communes française peut être téléchargée et chargée ::

    cd /tmp
    wget "http://ishtar-archeo.net/fixtures/initial_towns-fr.tar.bz2"
    tar xvjf initial_towns-fr.tar.bz2

    ISHTAR_PATH=/srv/ishtar # répertoire par défaut de l'installation pour le paquet Debian
    PROJECT_PATH=$ISHTAR_PATH/le_nom_de_mon_instance
    cd $PROJECT_PATH
    ./manage.py loaddata /tmp/towns_norel-fr.json
    ./manage.py loaddata /tmp/towns-fr.json
    rm /tmp/initial_towns-fr.tar.bz2
    rm /tmp/towns-*


Mises à jour
------------

Mise à jour version 3 (sur Debian Buster) vers la version 4 (sur Debian Bullseye)
*********************************************************************************

Comme lors de toute migration non triviale, il est préférable de faire une sauvegarde de la base de données et des médias associés juste avant de lancer la mise à jour.

Le changement de version nécessite préalablement la mise à jour vers la version de Debian 11 Bullseye.

Pour faciliter cette mise à jour, si ceux-ci ont été installés, il est conseillé de purger en amont les paquets optionnels `ishtar-libreoffice` et `ishtar-tasks` ainsi que tous les paquets `libreoffice` ::

    apt purge ishtar-libreoffice ishtar-tasks libreoffice-*
    apt autoremove


Ceux-ci pourront être réinstallés une fois la mise à jour vers Bullseye finie.

Éteignez les services web et ensuite procédez à la mise à jour de Debian selon la documentation Debian officielle (ou votre protocole/vos habitudes). Ne changez pas tout de suite le fichier source list (ou les lignes) correspondant au dépôt Ishtar.

Ensuite vous pouvez opérer la migration de la base de données PostgreSQL vers la version 13 ::

    systemctl stop postgresql
    pg_dropcluster 13 main
    pg_upgradecluster  -m dump 11 main  # utiliser "dump" pour éviter les complications avec postgis
    systemctl start postgresql


Mettez à jour vers Bullseye le dépôt correspondant à Ishtar (a priori dans le fichier `/etc/apt/sources.list.d/iggdrasil.list`) et lancez la mise à jour ::

    apt update
    apt upgrade
    apt dist-upgrade

Le paquet `python3-weasyprint` empêche parfois la mise à jour (à cause d'un conflit de version). Dans ce cas, le supprimer explicitement et relancer le paramétrage ::

    apt remove python3-weasyprint
    apt --fix-broken install

Pour finaliser la mise à jour des paquets, supprimer les paquets obsolètes ::

    apt autoremove

Vous pouvez ensuite, le cas échéant, ré-installer les paquets `ishtar-libreoffice` et `ishtar-tasks` ::

    apt install ishtar-tasks ishtar-libreoffice

On peut ensuite passer à la migration des données. Attention cette migration peut être longue (plusieurs heures), assurez-vous que le processus de migration ne soit pas interrompu (a minima lancez-le via un outil comme `screen`). Pour chaque instance, dans le répertoire `/srv/ishtar/` lancez les commandes ::

    cd /srv/ishtar/{le_nom_de_mon_instance}
    # mise à jour des données par défaut
    ./manage.py loaddata /usr/share/python3-django-ishtar/fixtures/initial_data-auth-fr.json 
    ./manage.py loaddata /usr/lib/python3/dist-packages/archaeological_files/fixtures/initial_data-fr.json
    # migration des données pour la nouvelle gestion géographique
    editor local_settings.py
    (...)  # à la fin du fichier ajouter les lignes
    ISHTAR_MIGRATE_V4 = True
    USE_BACKGROUND_TASK = False

    ## nombre-de-processus dépend du processeur et du nombre de fils d'exécution disponible
    ## plus il y en a, plus rapide est la conversion mais laissez quand même un minimum de marge 
    ## pour ne pas rendre la machine inutilisable
    ./manage.py migrate_to_geo_v4 --process {nombre-de-processus}
    # une fois la migration finie
    editor local_settings.py
    (...)  # supprimer les deux lignes ajoutées

Si vous utilisez `ishtar-tasks`, sur cette nouvelle version la gestion du service `rabbitmq` n'est plus assurée via `systemd` mais via `supervisor.` La migration de `systemd` vers `supervisor` n'est pas gérée automatiquement via le paquet, seules les nouvelles instances ont un fichier `supervisor` créé à l'installation. Pour gérer cette migration supprimez les fichiers de configuration `systemd` résiduels ::

    rm -f /etc/systemd/system/rabbitmq-notify-email\@.service
    rm -f /etc/systemd/system/rabbitmq-server.service
    rm -f /etc/systemd/system/multi-user.target.wants/rabbitmq-server.service

Puis créez les fichiers de configuration `supervisor` pour chaque instance ::

    rm -f /etc/monit/conf-enabled/celery-*
    rm -f /etc/systemd/system/celery-*
    rm -f /etc/systemd/system/multi-user.target.wants/celery-*

    # pour chaque instance listée dans /etc/ishtar/instances
    editor /etc/supervisor/conf.d/celery_le_nom_de_mon_instance.conf

    [program:celery_le_nom_de_mon_instance]
    command=/usr/bin/celery -A le_nom_de_mon_instance worker --loglevel=INFO -c 6
    directory=/srv/ishtar/
    user=www-data
    autostart=true
    autorestart=true
    stdout_logfile=/var/log/celery/le_nom_de_mon_instance.log
    redirect_stderr=true
    stopasgroup=true

    supervisorctl reread  # prise en compte des nouveaux fichiers de configuration
    supervisorctl update  # mise à jour

La migration est terminée.
Redémarrez la machine et assurez-vous que tous les services fonctionnent convenablement en particulier via les commandes `systemctl --failed` et `supervisorctl status`.

En ce qui concerne le paramétrage, il est impératif de modifier les types de droits utilisateurs pour l'accès aux données géographiques. Ouvrez votre instance sur un navigateur web en admin et rendez-vous à la page : `http(s)://{my-ihstar}/admin/ishtar_common/profiletypesummary/`  pour ajouter les permissions nécessaires.

Une fois que vous vous serez assuré que tout est fonctionnel, vous pourrez effacer l'ancien cluster PostgreSQL correspondant à la version 11 ::

    pg_dropcluster 11 main

..
  TODO:
  .. warning:: L'installateur présume que vous avez un nom de domaine dédié pour votre installation. Si cela n'est pas le cas, reportez-vous à la section de la documentation concernée.
  paramètres de settings utiles :DEFAULT_FROM_EMAIL = "robot@iggdrasil.net", SERVER_EMAIL EMAIL_HOST  EMAIL_PORT  EMAIL_HOST_USER EMAIL_HOST_PASSWORD EMAIL_USE_TLS
  installation depuis les sources

