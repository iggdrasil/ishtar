from django.conf import settings
from django.conf.urls import include, url

from django.contrib import admin
from ishtar_common.apps import admin_site

from ishtar_common.views import index
from ishtar_common.urls_registration import urlpatterns

admin.autodiscover()

urlpatterns = urlpatterns[:]

APP_LIST = ['archaeological_files',
            'archaeological_operations', 'archaeological_context_records',
            'archaeological_warehouse', 'archaeological_finds']
for app in APP_LIST:
    urlpatterns += [
        url('', include(app + '.urls')),
    ]

urlpatterns += [
    url(r'^admin/', include(admin_site.urls[:2])),
    url(r'', include('ishtar_common.urls')),
    url(r'^$', index, name='start'),
]

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG_TOOLBAR:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns

