# run test for setup.py
import os
import shutil
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'example_project.settings'
test_dir = os.path.dirname(__file__)
sys.path.insert(0, test_dir)
local_settings = os.path.join(test_dir, "local_settings.py")
local_settings_exists = True
if not os.path.exists(local_settings):
    local_settings_exists = False
    shutil.copy(
        os.path.join(test_dir, "local_settings.py.setup"),
        local_settings)

import django
from django.test.utils import get_runner
from django.conf import settings
from django.core.management import call_command


def runtests():
    django.setup()
    call_command('collectstatic', interactive=False, verbosity=0)
    TestRunner = get_runner(settings)
    test_runner = TestRunner(verbosity=1, interactive=True,
                             exclude_tags=["not-setup-py"])
    failures = test_runner.run_tests(
        ["ishtar_common.tests",
         "archaeological_files.tests",
         "archaeological_operations.tests",
         "archaeological_context_records.tests",
         "archaeological_finds.tests",
         "archaeological_warehouse.tests"],
    )
    if not local_settings_exists:
        os.remove(local_settings)
    sys.exit(failures)


if __name__ == '__main__':
    runtests()
