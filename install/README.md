Follow these instruction to install Ishtar from sources on a Debian Buster installation.

### Get installations scripts

```
apt-get install wget
cd /usr/local/src
wget https://ishtar-archeo.net/install/install-ishtar.tar.bz2

tar xvzf ishtar-install.tar.gz
rm ishtar-install.tar.gz
```

#### Install Ishtar

```
cd /usr/local/src/install
bash ./ishtar-install
```

#### Configure an Ishtar instance

```
cd /usr/local/src/install
bash ./ishtar-prepare-instance
```

#### Delete an Ishtar instance

```
cd /usr/local/src/install
bash ./ishtar-delete-instance
```
