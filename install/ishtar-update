#!/bin/bash

PYTHON=python3

set -e

cecho() {
  local code="\033["
  case "$1" in
    black  | bk) color="${code}0;30m";;
    red    |  r) color="${code}1;31m";;
    green  |  g) color="${code}1;32m";;
    yellow |  y) color="${code}1;33m";;
    blue   |  b) color="${code}1;34m";;
    purple |  p) color="${code}1;35m";;
    cyan   |  c) color="${code}1;36m";;
    gray   | gr) color="${code}0;37m";;
    *) local text="$1"
  esac
  [ -z "$text" ] && local text="$color$2${code}0m"
  echo -e "$text"
}

update_instances() {
    echo ""
    cecho g "*******************************************************************************"
    cecho g "++++++                  Ishtar instance update instances                 ++++++"
    cecho g "*******************************************************************************"
    echo ""

    # check user
    user="$(id -un 2>/dev/null || true)"

    sh_c='sh -c'
    if [ "$user" != 'root' ]; then
        if command_exists sudo; then
            sh_c='sudo -E sh -c'
        elif command_exists su; then
            sh_c='su -c'
        else
            cecho r "  Error: this updater needs the ability to run commands as root."
            cecho r "  We are unable to find either "sudo" or "su" available to make this happen."
            exit 1
        fi
    fi

    if [ ! -z '$CONFIG_PATH' ]; then
        CONFIG_PATH="/etc/ishtar/"
    fi

    if [ ! -f $CONFIG_PATH/config ]; then
        echo "";
        cecho r ""$CONFIG_PATH" is not a valid config file."
        echo "Have you properly install Ishtar sources?"
        echo "Run ishtar-install before this script.";
        echo "";
        exit 1;
    fi

    source $CONFIG_PATH/config

    cd $ISHTAR_PATH

    cecho g "Update Ishtar library from git repository"
    git pull

    INSTANCES_FILE=$CONFIG_PATH/instances
    instances="$( (cat $INSTANCES_FILE 2>/dev/null || true) | xargs )"

    if [ -n "$instances" ]; then
        translated=''
        for instance in $instances; do

            if [ "$translated" == '' ]; then
                cecho g "* Compile translations"
                maybe_localized=$(cd $ISHTAR_PATH; find -maxdepth 2 -name 'locale')
                for candidate in $maybe_localized; do
                    if find $ISHTAR_PATH/$candidate -name '*.po' >/dev/null 2>&1; then
                        app=${candidate%%/locale}
                        (cd $ISHTAR_PATH/$app; $PYTHON $ISHTAR_PATH/$instance/manage.py compilemessages)
                    fi
                done
                translated='true'
            fi

            cd $ISHTAR_PATH/$instance
            cecho g "Instance: $instance"
            cecho y " * collect static"
            $PYTHON manage.py collectstatic --noinput 2> /dev/null
            cecho y " * database migrations"
            $PYTHON manage.py migrate
            cecho y " * regenerate permissions"
            $PYTHON manage.py regenerate_permissions

        done
    fi
    echo ""
    cecho g "All instances have been updated"
    cat >&2 <<-'EOF'

    You should restart uwsgi and nginx:
EOF
    cecho y "systemctl restart uwsgi nginx"
    echo ""
}

update_instances
