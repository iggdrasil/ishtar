from django.contrib import admin

from overload_translation import models
from ishtar_common.apps import admin_site


class TranslationOverloadAdmin(admin.ModelAdmin):
    list_display = ("message", "context", "lang", "translation")
    list_filter = ("lang",)
    search_fields = ("message", "context")


admin_site.register(models.TranslationOverload, TranslationOverloadAdmin)
