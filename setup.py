# -*- coding: utf-8 -*-
import os
from setuptools import setup  # , find_packages
from ishtar_common import version

try:
    reqs = open(os.path.join(os.path.dirname(__file__),
                             'requirements.txt')).read()
    reqs = "\n".join(
        [r for r in reqs.split("\n") if r and not r.startswith('-e')]
    )
except (IOError, OSError):
    reqs = ''

setup(
    name='ishtar',
    version=version.get_version(),
    description="Ishtar is a database to manage the finds and "
                "documentation from archaeological operations.",
    long_description=open('README.rst').read(),
    author='Étienne Loks',
    author_email='etienne.loks@iggdrasil.net',
    url='https://ishtar-archeo.net/',
    license='AGPL v3 licence, see COPYING',
    packages=[
        'bootstrap_datepicker',
        'ooopy',
        'overload_translation',
        'archaeological_finds',
        'archaeological_warehouse', 'archaeological_files_pdl',
        'archaeological_files', 'ishtar_common',
        'archaeological_operations', 'archaeological_context_records'],
    include_package_data=True,
    install_requires=reqs,
    test_suite="example_project.runtests.runtests",
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Operating System :: OS Independent',
    ]
)
