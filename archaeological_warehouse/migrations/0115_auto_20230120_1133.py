# Generated by Django 2.2.24 on 2023-01-20 11:33

from django.db import migrations, models
import django.db.models.deletion


def copy_external_id_to_slug(apps, schema):
    Warehouse = apps.get_model("archaeological_warehouse", "Warehouse")
    for w in Warehouse.objects.all():
        w.skip_history_when_saving = True
        w.slug = w.external_id
        w.save()


def update_profile(apps, schema):
    IshtarSiteProfile = apps.get_model("ishtar_common", "IshtarSiteProfile")
    for p in IshtarSiteProfile.objects.filter(
            warehouse_external_id="{name|slug}").all():
        p.warehouse_external_id = "{slug}"
        p.save()


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_warehouse', '0114_auto_20220707_1633'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalwarehouse',
            name='cached_town_label',
            field=models.TextField(blank=True, default='', help_text='Generated automatically - do not edit', verbose_name='Cached town label'),
        ),
        migrations.AddField(
            model_name='historicalwarehouse',
            name='slug',
            field=models.SlugField(blank=True, default='', max_length=200, verbose_name='Textual ID'),
        ),
        migrations.AddField(
            model_name='warehouse',
            name='cached_town_label',
            field=models.TextField(blank=True, default='', help_text='Generated automatically - do not edit', verbose_name='Cached town label'),
        ),
        migrations.AddField(
            model_name='warehouse',
            name='slug',
            field=models.SlugField(blank=True, default='', max_length=200, verbose_name='Textual ID'),
        ),
        migrations.AlterField(
            model_name='container',
            name='geodata',
            field=models.ManyToManyField(blank=True, related_name='related_items_archaeological_warehouse_container', to='ishtar_common.GeoVectorData', verbose_name='Geodata'),
        ),
        migrations.AlterField(
            model_name='container',
            name='main_geodata',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='main_related_items_archaeological_warehouse_container', to='ishtar_common.GeoVectorData', verbose_name='Main geodata'),
        ),
        migrations.AlterField(
            model_name='historicalwarehouse',
            name='main_geodata',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='ishtar_common.GeoVectorData', verbose_name='Main geodata'),
        ),
        migrations.AlterField(
            model_name='warehouse',
            name='geodata',
            field=models.ManyToManyField(blank=True, related_name='related_items_archaeological_warehouse_warehouse', to='ishtar_common.GeoVectorData', verbose_name='Geodata'),
        ),
        migrations.AlterField(
            model_name='warehouse',
            name='main_geodata',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='main_related_items_archaeological_warehouse_warehouse', to='ishtar_common.GeoVectorData', verbose_name='Main geodata'),
        ),
        migrations.RunPython(copy_external_id_to_slug),
        migrations.RunPython(update_profile),
    ]
