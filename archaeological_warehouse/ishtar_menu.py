#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2012 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from ishtar_common.utils import ugettext_lazy as _

from ishtar_common.menu_base import SectionItem, MenuItem

from . import models

# be careful: each access_controls must be relevant with check_rights in urls


MENU_SECTIONS = [
    (
        80,
        SectionItem(
            "warehouse",
            _("Warehouse"),
            profile_restriction="warehouse",
            css="menu-warehouse",
            childs=[
                MenuItem(
                    "warehouse_search",
                    _("Search"),
                    model=models.Warehouse,
                    access_controls=["view_warehouse", "view_own_warehouse"],
                ),
                MenuItem(
                    "warehouse_creation",
                    _("Creation"),
                    model=models.Warehouse,
                    access_controls=[
                        "add_warehouse",
                    ],
                ),
                MenuItem(
                    "warehouse_modification",
                    _("Modification"),
                    model=models.Warehouse,
                    access_controls=[
                        "change_warehouse",
                    ],
                ),
                MenuItem(
                    "warehouse_deletion",
                    _("Deletion"),
                    model=models.Warehouse,
                    access_controls=[
                        "change_warehouse",
                    ],
                ),
                SectionItem(
                    "containers",
                    _("Containers"),
                    childs=[
                        MenuItem(
                            "container_search",
                            _("Search"),
                            model=models.Warehouse,
                            access_controls=["view_container", "view_own_container"],
                        ),
                        MenuItem(
                            "container_creation",
                            _("Creation"),
                            model=models.Warehouse,
                            access_controls=["add_container", "add_own_container"],
                        ),
                        MenuItem(
                            "container_modification",
                            _("Modification"),
                            model=models.Warehouse,
                            access_controls=[
                                "change_container",
                                "change_own_container",
                            ],
                        ),
                        MenuItem(
                            "container-merge",
                            _("Automatic merge"),
                            model=models.Container,
                            access_controls=["administrator"],
                        ),
                        MenuItem(
                            "container-manual-merge",
                            _("Manual merge"),
                            model=models.Container,
                            access_controls=["administrator"],
                        ),
                        MenuItem(
                            "container_deletion",
                            _("Deletion"),
                            model=models.Warehouse,
                            access_controls=[
                                "change_container",
                                "change_own_container",
                            ],
                        ),
                    ],
                ),
            ],
        ),
    )
]
"""
    MenuItem('warehouse_inventory', _("Inventory"),
        model=models.Warehouse,
        access_controls=['change_warehouse',]),
    MenuItem('warehouse_recording', _("Recording"),
        model=Treatment,
        access_controls=['add_treatment', 'add_own_treatment']),
    MenuItem('warehouse_lend', _("Lending"),
        model=Treatment,
        access_controls=['add_treatment', 'add_own_treatment']),
"""
