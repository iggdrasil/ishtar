#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2010-2016 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.conf.urls import url
from django.urls import path

from ishtar_common.utils import check_rights

from archaeological_warehouse import models, views, views_api

# be careful: each check_rights must be relevant with ishtar_menu

# forms
urlpatterns = [
    url(
        r"warehouse_packaging/(?P<step>.+)?$",  # AFAC
        check_rights(["change_find", "change_own_find"])(
            views.warehouse_packaging_wizard
        ),
        name="warehouse_packaging",
    ),
    url(
        r"new-warehouse/(?P<parent_name>.+)?/$",
        views.new_warehouse,
        name="new-warehouse",
    ),
    url(
        r"^show-warehouse(?:/(?P<pk>.+))?/(?P<type>.+)?$",
        views.show_warehouse,
        name=models.Warehouse.SHOW_URL,
    ),
    url(
        r"^show-historized-warehouse/(?P<pk>.+)?/(?P<date>.+)?$",
        views.show_warehouse,
        name="show-historized-warehouse",
    ),
    url(
        r"^revert-warehouse/(?P<pk>.+)/(?P<date>.+)$",
        views.revert_warehouse,
        name="revert-warehouse",
    ),
    url(
        r"autocomplete-warehouse/$",
        views.autocomplete_warehouse,
        name="autocomplete-warehouse",
    ),
    url(
        r"new-container/(?P<parent_name>.+)?/$",
        views.new_container,
        name="new-container",
    ),
    url(r"get-container/(?P<type>.+)?$", views.get_container, name="get-container"),
    url(
        r"get-divisions-container/(?P<type>.+)?$",
        views.get_divisions_container,
        name="get-divisions-container",
    ),
    url(
        r"get-non-divisions-container/(?P<type>.+)?$",
        views.get_non_divisions_container,
        name="get-non-divisions-container",
    ),
    url(r"get-warehouse/(?P<type>.+)?$", views.get_warehouse, name="get-warehouse"),
    url(
        r"get-warehouse-shortcut/(?P<type>.+)?$",
        views.get_warehouse,
        name="get-warehouse-shortcut",
        kwargs={"full": "shortcut"},
    ),
    url(
        r"autocomplete-container/(?P<warehouse_id>\d+)?(?:/)?$",
        views.autocomplete_container,
        name="autocomplete-container",
    ),
    url(
        r"^show-container(?:/(?P<pk>.+))?/(?P<type>.+)?$",
        views.show_container,
        name=models.Container.SHOW_URL,
    ),
    url(
        r"^show-historized-container/(?P<pk>.+)?/(?P<date>.+)?$",
        views.show_container,
        name="show-historized-container",
    ),
    url(
        r"^revert-container/(?P<pk>.+)/(?P<date>.+)$",
        views.revert_container,
        name="revert-container",
    ),
    url(
        r"^warehouse_search/(?P<step>.+)?$",
        check_rights(["view_warehouse", "view_own_warehouse"])(
            views.warehouse_search_wizard
        ),
        name="warehouse_search",
    ),
    url(
        r"^warehouse_creation/(?P<step>.+)?$",
        check_rights(["add_warehouse"])(views.warehouse_creation_wizard),
        name="warehouse_creation",
    ),
    url(
        r"^warehouse_modification/(?P<step>.+)?$",
        check_rights(["change_warehouse"])(views.warehouse_modification_wizard),
        name="warehouse_modification",
    ),
    url(
        r"warehouse-modify/(?P<pk>.+)/$",
        views.warehouse_modify,
        name="warehouse_modify",
    ),
    url(
        r"^warehouse_deletion/(?P<step>.+)?$",
        check_rights(["change_warehouse"])(views.warehouse_deletion_wizard),
        name="warehouse_deletion",
    ),
    url(
        r"warehouse-delete/(?P<pk>.+)/$",
        views.warehouse_delete,
        name="delete-warehouse",
    ),
    url(
        r"^warehouse-qa-lock/(?P<pks>[0-9-]+)?/$",
        views.QAWarehouseLockView.as_view(),
        name="warehouse-qa-lock",
        kwargs={"model": models.Warehouse},
    ),
    url(
        r"^warehouse-qa-bulk-update/(?P<pks>[0-9-]+)?/$",
        check_rights([
            "change_warehouse",
            "change_own_warehouse"
        ])(
            views.QAWarehouseForm.as_view()
        ),
        name="warehouse-qa-bulk-update",
    ),
    url(
        r"^warehouse-qa-bulk-update/(?P<pks>[0-9-]+)?/confirm/$",
        check_rights([
            "change_warehouse",
            "change_own_warehouse",
        ])(
            views.QAWarehouseForm.as_view()
        ),
        name="warehouse-qa-bulk-update-confirm",
        kwargs={"confirm": True},
    ),
    url(
        r"^container-add-treatment/(?P<pk>[0-9-]+)/$",
        check_rights(["change_find", "change_own_find"])(views.container_treatment_add),
        name="container-add-treatment",
    ),
    url(
        r"^container_search/(?P<step>.+)?$",
        check_rights(["view_container", "view_own_container"])(
            views.container_search_wizard
        ),
        name="container_search",
    ),
    url(
        r"^container_creation/(?P<step>.+)?$",
        check_rights(["add_container", "add_own_container"])(
            views.container_creation_wizard
        ),
        name="container_creation",
    ),
    url(
        r"^container_modification/(?P<step>.+)?$",
        check_rights(["change_container", "change_own_container"])(
            views.container_modification_wizard
        ),
        name="container_modification",
    ),
    url(
        r"container-modify/(?P<pk>.+)/$",
        views.container_modify,
        name="container_modify",
    ),
    url(
        r"^container_deletion/(?P<step>.+)?$",
        check_rights(["change_container", "change_own_container"])(
            views.container_deletion_wizard
        ),
        name="container_deletion",
    ),
    url(
        r"container-delete/(?P<pk>.+)/$",
        views.container_delete,
        name="delete-container",
    ),
    url(
        r"^container-qa-bulk-update/(?P<pks>[0-9-]+)?/$",
        check_rights(["change_container", "change_own_container"])(
            views.QAContainerForm.as_view()
        ),
        name="container-qa-bulk-update",
    ),
    url(
        r"^container-qa-bulk-update/(?P<pks>[0-9-]+)?/confirm/$",
        check_rights(["change_container", "change_own_container"])(
            views.QAContainerForm.as_view()
        ),
        name="container-qa-bulk-update-confirm",
        kwargs={"confirm": True},
    ),
    url(
        r"^container-qa-lock/(?P<pks>[0-9-]+)?/$",
        views.QAContainerLockView.as_view(),
        name="container-qa-lock",
        kwargs={"model": models.Container},
    ),
    url(
        r"container-merge/(?:(?P<page>\d+)/)?$",
        views.container_merge,
        name="container_merge",
    ),
    url(
        r"container-manual-merge/$",
        views.ContainerManualMerge.as_view(),
        name="container_manual_merge",
    ),
    url(
        r"container-manual-merge-items/(?P<pks>[0-9_]+?)/$",
        views.ContainerManualMergeItems.as_view(),
        name="container_manual_merge_items",
    ),
    url(
        r"generate-stats-container/(?P<pk>.+)/",
        views.GenerateStatsContainer.as_view(),
        name="generate-stats-container",
    ),
    url(
        r"generate-stats-warehouse/(?P<pk>.+)/",
        views.GenerateStatsWarehouse.as_view(),
        name="generate-stats-warehouse",
    ),
    url(
        r"api/facets/warehouse/$", views_api.FacetWarehouseAPIView.as_view(),
        name="api-facets-warehouse"
    ),
    url(
        r"api/search/warehouse/$", views_api.SearchWarehouseAPI.as_view(),
        name="api-search-warehouse"
    ),
    url(
        r"api/search/container/$", views_api.SearchContainerAPI.as_view(),
        name="api-search-container"
    ),
    path(
        "api/export/warehouse/<slug:slug>/",
        views_api.ExportWarehouseAPI.as_view(),
        name="api-export-warehouse"
    ),
    path(
        "api/export/container/<slug:slug>/",
        views_api.ExportContainerAPI.as_view(),
        name="api-export-container"
    ),
    path(
        "api/get/warehouse/<int:pk>/", views_api.GetWarehouseAPI.as_view(),
        name="api-get-warehouse"
    ),
    path(
        "api/get/container/<int:pk>/", views_api.GetContainerAPI.as_view(),
        name="api-get-container"
    ),
]
