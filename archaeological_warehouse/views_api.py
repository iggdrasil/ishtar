from ishtar_common.rest import SearchAPIView, FacetAPIView, GetAPIView, ExportAPIView
from archaeological_warehouse import models, forms


class FacetWarehouseAPIView(FacetAPIView):
    models = [models.Warehouse, models.Container]
    select_forms = [forms.WarehouseSelect, forms.ContainerSelect]


class SearchWarehouseAPI(SearchAPIView):
    model = models.Warehouse


class SearchContainerAPI(SearchAPIView):
    model = models.Container


class ExportWarehouseAPI(ExportAPIView):
    model = models.Warehouse


class ExportContainerAPI(ExportAPIView):
    model = models.Container


class GetWarehouseAPI(GetAPIView):
    model = models.Warehouse


class GetContainerAPI(GetAPIView):
    model = models.Container
