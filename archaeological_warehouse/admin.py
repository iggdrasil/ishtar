#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2012 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.contrib import admin

from ishtar_common.apps import admin_site
from ishtar_common.admin import HistorizedObjectAdmin, GeneralTypeAdmin, MainGeoDataItem

from . import models


class DivisionInline(admin.TabularInline):
    model = models.Warehouse.associated_divisions.through
    exclude = ("division",)
    extra = 1


class WarehouseAdmin(HistorizedObjectAdmin, MainGeoDataItem):
    list_display = ("name", "warehouse_type", "town")
    list_filter = ("warehouse_type",)
    search_fields = ("name", "town", "warehouse_type")
    model = models.Warehouse
    readonly_fields = HistorizedObjectAdmin.readonly_fields + ["precise_town_id"]
    autocomplete_fields = HistorizedObjectAdmin.autocomplete_fields + \
                          MainGeoDataItem.autocomplete_fields + [
        "person_in_charge",
        "organization"
    ]
    inlines = [DivisionInline]
    exclude = ["documents", "main_image"]


admin_site.register(models.Warehouse, WarehouseAdmin)


class ContainerTypeAdmin(GeneralTypeAdmin):
    LIST_DISPLAY = (
        "label",
        "reference",
        "stationary",
        "length",
        "width",
        "height",
        "volume",
    )
    model = models.ContainerType
    list_filter = ("available", "stationary")


admin_site.register(models.ContainerType, ContainerTypeAdmin)


class ContainerAdmin(HistorizedObjectAdmin, MainGeoDataItem):
    list_display = (
        "reference",
        "location",
        "container_type",
    )
    list_filter = ("container_type",)
    search_fields = ("reference", "container_type__label", "cached_label", "responsible__name")
    model = models.Container
    readonly_fields = HistorizedObjectAdmin.readonly_fields + [
        "merge_key",
        "merge_exclusion",
        "merge_candidate",
    ]
    autocomplete_fields = HistorizedObjectAdmin.autocomplete_fields + \
                          MainGeoDataItem.autocomplete_fields + [
        "location", "responsible", "responsibility", "parent"
    ]
    exclude = ["documents", "main_image"]


admin_site.register(models.Container, ContainerAdmin)

general_models = [models.WarehouseType]
for model in general_models:
    admin_site.register(model, GeneralTypeAdmin)
