from django.db.models import Q
from ishtar_common.serializers_utils import generic_get_results, archive_serialization
from archaeological_warehouse import models


WAREHOUSE_MODEL_LIST = [
    models.Warehouse,
    models.Container,
    models.WarehouseDivisionLink,
    models.ContainerLocalisation,
]

# TODO: associated documents


def generate_warehouse_queryset(base_query_key, ids):
    container_ids, warehouse_ids = set(), set()
    warehouse_div_ids, container_loca_ids = set(), set()

    for find_key in ("finds", "finds_ref"):
        base_key = "{}__{}".format(find_key, base_query_key)
        q = Q(**{base_key: ids})
        container_ids.update(
            list(models.Container.objects.filter(q).values_list("id", flat=True))
        )
        q_loca = Q(**{"container__{}__{}".format(find_key, base_query_key): ids})
        container_loca_ids.update(
            list(
                models.ContainerLocalisation.objects.filter(q_loca).values_list(
                    "id", flat=True
                )
            )
        )
        for container_key in ("containers", "owned_containers"):
            q = Q(**{"{}__{}__{}".format(container_key, find_key, base_query_key): ids})
            q_div = Q(
                **{
                    "warehouse__{}__{}__{}".format(
                        container_key, find_key, base_query_key
                    ): ids
                }
            )
            warehouse_ids.update(
                list(models.Warehouse.objects.filter(q).values_list("id", flat=True))
            )
            warehouse_div_ids.update(
                list(
                    models.WarehouseDivisionLink.objects.filter(q_div).values_list(
                        "id", flat=True
                    )
                )
            )
    result_queryset = {
        models.Warehouse.__name__: models.Warehouse.objects.filter(
            id__in=warehouse_ids
        ),
        models.Container.__name__: models.Container.objects.filter(
            id__in=container_ids
        ),
        models.WarehouseDivisionLink.__name__: models.WarehouseDivisionLink.objects.filter(
            id__in=warehouse_div_ids
        ),
        models.ContainerLocalisation.__name__: models.ContainerLocalisation.objects.filter(
            id__in=container_loca_ids
        ),
    }
    return result_queryset


def warehouse_serialization(
    archive=False,
    return_empty_types=False,
    archive_name=None,
    operation_queryset=None,
    site_queryset=None,
    cr_queryset=None,
    find_queryset=None,
    warehouse_queryset=None,
    get_queryset=False,
    no_geo=True,
    put_locks=False,
    lock_user=None,
):
    result_queryset = {}
    if operation_queryset:
        operation_ids = operation_queryset.values_list("id", flat=True)
        base_query_key = "base_finds__context_record__operation_id__in"
        result_queryset = generate_warehouse_queryset(base_query_key, operation_ids)
    elif site_queryset:
        site_ids = site_queryset.values_list("id", flat=True)
        base_query_key = (
            "base_finds__context_record__" "operation__archaeological_sites__id__in"
        )
        result_queryset = generate_warehouse_queryset(base_query_key, site_ids)
    elif cr_queryset:
        cr_ids = cr_queryset.values_list("id", flat=True)
        base_query_key = "base_finds__context_record__id__in"
        result_queryset = generate_warehouse_queryset(base_query_key, cr_ids)
    elif find_queryset:
        find_ids = find_queryset.values_list("id", flat=True)
        base_query_key = "id__in"
        result_queryset = generate_warehouse_queryset(base_query_key, find_ids)
    elif warehouse_queryset:
        warehouse_ids = warehouse_queryset.values_list("id", flat=True)
        result_queryset = {
            models.Warehouse.__name__: warehouse_queryset,
            models.Container.__name__: models.Container.objects.filter(
                Q(location__id__in=warehouse_ids) | Q(responsible__id__in=warehouse_ids)
            ),
            models.WarehouseDivisionLink.__name__: models.WarehouseDivisionLink.objects.filter(
                warehouse_id__in=warehouse_ids
            ),
            models.ContainerLocalisation.__name__: models.ContainerLocalisation.objects.filter(
                division__warehouse_id__in=warehouse_ids
            ),
        }
    if get_queryset:
        return result_queryset
    result = generic_get_results(
        WAREHOUSE_MODEL_LIST,
        "warehouse",
        result_queryset=result_queryset,
        no_geo=no_geo,
    )
    if put_locks:
        for model in WAREHOUSE_MODEL_LIST:
            if not hasattr(model, "locked"):
                continue
            q = model.objects
            if result_queryset and model.__name__ in result_queryset:
                q = result_queryset[model.__name__]
            q.update(locked=True, lock_user=lock_user)

    full_archive = archive_serialization(
        result,
        archive_dir="warehouse",
        archive=archive,
        return_empty_types=return_empty_types,
        archive_name=archive_name,
    )
    return full_archive
