#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2012-2016  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.http import HttpResponseRedirect
from django.shortcuts import render, reverse

from ishtar_common.forms import reverse_lazy
from ishtar_common.wizards import Wizard, SearchWizard, MultipleDeletionWizard
from archaeological_finds.wizards import TreatmentWizard

from archaeological_finds.models import Treatment, TreatmentType
from . import models


class WarehouseSearch(SearchWizard):
    model = models.Warehouse


class ContainerSearch(SearchWizard):
    model = models.Container


class PackagingWizard(TreatmentWizard):
    basket_step = "base-packaging"

    def get_form_initial(self, step, data=None):
        initial = super(PackagingWizard, self).get_form_initial(step)
        user = self.request.user
        if (
            step != "base-packaging"
            or not getattr(user, "ishtaruser", None)
            or not user.ishtaruser.person
        ):
            return initial
        initial["person"] = user.ishtaruser.person.pk
        return initial

    def save_model(self, dct, m2m, whole_associated_models, form_list, return_object):
        dct = self.get_extra_model(dct, m2m, form_list)
        obj = self.get_current_saved_object()
        dct["location"] = dct["container"].location
        items = None
        if "items" in dct:
            items = dct.pop("items")
        if "basket" in dct:
            if not items:
                items = dct.pop("basket")
            else:
                dct.pop("basket")
        if "treatment_type_list" in dct:
            dct.pop("treatment_type_list")
        treatment = Treatment(**dct)
        extra_args_for_new = {"container": dct["container"]}
        treatment.save(
            items=items, user=self.request.user, extra_args_for_new=extra_args_for_new
        )
        packaging = TreatmentType.objects.get(txt_idx="packaging")
        treatment.treatment_types.add(packaging)
        res = render(self.request, "ishtar/wizard/wizard_done.html", {})
        return return_object and (obj, res) or res


class WarehouseWizard(Wizard):
    model = models.Warehouse
    wizard_done_window = reverse_lazy("show-warehouse")
    redirect_url = "warehouse_modification"

    def save_model(self, dct, m2m, whole_associated_models, form_list, return_object):
        create_organization = False
        if "create_organization" in dct:
            create_organization = dct.pop("create_organization")
        obj, res = super(WarehouseWizard, self).save_model(
            dct, m2m, whole_associated_models, form_list, True
        )
        if self.modification or not create_organization:
            return return_object and (obj, res) or res
        obj.create_attached_organization()
        return return_object and (obj, res) or res


class WarehouseModificationWizard(Wizard):
    model = models.Warehouse
    modification = True
    wizard_done_window = reverse_lazy("show-warehouse")
    wizard_templates = {
        "divisions-warehouse_modification": "ishtar/wizard/wizard_warehouse_divisions.html",
    }
    redirect_url = "warehouse_modification"


class WarehouseDeletionWizard(MultipleDeletionWizard):
    model = models.Warehouse
    redirect_url = "warehouse_deletion"
    fields = ["name", "warehouse_type", "containers"]
    wizard_templates = {
        "final-warehouse_deletion": "ishtar/wizard/wizard_warehouse_deletion.html"
    }


class ContainerWizard(Wizard):
    model = models.Container
    wizard_templates = {
        "container-container_creation": "ishtar/wizard/wizard_container.html",
        "container-container_modification": "ishtar/wizard/wizard_container.html",
    }
    ignore_init_steps = ["localisation"]
    wizard_done_window = reverse_lazy("show-container")
    redirect_url = "container_modification"

    def get_form_kwargs(self, step=None):
        kwargs = super(ContainerWizard, self).get_form_kwargs(step)
        if step == "localisation-" + self.url_name:
            container_pk = self.session_get_value("selec-" + self.url_name, "pk")
            q = models.Container.objects.filter(pk=container_pk)
            if q.count():
                kwargs["container"] = q.all()[0]
            warehouse_pk = self.session_get_value(
                "container-" + self.url_name, "location"
            )
            q = models.Warehouse.objects.filter(pk=warehouse_pk)
            if q.count():
                kwargs["warehouse"] = q.all()[0]
        return kwargs

    def done(self, form_list, **kwargs):
        """
        Save the localisation
        """
        super(ContainerWizard, self).done(form_list)
        obj = None
        for idx, form in enumerate(form_list):
            if not form.is_valid():
                return self.render(form)
            container = (
                self.get_current_object()
                or hasattr(self, "current_object")
                and self.current_object
            )
            if container and form.prefix == "localisation-" + self.url_name:
                for div_name in form.cleaned_data:
                    try:
                        division = models.WarehouseDivisionLink.objects.get(
                            pk=div_name.split("_")[-1], warehouse=container.location
                        )  # check the localisation match with the container
                    except models.WarehouseDivisionLink.DoesNotExist:
                        return self.render(form)
                    (
                        localisation,
                        created,
                    ) = models.ContainerLocalisation.objects.get_or_create(
                        container=container, division=division
                    )
                    localisation.reference = form.cleaned_data[div_name]
                    localisation.save()
            obj = container
            self.current_object = container
        url = reverse(self.redirect_url)
        if obj:
            url += "?open_item={}".format(obj.pk)
        return HttpResponseRedirect(url)


class ContainerModificationWizard(ContainerWizard):
    modification = True


class ContainerDeletionWizard(MultipleDeletionWizard):
    model = models.Container
    fields = [
        "container_type",
        "reference",
        "comment",
        "location",
        "index",
        "cached_label",
    ]
    redirect_url = "container_deletion"
    wizard_templates = {
        "final-container_deletion": "ishtar/wizard/wizard_container_deletion.html"
    }
