## Develop environnement for Debian buster

Installation instruction for a debian buster with sudo.

### Install base source code
``` bash
sudo apt-get update
sudo apt-get install git sed python3-venv python3-wheel python3-pip libpq-dev \
	python3-dev libjpeg-dev zlib1g-dev libxml2-dev libxslt1-dev libgeos-dev \
	python3-cairocffi tidy libtidy-dev binutils libproj-dev gdal-bin \
	libpangocairo-1.0-0 pandoc graphviz gettext

cd my_workspace
python3 -m venv ishtar-venv
. ./ishtar-venv/bin/activate
git clone https://gitlab.com/iggdrasil/ishtar.git  # or your forked project on gitlab
cd ishtar
pip3 install -r requirements.txt
```

### Install postgresql database

``` bash
sudo apt install postgresql postgresql-contrib postgresql-11-postgis-2.5 \
	postgresql-11-postgis-2.5-scripts
sudo -u postgres psql
```

``` sql
CREATE DATABASE ishtar;
CREATE USER ishtar WITH ENCRYPTED PASSWORD 'mypassword';
GRANT ALL PRIVILEGES ON DATABASE ishtar TO ishtar;
ALTER ROLE ishtar superuser;
```

### Configure and initialize your instance

``` bash
cd my_workspace
. ./ishtar-venv/bin/activate
cd ishtar
ISHTAR_PATH=`pwd`
cp example_project/local_settings.py.sample example_project/local_settings.py
editor example_project/local_settings.py
# edit settings: SECRET_KEY, DATABASE, comment STATIC_URL and MEDIA_URL
```

``` python
SECRET_KEY = "change-this-secret"

DATABASES = {
    'default': {
        'NAME': 'ishtar',
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'HOST': '127.0.0.1',
        'PORT': '5432',
        'USER': 'ishtar',
        'PASSWORD': 'mypassword',
    },
}

# STATIC_URL = BASE_URL + 'static/'
# MEDIA_URL = BASE_URL + 'mydomain/media/'
```

``` bash
cd example_project
# collect static data
python3 ./manage.py collectstatic --noinput

# compile translations
LOCALE=fr
python3 manage.py compilemessages -l $LOCALE

# DB feeding
python3 ./manage.py migrate

# loading fixtures
FIXTURES="$ISHTAR_PATH/fixtures/initial_data-auth-fr.json $ISHTAR_PATH/ishtar_common/fixtures/initial_data-fr.json $ISHTAR_PATH/ishtar_common/fixtures/initial_importtypes-fr.json $ISHTAR_PATH/archaeological_operations/fixtures/initial_data-fr.json $ISHTAR_PATH/archaeological_operations/fixtures/initial_data_relation_type_norel-fr.json $ISHTAR_PATH/archaeological_operations/fixtures/initial_data_relation_type-fr.json $ISHTAR_PATH/archaeological_context_records/fixtures/initial_data-fr.json $ISHTAR_PATH/archaeological_context_records/fixtures/initial_data_relation_type_norel-fr.json $ISHTAR_PATH/archaeological_context_records/fixtures/initial_data_relation_type-fr.json $ISHTAR_PATH/archaeological_files/fixtures/initial_data-fr.json $ISHTAR_PATH/archaeological_finds/fixtures/initial_data-fr.json $ISHTAR_PATH/archaeological_warehouse/fixtures/initial_data-fr.json"
for data in $FIXTURES; do
	echo $data;
	python3 ./manage.py loaddata $data;
done

# create superuser
python3 ./manage.py createsuperuser
```
