DROP TEXT SEARCH CONFIGURATION IF EXISTS french_archeo;
DROP TEXT SEARCH DICTIONARY IF EXISTS thesaurus_french_archeo;


CREATE TEXT SEARCH CONFIGURATION french_archeo (
   COPY = french
);

ALTER TEXT SEARCH CONFIGURATION french_archeo
   DROP MAPPING FOR hword_asciipart;
ALTER TEXT SEARCH CONFIGURATION french_archeo
   DROP MAPPING FOR hword_part;


CREATE TEXT SEARCH DICTIONARY thesaurus_french_archeo (
    TEMPLATE = thesaurus,
    DictFile = thesaurus_french_archeo,
    Dictionary = pg_catalog.simple
);

ALTER TEXT SEARCH CONFIGURATION french_archeo
   ALTER MAPPING FOR asciihword, asciiword, hword, word
   WITH thesaurus_french_archeo, french_stem;
