#!/bin/bash

set -e

MSG="install_dict.sh dbname [PG_SEARCH_DIR_PATH]"
if [ $# -eq 0 ]; then
  echo $MSG
  exit 1
fi

DB=$1
PG_DIR=/usr/share/postgresql/13/
PG_SEARCH_DIR="$PG_DIR"tsearch_data
if [ $# -eq 2 ]; then
  PG_SEARCH_DIR=$2
fi

rm -f $PG_SEARCH_DIR/thesaurus_french_archeo.ths
ln -s `pwd`/thesaurus_french_archeo.ths $PG_SEARCH_DIR/

systemctl restart postgresql
touch /tmp/install_error
sudo -u postgres psql -d $1 -f create.sql > /dev/null 2> /tmp/install_error

if grep ERR /tmp/install_error; then
  exit 1;
fi

echo '"french_archeo" dictionnary successfully installed for '$DB
echo 'Change your django local_settings.py file to add the line:'
echo 'ISHTAR_SEARCH_LANGUAGE = "french_archeo"'

