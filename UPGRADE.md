v1 to v2.0
----------

Before upgrading to v2.0, update to the last v1 version. Then apply all
databases migrations:

```
cd <application-path>
./manage.py migrate
```

Just after upgrade of the code and of the lib, some manual db migrations (user,
database name and password can be found in your local settings):

```
cd <ishtar-path>
psql -h 127.0.0.1 -U myishtardbuser myishtardb -f scripts/migrate_from_v1_to_v2.sql
```

Upgrade to ishtar sources and fake initial migrations.

```
cd <application-path>
./manage.py migrate --fake admin
./manage.py migrate --fake auth
./manage.py migrate --fake contenttypes
./manage.py migrate --fake registration
./manage.py migrate --fake sessions
./manage.py migrate --fake sites
./manage.py migrate --fake ishtar_common 0001_initial
./manage.py migrate --fake archaeological_operations 0003_views
./manage.py migrate --fake archaeological_files 0003_views
./manage.py migrate --fake archaeological_context_records 0004_views
./manage.py migrate --fake archaeological_finds 0003_views
./manage.py migrate --fake archaeological_warehouse 0002_auto_20170414_2123
./manage.py migrate
```

Finally create indexes the new full text search engine

```
cd <application-path>
./manage.py update_search_vectors
```
