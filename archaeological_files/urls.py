#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2010-2016 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.conf.urls import url
from django.urls import path

from ishtar_common.utils import check_rights
from archaeological_files import views, views_api
from archaeological_operations.views import administrativeactfile_document

# be carreful: each check_rights must be relevant with ishtar_menu

# forms:
urlpatterns = [
    url(
        r"file_administrativeactfile_search/(?P<step>.+)?$",
        check_rights(["change_administrativeact"])(
            views.file_administrativeactfile_search_wizard
        ),
        name="file_administrativeactfile_search",
    ),
    url(
        r"^file_administrativeactfile/(?P<step>.+)?$",
        check_rights(["change_administrativeact"])(
            views.file_administrativeactfile_wizard
        ),
        name="file_administrativeactfile",
    ),
    url(
        r"file_administrativeactfile_modify/(?P<pk>.+)/$",
        views.file_administrativeactfile_modify,
        name="file_administrativeactfile_modify",
    ),
    url(
        r"file_administrativeactfile_deletion/(?P<step>.+)?$",
        check_rights(["change_administrativeact"])(
            views.file_administrativeactfile_deletion_wizard
        ),
        name="file_administrativeactfile_deletion",
    ),
    url(
        r"file_administrativeactfile_modification/(?P<step>.+)?$",
        check_rights(["change_administrativeact"])(
            views.file_administrativeactfile_modification_wizard
        ),
        name="file_administrativeactfile_modification",
    ),
    url(
        r"file_administrativeactfile_delete/(?P<pk>.+)/$",
        views.file_administrativeact_delete,
        name="delete-administrativeact-file",
    ),
    url(
        r"file_search/(?P<step>.+)?$",
        check_rights(["view_file", "view_own_file"])(views.file_search_wizard),
        name="file_search",
    ),
    url(
        r"^file_creation/(?P<step>.+)?$",
        check_rights(["add_file"])(views.file_creation_wizard),
        name="file_creation",
    ),
    url(
        r"^file_modification/(?P<step>.+)?$",
        check_rights(["change_file", "change_own_file"])(
            views.file_modification_wizard
        ),
        name="file_modification",
    ),
    url(r"^file_modify/(?P<pk>.+)/$", views.file_modify, name="file_modify"),
    url(
        r"^file_closing/(?P<step>.+)?$",
        check_rights(["change_file"])(views.file_closing_wizard),
        name="file_closing",
    ),
    url(
        r"file_deletion/(?P<step>.+)?$",
        check_rights(["delete_file", "delete_own_file"])(views.file_deletion_wizard),
        name="file_deletion",
    ),
    url(r"^file_delete/(?P<pk>.+)/$", views.file_delete, name="delete-file"),
    url(r"autocomplete-file/$", views.autocomplete_file, name="autocomplete-file"),
    url(r"get-file/(?P<type>.+)?$", views.get_file, name="get-file"),
    url(
        r"get-file-full/(?P<type>.+)?$",
        views.get_file,
        name="get-file-full",
        kwargs={"full": True},
    ),
    url(
        r"get-file-shortcut/(?P<type>.+)?$",
        views.get_file,
        name="get-file-shortcut",
        kwargs={"full": "shortcut"},
    ),
    url(
        r"get-administrativeactfile/(?P<type>.+)?$",
        views.get_administrativeactfile,
        name="get-administrativeactfile",
    ),
    url(r"show-file(?:/(?P<pk>.+))?/(?P<type>.+)?$", views.show_file, name="show-file"),
    url(
        r"show-historized-file/(?P<pk>.+)?/(?P<date>.+)?$",
        views.show_file,
        name="show-historized-file",
    ),
    url(r"revert-file/(?P<pk>.+)/(?P<date>.+)$", views.revert_file, name="revert-file"),
    url(
        r"^file-add-adminact/(?P<pk>[0-9-]+)/$",
        check_rights(["add_administrativeact"])(views.file_adminact_add),
        name="file-add-adminact",
    ),
    url(
        r"file_administrativeact_document/$",
        administrativeactfile_document,
        name="file-administrativeact-document",
        kwargs={"file": True},
    ),
    url(
        r"^file-add-operation/(?P<pk>[0-9-]+)/$",
        check_rights(["add_operation"])(views.file_add_operation),
        name="file-add-operation",
    ),
    url(
        r'^file/edit-preventive-price/(?P<pk>\d+)/$',
        check_rights(["change_file", "change_own_file"])(
            views.PreventiveSimpleEditView.as_view()
        ),
        name="file-edit-preventive-price",
    ),
    url(
        r"^file-parcels-modify/(?P<pk>.+)/$",
        views.file_modify_parcels,
        name="file-parcels-modify",
    ),
    url(
        r'^file/edit-preventive/(?P<pk>\d+)/$',
        check_rights(["change_file", "change_own_file"])(
            views.PreventiveEditView.as_view()
        ),
        name="file-edit-preventive",
    ),
    url(
        r'^file/edit-preventive/add-default-cost/(?P<pk>\d+)/$',
        check_rights(["change_file", "change_own_file"])(
            views.file_edit_preventive_add_default
        ),
        name="file-edit-preventive-default-cost",
    ),
    url(
        r'^file/edit-preventive/copy-planned/(?P<pk>\d+)/$',
        check_rights(["change_file", "change_own_file"])(
            views.file_edit_preventive_copy_planned
        ),
        name="file-edit-preventive-copy-planned",
    ),
    url(
        r"townplanning-edit/$",
        views.TownPlanningCreate.as_view(),
        name="townplanning_create",
    ),
    url(
        r"townplanning-edit/(?P<pk>\d+)$",
        views.TownPlanningEdit.as_view(),
        name="townplanning_edit",
    ),
    url(
        r"api/facets/file/$", views_api.FacetFileAPIView.as_view(),
        name="api-facets-file"
    ),
    url(
        r"api/search/file/$", views_api.SearchFileAPI.as_view(),
        name="api-search-file"
    ),
    path(
        "api/export/file/<slug:slug>/",
        views_api.ExportFileAPI.as_view(),
        name="api-export-file"
    ),
    path(
        "api/get/file/<int:pk>/", views_api.GetFileAPI.as_view(),
        name="api-get-file"
    ),
]
