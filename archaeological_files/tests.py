#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2010-2017 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

import datetime
import json

from django.conf import settings
from django.contrib.auth.models import User
from django.test.client import Client
from django.urls import reverse

from ishtar_common.tests import (
    TestCase,
    create_superuser,
    AutocompleteTestBase,
    AcItem,
    WizardTest,
    WizardTestFormData as FormData,
    FILE_TOWNS_FIXTURES,
)

from ishtar_common.models import Town, IshtarSiteProfile, Person, PersonType, \
    Organization, OrganizationType
from ishtar_common.utils import ugettext_lazy as _

from archaeological_files import models, views
from archaeological_operations.models import (
    Parcel,
    ParcelOwner,
    ActType,
    AdministrativeAct,
)
from archaeological_operations.tests import OperationInitTest, FileInit

"""
def create_administrativact(user, fle):
    act_type, created = ActType.objects.get_or_create(
        txt_idx="act_type_F", intented_to="F"
    )
    dct = {
        "history_modifier": user,
        "act_type": act_type,
        "associated_file": fle,
        "signature_date": datetime.date(2017, 7, 10),
        "index": 22,
    }
    adminact, created = AdministrativeAct.objects.get_or_create(**dct)
    return [act_type], [adminact]
"""


class FileTest(TestCase, FileInit):
    fixtures = FILE_TOWNS_FIXTURES
    model = models.File

    def setUp(self):
        self.profile = IshtarSiteProfile.objects.create()
        self.create_file()

    def test_external_id(self):
        self.assertEqual(
            self.item.external_id,
            "{}-{}".format(self.item.year, self.item.numeric_reference),
        )

    def test_cached_label(self):
        self.item = models.File.objects.get(pk=self.item.pk)
        # localisation fix
        lbls = []
        for town_lbl in ("No town", "Pas de commune"):
            lbls.append(
                settings.JOINT.join(
                    [town_lbl, self.item.external_id, self.item.internal_reference]
                )
            )
        self.assertIn(self.item.cached_label, lbls)
        default_town = Town.objects.create(name="Paris", numero_insee="75001")
        self.item.towns.add(default_town)
        # manually done inside wizards
        self.item._cached_label_checked = False
        self.item._test = True
        self.item.save()
        self.item = models.File.objects.get(pk=self.item.pk)
        lbl = lbls[0].replace("No town", "Paris")
        self.assertEqual(self.item.cached_label, lbl)

    def testAddAndGetHistorized(self):
        """
        Test correct new version and correct access to history
        """
        item = models.File.objects.get(pk=self.item.pk)
        nb_hist = item.history.count()
        self.assertTrue(item.history.count() >= 1)
        base_label = item.internal_reference
        item.internal_reference = "Unité_Test"
        item.history_modifier = self.user
        item.save()
        self.assertEqual(item.history.count(), nb_hist + 1)
        self.assertEqual(item.history.all()[1].internal_reference, base_label)
        item = models.File.objects.get(pk=self.item.pk)
        item.internal_reference = "Unité_Testée"
        item.history_modifier = self.user
        item.skip_history_when_saving = True
        item.save()
        item.skip_history_when_saving = False
        self.assertEqual(item.history.count(), nb_hist + 1)

    def testCreatorHistorized(self):
        """
        Test creator association
        """
        self.assertEqual(self.item.history_creator, self.o_user)
        altuser, created = User.objects.get_or_create(username="altusername")
        item = models.File.objects.get(pk=self.item.pk)
        item.internal_reference = "Unité_Test"
        item.history_modifier = altuser
        item.save()
        self.assertEqual(item.history_creator, self.o_user)

    def testIntelligentHistorisation(self):
        """
        Test that two identical version are not recorded twice in the history
        and that multiple saving in a short time are not considered
        """
        item = models.File.objects.get(pk=self.item.pk)
        nb_hist = item.history.count()
        item.internal_reference = "Unité_Test"
        item.history_modifier = self.user
        item.save()
        self.assertEqual(item.history.count(), nb_hist + 1)
        nb_hist = item.history.count()
        item = models.File.objects.get(pk=self.item.pk)
        item.save()
        self.assertEqual(item.history.count(), nb_hist)

    def testRollbackFile(self):
        nb_hist = self.item.history.count()
        initial_values = self.item.values()
        backup_date = self.item.history.all()[0].history_date
        self.item.internal_reference = "Unité_Test"
        self.item.history_modifier = self.user
        self.item.save()
        self.item = models.File.objects.get(pk=self.item.pk)
        self.item.rollback(backup_date)
        self.assertEqual(self.item.history.count(), nb_hist)
        new_values = self.item.values()
        for k in initial_values.keys():
            if k in ("last_modified", "search_vector", "qrcode"):
                continue
            elif k == "history_m2m" and not initial_values[k]:
                initial_values[k] = dict([(j, []) for j in models.File.HISTORICAL_M2M])
            self.assertTrue(k in new_values, msg="%s not in new values" % k)
            self.assertEqual(
                new_values[k],
                initial_values[k],
                msg="for %s: %s != %s"
                % (k, str(new_values[k]), str(initial_values[k])),
            )

    def testRESTGetFile(self):
        response = self.client.post(
            "/get-file/", {"numeric_reference": self.item.numeric_reference}
        )
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode())
        # not allowed -> no data
        self.assertTrue(not data)

        self.login_as_superuser()
        response = self.client.post(
            "/get-file/", {"numeric_reference": self.item.numeric_reference}
        )
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode())
        self.assertTrue("recordsTotal" in data)
        self.assertEqual(data["recordsTotal"], 1)

    def testRESTGetOldFile(self):
        initial_ref = self.item.internal_reference
        new_ref = "Unité_Test_old_file"
        new_ref = initial_ref != new_ref and new_ref or new_ref + "extra"
        item = models.File.objects.get(pk=self.item.pk)
        item.internal_reference = new_ref
        item.history_modifier = self.user
        item.save()
        response = self.client.post(
            "/get-file/", {"numeric_reference": item.numeric_reference, "old": 1}
        )
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode())
        # not allowed -> no data
        self.assertTrue(not data)

        self.login_as_superuser()
        response = self.client.post(
            "/get-file/", {"numeric_reference": item.numeric_reference, "old": 1}
        )
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode())
        self.assertIn("recordsTotal", data)
        self.assertEqual(data["recordsTotal"], 1)
        self.assertEqual(data["rows"][0]["internal_reference"], initial_ref)

    def testPostDeleteParcels(self):
        fle = self.item
        town = Town.objects.create(name="plouf", numero_insee="20000")
        parcel = Parcel.objects.create(town=town)
        parcel_nb = Parcel.objects.count()
        fle.parcels.add(parcel)
        fle.delete()
        # our parcel has no operation attached is... no more be deleted
        self.assertEqual(parcel_nb, Parcel.objects.count())

        self.create_file()
        fle = self.item
        parcel = Parcel.objects.create(town=town)
        parcel_nb = Parcel.objects.count()
        fle.parcels.add(parcel)
        fle.parcels.clear()  # no signal raised... should resave
        Parcel.objects.filter(pk=parcel.pk).all()[0].save()
        # our parcel has no operation attached and... is no more automatically
        # deleted
        self.assertEqual(parcel_nb, Parcel.objects.count())

    def test_show(self):
        url = "show-file"
        pk = self.item.pk
        response = self.client.get(reverse(url, kwargs={"pk": pk}))
        self.assertEqual(response.status_code, 200)
        # empty content when not allowed
        self.assertEqual(response.content.decode(), "")

        self.login_as_superuser()
        response = self.client.get(reverse(url, kwargs={"pk": pk}))
        self.assertEqual(response.status_code, 200)
        self.assertIn('class="card sheet"', response.content.decode())

    def test_select_page(self):
        url = "file_search"
        # need auth
        response = self.client.get(reverse(url))
        self.assertRedirects(response, "/", status_code=302)
        # need file module
        self.login_as_superuser()
        response = self.client.get(reverse(url))
        self.assertRedirects(response, "/file_search/general-file_search",
                             status_code=302)


class FileCostTest(TestCase, FileInit):
    fixtures = FILE_TOWNS_FIXTURES
    model = models.File

    def setUp(self):
        IshtarSiteProfile.objects.create()
        self.create_file()

    def test_admin_copy_cost(self):
        default = models.PriceAgreement.objects.all()[0]
        new = models.PriceAgreement.objects.create(label="New")
        ln_equipment_service_costs = default.equipment_service_costs.count()
        self.assertNotEqual(ln_equipment_service_costs, 0)
        self.assertEqual(new.equipment_service_costs.count(), 0)
        ln_jobs = default.jobs.count()
        self.assertNotEqual(ln_jobs, 0)
        self.assertEqual(new.jobs.count(), 0)
        url = "/admin/archaeological_files/priceagreement/copy-price-agreement/"
        response = self.client.get(url)
        self.assertRedirects(
            response,
            reverse("admin:login"),
            status_code=302,
        )
        self.login_as_superuser()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response = self.client.post(
            url,
            {"source": default.pk, "destination": default.pk,
             "action": "import_generic", "apply": "1"}
        )
        # cannot copy on same price agreement (nonsense)
        self.assertContains(
            response,
            str(_("Source and destination must be different."))
        )
        response = self.client.post(
            url,
            {"source": default.pk, "destination": new.pk,
             "action": "import_generic", "apply": "1"}
        )
        self.assertEqual(response.status_code, 302)
        new = models.PriceAgreement.objects.get(pk=new.pk)
        self.assertEqual(new.jobs.count(), ln_jobs)
        self.assertEqual(new.equipment_service_costs.count(),
                         ln_equipment_service_costs)
        # repost - verify no duplication
        response = self.client.post(
            url,
            {"source": default.pk, "destination": new.pk,
             "action": "import_generic", "apply": "1"}
        )
        self.assertEqual(response.status_code, 302)
        new = models.PriceAgreement.objects.get(pk=new.pk)
        self.assertEqual(new.jobs.count(), ln_jobs)
        self.assertEqual(new.equipment_service_costs.count(),
                         ln_equipment_service_costs)

    def test_preventive_price_agreement(self):
        pk = self.item.pk
        self.login_as_superuser()
        url = reverse("file-edit-preventive", kwargs={"pk": pk})
        response = self.client.get(url)
        price_url = reverse("file-edit-preventive-price", kwargs={"pk": pk})
        self.assertRedirects(
            response,
            price_url,
            status_code=302,
            target_status_code=200,
            fetch_redirect_response=True,
        )
        self.item.price_agreement_id = models.PriceAgreement.objects.all()[0]
        self.item.save()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        job1 = models.Job.objects.all()[0]
        models.PreventiveFileJob.objects.create(
            file=self.item,
            job=job1
        )

        cost1 = models.EquipmentServiceCost.objects.filter(
            equipment_service_type=models.EquipmentServiceType.objects.all()[0]
        ).all()[0]
        models.PreventiveFileEquipmentServiceCost.objects.create(
            file=self.item,
            equipment_service_cost=cost1,
            quantity_by_day_planned=1,
            days_planned=1
        )
        q_equip = models.PreventiveFileEquipmentServiceCost.objects.filter(
            file=self.item,
        )
        q_job = models.PreventiveFileJob.objects.filter(
            file=self.item,
        )
        self.assertEqual(q_equip.count(), 1)
        self.assertEqual(q_job.count(), 1)

        p = models.PriceAgreement.objects.create(label="New price", txt_idx="new-price")

        # test remove of associated jobs
        response = self.client.post(price_url, {"price_agreement": p.pk})

        self.assertRedirects(
            response,
            url,
            status_code=302,
            target_status_code=200,
            fetch_redirect_response=True,
        )
        self.assertEqual(q_equip.count(), 0)
        self.assertEqual(q_job.count(), 0)

    def test_preventive(self):
        pk = self.item.pk
        self.item.price_agreement_id = models.PriceAgreement.objects.all()[0]
        self.item.save()
        url = reverse("file-edit-preventive", kwargs={"pk": pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)
        self.login_as_superuser()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        job1 = models.Job.objects.all()[0]
        job2 = models.Job.objects.all()[1]
        cost1 = models.EquipmentServiceCost.objects.filter(
            equipment_service_type=models.EquipmentServiceType.objects.all()[0]
        ).all()[0]
        cost2 = models.EquipmentServiceCost.objects.filter(
            equipment_service_type=models.EquipmentServiceType.objects.all()[1]
        ).all()[1]
        data = {
            "pk": pk,
            "start_date": "2021-05-03",
            "total_surface": 6000,
            "linear_meter": 200,
            "preventive-040-ground-jobs-0-file_id": pk,
            "preventive-040-ground-jobs-0-job": job1.pk,
            "preventive-040-ground-jobs-0-man_by_day_planned": 5,
            "preventive-040-ground-jobs-0-days_planned": 6,
            "preventive-040-ground-jobs-0-man_by_day_worked": 0,
            "preventive-040-ground-jobs-0-days_worked": 0,
            "preventive-030-post-excavation-0-file_id": pk,
            "preventive-030-post-excavation-0-job": job2.pk,
            "preventive-030-post-excavation-0-man_by_day_planned": 5,
            "preventive-030-post-excavation-0-days_planned": 6,
            "preventive-030-post-excavation-0-man_by_day_worked": 0,
            "preventive-030-post-excavation-0-days_worked": 0,
            "engins-mecaniques-0-file_id": pk,
            "engins-mecaniques-0-equipment_service_cost": cost1.pk,
            "engins-mecaniques-0-quantity_by_day_planned": 5,
            "engins-mecaniques-0-days_planned": 6,
            "engins-mecaniques-0-quantity_by_day_worked": 0,
            "engins-mecaniques-0-days_worked": 0,
            "locaux-de-chantiers-et-prestations-techniques-0-file_id": pk,
            "locaux-de-chantiers-et-prestations-techniques-0-equipment_service_cost": cost2.pk,
            "locaux-de-chantiers-et-prestations-techniques-0-quantity_by_day_planned": 5,
            "locaux-de-chantiers-et-prestations-techniques-0-days_planned": 6,
            "locaux-de-chantiers-et-prestations-techniques-0-quantity_by_day_worked": 0,
            "locaux-de-chantiers-et-prestations-techniques-0-days_worked": 0,
        }
        for k in (
            "preventive-040-ground-jobs",
            "preventive-030-post-excavation",
            "engins-mecaniques",
            "locaux-de-chantiers-et-prestations-techniques",
        ):
            data[k + "-TOTAL_FORMS"] = 1
            data[k + "-INITIAL_FORMS"] = 0
            data[k + "-MIN_NUM_FORMS"] = 0
            data[k + "-MAX_NUM_FORMS"] = 1000
        response = self.client.post(url, data)
        self.assertRedirects(
            response,
            url + "?current_tab=planned",
            status_code=302,
            target_status_code=200,
            fetch_redirect_response=True,
        )
        file = models.File.objects.get(pk=self.item.pk)
        self.assertEqual(file.total_surface, 6000)
        self.assertEqual(file.linear_meter, 200)
        self.assertEqual(file.start_date, datetime.date(2021, 5, 3))
        self.assertEqual(
            models.PreventiveFileGroundJob.objects.filter(file_id=pk, job=job1).count(),
            1,
        )
        self.assertEqual(
            models.PreventiveFileGroundJob.objects.filter(
                file_id=pk, job=job1.child
            ).count(),
            1,
        )
        self.assertEqual(
            models.PreventiveFileJob.objects.filter(file_id=pk, job=job2).count(), 1
        )
        self.assertEqual(
            models.PreventiveFileJob.objects.filter(file_id=pk, job=job2.child).count(),
            1,
        )
        self.assertEqual(
            models.PreventiveFileEquipmentServiceCost.objects.filter(
                file_id=pk, equipment_service_cost=cost1.pk
            ).count(),
            1,
        )
        self.assertEqual(
            models.PreventiveFileEquipmentServiceCost.objects.filter(
                file_id=pk, equipment_service_cost=cost2.pk
            ).count(),
            1,
        )

    def test_preventive_add_default(self):
        pk = self.item.pk
        self.item.price_agreement_id = models.PriceAgreement.objects.all()[0]
        self.item.save()
        url = reverse("file-edit-preventive", kwargs={"pk": pk})
        self.login_as_superuser()
        for job in models.Job.objects.all()[:5]:
            job.default_daily_need_on_ground = 4
            job.save()
        for job in models.Job.objects.all()[:3]:
            job.default_daily_need = 5
            job.save()
        nb = models.EquipmentServiceCost.objects.update(default_quantity_by_day=10)
        response = self.client.get(
            reverse("file-edit-preventive-default-cost", kwargs={"pk": pk})
        )
        self.assertRedirects(
            response,
            url,
            status_code=302,
            target_status_code=200,
            fetch_redirect_response=True,
        )
        self.assertEqual(models.PreventiveFileJob.objects.filter(file_id=pk).count(), 3)
        self.assertEqual(
            models.PreventiveFileGroundJob.objects.filter(file_id=pk).count(), 5
        )
        self.assertEqual(
            models.PreventiveFileEquipmentServiceCost.objects.filter(
                file_id=pk
            ).count(),
            nb,
        )

    def test_preventive_copy_planned(self):
        pk = self.item.pk
        self.item.price_agreement_id = models.PriceAgreement.objects.all()[0]
        self.item.save()
        url = reverse("file-edit-preventive", kwargs={"pk": pk})
        self.login_as_superuser()

        for job in models.Job.objects.all()[:5]:
            models.PreventiveFileJob.objects.create(
                file_id=pk, job=job, man_by_day_planned=5, days_planned=42
            )
            models.PreventiveFileGroundJob.objects.create(
                file_id=pk, job=job, man_by_day_planned=2, days_planned=50
            )

        nb = models.EquipmentServiceCost.objects.count()
        for cost in models.EquipmentServiceCost.objects.all():
            models.PreventiveFileEquipmentServiceCost.objects.create(
                equipment_service_cost=cost,
                file_id=pk,
                quantity_by_day_planned=4,
                days_planned=3,
            )

        response = self.client.get(
            reverse("file-edit-preventive-copy-planned", kwargs={"pk": pk})
        )
        self.assertRedirects(
            response,
            url + "?current_tab=worked",
            status_code=302,
            target_status_code=200,
            fetch_redirect_response=True,
        )
        self.assertEqual(
            models.PreventiveFileJob.objects.filter(
                file_id=pk, man_by_day_worked=5, days_worked=42
            ).count(),
            5,
        )
        self.assertEqual(
            models.PreventiveFileGroundJob.objects.filter(
                file_id=pk, man_by_day_worked=2, days_worked=50
            ).count(),
            5,
        )
        self.assertEqual(
            models.PreventiveFileEquipmentServiceCost.objects.filter(
                file_id=pk,
                days_worked=3,
                quantity_by_day_worked=4,
            ).count(),
            nb,
        )


class FileOperationTest(TestCase, OperationInitTest, FileInit):
    model = models.File
    fixtures = FILE_TOWNS_FIXTURES

    def setUp(self):
        self.create_file()
        self.orgas = self.create_orgas(self.user)
        self.operations = self.create_operation(self.user, self.orgas[0])
        self.operation = self.operations[0]

    def test_file_parcel_modify(self):
        username, password, user = create_superuser()

        c = Client()
        c.login(username=username, password=password)

        town_1 = Town.objects.create(name="Minas Morgul", numero_insee="10920")
        town_2 = Town.objects.create(name="Minas Tirith", numero_insee="10901")
        parcel_1 = Parcel.objects.create(
            town=town_1, year=2640, section="M", parcel_number="1", public_domain=False, associated_file=self.item
        )
        parcel_2 = Parcel.objects.create(
            town=town_2, year=2620, section="M", parcel_number="2", public_domain=True, associated_file=self.item
        )

        response = c.get(reverse("file-parcels-modify", kwargs={"pk": self.item.pk}))

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, town_1.name)
        self.assertContains(response, town_2.name)
        self.assertContains(response, parcel_1.year)
        self.assertContains(response, parcel_2.year)
        self.assertContains(response, parcel_1.section)
        self.assertContains(response, parcel_2.section)
        self.assertContains(response, parcel_1.parcel_number)
        self.assertContains(response, parcel_2.parcel_number)

        data = {}
        for idx in range(2 + views.PARCEL_FORMSET_EXTRA_FORM):
            data.update({
                f"form-{idx}-pk": "",
                f"form-{idx}-year": "",
                f"form-{idx}-section": "",
                f"form-{idx}-parcel_number": "",
                f"form-{idx}-town": town_1.pk,
                f"form-{idx}-DELETE": '',
            })
        data.update({
            "form-0-pk": parcel_1.pk,
            "form-0-year": 2021,
            "form-0-section": "SCT1",
            "form-0-parcel_number": "001",
            "form-0-town": town_2.pk,
            "form-0-DELETE": "on",
            "form-1-pk": parcel_2.pk,
            "form-1-year": 2021,
            "form-1-section": "SCT2",
            "form-1-parcel_number": "002",
            "form-1-town": town_2.pk,
            "form-2-year": 2011,
            "form-2-section": "XXX",
            "form-2-parcel_number": "003",
            "form-2-town": town_2.pk,
        })

        post_response = c.post("/file-parcels-modify/" + str(self.item.pk) + "/", data)

        self.assertEqual(post_response.status_code, 302)

        response = c.get(reverse("file-parcels-modify", kwargs={"pk": self.item.pk}))

        self.assertContains(response, town_2.name)
        self.assertContains(response, 2021)
        self.assertContains(response, "SCT2")
        self.assertContains(response, "SCT1", count=0)
        self.assertContains(response, "001", count=0)
        self.assertContains(response, "002")
        self.assertContains(response, "003")
        self.assertContains(response, "XXX")

        parcels = Parcel.objects.all()
        self.assertEqual(parcels.count(), 2)

        deleted_parcel = Parcel.objects.filter(pk=parcel_1.pk)
        self.assertEqual(deleted_parcel.count(), 0)

        data.update({
            "_town": town_2.pk,
            "_parcel_selection": "2013:XD:10 to 20, YD:24",
        })

        post_response = c.post("/file-parcels-modify/" + str(self.item.pk) + "/", data)
        response = c.get(reverse("file-parcels-modify", kwargs={"pk": self.item.pk}))

        self.assertContains(response, 2013)
        self.assertContains(response, "XD")
        self.assertContains(response, "YD")
        self.assertContains(response, "24")
        for i in range(10, 20):
            self.assertContains(response, i)
        parcels = Parcel.objects.all()
        self.assertEqual(parcels.count(), 14)


    def testFileAssociation(self):
        # parcel association
        default_town = Town.objects.all()[0]
        for p in range(0, 10):
            parcel = Parcel.objects.create(
                parcel_number=str(p),
                section="YY",
                town=default_town,
                operation=self.operation,
            )
            if p == 1:
                ParcelOwner.objects.create(
                    owner=self.extra_models["person"],
                    parcel=parcel,
                    start_date=datetime.date.today(),
                    end_date=datetime.date.today(),
                )
        initial_nb = self.item.parcels.count()
        # no parcel on the file -> new parcels are copied from the
        # operation
        self.operation.associated_file = self.item
        self.operation.save()
        self.assertEqual(self.item.parcels.count(), initial_nb + 10)
        # parcel owner well attached
        q = ParcelOwner.objects.filter(parcel__associated_file=self.item)
        self.assertEqual(q.count(), 1)

        # when attaching parcel from a file to an operation, copy is done
        parcel = Parcel.objects.create(
            parcel_number="42",
            section="ZZ",
            town=default_town,
            associated_file=self.item,
        )
        ParcelOwner.objects.create(
            owner=self.extra_models["person"],
            parcel=parcel,
            start_date=datetime.date.today(),
            end_date=datetime.date.today(),
        )
        parcel.operation = self.operation
        parcel.save()
        # double reference to operation and associated_file is deleted
        self.assertEqual(parcel.operation, None)
        # now 2 objects with the same parameters
        q = Parcel.objects.filter(parcel_number="42", section="ZZ", town=default_town)
        self.assertEqual(q.count(), 2)
        q = q.filter(operation=self.operation, associated_file=None)
        self.assertEqual(q.count(), 1)
        # parcel owner well attached
        q = ParcelOwner.objects.filter(
            parcel__operation=self.operation, parcel__parcel_number="42"
        )
        self.assertEqual(q.count(), 1)


class AutocompleteTest(AutocompleteTestBase, TestCase):
    fixtures = FILE_TOWNS_FIXTURES
    models = [
        AcItem(models.File, "autocomplete-file", prepare_func="create_file"),
    ]

    def create_file(self, base_name):
        item, __ = models.File.objects.get_or_create(
            name=base_name, file_type=models.FileType.objects.all()[0]
        )
        return item, None


class FileWizardCreationTest(WizardTest, OperationInitTest, TestCase):
    fixtures = FILE_TOWNS_FIXTURES
    url_name = "file_creation"
    wizard_name = "file_wizard"
    steps = views.file_creation_steps
    redirect_url = (
        "/file_modification/selec-file_modification?open_item={last_id}"
    )
    model = models.File

    form_datas = [
        FormData(
            "Create a research file",
            form_datas={
                "general": {
                    "file_type": None,
                    "year": None,
                    "creation_date": None,
                    "reception_date": None
                },
                "researchaddress": {},
                "research": {
                    "requested_operation_type": None,
                    "scientist": None,
                },
                "instruction": {
                    "in_charge": None
                }
            },
            ignored=(
                "preventivetype-file_creation",
                "preventiveplanning-file_creation",
                "generalcontractor-file_creation",
                "planningservice-file_creation",
            ),
        ),
        FormData(
            "Create a preventive file",
            form_datas={
                "general": {
                    "file_type": None,
                    "year": None,
                    "creation_date": None,
                    "reception_date": None
                },
                "preventivetype-file_creation": {
                    "saisine_type": None,
                },
                "preventiveplanning-file_creation": {},
                "generalcontractor-file_creation": {
                    "corporation_general_contractor": None
                },
                "instruction": {
                    "in_charge": None
                }
            },
            ignored=(
                "planningservice-file_creation",
                "researchaddress-file_creation",
                "research-file_creation",
            ),
        ),
    ]

    def pre_wizard(self):
        profile, created = IshtarSiteProfile.objects.get_or_create(
            slug="default", active=True
        )
        profile.files = True
        profile.save()

        if "general" not in self.form_datas[0].form_datas:
            super().pre_wizard()
            return

        file_type_pk = models.FileType.objects.get(txt_idx="prog").pk
        # force cache reinit because previous mess up that - do not know why
        models.FileType.is_preventive(file_type_pk, force=True)
        self.form_datas[0].set(
            "general",
            "file_type",
            file_type_pk
        )
        file_type_pk = models.FileType.objects.get(txt_idx="preventive").pk
        models.FileType.is_preventive(file_type_pk, force=True)
        self.form_datas[1].set(
            "general",
            "file_type",
            file_type_pk
        )

        responsability = Person.objects.create(name="OK", surname="SuperComputer")
        responsability.person_types.add(PersonType.objects.get(txt_idx="sra_agent"))
        for idx in range(2):
            self.form_datas[idx].set("general", "year", 2022)
            self.form_datas[idx].set("general", "creation_date", "2022-10-01")
            self.form_datas[idx].set("general", "reception_date", "2022-10-03")
            self.form_datas[idx].set("instruction", "in_charge", responsability.pk)

        ope_type = models.OperationType.objects.get(txt_idx="documents_study_research")
        self.form_datas[0].set("research", "requested_operation_type", ope_type.pk)
        person = Person.objects.create(name="OK", surname="Computer")
        person.person_types.add(PersonType.objects.get(txt_idx="head_scientist"))
        self.form_datas[0].set("research", "scientist", person.pk)

        self.form_datas[1].set(
            "preventivetype", "saisine_type",
            models.SaisineType.objects.filter(delay__gt=0).all()[0].pk
        )
        self.form_datas[1].set(
            "preventivetype", "permit_type",
            models.PermitType.objects.all()[0].pk
        )
        orga = Organization.objects.create(
            name="Big corpo",
            organization_type=OrganizationType.objects.get(txt_idx="general_contractor")
        )
        self.form_datas[1].set(
            "generalcontractor", "corporation_general_contractor", orga.pk
        )

        self.file_number = models.File.objects.count()
        super().pre_wizard()

    def post_wizard(self):
        self.assertEqual(models.File.objects.count(), self.file_number + 2)


class FileWizardModifTest(WizardTest, OperationInitTest, TestCase):
    fixtures = FILE_TOWNS_FIXTURES
    url_name = "file_modification"
    wizard_name = url_name + "_wizard"
    steps = views.file_modification_steps
    redirect_url = (
        "/file_modification/selec-file_modification?open_item={current_id}"
    )
    model = models.File

    form_datas = [
        FormData(
            "Modify a research file",
            form_datas={
                "selec": {},
                "general": {
                    "year": 2019,
                },
                "generalcontractor": {
                    "corporation_general_contractor": None
                },
                "instruction": {
                    "in_charge": None
                }
            },
            ignored=(
                "preventivetype-file_modification",
                "preventiveplanning-file_modification",
                "researchaddress-file_modification",
                "planningservice-file_modification",
                "research-file_modification",
            ),
        ),
        FormData(
            "Modify a preventive file",
            form_datas={
                "selec": {},
                "general": {
                    "file_type": None,
                    "year": 2012,
                    "creation_date": None,
                    "reception_date": None
                },
                "preventivetype": {
                    "saisine_type": None,
                },
                "preventiveplanning": {},
                "planningservice":{
                    "permit_reference": "XKCD"
                },
                "generalcontractor": {
                    "corporation_general_contractor": None
                },
                "instruction": {
                    "in_charge": None
                }
            },
            ignored=(
                "researchaddress-file_modification",
                "research-file_modification",
            ),
        ),
    ]

    def pre_wizard(self):
        profile, created = IshtarSiteProfile.objects.get_or_create(
            slug="default", active=True
        )
        profile.files = True
        profile.save()

        if "general" not in self.form_datas[0].form_datas:
            super().pre_wizard()
            return

        responsability = Person.objects.create(name="OK", surname="SuperComputer")
        responsability.person_types.add(PersonType.objects.get(txt_idx="sra_agent"))
        ope_type = models.OperationType.objects.get(txt_idx="documents_study_research")
        person = Person.objects.create(name="OK", surname="Computer")
        person.person_types.add(PersonType.objects.get(txt_idx="head_scientist"))
        orga = Organization.objects.create(
            name="Big corpo",
            organization_type=OrganizationType.objects.get(txt_idx="general_contractor")
        )

        file_type_pk = models.FileType.objects.get(txt_idx="prog").pk
        # force cache reinit because previous mess up that - do not know why
        models.FileType.is_preventive(file_type_pk, force=True)
        fle = models.File.objects.create(
            file_type_id=file_type_pk,
            year=2022,
            creation_date="2022-10-01",
            reception_date="2022-10-03",
            in_charge=responsability,
            requested_operation_type=ope_type,
            scientist=person,
            corporation_general_contractor=orga,
        )
        file_type_pk = models.FileType.objects.get(txt_idx="preventive").pk
        # force cache reinit because previous mess up that - do not know why
        models.FileType.is_preventive(file_type_pk, force=True)
        fle2 = models.File.objects.create(
            file_type_id=file_type_pk,
            year=2021,
            creation_date="2022-09-01",
            reception_date="2022-10-03",
            in_charge=responsability,
            corporation_general_contractor=orga,
            saisine_type=models.SaisineType.objects.filter(delay__gt=0).all()[0]
        )
        self.files = [fle, fle2]

        data = self.form_datas[0].form_datas
        data["selec"]["pk"] = str(fle.pk)
        data["general"]["file_type"] = fle.file_type.pk
        data["general"]["creation_date"] = fle.creation_date
        data["general"]["reception_date"] = fle.reception_date
        data["generalcontractor"]["corporation_general_contractor"] = \
            fle.corporation_general_contractor.pk
        data["instruction"]["in_charge"] = fle.in_charge.pk
        data["instruction"]["numeric_reference"] = fle.numeric_reference

        data2 = self.form_datas[1].form_datas
        data2["selec"]["pk"] = str(fle2.pk)
        data2["general"]["file_type"] = fle2.file_type.pk
        data2["general"]["creation_date"] = fle2.creation_date
        data2["general"]["reception_date"] = fle2.reception_date
        data2["preventivetype"]["saisine_type"] = fle2.saisine_type.pk
        data2["generalcontractor"]["corporation_general_contractor"] = \
            fle2.corporation_general_contractor.pk
        data2["instruction"]["in_charge"] = fle2.in_charge.pk
        data2["instruction"]["numeric_reference"] = fle2.numeric_reference
        self.file_number = models.File.objects.count()

        def post_first_wizard(test_object, final_step_response):
            fle = models.File.objects.get(pk=test_object.files[0].pk)
            test_object.assertEqual(fle.year, 2019)

        def post_second_wizard(test_object, final_step_response):
            fle = models.File.objects.get(pk=test_object.files[1].pk)
            test_object.assertEqual(fle.year, 2012)

        self.form_datas[0].extra_tests = [post_first_wizard]
        self.form_datas[1].extra_tests = [post_second_wizard]
        super().pre_wizard()

    def post_wizard(self):
        self.assertEqual(models.File.objects.count(), self.file_number)


class FileWizardDeleteTest(FileWizardCreationTest):
    fixtures = FILE_TOWNS_FIXTURES
    url_name = "file_deletion"
    wizard_name = url_name + "_wizard"
    steps = views.file_deletion_steps
    redirect_url = "/{}/selec-{}".format(url_name, url_name)
    form_datas = [
        FormData(
            "Wizard deletion test",
            form_datas={
                "selec": {"pks": None},
            },
        )
    ]

    def pass_test(self):
        if not settings.TEST_VIEWS:
            # with no migration the views are not created
            return True

    def pre_wizard(self):
        self.file = models.File.objects.create(
            file_type=models.FileType.objects.get(txt_idx="prog"),
            year=2022,
        )
        self.form_datas[0].form_datas["selec"]["pks"] = self.file.pk
        self.file_number = models.File.objects.count()
        super().pre_wizard()

    def post_wizard(self):
        self.assertEqual(self.file_number - 1, models.File.objects.count())


class FileWizardClosingTest(FileWizardCreationTest):
    fixtures = FILE_TOWNS_FIXTURES
    url_name = "file_closing"
    wizard_name = url_name + "_wizard"
    steps = views.file_closing_steps
    redirect_url = "/file_closing/done"
    form_datas = [
        FormData(
            "Wizard closing test",
            form_datas={
                "selec": {"pk": None},
                "date": {"end_date": "2016-01-01"},
            },
        )
    ]

    def pre_wizard(self):
        self.file = models.File.objects.create(
            file_type=models.FileType.objects.get(txt_idx="prog"),
            year=2022,
        )
        self.form_datas[0].form_datas["selec"]["pk"] = self.file.pk
        self.assertTrue(self.file.is_active())
        super().pre_wizard()

    def post_wizard(self):
        fle = models.File.objects.get(pk=self.file.pk)
        self.assertFalse(fle.is_active())
        self.assertEqual(
            fle.closing()["date"].strftime("%Y-%d-%m"),
            self.form_datas[0].form_datas["date-" + self.url_name]["end_date"]
        )


class FileAdminActWizardCreationTest(WizardTest, OperationInitTest, TestCase):
    fixtures = FILE_TOWNS_FIXTURES
    url_name = "file_administrativeactfile"
    wizard_name = "file_administrative_act_wizard"
    steps = views.administrativeact_steps
    form_datas = [
        FormData(
            "Admin act creation",
            form_datas={
                "selec": {},
                "administrativeact": {
                    "signature_date": str(datetime.date.today())
                },
            },
        )
    ]

    def pre_wizard(self):
        self.file = models.File.objects.create(
            file_type=models.FileType.objects.get(txt_idx="prog"),
            year=2022,
        )
        data = self.form_datas[0].form_datas
        data["selec"]["pk"] = self.file.pk
        act = ActType.objects.filter(intented_to="F").all()[0].pk
        data["administrativeact"]["act_type"] = act
        self.number = AdministrativeAct.objects.count()
        super().pre_wizard()

    def post_wizard(self):
        self.assertEqual(AdministrativeAct.objects.count(), self.number + 1)
