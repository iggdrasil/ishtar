#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import datetime, time

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from archaeological_files.models import File, PermitType

replaced = (('cu', 'CU'),
            ('pc', 'PC'),
            ('ei', 'EI'),
            ('', 'NP')
            )


class Command(BaseCommand):
    help = "Update file types"

    def handle(self, *args, **options):
        self.stdout.write("* Update file types\n")
        for origin, dest in replaced:
            permit_type = PermitType.objects.get(txt_idx=dest)
            if origin:
                File.objects.filter(permit_type__txt_idx=origin).update(
                                            permit_type=permit_type)
                PermitType.objects.filter(txt_idx=origin).update(available=False)
            else:
                File.objects.filter(permit_type__isnull=True).update(
                                            permit_type=permit_type)

