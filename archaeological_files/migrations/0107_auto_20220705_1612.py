# -*- coding: utf-8 -*-
# Generated by Django 1.11.28 on 2022-07-05 16:12
from __future__ import unicode_literals

import datetime
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import ishtar_common.models_common
import re


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_files', '0106_auto_20210803_1730'),
    ]

    operations = [
        migrations.CreateModel(
            name='PriceAgreement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('label', models.TextField(verbose_name='Label')),
                ('txt_idx', models.TextField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', unique=True, validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+\\Z', 32), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')], verbose_name='Textual ID')),
                ('comment', models.TextField(blank=True, default='', verbose_name='Comment')),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
                ('order', models.IntegerField(default=10, verbose_name='Order')),
                ('start_date', models.DateField(blank=True, null=True, verbose_name='Start date')),
                ('end_date', models.DateField(blank=True, null=True, verbose_name='End date')),
            ],
            options={
                'verbose_name': 'Price agreement',
                'verbose_name_plural': 'Price agreement',
                'ordering': ('order', 'label'),
            },
            bases=(ishtar_common.models_common.Cached, models.Model),
        ),
        migrations.AlterField(
            model_name='file',
            name='last_modified',
            field=models.DateTimeField(blank=True, default=datetime.datetime.now),
        ),
        migrations.AlterField(
            model_name='historicalfile',
            name='last_modified',
            field=models.DateTimeField(blank=True, default=datetime.datetime.now),
        ),
        migrations.AddField(
            model_name='equipmentservicecost',
            name='price_agreement',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='archaeological_files.PriceAgreement', verbose_name='Price agreement'),
        ),
        migrations.AddField(
            model_name='file',
            name='price_agreement',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='archaeological_files.PriceAgreement', verbose_name='Price agreement'),
        ),
        migrations.AddField(
            model_name='historicalfile',
            name='price_agreement',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='archaeological_files.PriceAgreement', verbose_name='Price agreement'),
        ),
        migrations.AddField(
            model_name='job',
            name='price_agreement',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='archaeological_files.PriceAgreement', verbose_name='Price agreement'),
        ),
    ]
