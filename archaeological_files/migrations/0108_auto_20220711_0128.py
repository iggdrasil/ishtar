# -*- coding: utf-8 -*-
# Generated by Django 1.11.28 on 2022-07-11 01:28
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_files', '0107_auto_20220705_1612'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='priceagreement',
            options={'ordering': ('order', 'start_date', 'end_date', 'label'), 'verbose_name': 'Price agreement', 'verbose_name_plural': 'Price agreement'},
        ),
        migrations.AddField(
            model_name='file',
            name='intervention_period',
            field=models.CharField(blank=True, default='', max_length=200, verbose_name='Intervention period'),
        ),
        migrations.AddField(
            model_name='file',
            name='report_due_period',
            field=models.CharField(blank=True, default='', max_length=200, verbose_name='Report due period'),
        ),
        migrations.AddField(
            model_name='historicalfile',
            name='intervention_period',
            field=models.CharField(blank=True, default='', max_length=200, verbose_name='Intervention period'),
        ),
        migrations.AddField(
            model_name='historicalfile',
            name='report_due_period',
            field=models.CharField(blank=True, default='', max_length=200, verbose_name='Report due period'),
        ),
        migrations.AlterField(
            model_name='equipmentservicecost',
            name='unit',
            field=models.CharField(blank=True, choices=[('D', 'days'), ('W', 'weeks'), ('M', 'months'), ('L', 'linear m.')], max_length=1, null=True, verbose_name='Unit'),
        ),
        migrations.AlterField(
            model_name='file',
            name='permit_reference',
            field=models.TextField(blank=True, default='', verbose_name='Permit/order reference'),
        ),
        migrations.AlterField(
            model_name='historicalfile',
            name='permit_reference',
            field=models.TextField(blank=True, default='', verbose_name='Permit/order reference'),
        ),
        migrations.AlterField(
            model_name='job',
            name='default_daily_need',
            field=models.FloatField(default=0, verbose_name='Def. daily number on study'),
        ),
        migrations.AlterField(
            model_name='preventivefilegroundjob',
            name='job',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='archaeological_files.Job', verbose_name='Job'),
        ),
        migrations.AlterField(
            model_name='preventivefilejob',
            name='job',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='archaeological_files.Job', verbose_name='Job'),
        ),
    ]
