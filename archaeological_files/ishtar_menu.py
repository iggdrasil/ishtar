#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2010-2016 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from ishtar_common.utils import ugettext_lazy as _

from ishtar_common.menu_base import SectionItem, MenuItem

from archaeological_operations.models import AdministrativeAct

from . import models

# be carreful: each access_controls must be relevant with check_rights in urls

MENU_SECTIONS = [
    (
        20,
        SectionItem(
            "file_management",
            _("Archaeological file"),
            profile_restriction="files",
            css="menu-file",
            childs=[
                MenuItem(
                    "file_search",
                    _("Search"),
                    model=models.File,
                    access_controls=["view_file", "view_own_file"],
                ),
                MenuItem(
                    "file_creation",
                    _("Creation"),
                    model=models.File,
                    access_controls=["add_file", "add_own_file"],
                ),
                MenuItem(
                    "file_modification",
                    _("Modification"),
                    model=models.File,
                    access_controls=["change_file", "change_own_file"],
                ),
                MenuItem(
                    "file_closing",
                    _("Closing"),
                    model=models.File,
                    access_controls=["close_file"],
                ),
                MenuItem(
                    "file_deletion",
                    _("Deletion"),
                    model=models.File,
                    access_controls=["delete_file", "delete_own_file"],
                ),
                SectionItem(
                    "admin_act_files",
                    _("Administrative act"),
                    childs=[
                        MenuItem(
                            "file_administrativeactfile_search",
                            _("Search"),
                            model=AdministrativeAct,
                            access_controls=["change_administrativeact"],
                        ),
                        MenuItem(
                            "file_administrativeactfile",
                            _("Creation"),
                            model=AdministrativeAct,
                            access_controls=["change_administrativeact"],
                        ),
                        MenuItem(
                            "file_administrativeactfile_modification",
                            _("Modification"),
                            model=AdministrativeAct,
                            access_controls=["change_administrativeact"],
                        ),
                        MenuItem(
                            "file_administrativeactfile_deletion",
                            _("Deletion"),
                            model=AdministrativeAct,
                            access_controls=["change_administrativeact"],
                        ),
                    ],
                ),
            ],
        ),
    ),
]
