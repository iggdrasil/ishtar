#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012-2015  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Max
from ishtar_common.utils import ugettext_lazy as _

from ishtar_common.forms import reverse_lazy
from ishtar_common.wizards import ClosingWizard, SearchWizard, MultipleDeletionWizard
from archaeological_operations.wizards import (
    OperationWizard,
    OperationAdministrativeActWizard,
)
from archaeological_operations.models import AdministrativeAct, Parcel
from . import models


class FileSearch(SearchWizard):
    model = models.File


class FileWizard(OperationWizard):
    model = models.File
    object_parcel_type = "associated_file"
    parcel_step_key = "parcelspdl-"
    town_step_keys = ["preventiveplanning-", "researchaddress-"]
    wizard_done_window = reverse_lazy("show-file")
    redirect_url = "file_modification"
    town_input_id = "town"
    towns_formset = False
    multi_towns = True
    wizard_templates = {
        "planningservice-%(url_name)s": "ishtar/wizard/wizard_planningservice.html",
        "instruction-%(url_name)s": "ishtar/wizard/wizard_instruction.html",
        "preventiveplanning-%(url_name)s": "ishtar/wizard/wizard_preventiveplanning.html",
    }
    wizard_confirm = "ishtar/wizard/file_confirm_wizard.html"

    def get_current_year(self):
        general_form_key = "general-" + self.url_name
        return self.session_get_value(general_form_key, "year")

    def get_extra_model(self, dct, m2m, form_list):
        dct = super(FileWizard, self).get_extra_model(dct, m2m, form_list)
        if not dct.get("numeric_reference"):
            current_ref = models.File.objects.filter(year=dct["year"]).aggregate(
                Max("numeric_reference")
            )["numeric_reference__max"]
            dct["numeric_reference"] = current_ref and current_ref + 1 or 1
        return dct

    def get_form_kwargs(self, *args, **kwargs):
        returned = super(FileWizard, self).get_form_kwargs(*args, **kwargs)
        if args and args[0].startswith("generalcontractor-"):
            if "status" in self.request.GET:
                returned["status"] = self.request.GET["status"]
        if args and args[0].startswith("instruction-"):
            returned["year"] = self.get_current_year()
            returned["saisine_type"] = self.get_saisine_type()
            returned["reception_date"] = self.session_get_value(
                "general-" + self.url_name, "reception_date"
            )
        return returned

    def get_saisine_type(self):
        try:
            idx = int(
                self.session_get_value(
                    "preventivetype-" + self.url_name, "saisine_type"
                )
            )
            return models.SaisineType.objects.get(pk=idx)
        except (TypeError, ValueError, models.PermitType.DoesNotExist):
            pass

    def get_context_data(self, form, **kwargs):
        context = super(FileWizard, self).get_context_data(form)
        formplanning = "planningservice-" + self.url_name
        forminstruction = "instruction-" + self.url_name
        formfinal = "final-" + self.url_name
        if self.steps.current == formplanning:
            try:
                idx = int(
                    self.session_get_value(
                        "preventivetype-" + self.url_name, "permit_type"
                    )
                )
                permit_type = models.PermitType.objects.get(pk=idx)
                context["permit_type"] = str(permit_type)
                context["permit_type_code"] = str(permit_type.txt_idx)
            except (TypeError, ValueError, models.PermitType.DoesNotExist):
                pass
        elif self.steps.current == forminstruction:
            saisine_type = self.get_saisine_type()
            context["FILE_PREFIX"] = settings.ISHTAR_FILE_PREFIX
            if saisine_type:
                context["saisine_type"] = str(saisine_type)
                context["saisine_type_message"] = str(saisine_type)
                if saisine_type.delay:
                    context["saisine_type_message"] += str(
                        _(": delay of {} days")
                    ).format(saisine_type.delay)
        elif self.steps.current == formfinal:
            if not self.steps.current.endswith("creation"):  # modification only
                try:
                    numeric_reference = int(
                        self.session_get_value(
                            "instruction-" + self.url_name, "numeric_reference"
                        )
                    )

                    q = models.File.objects.filter(
                        numeric_reference=numeric_reference,
                        year=self.get_current_year(),
                    ).exclude(pk=self.get_current_object().pk)
                    context["numeric_reference_files"] = q.all()
                except (ValueError, TypeError):
                    pass

        return context

    def done(self, form_list, **kwargs):
        """
        Make numeric_reference unique
        """
        r = super(FileWizard, self).done(form_list, return_object=True, **kwargs)
        if type(r) not in (list, tuple) or len(r) != 2:
            return r
        obj, res = r
        # numeric_reference check
        if not self.modification:
            numeric_reference = obj.numeric_reference
            changed = False
            while (
                obj.__class__.objects.filter(
                    numeric_reference=numeric_reference, year=obj.year
                )
                .exclude(pk=obj.pk)
                .count()
            ):
                numeric_reference += 1
                changed = True
            if changed:
                obj.numeric_reference = numeric_reference
                obj.save()
        return res


class FileModificationWizard(FileWizard):
    modification = True


FILE_FIELDS = [
    "year",
    "numeric_reference",
    "internal_reference",
    "file_type",
    "in_charge",
    "general_contractor",
    "creation_date",
    "reception_date",
    "total_surface",
    "total_developed_surface",
    "address",
    "address_complement",
    "postal_code",
    "comment",
    "administrative_act",
]


class FileClosingWizard(ClosingWizard):
    model = models.File
    fields = FILE_FIELDS
    if settings.COUNTRY == "fr":
        fields += ["saisine_type", "permit_reference"]
    fields += ["towns"]


class FileDeletionWizard(MultipleDeletionWizard, FileClosingWizard):
    model = models.File
    redirect_url = "file_deletion"
    fields = FILE_FIELDS
    wizard_templates = {
        "final-file_deletion": "ishtar/wizard/wizard_file_deletion.html"
    }


class FileAdministrativeActWizard(OperationAdministrativeActWizard):
    model = models.File
    current_obj_slug = "administrativeactfile"
    ref_object_key = "associated_file"

    def get_reminder(self):
        form_key = "selec-" + self.url_name
        if self.url_name.endswith("_administrativeactfile"):
            # modification and deletion are suffixed with '_modification'
            # and '_deletion' so it is creation
            file_id = self.session_get_value(form_key, "pk")
            try:
                return (
                    (
                        _("Archaeological file"),
                        str(models.File.objects.get(pk=file_id)),
                    ),
                )
            except models.File.DoesNotExist:
                return
        else:
            admin_id = self.session_get_value(form_key, "pk")
            try:
                admin = AdministrativeAct.objects.get(pk=admin_id)
                if not admin.associated_file:
                    return
                return ((_("Archaeological file"), str(admin.associated_file)),)
            except AdministrativeAct.DoesNotExist:
                return


class FileEditAdministrativeActWizard(FileAdministrativeActWizard):
    model = AdministrativeAct
    edit = True

    def get_associated_item(self, dct):
        return self.get_current_object().associated_file
