from ishtar_common.rest import SearchAPIView, FacetAPIView, GetAPIView, ExportAPIView
from archaeological_files import models, forms


class FacetFileAPIView(FacetAPIView):
    models = [models.File]
    select_forms = [forms.FileSelect]


class SearchFileAPI(SearchAPIView):
    model = models.File


class ExportFileAPI(ExportAPIView):
    model = models.File


class GetFileAPI(GetAPIView):
    model = models.File
